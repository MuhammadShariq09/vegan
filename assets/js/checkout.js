$("#create_an_account").on("click", function() {
  if ($("#create_an_account").is(':checked')) {
    $('.cr_an_ac').removeClass('hide');
  } else {
    $('.cr_an_ac').addClass('hide');
    $('#bill_password').val('');
  }
})

$(document).ready(function() {
  $("#couponblock").load(base_url + "shopping/get-coupon-block");
  recalculateTotal();
});

$(document).on('click', ".removePromoCode", function() {
  $.ajax({
    url: base_url + 'shopping/remove-promo-code',
    success: function(response) {
      $("#couponblock").load(base_url + "shopping/get-coupon-block");
      recalculateTotal();
    }
  });
});


function recalculateTotal() {
  $.ajax({
    url: base_url + 'shopping/process-cart-settings',
    success: function(response) {
      $(".netTotal").text('$ ' + Number(response));
    }
  })
}


$(document).on('change', "#promocode", function() {
  $.ajax({
    url: base_url + 'shopping/apply-promo-code',
    data: {
      promocode: $(this).val()
    },
    success: function(response) {
      if (response.status == 200) {
        $("#couponblock").load(base_url + "shopping/get-coupon-block");
        recalculateTotal();
      } else {
        $(".coupon-text").addClass("red bold").text(response.msg);
        setTimeout(function() {
          $(".coupon-text").removeClass("red bold").text('');
        }, 3000);
      }
    }
  })
})


function scrollToError() {
  $('html, body').animate({
    scrollTop: $(".note-block").offset().top
  }, 2000);
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

$(".processBilling").on('click', function() {
  if ($("#first_name").val() == "") {
    $("#first_name").css('border', '1px solid red');
    setTimeout(function() {
      $("#first_name").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if ($("#last_name").val() == "") {
    $("#last_name").css('border', '1px solid red');
    setTimeout(function() {
      $("#last_name").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if ($("#bill_email").val() == "") {
    $("#bill_email").css('border', '1px solid red');
    setTimeout(function() {
      $("#bill_email").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if (validateEmail($("#bill_email").val()) == false) {
    $("#bill_email").css('border', '1px solid red');
    setTimeout(function() {
      $("#bill_email").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if ($("#phone").val() == "") {
    $("#phone").css('border', '1px solid red');
    setTimeout(function() {
      $("#phone").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if ($("#countries").val() == "") {
    $("#countries").css('border', '1px solid red');
    setTimeout(function() {
      $("#countries").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if ($("#zip_code").val() == "") {
    $("#zip_code").css('border', '1px solid red');
    setTimeout(function() {
      $("#zip_code").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if ($("#city").val() == "") {
    $("#city").css('border', '1px solid red');
    setTimeout(function() {
      $("#city").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if ($("#address").val() == "") {
    $("#address").css('border', '1px solid red');
    setTimeout(function() {
      $("#address").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else if ($("#create_an_account").is(':checked') && $("#bill_password").val() == "") {
    $('.cr_an_ac').removeClass('hide');
    $("#bill_password").css('border', '1px solid red');
    setTimeout(function() {
      $("#bill_password").css('border', '1px solid #ccc');
    }, 3000);
    scrollToError();
  } else {
    var post = new Object();
    post.first_name = $('#first_name').val();
    post.last_name = $('#last_name').val();
    post.bill_email = $('#bill_email').val();
    post.phone = $('#phone').val();
    post.countries = $('#countries').val();
    post.zip_code = $('#zip_code').val();
    post.city = $('#city').val();
    post.address = $('#address').val();
    if ($('#create_an_account').is(':checked'))
      post.create_an_account = 1;
    else
      post.create_an_account = 0;
    post.bill_password = $('#bill_password').val();

    //alert('password:'+post.bill_password);
    $.ajax({
      url: base_url + '/shopping/process-billings',
      type: 'post',
      data: post,
      success: function(response) {
        if (response.status == 200) {
          $("html, body").animate({
            scrollTop: 0
          }, "slow");
          $(".alert-success").removeClass('hide');
          $(".b-m").html(`<p>You can now proceed to <strong>Order Payment Page</strong>. You will be redirected in <b>5 seconds</b>.</p>
          <p>Or <a href="${response.url}"><strong>Click Here </strong></a> for direct Link</p>`);
          setTimeout(function() {
            location.replace(response.url);
          }, 5000);
        } else {
          $(".alert-danger").removeClass('hide');
          $(".a-msg").html(response.msg);
        }
      }
    })
  }
})


$(".pay_via").on("change", function() {
  if ($(this).val() == 1) {
    $(".payment-form").removeClass("hide");
  } else {
    $(".payment-form").addClass("hide");
  }
})