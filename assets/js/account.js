$("#uppf").on('click', function() {
  var formData = $('#updateProfile').serialize();
  $.ajax({
    url: base_url + 'user/update-profile',
    data: formData,
    type: 'post',
    success: function(response) {
      var notice = new PNotify({
        title: 'Success !',
        text: response.msg,
        type: 'success',
        buttons: {
          closer: false,
          sticker: false
        }
      });
      notice.get().click(function() {
        notice.remove();
      });
    }

  })
})

function performCancel(id){
    $.ajax({
        url: base_url + '/user/cancel-order/'+id,
        success: function(response){
            if(response.status == 200){
                   title= 'Success !';
                   text= response.msg;
                   type= 'success';
                   $("#cancelOrder"+id).remove();
                   $("#orderStatus"+id).text('Cancelled');
            }
            else{
                   title= 'Error !';
                   text= response.msg;
                   type= 'error';
            
            }
            var notice = new PNotify({
              title: title,
              text: text,
              type: type,
              buttons: {
                closer: false,
                sticker: false
              }
            });
            notice.get().click(function() {
              notice.remove();
            });                
        }
    })    
}

function cancelorder(id){
    var title;
    var text;
    var type;
    (new PNotify({
        title: 'Confirmation Needed',
        text: 'Are you sure?',
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    })).get().on('pnotify.confirm', function() {
        performCancel(id);
    }).on('pnotify.cancel', function() {
    });

}