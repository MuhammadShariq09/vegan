$(document).ready(function() {
  $(".fixedrate").rate("destroy");
})

$(".addFeedback").on('click', function() {

  if (Number(is_logged) == 0) {
    var notice = new PNotify({
      title: 'Error !',
      text: "Please Login to post a review",
      type: 'error',
      buttons: {
        closer: false,
        sticker: false
      }
    });
    notice.get().click(function() {
      notice.remove();
    });
  } else {
    var post = new Object();
    post.name = $("#name").val();
    post.email = $("#email").val();
    post.msg = $("#msg").val();
    post.rate = $(".rateitfeed").attr("data-rate-value");
    post.id = $(this).data("id");
    var id = $(this).data("id");

    if (post.name == '' || post.email == '' || post.msg == '') {
      var notice = new PNotify({
        title: 'Error !',
        text: "Please Fill the required fields",
        type: 'error',
        buttons: {
          closer: false,
          sticker: false
        }
      });
      notice.get().click(function() {
        notice.remove();
      });
    } else {
      $.ajax({
        url: base_url + 'products/addfeedback',
        type: 'post',
        data: post,
        success: function(response) {
          getfeedback(id);
          var notice = new PNotify({
            title: response.class + ' !',
            text: response.msg,
            type: response.class,
            buttons: {
              closer: false,
              sticker: false
            }
          });
          notice.get().click(function() {
            notice.remove();
          });

          $("#name").val('');
          $("#email").val('');
          $("#msg").val('');
        }
      })
    }
  }

})

$(document).ready(function() {
  getfeedback($('#pid').val());
})

function getfeedback(id) {
  $.ajax({
    url: base_url + 'products/getfeedback',
    type: 'post',
    data: {
      id: id
    },
    success: function(response) {
      $("#loadfeedback").html(response);
      $(".rateit").rate();
      //or for example
      var options = {
        max_value: 5,
        step_size: 0.5,
      }
      $(".rateit").rate(options);

    }
  });
}