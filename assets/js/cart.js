$(document).on('click', '.atc_pdp', function() {
  var id = $(this).data("id");
  $.ajax({
    url: base_url + 'shopping/add-to-cart',
    data: {
      id: id
    },
    type: 'post',
    success: function(response) {
      if (response.status == 200) {
        $(".alert-success").removeClass('hide');
        $(".a-msg").html(response.msg);

        if (base_url != window.location.href) {
          $("html, body").animate({
            scrollTop: 0
          }, "slow");
        }
        setTimeout(function() {
          $(".alert-success").addClass('hide');
        }, 5000);
      } else {
        $(".alert-danger").removeClass('hide');
        $(".a-msg").html(response.msg);
        $("html, body").animate({
          scrollTop: 0
        }, "slow");
        setTimeout(function() {
          $(".alert-danger").addClass('hide');
        }, 5000);
      }
    }
  })
})

$(document).on('click', '.dfc', function() {
  var post = new Object();
  var id = $(this).data("id");
  post.id = id;
  $.ajax({
    url: base_url + '/shopping/delete-item-from-cart',
    type: 'post',
    data: post,
    success: function(response) {
      $(".cb" + id).remove();
    }
  })
})


function getCart() {
  $.ajax({
    url: base_url + "/shopping/get-cart",
    success: function(response) {
      $("#cart-wrapper").html(response);
    }
  })
}

$(document).on('change', '.qty', function() {
  if (Number($(this).val()) != 0) {
    var q = $(this).val();
    var dataid = $(this).data("id");
    var price = $(this).data("price");
    $.ajax({
      url: base_url + '/shopping/update-cart',
      data: {
        qty: $(this).val(),
        id: $(this).data("id")
      },
      type: 'post',
      success: function(response) {
        if (response.status != 500) {
          $(".total" + dataid).text('$' + Number(q) * Number(price));
          getCart();
          var netTotal = $('.qty').map(function() {
            return Number(this.value) * $(this).data("price");
          }).get();
          const newArray = netTotal.filter(function(value) {
            return !Number.isNaN(value);
          });
          var $t = newArray.reduce((a, b) => a + b, 0);
          $(".subtotal").text($t);
          recalculateTotal();
        } else {
          $(".stock_alert").removeClass('hide');
          $(".stock_alert").html(response.msg);
          setTimeout(function() {
            $(".stock_alert").addClass('hide');
          }, 3000);
          $(this).val(q);
        }
      }
    })
  }
})


$(".open-cart").on('click', function() {
  getCart();

})