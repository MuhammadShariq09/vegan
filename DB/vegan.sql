-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2018 at 06:54 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vegan`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `name` varchar(244) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `updateby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attributes_values`
--

CREATE TABLE `attributes_values` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image_path` text,
  `attribute_id` int(11) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `updateby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `blog_title` text NOT NULL,
  `blog_slug` text NOT NULL,
  `featured_image` text NOT NULL,
  `body` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `keywords` text,
  `isfeatured` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `created_at`, `blog_title`, `blog_slug`, `featured_image`, `body`, `author`, `keywords`, `isfeatured`) VALUES
(2, '2018-08-24', 'In Virtual Reality, How Much Body1 Do You Need?', 'in-virtual-reality-how-much-body1-do-you-need', '11caf99f787720620c66a4f6a0d895e0.png', '<h1 style=\"text-align:center\"><strong>How connected are your body and your consciousness?</strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px\">When Michiteru Kitazaki, a professor of engineering at Toyohashi University of Technology in Japan, recently posed this question in an email, he evoked an idea from Japanese culture known as tamashii, or the soul without a body.</span></p>\r\n\r\n<p><img alt=\"\" src=\"http://www.elathemes.com/themes/lager/assets/images/blog-3.jpg\" style=\"float:left\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Will it soon be possible, he wondered, to simulate the feeling of a spirit not attached to any particular physical form using virtual or augmented reality?</p>\r\n\r\n<p>If so, a good place to start would be to figure out the minimal amount of body we need to feel a sense of self, especially in digital environments where more and more people may find themselves for work or play. It might be as little as a pair of hands and feet, report Dr. Kitazaki and a Ph.D. student, Ryota Kondo.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"This is an alternative image description. It will generate auto caption.\" src=\"http://www.elathemes.com/themes/lager/assets/images/blog-4.jpg\" /></p>\r\n\r\n<p>This is an alternative image description. It will generate auto caption.</p>\r\n\r\n<p>In a paper published Tuesday in Scientific Reports, they showed that animating virtual hands and feet alone is enough to make people feel their sense of body drift toward an invisible avatar.</p>\r\n\r\n<h2><strong>Virtual reality</strong></h2>\r\n\r\n<h2><span style=\"font-size:14px\">The original body ownership trick was the rubber-hand illusion. In the 1990s, researchers found that if they hid a person&rsquo;s actual hand behind a partition, placed a rubber hand in view next to it and repeatedly tapped and stroked the real and fake hand in synchrony, the subject would soon eerily start to feel sensation in the rubber hand.</span></h2>\r\n\r\n<p><span style=\"font-size:14px\">Today, technologists working on virtual reality are using modern-day riffs on the rubber-hand illusion to understand how users will adjust when presented with digital bodies that do not match their own. Some researchers have suggested that having users digitally swap bodies with people of other races, genders, ages or abilities could reduce implicit bias, though this work has its limits.</span></p>\r\n\r\n<p><span style=\"font-size:14px\">Using an Oculus Rift virtual reality headset and a motion sensor, Dr. Kitazaki&rsquo;s team performed a series of experiments in which volunteers watched disembodied hands and feet move two meters in front of them in a virtual room. In one experiment, when the hands and feet mirrored the participants&rsquo; own movements, people reported feeling as if the space between the appendages were their own bodies.</span></p>\r\n\r\n<p>This demonstrates the power of synchronized actions and our brain&rsquo;s ability to fill in missing information, said V.S. Ramachandran, a professor at the University of California, San Diego and rubber-hand illusion pioneer who did not participate in the new study. The &ldquo;improbability of synchrony occurring by chance&rdquo; overrides all other information, he said, even knowledge that an invisible body cannot be yours.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'John Doe', NULL, 1),
(3, '2018-08-24', 'In Virtual Reality, How Much Body2 Do You Need?', 'in-virtual-reality-how-much-body2-do-you-need', '11caf99f787720620c66a4f6a0d895e0.png', '<h1 style=\"text-align:center\"><strong>How connected are your body and your consciousness?</strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px\">When Michiteru Kitazaki, a professor of engineering at Toyohashi University of Technology in Japan, recently posed this question in an email, he evoked an idea from Japanese culture known as tamashii, or the soul without a body.</span></p>\r\n\r\n<p><img alt=\"\" src=\"http://www.elathemes.com/themes/lager/assets/images/blog-3.jpg\" style=\"float:left\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Will it soon be possible, he wondered, to simulate the feeling of a spirit not attached to any particular physical form using virtual or augmented reality?</p>\r\n\r\n<p>If so, a good place to start would be to figure out the minimal amount of body we need to feel a sense of self, especially in digital environments where more and more people may find themselves for work or play. It might be as little as a pair of hands and feet, report Dr. Kitazaki and a Ph.D. student, Ryota Kondo.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"This is an alternative image description. It will generate auto caption.\" src=\"http://www.elathemes.com/themes/lager/assets/images/blog-4.jpg\" /></p>\r\n\r\n<p>This is an alternative image description. It will generate auto caption.</p>\r\n\r\n<p>In a paper published Tuesday in Scientific Reports, they showed that animating virtual hands and feet alone is enough to make people feel their sense of body drift toward an invisible avatar.</p>\r\n\r\n<h2><strong>Virtual reality</strong></h2>\r\n\r\n<h2><span style=\"font-size:14px\">The original body ownership trick was the rubber-hand illusion. In the 1990s, researchers found that if they hid a person&rsquo;s actual hand behind a partition, placed a rubber hand in view next to it and repeatedly tapped and stroked the real and fake hand in synchrony, the subject would soon eerily start to feel sensation in the rubber hand.</span></h2>\r\n\r\n<p><span style=\"font-size:14px\">Today, technologists working on virtual reality are using modern-day riffs on the rubber-hand illusion to understand how users will adjust when presented with digital bodies that do not match their own. Some researchers have suggested that having users digitally swap bodies with people of other races, genders, ages or abilities could reduce implicit bias, though this work has its limits.</span></p>\r\n\r\n<p><span style=\"font-size:14px\">Using an Oculus Rift virtual reality headset and a motion sensor, Dr. Kitazaki&rsquo;s team performed a series of experiments in which volunteers watched disembodied hands and feet move two meters in front of them in a virtual room. In one experiment, when the hands and feet mirrored the participants&rsquo; own movements, people reported feeling as if the space between the appendages were their own bodies.</span></p>\r\n\r\n<p>This demonstrates the power of synchronized actions and our brain&rsquo;s ability to fill in missing information, said V.S. Ramachandran, a professor at the University of California, San Diego and rubber-hand illusion pioneer who did not participate in the new study. The &ldquo;improbability of synchrony occurring by chance&rdquo; overrides all other information, he said, even knowledge that an invisible body cannot be yours.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'John Doe', NULL, 1),
(4, '2018-08-24', 'In Virtual Reality, How Much Body3 Do You Need?', 'in-virtual-reality-how-much-body3-do-you-need', '11caf99f787720620c66a4f6a0d895e0.png', '<h1 style=\"text-align:center\"><strong>How connected are your body and your consciousness?</strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px\">When Michiteru Kitazaki, a professor of engineering at Toyohashi University of Technology in Japan, recently posed this question in an email, he evoked an idea from Japanese culture known as tamashii, or the soul without a body.</span></p>\r\n\r\n<p><img alt=\"\" src=\"http://www.elathemes.com/themes/lager/assets/images/blog-3.jpg\" style=\"float:left\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Will it soon be possible, he wondered, to simulate the feeling of a spirit not attached to any particular physical form using virtual or augmented reality?</p>\r\n\r\n<p>If so, a good place to start would be to figure out the minimal amount of body we need to feel a sense of self, especially in digital environments where more and more people may find themselves for work or play. It might be as little as a pair of hands and feet, report Dr. Kitazaki and a Ph.D. student, Ryota Kondo.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"This is an alternative image description. It will generate auto caption.\" src=\"http://www.elathemes.com/themes/lager/assets/images/blog-4.jpg\" /></p>\r\n\r\n<p>This is an alternative image description. It will generate auto caption.</p>\r\n\r\n<p>In a paper published Tuesday in Scientific Reports, they showed that animating virtual hands and feet alone is enough to make people feel their sense of body drift toward an invisible avatar.</p>\r\n\r\n<h2><strong>Virtual reality</strong></h2>\r\n\r\n<h2><span style=\"font-size:14px\">The original body ownership trick was the rubber-hand illusion. In the 1990s, researchers found that if they hid a person&rsquo;s actual hand behind a partition, placed a rubber hand in view next to it and repeatedly tapped and stroked the real and fake hand in synchrony, the subject would soon eerily start to feel sensation in the rubber hand.</span></h2>\r\n\r\n<p><span style=\"font-size:14px\">Today, technologists working on virtual reality are using modern-day riffs on the rubber-hand illusion to understand how users will adjust when presented with digital bodies that do not match their own. Some researchers have suggested that having users digitally swap bodies with people of other races, genders, ages or abilities could reduce implicit bias, though this work has its limits.</span></p>\r\n\r\n<p><span style=\"font-size:14px\">Using an Oculus Rift virtual reality headset and a motion sensor, Dr. Kitazaki&rsquo;s team performed a series of experiments in which volunteers watched disembodied hands and feet move two meters in front of them in a virtual room. In one experiment, when the hands and feet mirrored the participants&rsquo; own movements, people reported feeling as if the space between the appendages were their own bodies.</span></p>\r\n\r\n<p>This demonstrates the power of synchronized actions and our brain&rsquo;s ability to fill in missing information, said V.S. Ramachandran, a professor at the University of California, San Diego and rubber-hand illusion pioneer who did not participate in the new study. The &ldquo;improbability of synchrony occurring by chance&rdquo; overrides all other information, he said, even knowledge that an invisible body cannot be yours.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'John Doe', NULL, 1),
(5, '2018-08-24', 'Automatically lock the front door when you head to work', 'automatically-lock-the-front-door-when-you-head-to-work', 'c41ebb2f5ff3adcfb56d68c9387befac.png', '<h1 style=\"text-align:center\"><strong>How connected are your body and your consciousness?</strong></h1>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:14px\">When Michiteru Kitazaki, a professor of engineering at Toyohashi University of Technology in Japan, recently posed this question in an email, he evoked an idea from Japanese culture known as tamashii, or the soul without a body.</span></p>\r\n\r\n<p><img alt=\"\" src=\"http://www.elathemes.com/themes/lager/assets/images/blog-3.jpg\" /></p>\r\n\r\n<p>Will it soon be possible, he wondered, to simulate the feeling of a spirit not attached to any particular physical form using virtual or augmented reality?</p>\r\n\r\n<p>If so, a good place to start would be to figure out the minimal amount of body we need to feel a sense of self, especially in digital environments where more and more people may find themselves for work or play. It might be as little as a pair of hands and feet, report Dr. Kitazaki and a Ph.D. student, Ryota Kondo.</p>\r\n\r\n<p><img alt=\"\" src=\"http://www.elathemes.com/themes/lager/assets/images/blog-4.jpg\" /></p>\r\n\r\n<p>This is an alternative image description. It will generate auto caption.</p>\r\n\r\n<p>In a paper published Tuesday in Scientific Reports, they showed that animating virtual hands and feet alone is enough to make people feel their sense of body drift toward an invisible avatar.</p>\r\n\r\n<h2><strong>Virtual reality</strong></h2>\r\n\r\n<p><span style=\"font-size:14px\">The original body ownership trick was the rubber-hand illusion. In the 1990s, researchers found that if they hid a person&rsquo;s actual hand behind a partition, placed a rubber hand in view next to it and repeatedly tapped and stroked the real and fake hand in synchrony, the subject would soon eerily start to feel sensation in the rubber hand.</span></p>\r\n\r\n<p><span style=\"font-size:14px\">Today, technologists working on virtual reality are using modern-day riffs on the rubber-hand illusion to understand how users will adjust when presented with digital bodies that do not match their own. Some researchers have suggested that having users digitally swap bodies with people of other races, genders, ages or abilities could reduce implicit bias, though this work has its limits.</span></p>\r\n\r\n<p><span style=\"font-size:14px\">Using an Oculus Rift virtual reality headset and a motion sensor, Dr. Kitazaki&rsquo;s team performed a series of experiments in which volunteers watched disembodied hands and feet move two meters in front of them in a virtual room. In one experiment, when the hands and feet mirrored the participants&rsquo; own movements, people reported feeling as if the space between the appendages were their own bodies.</span></p>\r\n\r\n<p>This demonstrates the power of synchronized actions and our brain&rsquo;s ability to fill in missing information, said V.S. Ramachandran, a professor at the University of California, San Diego and rubber-hand illusion pioneer who did not participate in the new study. The &ldquo;improbability of synchrony occurring by chance&rdquo; overrides all other information, he said, even knowledge that an invisible body cannot be yours.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'James Park', NULL, 1),
(6, '2018-08-31', 'this is new blog', 'this-is-new-blog', '2d086ba946d0b053198dcd9642f63ce8.png', '<p>sdvsdv</p>\r\n', 'sdvsdv', '', 0),
(8, '2018-09-13', 'this is test blog', 'this-is-test-blog', '16e43174865154b6d3a1c3e587457be1.png', '<p>dfbfdbfdb</p>\r\n', 'janert', '123456', 0);

-- --------------------------------------------------------

--
-- Table structure for table `brandings`
--

CREATE TABLE `brandings` (
  `id` int(11) NOT NULL,
  `footer_col_title` varchar(255) NOT NULL,
  `footer_col_body` text NOT NULL,
  `fp_title` varchar(255) NOT NULL,
  `fp_subtitle` varchar(255) NOT NULL,
  `fc_title` varchar(255) NOT NULL,
  `fc_subtitle` varchar(255) NOT NULL,
  `bl_title` varchar(255) NOT NULL,
  `bl_subtitle` varchar(255) NOT NULL,
  `fb_link` varchar(255) NOT NULL,
  `tw_link` varchar(255) NOT NULL,
  `lk_link` varchar(255) NOT NULL,
  `gp_link` varchar(255) NOT NULL,
  `inst_link` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brandings`
--

INSERT INTO `brandings` (`id`, `footer_col_title`, `footer_col_body`, `fp_title`, `fp_subtitle`, `fc_title`, `fc_subtitle`, `bl_title`, `bl_subtitle`, `fb_link`, `tw_link`, `lk_link`, `gp_link`, `inst_link`) VALUES
(1, '© 2018 Vegan Day & Night. All', '', 'ALWAYS THE BEST', 'what we offer', '', '', '', '', '#', '#', '#', '#', '#');

-- --------------------------------------------------------

--
-- Table structure for table `cart_settings`
--

CREATE TABLE `cart_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rate` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_settings`
--

INSERT INTO `cart_settings` (`id`, `name`, `rate`) VALUES
(1, 'VAT', '2'),
(2, 'Shipping', '7');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '0 if menu is root level or menuid if this is child on any menu',
  `active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for disabled menu or 1 for enabled menu',
  `sorting_id` int(11) DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `featured` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `parent_id`, `active`, `sorting_id`, `image`, `description`, `icon`, `featured`) VALUES
(30, 'Air Compressor Parts & Accessories', 'air-compressor-parts-accessories', 0, '1', 1, 'faf00c8fc205763b67b4610cd8f86d92.jpg', 'lorem ipsum dolor sat amet', 'a47b0d11058fedeaecfeca7221eac986.jpg', 1),
(31, 'Breakers & Demolition Hammers', 'breakers-demolition-hammers', 0, '1', 2, 'fa240e849dabaa870ed15060b562b32f.jpg', 'lorem ipsum dolor sat amet', 'e4e19fe0fd1ab3e11913087c85ba5e74.jpg', 1),
(32, 'Generator Parts & Accessories', 'generator-parts-accessories', 0, '1', 3, '1b9e62bed76a5b5a4624e677b8df00ee.jpg', 'lorem ipsum dolor sat amet', '2806f02001dc2aaa33aae9ae198da186.jpg', 1),
(33, 'Hand Tools', 'hand-tools', 0, '1', 4, 'c4c05e1c56c8832824fc9d5319c88ce2.jpg', 'lorem ipsum dolor sat amet', '8703eb546864fd8972f458b2ca322560.jpg', 1),
(34, 'Heaters Jobsite', 'heaters-jobsite', 0, '1', 5, 'c9d333e36f3d74b1956ca6b118ce1d6e.jpg', 'lorem ipsum dolor sat amet', '500175b3939cd3384cb8d9743930b846.jpg', 0),
(35, 'Mixers Concrete & Mortar', 'mixers-concrete-mortar', 0, '1', 6, '170fe247e53d98f14ddc6bfc244b5fbe.jpg', 'lorem ipsum dolor sat amet', '5b6c7e716a2fdd3d495e3a2953823553.jpg', 0),
(36, 'Tool Batteries & Chargers', 'tool-batteries-chargers', 0, '1', 7, 'fe22382341833d08f5875ddf2abcbc7e.jpg', 'lorem ipsum dolor sat amet', 'eea8751b07dc47ba1afcc780336b4e89.jpg', 0),
(37, 'Hoists', 'hoists', 0, '1', 8, '4bb7d6722fc12c84accf4a97af0423b1.jpg', 'lorem ipsum dolor sat amet', '9bb39115132233e2148583d7493d6226.jpg', 0),
(38, 'Sanders', 'sanders', 0, '1', 9, '772a4f8c2b3593bbac5c72e3f2c8551e.jpg', 'lorem ipsum dolor sat amet', '07d5b49e6f5d07d74f9eae2d301241d0.jpg', 0),
(39, 'test category', 'test-category', 0, '0', 10, 'b6e4e98d83c8d9fe8b0c6c444ca30e63.png', 'abc', NULL, 0),
(46, 'testtesttesttesttesttesttesttesttesttest', 'test', 0, '0', 0, 'acf4fe665cf38fcf8f152c1ba28fac5f.png', '<p>rette</p>', NULL, 0),
(47, 'Test', 'test-1', 0, '1', 0, 'f9e2b19ddf8acd0376b7687624fee966.png', 'test', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People\'s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People\'s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'SS', 'South Sudan'),
(203, 'ES', 'Spain'),
(204, 'LK', 'Sri Lanka'),
(205, 'SH', 'St. Helena'),
(206, 'PM', 'St. Pierre and Miquelon'),
(207, 'SD', 'Sudan'),
(208, 'SR', 'Suriname'),
(209, 'SJ', 'Svalbard and Jan Mayen Islands'),
(210, 'SZ', 'Swaziland'),
(211, 'SE', 'Sweden'),
(212, 'CH', 'Switzerland'),
(213, 'SY', 'Syrian Arab Republic'),
(214, 'TW', 'Taiwan'),
(215, 'TJ', 'Tajikistan'),
(216, 'TZ', 'Tanzania, United Republic of'),
(217, 'TH', 'Thailand'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad and Tobago'),
(222, 'TN', 'Tunisia'),
(223, 'TR', 'Turkey'),
(224, 'TM', 'Turkmenistan'),
(225, 'TC', 'Turks and Caicos Islands'),
(226, 'TV', 'Tuvalu'),
(227, 'UG', 'Uganda'),
(228, 'UA', 'Ukraine'),
(229, 'AE', 'United Arab Emirates'),
(230, 'GB', 'United Kingdom'),
(231, 'US', 'United States'),
(232, 'UM', 'United States minor outlying islands'),
(233, 'UY', 'Uruguay'),
(234, 'UZ', 'Uzbekistan'),
(235, 'VU', 'Vanuatu'),
(236, 'VA', 'Vatican City State'),
(237, 'VE', 'Venezuela'),
(238, 'VN', 'Vietnam'),
(239, 'VG', 'Virgin Islands (British)'),
(240, 'VI', 'Virgin Islands (U.S.)'),
(241, 'WF', 'Wallis and Futuna Islands'),
(242, 'EH', 'Western Sahara'),
(243, 'YE', 'Yemen'),
(244, 'ZR', 'Zaire'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `CouponName` varchar(100) DEFAULT NULL,
  `CouponCode` varchar(50) DEFAULT NULL,
  `CouponPercentage` int(11) DEFAULT NULL,
  `CouponLimit` int(11) DEFAULT NULL,
  `CouponUsed` int(11) DEFAULT '0',
  `CouponFrom` timestamp NULL DEFAULT NULL,
  `CouponTo` timestamp NULL DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '0',
  `type` int(1) DEFAULT '0',
  `categories` varchar(255) DEFAULT NULL,
  `products` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `CouponName`, `CouponCode`, `CouponPercentage`, `CouponLimit`, `CouponUsed`, `CouponFrom`, `CouponTo`, `createdOn`, `status`, `type`, `categories`, `products`) VALUES
(13, 'test', 'test', 90, NULL, 0, '2018-10-15 19:00:00', '2018-10-16 19:00:00', '2018-10-16 03:04:16', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `product_id` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`id`, `user_id`, `product_id`, `created_at`) VALUES
(4, '44', ',2,4,3,26', '2018-08-20 10:33:56'),
(5, '25', '2,16,15,14,4', '2018-08-30 15:49:00'),
(6, '67', ',55', '2018-10-16 01:09:58'),
(7, '68', '', '2018-10-17 16:49:10');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `review` varchar(255) NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `product_id`, `name`, `email`, `body`, `created_at`, `review`, `approved`) VALUES
(1, '26', 'Johnny Rockets', 'johnny@gmail.com', 'hello this is demo', '2018-08-21 12:58:13', '3.5', 1),
(2, '26', 'Dwyane Johonson', 'dwayne@rock.com', 'booyah', '2018-08-21 13:16:26', '2', 1),
(3, '2', 'Pique', 'pique@gmail.com', 'hello this product is awesome', '2018-08-30 15:33:45', '5', 1),
(4, '2', 'James Grime', 'james_grime@gmail.com', 'what is this', '2018-08-30 15:35:18', '5', 1),
(8, '1', 'Anthony Hopkins', 'anthony_hopkin@gmail.com', 'sdvsdv', '2018-09-03 09:44:53', '5', 1),
(9, '1', 'Denny Hopkins', 'dennyhopkin@gmail.com', 'sdvsdv', '2018-09-03 09:45:01', '3.5', 1),
(11, '4', 'Giacomo Medina', 'giamoco@gmail.com', 'test', '2018-09-11 14:27:58', '3', 1),
(12, '4', 'denny', 'dennyhopkin@gmail.com', 'test', '2018-09-11 14:29:43', '3.5', 1),
(13, '4', 'test', 'test@test.com', 'test', '2018-09-11 14:33:55', '5', 1),
(14, '30', 'james philipp', 'jamesphilippp407@gmail.com', 'test', '2018-09-28 09:22:20', '4', 1),
(15, '19', 'test', 'test@gmail.com', 'test', '2018-09-28 09:30:58', '4', 1),
(16, '30', 'james', 'philip@test.com', 'test', '2018-09-28 09:32:04', '4', 1),
(17, '11', 'test user', 'test@gmail.com', '1234', '2018-10-01 08:56:04', '5', 1),
(18, '24', 'juvesaxemu', 'zygize@mailinator.net', 'Aut atque deserunt eum nihil aut nostrud pariatur Officia perspiciatis deserunt qui mollitia autem doloribus nihil facilis at beatae', '2018-10-17 16:09:37', '5', 1),
(19, '24', 'gozucyn', 'sygizygym@mailinator.net', 'Atque quas aut tempora excepteur cumque ipsum ut assumenda est aut aliquip mollit quibusdam ea dolores qui impedit qui fugiat', '2018-10-17 16:16:04', '5', 1),
(20, '24', 'rexoti', 'bydir@mailinator.net', 'Necessitatibus et culpa molestiae amet vel aut consequatur est consequatur', '2018-10-17 16:18:33', '5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created_on` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `created_on`) VALUES
(1, 'admin', 'ADMIN', 1268889823),
(2, 'members', 'General User', 1268889860),
(3, 'banned', 'Banned Users', 1516806633),
(4, 'vendor', 'VENDORS', 1525859077),
(5, 'superadmin', 'Super Admin', 1526382461),
(6, 'administrator', 'ADMINISTRATOR', 1526383462);

-- --------------------------------------------------------

--
-- Table structure for table `groups_permissions`
--

CREATE TABLE `groups_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(4) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups_permissions`
--

INSERT INTO `groups_permissions` (`id`, `group_id`, `perm_id`, `value`, `created_at`, `updated_at`) VALUES
(14, 2, 1, 1, 1516808624, 1516808624),
(17, 1, 1, 1, 1519224024, 1519224024),
(18, 1, 2, 1, 1519224024, 1519224024),
(540, 1, 8, 0, 2018, 2018),
(541, 1, 24, 1, 2018, 2018),
(542, 1, 27, 1, 2018, 2018),
(543, 1, 26, 1, 2018, 2018),
(544, 1, 25, 1, 2018, 2018),
(545, 1, 28, 1, 2018, 2018),
(546, 1, 23, 1, 2018, 2018),
(547, 1, 30, 0, 2018, 2018),
(548, 1, 32, 0, 2018, 2018),
(549, 1, 31, 0, 2018, 2018),
(550, 1, 29, 0, 2018, 2018),
(551, 1, 20, 1, 2018, 2018),
(552, 1, 22, 1, 2018, 2018),
(553, 1, 21, 1, 2018, 2018),
(554, 1, 19, 1, 2018, 2018),
(555, 1, 34, 1, 2018, 2018),
(556, 1, 33, 0, 2018, 2018),
(557, 1, 17, 1, 2018, 2018),
(558, 1, 18, 1, 2018, 2018),
(559, 1, 16, 1, 2018, 2018),
(560, 1, 13, 1, 2018, 2018),
(561, 1, 9, 1, 2018, 2018),
(562, 1, 12, 1, 2018, 2018),
(563, 1, 10, 1, 2018, 2018),
(564, 1, 11, 1, 2018, 2018),
(565, 4, 8, 1, 2018, 2018),
(566, 4, 24, 0, 2018, 2018),
(567, 4, 27, 0, 2018, 2018),
(568, 4, 26, 0, 2018, 2018),
(569, 4, 25, 0, 2018, 2018),
(570, 4, 28, 0, 2018, 2018),
(571, 4, 23, 0, 2018, 2018),
(572, 4, 30, 0, 2018, 2018),
(573, 4, 32, 0, 2018, 2018),
(574, 4, 31, 0, 2018, 2018),
(575, 4, 29, 0, 2018, 2018),
(576, 4, 20, 1, 2018, 2018),
(577, 4, 22, 1, 2018, 2018),
(578, 4, 21, 1, 2018, 2018),
(579, 4, 19, 1, 2018, 2018),
(580, 4, 34, 1, 2018, 2018),
(581, 4, 33, 0, 2018, 2018),
(582, 4, 17, 1, 2018, 2018),
(583, 4, 18, 1, 2018, 2018),
(584, 4, 16, 1, 2018, 2018),
(585, 4, 13, 1, 2018, 2018),
(586, 4, 9, 0, 2018, 2018),
(587, 4, 12, 0, 2018, 2018),
(588, 4, 10, 0, 2018, 2018),
(589, 4, 11, 0, 2018, 2018),
(614, 6, 24, 1, 2018, 2018),
(615, 6, 27, 1, 2018, 2018),
(616, 6, 26, 1, 2018, 2018),
(617, 6, 25, 1, 2018, 2018),
(618, 6, 28, 1, 2018, 2018),
(619, 6, 23, 1, 2018, 2018),
(620, 6, 30, 1, 2018, 2018),
(621, 6, 32, 1, 2018, 2018),
(622, 6, 31, 1, 2018, 2018),
(623, 6, 29, 1, 2018, 2018),
(624, 6, 20, 1, 2018, 2018),
(625, 6, 22, 1, 2018, 2018),
(626, 6, 21, 1, 2018, 2018),
(627, 6, 19, 1, 2018, 2018),
(628, 6, 34, 1, 2018, 2018),
(629, 6, 33, 1, 2018, 2018),
(630, 6, 17, 1, 2018, 2018),
(631, 6, 18, 1, 2018, 2018),
(632, 6, 16, 1, 2018, 2018),
(633, 6, 13, 1, 2018, 2018),
(634, 6, 9, 1, 2018, 2018),
(635, 6, 12, 1, 2018, 2018),
(636, 6, 10, 1, 2018, 2018),
(637, 6, 11, 1, 2018, 2018),
(664, 5, 1, 1, 2018, 2018),
(665, 5, 27, 1, 2018, 2018),
(666, 5, 29, 1, 2018, 2018),
(667, 5, 28, 1, 2018, 2018),
(668, 5, 26, 1, 2018, 2018),
(669, 5, 19, 1, 2018, 2018),
(670, 5, 20, 1, 2018, 2018),
(671, 5, 18, 1, 2018, 2018),
(672, 5, 25, 1, 2018, 2018),
(673, 5, 17, 1, 2018, 2018),
(674, 5, 9, 1, 2018, 2018),
(675, 5, 10, 1, 2018, 2018),
(676, 5, 8, 1, 2018, 2018),
(677, 5, 15, 1, 2018, 2018),
(678, 5, 6, 1, 2018, 2018),
(679, 5, 13, 1, 2018, 2018),
(680, 5, 14, 1, 2018, 2018),
(681, 5, 12, 1, 2018, 2018),
(682, 5, 11, 1, 2018, 2018),
(683, 5, 23, 1, 2018, 2018),
(684, 5, 24, 1, 2018, 2018),
(685, 5, 22, 1, 2018, 2018),
(686, 5, 21, 1, 2018, 2018),
(687, 5, 4, 1, 2018, 2018),
(688, 5, 5, 1, 2018, 2018),
(689, 5, 3, 1, 2018, 2018),
(690, 5, 16, 1, 2018, 2018);

-- --------------------------------------------------------

--
-- Table structure for table `homepagebanner`
--

CREATE TABLE `homepagebanner` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `text` text NOT NULL,
  `inactive` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homepagebanner`
--

INSERT INTO `homepagebanner` (`id`, `image`, `text`, `inactive`) VALUES
(2, '1b059cd6d0568aceea334c8275c1557d.jpg', 'Fireplace Industry and <br> Water Heaters', '0'),
(12, '6791bcc5997a59d0fe171e401b020f9e.jpg', 'Fireplace Banner', '0'),
(13, 'fee531732f0fd68fb8882a11f98cb9f9.jpg', 'test', '0');

-- --------------------------------------------------------

--
-- Table structure for table `homepage_elements`
--

CREATE TABLE `homepage_elements` (
  `id` int(11) NOT NULL,
  `title` text,
  `subtitle` text,
  `image` text,
  `on_hover_text` text,
  `btn_text` varchar(255) DEFAULT NULL,
  `btn_link` text,
  `percentoff` varchar(10) DEFAULT NULL,
  `type` varchar(5) DEFAULT NULL COMMENT '0=>below banner, 1=> above featured , 2=>below featured'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homepage_elements`
--

INSERT INTO `homepage_elements` (`id`, `title`, `subtitle`, `image`, `on_hover_text`, `btn_text`, `btn_link`, `percentoff`, `type`) VALUES
(4, 'Vegan Day & Night', 'Veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \r\n<br>\r\ncommodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deseruntm.', 'hp_elements/section1/top-right-img.png', '', '', '', NULL, '1'),
(6, 'Backed Mac and cheese', 'With Almond Cheese or Cashew. sautéed onions and peppers, parmesan cheese, topped with bread crumbs', 'hp_elements/section2/1.png', '', 'Book Now', '#', '', '2'),
(7, 'Fried Chick’n Mushrooms', 'With or Without gravy', 'hp_elements/section2/2.png', '', 'Shop Now', '#', '', '2'),
(8, 'from tableware to', 'TENTS', 'hp_elements/section3/sec3.jpg', 'Our event planners will help you figure out exactly what you need to make your event just right.', 'Contact Us', '#', NULL, '3'),
(9, 'we do special', 'EVENTS', 'hp_elements/section4/sec4.jpg', 'Planning an event can be stressful, that’s why we cater the food and plan the event for you. Having an invent shouldn’t be your burden, leave that up to us to alleviate the worry. We can take care of the decorations, designs and food and entertainment. All you have to do is get the people there and on tim', 'Contact Us', '#', NULL, '4'),
(10, 'Meatloaf Loaded', 'With Broccoli, onions and Ketchup', 'hp_elements/section2/3.png', '', 'Book Now', '#', '', '2'),
(11, 'Biscuits and Gravy', 'Buttery biscuits smothered in an onion gravy or mushroom gravy', 'hp_elements/section2/4.png', '', 'Book Now', '#', '', '2');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image_path` text,
  `product_id` int(11) DEFAULT NULL,
  `main_img` int(11) NOT NULL DEFAULT '0',
  `createdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_path`, `product_id`, `main_img`, `createdate`) VALUES
(2, 'befe067b1d6588959cc40df4ced61b86.png', 2, 1, '2018-10-01 09:27:08'),
(3, 'e996c5a2b739e793f1e32a579d225df2.png', 3, 1, '2018-10-01 09:34:43'),
(5, 'da0e8ef596b8c998b07ae97a9b53f24b.png', 4, 1, '2018-10-01 10:00:59'),
(6, '6c2d462680cc2f2095c0cea4bdeef5a7.png', 5, 1, '2018-10-01 10:08:19'),
(7, '89376315c9b10413084a3c6cff42acc5.png', 6, 1, '2018-10-01 10:13:43'),
(8, '075d041969803798c0c36b5f6516afd7.png', 7, 1, '2018-10-01 10:20:44'),
(9, '6efff1fbc9f50551440348bd13443921.png', 8, 1, '2018-10-01 10:29:03'),
(10, '8157f9997174c5589f2b4beff3597b0a.png', 9, 1, '2018-10-01 10:33:53'),
(11, '2d8bbc898c37e4c88a7c6d189a0e6063.png', 10, 1, '2018-10-01 10:36:54'),
(12, 'e4e1598fb3fcd6ab994a2fafce24184a.png', 11, 1, '2018-10-03 06:58:44'),
(13, 'd4924c2ab683127ce62783c4ac76e2fa.png', 12, 1, '2018-10-03 07:01:22'),
(14, '82cdb5b667fa98cfd6b2463566f30a73.png', 13, 1, '2018-10-03 07:03:17'),
(15, '7c7b15dd3a99a246b46a381a6ef915a8.png', 14, 1, '2018-10-03 07:05:13'),
(16, '2dc8ad8c1eefd40ce3f2b21e79eb58d2.png', 15, 1, '2018-10-03 07:08:28'),
(17, 'b83ffdc2a6bdf2dae051db3cab0f84d2.png', 16, 1, '2018-10-03 07:11:30'),
(18, '6c93670ce7a1d07c7d7848ae85c2a76e.png', 17, 1, '2018-10-03 07:14:28'),
(19, '285832514977ec639b7fb780e822e380.png', 18, 1, '2018-10-03 07:18:05'),
(20, '55fc12aec36b28756e85f18bcf78f04b.png', 19, 1, '2018-10-03 07:22:34'),
(21, '25419c4583f5eeea7f83a9b8855f3daf.png', 20, 1, '2018-10-03 07:27:18'),
(22, 'd2f8d1a4eb74d89605b5817a788058ca.png', 21, 1, '2018-10-03 07:30:07'),
(23, 'cc6b4b6aaf52abd0e3174ac34f4f4529.png', 22, 1, '2018-10-04 08:43:49'),
(24, '85e1917a9b1a6e6788fa141ca18f9eba.png', 23, 1, '2018-10-04 08:47:43'),
(26, 'b2510b187becf45e74b825dab3408dda.png', 25, 1, '2018-10-04 08:53:53'),
(27, '16c695b8d012528ecee5e4cf8d787bb1.png', 26, 1, '2018-10-04 08:57:07'),
(28, 'abc04e4bf778a4a6c1a4877400b8015f.png', 27, 1, '2018-10-04 09:00:22'),
(29, '1175ac7e89b754dcebc45dde6c77edd8.png', 28, 1, '2018-10-04 09:06:10'),
(30, '6e65b224e68b6f90ba6b186ed5bb79cf.png', 29, 1, '2018-10-04 09:15:21'),
(31, '9957e23e95f50a26773221e66f1996ac.png', 30, 1, '2018-10-04 09:18:26'),
(32, 'd945462b8dfdfa5ee5d9912ca6d6f7a8.png', 31, 1, '2018-10-04 09:21:03'),
(33, '718dc5954a43a9446314a0a5a68c629b.png', 32, 1, '2018-10-04 09:23:45'),
(34, 'eb6219e6c4066101fca28f666d5e6ee1.png', 33, 1, '2018-10-04 09:26:31'),
(35, 'e2a783883cbd61cd3037b809c8584171.png', 34, 1, '2018-10-04 09:29:15'),
(36, '4205202f05364eb18aeb86519a39b80b.png', 35, 1, '2018-10-04 09:31:57'),
(37, '7fa743f54649336bde24e83aad11f143.png', 36, 1, '2018-10-04 09:35:40'),
(38, '8009d2b69b515948b9d3192890eb0c9e.png', 37, 1, '2018-10-04 09:43:17'),
(39, '30b0810568324270c039e6144fe54ca6.png', 38, 1, '2018-10-04 09:47:25'),
(40, '12109e4f704b7f7ea2553d5b26db11c0.png', 39, 1, '2018-10-04 09:51:26'),
(41, '5bda4c33114d666cc7440a1c291ed044.png', 40, 1, '2018-10-04 09:54:49'),
(42, 'a1f8119ee4aa61985b26b872b1aacbd9.png', 41, 1, '2018-10-04 09:59:57'),
(43, 'c4fb6813157778d2f49fd1860ba4f2ac.png', 42, 1, '2018-10-04 10:12:07'),
(44, '072d9b606aa092cd2bdf4c3e04560ce8.png', 43, 1, '2018-10-04 10:19:03'),
(45, '20dfbc1ad4a05929e3e4a273e38a7e64.png', 44, 1, '2018-10-04 10:22:14'),
(46, '4cf506d0fd1a1e5d58a3a3d7d4f36fcb.png', 45, 1, '2018-10-04 10:24:23'),
(47, 'c4744c6c0b99f1e6995a2679677369c6.png', 46, 1, '2018-10-04 10:25:44'),
(48, '3269a0c4acebdf02211e5db5d44db253.png', 47, 1, '2018-10-04 10:26:52'),
(49, '66f621c921678050feab9126507de81d.png', 48, 1, '2018-10-04 10:28:02'),
(50, '9f074f67b0e34307cbffa2e5946e927a.png', 49, 1, '2018-10-04 10:28:59'),
(51, '5ad0a1b486c7cc12d9dba1c78898a8f1.png', 50, 1, '2018-10-04 10:30:02'),
(52, '5907a6480cebc1eeb2ac9720bb47055e.png', 51, 1, '2018-10-04 10:33:39'),
(53, '6d1c9ede71d03ab4832df02c69ce95ac.png', 52, 1, '2018-10-04 10:37:44'),
(54, '8c593aed395ef31423e841ce5b4e552c.png', 53, 1, '2018-10-04 10:40:32'),
(55, '4fe75d3048397852b59551682cd9e47b.png', 54, 1, '2018-10-04 10:43:48'),
(58, '5d843b27374d94903abd0fc6c2b3a3d5.png', 1, 1, '2018-10-08 09:40:12'),
(59, '8789481ccaa202733353ea2ef1b369a3.png', 1, 1, '2018-10-08 09:41:17'),
(61, '0734362e963ad3d78cce91f15f89be73.png', 55, 1, '2018-10-17 06:50:43'),
(63, '44e942b14c90d218563d9da7f0bac8fa.png', 24, 1, '2018-10-17 12:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `link_attributes_products`
--

CREATE TABLE `link_attributes_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `image_path` text,
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '0 = single, 1 = multi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `link_attributes_products`
--

INSERT INTO `link_attributes_products` (`id`, `product_id`, `attribute_id`, `attribute_value_id`, `price`, `image_path`, `type`) VALUES
(22, 1, 1, 1, 10, 'cddddb270b294fa9ac7916cf2d6f93e0.png', 0),
(23, 1, 1, 2, 30, 'ddsa', 0),
(24, 1, 1, 3, 50, 'ddddddddd', 0),
(25, 5, 1, 2, 0, NULL, 0),
(26, 8, 2, 5, 20, NULL, 0),
(27, 8, 2, 6, 30, NULL, 0),
(31, 9, 1, 2, 2, NULL, 0),
(32, 9, 1, 3, 3, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '110.93.214.36', 'jamesphilippp707@gmail.com', '0000-00-00 00:00:00'),
(2, '110.93.214.36', 'jamesphilippn@gamil.ocm', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `OrderDetailID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(255) NOT NULL,
  `ProductPrice` int(11) NOT NULL,
  `ProductSKU` int(11) NOT NULL,
  `ProductQuantity` int(11) NOT NULL,
  `ProductTotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`OrderDetailID`, `OrderID`, `ProductID`, `ProductName`, `ProductPrice`, `ProductSKU`, `ProductQuantity`, `ProductTotal`) VALUES
(1, 1, 1, 'Cable ignition', 7, 0, 1, 7),
(2, 2, 2, 'HV GENERATOR', 10, 0, 2, 20),
(3, 3, 1, 'Cable ignition', 7, 0, 1, 7),
(4, 3, 5, 'Pilot Burner Assembly', 58, 0, 2, 116),
(5, 3, 10, 'Pilot Burner Assembly', 65, 0, 3, 196),
(6, 4, 10, 'Pilot Burner Assembly', 65, 0, 1, 65),
(7, 4, 3, 'PILOT BURNER 2FL', 41, 0, 1, 41),
(8, 4, 1, 'Cable ignition', 7, 0, 1, 7),
(9, 5, 1, 'Cable ignition', 7, 0, 1, 7),
(10, 6, 10, 'Pilot Burner Assembly', 65, 0, 1, 65),
(11, 6, 7, 'PILOT BURNER ASSEMBLY', 65, 0, 1, 65),
(12, 6, 15, 'G-Fire Remote', 26, 0, 1, 26),
(13, 6, 1, 'Cable ignition', 7, 0, 1, 7),
(14, 7, 3, 'PILOT BURNER 2FL', 41, 0, 1, 41),
(16, 9, 55, 'test', 150, 0, 1, 150),
(17, 10, 55, 'test', 150, 0, 1, 150),
(18, 11, 24, 'VALVE SIT 630 EUROSIT 9/16', 160, 0, 1, 160),
(19, 12, 4, 'Pilot Burner Assembly', 58, 0, 1, 58),
(20, 13, 4, 'Pilot Burner Assembly', 58, 0, 1, 58),
(21, 14, 4, 'Pilot Burner Assembly', 58, 0, 1, 58);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `OrderID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0' COMMENT '0-Pending payment, 1-Processing, 2-On hold, 3-Completed, 4-Cancelled, 5-Cancelled, 6-Refunded, 7-Failed',
  `TransactionStatus` int(11) NOT NULL,
  `OrderAmount` int(11) NOT NULL,
  `OrderTrackingNo` varchar(100) NOT NULL DEFAULT '-',
  `OrderDiscount` varchar(11) NOT NULL DEFAULT '0',
  `OrderShipping` int(11) NOT NULL,
  `OrderCouponUsed` tinyint(1) NOT NULL DEFAULT '0',
  `OrderCouponId` varchar(11) DEFAULT NULL,
  `OrderCouponAmount` varchar(11) DEFAULT NULL,
  `OrderTotal` varchar(11) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isShipping` tinyint(4) NOT NULL DEFAULT '0',
  `billing_first_name` varchar(255) DEFAULT NULL,
  `billing_last_name` varchar(255) DEFAULT NULL,
  `billing_company` varchar(255) DEFAULT NULL,
  `billing_email` varchar(255) DEFAULT NULL,
  `billing_address_1` varchar(255) DEFAULT NULL,
  `billing_address_2` varchar(255) DEFAULT NULL,
  `billing_city` varchar(255) DEFAULT NULL,
  `billing_country` varchar(255) DEFAULT NULL,
  `billing_postcode` varchar(255) DEFAULT NULL,
  `billing_state` varchar(255) DEFAULT NULL,
  `billing_phone` varchar(255) DEFAULT NULL,
  `shipping_first_name` varchar(255) DEFAULT NULL,
  `shipping_last_name` varchar(255) DEFAULT NULL,
  `shipping_company` varchar(255) DEFAULT NULL,
  `shipping_email` varchar(255) DEFAULT NULL,
  `shipping_address_1` varchar(255) DEFAULT NULL,
  `shipping_address_2` varchar(255) DEFAULT NULL,
  `shipping_city` varchar(255) DEFAULT NULL,
  `shipping_postcode` varchar(255) DEFAULT NULL,
  `shipping_country` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_phone` varchar(255) DEFAULT NULL,
  `cart_settings` text,
  `coupon` text,
  `VendorID` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`OrderID`, `CustomerID`, `Status`, `TransactionStatus`, `OrderAmount`, `OrderTrackingNo`, `OrderDiscount`, `OrderShipping`, `OrderCouponUsed`, `OrderCouponId`, `OrderCouponAmount`, `OrderTotal`, `createdate`, `isShipping`, `billing_first_name`, `billing_last_name`, `billing_company`, `billing_email`, `billing_address_1`, `billing_address_2`, `billing_city`, `billing_country`, `billing_postcode`, `billing_state`, `billing_phone`, `shipping_first_name`, `shipping_last_name`, `shipping_company`, `shipping_email`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_state`, `shipping_phone`, `cart_settings`, `coupon`, `VendorID`) VALUES
(1, 0, 0, 1, 7, '-', '0', 0, 0, NULL, NULL, '7.39', '2018-10-04 16:36:38', 0, 'Max', 'D\'souza', NULL, 'max@dsouza.com', 'fdsafsdafsfsdfsdf', 'fdsafsdafsfsdfsdf', 'sdfsdfs', 'US', '234234', 'sdfsdfs', '12323132313', NULL, NULL, NULL, NULL, 'fdsafsdafsfsdfsdf', 'fdsafsdafsfsdfsdf', 'sdfsdfs', '234234', 'US', 'sdfsdfs', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"5\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(2, 0, 0, 1, 22, '-', '0', 0, 0, NULL, NULL, '22.37', '2018-10-05 16:24:45', 0, 'Max', 'Shaun', NULL, 'max@noemail.com', 'dfasfsafsafds', 'dfasfsafsafds', 'Dallas', 'US', '32423', 'Dallas', '123213123', NULL, NULL, NULL, NULL, 'dfasfsafsafds', 'dfasfsafsafds', 'Dallas', '32423', 'US', 'Dallas', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"5\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(3, 0, 0, 1, 357, '-', '0', 0, 0, NULL, NULL, '357.3', '2018-10-05 17:02:33', 0, 'Shaun', 'Smith', NULL, 'shaun@no-email.com', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', 'Dallas', 'US', '234234', 'Dallas', '23423423432', NULL, NULL, NULL, NULL, '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', 'Dallas', '234234', 'US', 'Dallas', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"5\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(4, 0, 0, 1, 127, '-', '0', 0, 0, NULL, NULL, '126.81', '2018-10-08 11:03:15', 0, 'ssdf', 'dfsdfsdf', NULL, 'fsdfdsfs@fwdsfsaf.com', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', 'xssfsdf', 'AL', '23423', 'xssfsdf', '4234234234', NULL, NULL, NULL, NULL, '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', 'xssfsdf', '23423', 'AL', 'xssfsdf', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"5\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(5, 0, 0, 1, 7, '-', '0', 0, 0, NULL, NULL, '7.39', '2018-10-08 05:04:30', 0, 'sdfaf', 'fdsfsdf', NULL, 'fsdfsdf@fsfsdf.com', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', 'dfsafsaf', 'US', '32343242', 'dfsafsaf', '23423423423', NULL, NULL, NULL, NULL, '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', 'dfsafsaf', '32343242', 'US', 'dfsafsaf', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"5\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(6, 0, 0, 1, 183, '-', '0', 0, 0, NULL, NULL, '182.54', '2018-10-08 05:44:44', 0, 'gdfgdf', 'dgdfgd', NULL, 'gdfgdfg@fsdfsdf.com', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', 'dfsdfsdf', 'AD', '324234', 'dfsdfsdf', '3423442423', NULL, NULL, NULL, NULL, '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', '2012 Champion Forest Drive                 Suite 700-155, Spring, Texas 77379 ', 'dfsdfsdf', '324234', 'AD', 'dfsdfsdf', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"5\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(7, 65, 0, 1, 46, '-', '0', 0, 0, NULL, NULL, '46.26', '2018-10-11 03:31:18', 0, 'Vin', 'Michael', NULL, 'vinmichael63@gmail.com', 'Jackson street, Dallas - TX', 'Jackson street, Dallas - TX', 'Dallas', 'US', '23423', 'Dallas', '123-456-7890', NULL, NULL, NULL, NULL, 'Jackson street, Dallas - TX', 'Jackson street, Dallas - TX', 'Dallas', '23423', 'US', 'Dallas', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"5\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(9, 67, 4, 1, 164, '-', '0', 0, 0, NULL, NULL, '163.5', '2018-10-16 04:21:28', 0, 'James', 'Philippp', NULL, 'jamesphilippp407@gmail.com', 'ggsdg', 'ggsdg', 'gdfgdg', 'DZ', 'fgdfg', 'gdfgdg', '4643364634', NULL, NULL, NULL, NULL, 'ggsdg', 'ggsdg', 'gdfgdg', 'fgdfg', 'DZ', 'gdfgdg', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"2\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(10, 67, 0, 1, 16, '-', '0', 0, 0, NULL, NULL, '16.34', '2018-10-16 20:06:53', 0, 'James', 'Philippp', NULL, 'jamesphilippp407@gmail.com', 'ggsdg', 'ggsdg', 'gdfgdg', 'AL', 'fgdfg', 'gdfgdg', '364636', NULL, NULL, NULL, NULL, 'ggsdg', 'ggsdg', 'gdfgdg', 'fgdfg', 'AL', 'gdfgdg', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"2\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', 'a:2:{s:4:\"code\";s:4:\"test\";s:4:\"disc\";s:2:\"90\";}', NULL),
(11, 0, 0, 1, 174, '-', '0', 0, 0, NULL, NULL, '174.02', '2018-10-17 11:16:04', 0, 'quwyfej', 'daseqywunu', NULL, 'dennyhopkin@gmail.com', 'Obcaecati esse cupiditate consequatur Dolor cupidatat voluptas tempor magnam mollitia porro dolor aut tenetur quod velit voluptas do dolor', 'Obcaecati esse cupiditate consequatur Dolor cupidatat voluptas tempor magnam mollitia porro dolor aut tenetur quod velit voluptas do dolor', 'Ipsam vel ducimus reiciendis asperiores quae quis quo ipsum soluta exercitationem voluptate quis eiusmod inventore Nam', 'PE', '83013', 'Ipsam vel ducimus reiciendis asperiores quae quis quo ipsum soluta exercitationem voluptate quis eiusmod inventore Nam', '+795-87-8777518', NULL, NULL, NULL, NULL, 'Obcaecati esse cupiditate consequatur Dolor cupidatat voluptas tempor magnam mollitia porro dolor aut tenetur quod velit voluptas do dolor', 'Obcaecati esse cupiditate consequatur Dolor cupidatat voluptas tempor magnam mollitia porro dolor aut tenetur quod velit voluptas do dolor', 'Ipsam vel ducimus reiciendis asperiores quae quis quo ipsum soluta exercitationem voluptate quis eiusmod inventore Nam', '83013', 'PE', 'Ipsam vel ducimus reiciendis asperiores quae quis quo ipsum soluta exercitationem voluptate quis eiusmod inventore Nam', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"2\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(12, 0, 0, 1, 63, '-', '0', 0, 0, NULL, NULL, '63.47', '2018-10-17 11:28:51', 0, 'hygizavy', 'lujalir', NULL, 'dennyhopkin@gmail.com', 'Velit ullam temporibus dolores tempora quis dolore obcaecati est excepteur quasi', 'Velit ullam temporibus dolores tempora quis dolore obcaecati est excepteur quasi', 'Rem cupidatat deleniti eveniet maiores ea dolores repudiandae quis vero minim quaerat quidem quibusdam omnis deserunt incidunt aut reiciendis quibusdam', 'AT', '23910', 'Rem cupidatat deleniti eveniet maiores ea dolores repudiandae quis vero minim quaerat quidem quibusdam omnis deserunt incidunt aut reiciendis quibusdam', '+332-26-3964106', NULL, NULL, NULL, NULL, 'Velit ullam temporibus dolores tempora quis dolore obcaecati est excepteur quasi', 'Velit ullam temporibus dolores tempora quis dolore obcaecati est excepteur quasi', 'Rem cupidatat deleniti eveniet maiores ea dolores repudiandae quis vero minim quaerat quidem quibusdam omnis deserunt incidunt aut reiciendis quibusdam', '23910', 'AT', 'Rem cupidatat deleniti eveniet maiores ea dolores repudiandae quis vero minim quaerat quidem quibusdam omnis deserunt incidunt aut reiciendis quibusdam', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"2\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(13, 68, 4, 1, 63, '-', '0', 0, 0, NULL, NULL, '63.47', '2018-10-17 11:34:03', 0, 'lotihit', 'cefelix', NULL, 'dennyhopkin@gmail.com', '26 Hague Extension', '26 Hague Extension', 'Cillum quia nemo excepturi sint est fugiat velit', 'US', 'Alias impedit irure voluptatem', 'Cillum quia nemo excepturi sint est fugiat velit', '+489-84-4055545', NULL, NULL, NULL, NULL, '26 Hague Extension', '26 Hague Extension', 'Cillum quia nemo excepturi sint est fugiat velit', 'Alias impedit irure voluptatem', 'US', 'Cillum quia nemo excepturi sint est fugiat velit', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"2\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL),
(14, 68, 4, 1, 63, '', '0', 0, 0, NULL, NULL, '63.47', '2018-10-17 12:12:55', 0, 'lotihit', 'cefelix', NULL, 'dennyhopkin@gmail.com', '26 Hague Extension', '26 Hague Extension', 'Cillum quia nemo excepturi sint est fugiat velit', 'AO', 'Alias impedit irure voluptatem', 'Cillum quia nemo excepturi sint est fugiat velit', 'sdvsdvsdv', NULL, NULL, NULL, NULL, '26 Hague Extension', '26 Hague Extension', 'Cillum quia nemo excepturi sint est fugiat velit', 'Alias impedit irure voluptatem', 'AO', 'Cillum quia nemo excepturi sint est fugiat velit', NULL, 'a:2:{i:0;a:3:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:3:\"VAT\";s:4:\"rate\";s:1:\"2\";}i:1;a:3:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:8:\"Shipping\";s:4:\"rate\";s:1:\"7\";}}', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

CREATE TABLE `order_payments` (
  `id` int(11) NOT NULL,
  `fkUserId` int(50) DEFAULT NULL,
  `OrderId` int(50) DEFAULT NULL,
  `paypal_timestamp` varchar(50) DEFAULT NULL,
  `correlation_id` varchar(50) DEFAULT NULL,
  `ack` varchar(50) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  `build` varchar(50) DEFAULT NULL,
  `amt` varchar(50) DEFAULT NULL,
  `currencycode` varchar(50) DEFAULT NULL,
  `avscode` varchar(50) DEFAULT NULL,
  `cvv2match` varchar(50) DEFAULT NULL,
  `transactionid` varchar(50) DEFAULT NULL,
  `payment_type` varchar(50) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_payments`
--

INSERT INTO `order_payments` (`id`, `fkUserId`, `OrderId`, `paypal_timestamp`, `correlation_id`, `ack`, `version`, `build`, `amt`, `currencycode`, `avscode`, `cvv2match`, `transactionid`, `payment_type`, `date_added`) VALUES
(1, 0, 1, '2018-10-04T16:36:37Z', 'ec745dfb21160', 'Success', '65.1', '46457558', '7.39', 'USD', 'Y', 'S', '0YV82102JC111311T', 'Pay with Credit Card', '2018-10-04 16:36:38'),
(2, 0, 2, '2018-10-05T16:24:45Z', '77bf36f9b8505', 'Success', '65.1', '46457558', '22.37', 'USD', 'Y', 'S', '8AG14146CV295490R', 'Pay with Credit Card', '2018-10-05 16:24:45'),
(3, 0, 3, '2018-10-05T17:02:33Z', '35e094139f46a', 'Success', '65.1', '46457558', '357.30', 'USD', 'Y', 'S', '81051472UR557671B', 'Pay with Credit Card', '2018-10-05 17:02:33'),
(4, 0, 4, '2018-10-08T11:03:15Z', '42019eed17374', 'Success', '65.1', '46457558', '126.81', 'USD', 'Y', 'S', '5NP08452GM202282R', 'Pay with Credit Card', '2018-10-08 11:03:15'),
(5, 0, 5, '2018-10-08T15:04:30Z', '4c6c43d7dca08', 'Success', '65.1', '46457558', '7.39', 'USD', 'Y', 'S', '2V982852LA147013G', 'Pay with Credit Card', '2018-10-08 15:04:30'),
(6, 0, 6, '2018-10-08T15:44:43Z', 'b34bb63a2f4d7', 'Success', '65.1', '46457558', '182.54', 'USD', 'Y', 'S', '2ET470555W380863U', 'Pay with Credit Card', '2018-10-08 15:44:44'),
(7, 65, 7, '2018-10-11T13:31:18Z', '27ad996b25ec5', 'Success', '65.1', '46457558', '46.26', 'USD', 'Y', 'S', '5U789265L0399792H', 'Pay with Credit Card', '2018-10-11 13:31:18'),
(8, 66, 8, '2018-10-11T15:37:39Z', '9d73370d7e2e1', 'Success', '65.1', '46457558', '46.26', 'USD', 'Y', 'S', '6C10207759998542P', 'Pay with Credit Card', '2018-10-11 15:37:39'),
(9, 67, 9, '2018-10-16 14:21:28', '', 'Success', '', '', '163.5', '', '', '', 'Zq8QgFx3CH5KtifdM', 'Cash On Delievery', '2018-10-16 14:21:28'),
(10, 67, 10, '2018-10-17T06:06:53Z', '52447ef099d33', 'Success', '65.1', '46457558', '16.34', 'USD', 'Y', 'M', '3JP99054K9904774F', 'Pay with Credit Card', '2018-10-17 06:06:53'),
(11, 0, 11, '2018-10-17 13:16:04', '', 'Success', '', '', '174.02', '', '', '', 'QhEXMYHbsvdioRUIe', 'Cash On Delievery', '2018-10-17 13:16:04'),
(12, 0, 12, '2018-10-17 13:28:51', '', 'Success', '', '', '63.47', '', '', '', '8LwuIstp2koN3Ac1l', 'Cash On Delievery', '2018-10-17 13:28:51'),
(13, 68, 13, '2018-10-17 13:34:03', '', 'Success', '', '', '63.47', '', '', '', 'g4jbWScesLUf93IY8', 'Cash On Delievery', '2018-10-17 13:34:03'),
(14, 68, 14, '2018-10-17 14:12:55', '', 'Success', '', '', '63.47', '', '', '', 'WZK5JPhiXt2l0T7gz', 'Cash On Delievery', '2018-10-17 14:12:55');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `body`, `created_at`, `status`) VALUES
(1, 'About Us', 'about-us', '<h1>About  <br> Vegan Day & Night</h1>\r\n<p>We are a company that believes in quality food and combining dishes from diverse cultures and backgrounds to make a scrumptious cuisine for you and your family to enjoy. </p>\r\n<p>We learned how to take food from your favorite culture and make it vegan, whether it’s Italian, Spanish, Oreintal, Caribbean, American, Soul-Food and more. We put our apron’s on when it counts and turn any dish you would love to taste, vegan.</p>', '2018-09-12 14:04:24', 0),
(2, 'Privacy Policy', 'privacy-policy', '<p>privacy policy</p>', '2018-09-12 14:22:44', 0),
(3, 'Refund Policy', 'refund-policy', '<p>Refund Policy</p>', '2018-09-12 14:23:01', 0),
(4, 'Terms and Conditions', 'terms-and-conditions', '<p>terms &amp; Conditions</p>', '2018-09-12 14:23:19', 0),
(5, 'FAQs', 'faqs', '<p><strong>Qno1 .Hello this is question</strong><br />\r\nThis is Answer</p>\r\n\r\n<p><strong>Qno2&nbsp;.Hello this is question</strong><br />\r\nThis is Answer</p>\r\n\r\n<p><strong>Qno3&nbsp;.Hello this is question</strong><br />\r\nThis is Answer</p>\r\n\r\n<p>&nbsp;</p>', '2018-09-12 14:26:13', 0);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `used` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`, `used`) VALUES
('dennyhopkin@gmail.com', 'f457c545a9ded88f18ecee47145a72c0', NULL, 1),
('dennyhopkin@gmail.com', '9a1158154dfa42caddbd0694a4e9bdc8', NULL, 1),
('dennyhopkin@gmail.com', '9a1158154dfa42caddbd0694a4e9bdc8', NULL, 1),
('dennyhopkin@gmail.com', '7f39f8317fbdb1988ef4c628eba02591', NULL, 1),
('jamesphilippp407@gmail.com', '735b90b4568125ed6c3f678819b6e058', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL,
  `cc_name` varchar(50) DEFAULT NULL,
  `cc_number` varchar(50) DEFAULT NULL,
  `cc_cvv` varchar(5) DEFAULT NULL,
  `cc_month` varchar(4) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_zip` varchar(10) DEFAULT NULL,
  `cc_type` enum('lead','reg') DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `perm_key` varchar(30) NOT NULL,
  `perm_name` varchar(100) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `perm_key`, `perm_name`, `created_on`) VALUES
(1, 'access_admin', 'Admin', '2018-03-12 07:19:20'),
(3, 'users_edit', 'Users Edit', '2018-03-12 07:19:20'),
(4, 'users_add', 'Users Add', '2018-03-12 07:19:20'),
(5, 'users_delete', 'Users Delete', '2018-03-12 07:19:20'),
(6, 'groups_view', 'Groups View', '2018-03-12 07:19:20'),
(8, 'groups_edit', 'Groups Edit', '2018-03-12 07:19:20'),
(9, 'groups_add', 'Groups Add', '2018-03-12 07:19:20'),
(10, 'groups_delete', 'Groups Delete', '2018-03-12 07:19:20'),
(11, 'permissions_view', 'Permissions View', '2018-03-12 07:19:20'),
(12, 'permissions_edit', 'Permissions Edit', '2018-03-12 07:19:20'),
(13, 'permissions_add', 'Permissions Add', '2018-03-12 07:19:20'),
(14, 'permissions_delete', 'Permissions Delete', '2018-03-12 07:19:20'),
(15, 'groups_permissions_edit', 'Groups Permission Edit', '2018-03-12 07:19:20'),
(16, 'users_permissions_edit', 'Users Permission Edit', '2018-03-12 07:19:20'),
(17, 'categories_view', 'Categories View', '2018-03-12 07:19:20'),
(18, 'categories_edit', 'Categories Edit', '2018-03-12 07:19:20'),
(19, 'categories_add', 'Categories Add', '2018-03-12 07:19:20'),
(20, 'categories_delete', 'Categories Delete', '2018-03-12 07:19:20'),
(21, 'products_view', 'Products View', '2018-03-12 07:19:20'),
(22, 'products_edit', 'Products Edit', '2018-03-12 07:19:20'),
(23, 'products_add', 'Products Add', '2018-03-12 07:19:20'),
(24, 'products_delete', 'Products Delete', '2018-03-12 07:19:20'),
(25, 'categories_sorting', 'Categories Sorting', '2018-03-12 07:19:20'),
(26, 'attributes_view', 'Attributes View', '2018-03-12 07:19:20'),
(27, 'attributes_add', 'Attributes Add', '2018-03-12 07:19:46'),
(28, 'attributes_edit', 'Attributes Edit', '2018-03-12 07:19:59'),
(29, 'attributes_delete', 'Attributes Delete', '2018-03-12 07:20:19'),
(30, 'coupon_view', 'Coupon View', '0000-00-00 00:00:00'),
(31, 'settings_view', 'Settings View', '2018-08-31 07:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `description` text,
  `product_specifications` text,
  `price` varchar(11) DEFAULT NULL,
  `stock_management` int(11) NOT NULL DEFAULT '0',
  `stock_qty` varchar(11) NOT NULL DEFAULT '0',
  `stock_status` enum('out of stock','in stock') DEFAULT 'in stock',
  `reg_price` varchar(11) DEFAULT NULL,
  `sale_from` date DEFAULT NULL,
  `sale_to` date DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `updateby` int(11) DEFAULT NULL,
  `featured` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `sku`, `description`, `product_specifications`, `price`, `stock_management`, `stock_qty`, `stock_status`, `reg_price`, `sale_from`, `sale_to`, `createdate`, `updatedate`, `createby`, `updateby`, `featured`) VALUES
(1, 'Cable ignition', 'cable-ignition', '0028515', 'Cable ignition, Length 600 (mm) with a Spark electrode terminal (2.4 mm pin)  SIT piezo series 073 VN receptacles', 'While we aim to provide accurate product information, it is provided by manufacturers, suppliers and others, and has not been verified by us. See our disclaimer.\r\n                                                    The beauty of the Q7C arc is a visual spectacle. Equipped with the features of the Q7 Flat, but with a bend for immersive viewing, it combines Q Color and an anti-glare screen for a dazzling display unlike any other.\r\n                                                    Brandname 65\" Class 4K(2160P) Curved Smart QLED TV QN65Q7CN (2018 Model)', '6.60', 0, '0', NULL, '6.60', NULL, NULL, '2018-10-01 09:18:51', '2018-10-08 10:03:52', 25, 25, 0),
(2, 'HV GENERATOR', 'hv-generator', '0073953', 'HV GENERATOR, Piezo (M14X1) without cover, version for Eurosit cover (included seeger)', NULL, '9.99', 0, '0', NULL, '9.99', NULL, NULL, '2018-10-01 09:27:08', '2018-10-04 11:25:41', 25, 25, 0),
(3, 'PILOT BURNER 2FL', 'pilot-burner-2fl', '0145033', 'PILOT BURNER 2FL, 2 flames, braket type: (B4-target) Left orientation & does not include: injector, Olive & electrode.', NULL, '41.31', 0, '0', NULL, '41.31', NULL, NULL, '2018-10-01 09:34:43', '2018-10-04 11:26:01', 25, 25, 0),
(4, 'Pilot Burner Assembly', 'pilot-burner-assembly', '0199059', 'Pilot Burner Assembly, 2 flames convertible, natural gas,  orientation 221. Injector 62 Marked 62 NG (0.977.166 Top convertible) Olive size 0 1/8 . Electrode with cable 620 (mm) HT', NULL, '58.23', 0, '8', NULL, '58.23', NULL, NULL, '2018-10-01 09:37:17', '2018-10-04 11:26:24', 25, 25, 0),
(5, 'Pilot Burner Assembly', 'pilot-burner-assembly-1', '0199060', 'Pilot Burner Assembly, 2 flames convertible, natural gas,  orientation 221. Injector 62 Marked 62 NG (0.977.166 Top convertible) Olive size 0 1/8 . Electrode with cable 620 (mm) HT', NULL, '58.23', 0, '8', NULL, '58.23', NULL, NULL, '2018-10-01 10:08:19', '2018-10-04 11:26:50', 25, 25, 0),
(6, 'PILOT BURNER ASSEMBLY', 'pilot-burner-assembly-2', '0199701', 'PILOT BURNER ASSEMBLY, 3 convertible flames, Propane gas type, Pilot pipe 24\" USA 7/16-M10 .  # 30 Injector size with electrode cable.', NULL, '65.32', 0, '10', NULL, '65.32', NULL, NULL, '2018-10-01 10:13:43', '2018-10-04 11:27:17', 25, 25, 0),
(7, 'PILOT BURNER ASSEMBLY', 'pilot-burner-assembly-3', '0199702', 'PILOT BURNER ASSEMBLY, 3 convertible flames, Natural gas type, Pilot pipe 24\" USA 7/16-M10 . # 51 Injector size with electrode cable.', NULL, '65.32', 0, '9', NULL, '65.32', NULL, NULL, '2018-10-01 10:20:44', '2018-10-04 11:27:42', 25, 25, 0),
(8, 'PILOT BURNER ASSEMBLY', 'pilot-burner-assembly-4', '0199706', 'PILOT BURNER ASSEMBLY, 3 convertible flames, Natural gas type, Pilot pipe 24\" USA 7/16-M10 . # 62 Injector size with electrode cable.', NULL, '65.32', 0, '10', NULL, '65.32', NULL, NULL, '2018-10-01 10:29:03', '2018-10-04 11:28:19', 25, 25, 0),
(9, 'Pilot Burner Assembly', 'pilot-burner-assembly-5', '0199768', 'PILOT BURNER ASSEMBLY, 3 convertible flames, Propane gas type, Assieme Pilot Pipe                             # 30 Injector size with electrode cable.', NULL, '65.32', 0, '10', NULL, '65.32', NULL, NULL, '2018-10-01 10:33:53', '2018-10-04 11:28:59', 25, 25, 0),
(10, 'Pilot Burner Assembly', 'pilot-burner-assembly-6', '0199769', 'PILOT BURNER ASSEMBLY, 3 convertible flames, Natural gas type, Assieme Pilot Pipe                              # 51 Injector size with electrode cable.', NULL, '65.32', 0, '5', NULL, '65.32', NULL, NULL, '2018-10-01 10:36:54', '2018-10-04 11:29:23', 25, 25, 0),
(11, 'TERMOCOPPIA INT 600 ASA TARG', 'termocoppia-int-600-asa-targ', '0270457', 'TERMOCOPPIA INT 600 ASA TARGET, Cooper pipe, Length (600 mm) Diameter (6 mmm) Connection nut size 11/32 ASA. Sleeve type; Target.', NULL, '22.53', 0, '20', NULL, '22.53', NULL, NULL, '2018-10-03 06:58:44', '2018-10-04 11:29:59', 25, 25, 0),
(12, 'T.C.INT.900', 'tcint900', '0270500', 'T.C. INT 900, Cooper pipe, Length (900 mm) Diameter (6 mmm) Connection nut size 11/32 ASA. Sleeve type; Target.', NULL, '22.63', 0, '2', NULL, '22.63', NULL, NULL, '2018-10-03 07:01:22', '2018-10-04 11:30:20', 25, 25, 0),
(13, 'T.C.600 ASA UNIF.SLEEVE', 'tc600-asa-unifsleeve', '0290024', 'T.C. INT 600, Cooper pipe, Length (900 mm) Diameter (6 mmm) Connection nut size 11/32 ASA. Sleeve type; Unified.', NULL, '22.11', 0, '10', NULL, '22.11', NULL, NULL, '2018-10-03 07:03:17', '2018-10-04 11:30:37', 25, 25, 0),
(14, 'TERMOCOPPIA 600 ASA TARG', 'termocoppia-600-asa-targ', '0290216', 'TERMOCOPPIA INT 600 ASA TARGET, Cooper pipe, Length (600 mm) Diameter (6 mmm) Connection nut size 11/32 ASA. Sleeve type; Target.', NULL, '22.11', 0, '50', NULL, '22.11', NULL, NULL, '2018-10-03 07:05:13', '2018-10-04 11:31:00', 25, 25, 0),
(15, 'G-Fire Remote', 'g-fire-remote', '0584008', 'G-Fire Remote control with On / Off Manual control', NULL, '25.75', 0, '4', NULL, '25.75', NULL, NULL, '2018-10-03 07:08:28', '2018-10-04 11:31:32', 25, 25, 0),
(16, 'Proflame transmitter (GTM )', 'proflame-transmitter-gtm', '0584022', 'Proflame Transmiter (GTM) Manual flame height control, room temperature display with settable On/Off and Smart thermostat modes, child safetly lock, and low battery alarm indicator.', NULL, '99.50', 0, '2', NULL, '99.50', NULL, NULL, '2018-10-03 07:11:30', '2018-10-04 11:31:50', 25, 25, 0),
(17, 'Proflame transmitter (GTMF)', 'proflame-transmitter-gtmf', '0584023', 'Manual flame height control (1~6), room temperature display with settable On/Off and Smart thermostat modes, manual fan speed control (1~6), child safety lock, and a low battery alarm indicator.', NULL, '99.50', 0, '2', NULL, '99.50', NULL, NULL, '2018-10-03 07:14:28', '2018-10-04 11:32:14', 25, 25, 0),
(19, 'Proflame Receiver Batt.  Cover', 'proflame-receiver-batt-cover', '0584806', 'Receiver battery cover replacement', NULL, '7.50', 0, '10', NULL, '7.50', NULL, NULL, '2018-10-03 07:22:34', '2018-10-04 11:33:25', 25, 25, 0),
(20, 'VALVE SIT 630 EUROSIT', 'valve-sit-630-eurosit', '0630508', 'SIT 630 EUROSIT SERIES, Thermostat range 13-48, Pressure regulator Po=10\" Pi=12\" Q=35,000 Btu/h for LPG. (Drop out-Hold in current) 160 320, Thermocouple connection 11/32 ASA. Pilot connection 7/16 24UNS, 0 1/4, 21 mm long. Inlet Side Thread 3/8 NPT. Inlet Bottom Thread 3/8 Outlet Side Thread 3/8 NPT. Outlet Bottom Thread 3/8 NPT.', NULL, '178.98', 0, '2', NULL, '178.98', NULL, NULL, '2018-10-03 07:27:18', '2018-10-04 11:33:44', 25, 25, 0),
(21, 'VALVE SIT 630 EUROSIT', 'valve-sit-630-eurosit-1', '0630509', 'SIT 630 EUROSIT SERIES, Thermostat range 13-48, Pressure regulator Po=10\" Pi=12\" Q=35,000 Btu/h for LPG. (Drop out-Hold in current) >65<170, Thermocouple connection 11/32 ASA. Pilot connection 7/16 24UNS, 0 1/4, 21 mm long. Inlet Side Thread 3/8 NPT. Inlet Bottom Thread 3/8 Outlet Side Thread 3/8 NPT. Outlet Bottom Thread 3/8 NPT.', NULL, '178.98', 0, '2', NULL, '178.98', NULL, NULL, '2018-10-03 07:30:07', '2018-10-04 11:34:07', 25, 25, 0),
(22, 'VALVE SIT 630 EUROSIT', 'valve-sit-630-eurosit-2', '0630515', 'SIT 630 EUROSIT SERIES, Thermostat range 13-48, Pressure regulator Po=3.5\" Pi=7\" Q=27,000 Btu/h for NG. (Drop out-Hold in current) 160 -320, Thermocouple connection 11/32 ASA. Pilot connection 7/16 24UNS, 0 1/4, 21 mm long. Inlet Side Thread 3/8 NPT. Inlet Bottom Thread 3/8 Outlet Side Thread 3/8 NPT. Outlet Bottom Thread 3/8 NPT.', NULL, '149.15', 0, '2', NULL, '149.15', NULL, NULL, '2018-10-04 08:43:49', '2018-10-04 11:34:28', 25, 25, 0),
(23, 'VALVE SIT 630 EUROSIT', 'valve-sit-630-eurosit-3', '0630516', 'SIT 630 EUROSIT SERIES, Thermostat range 13-48, Pressure regulator Not fitted. (Drop out-Hold in current) >40<200, Thermocouple connection M8X1. Pilot connection No.7/16 24UNS. Inlet Side Thread 9/16 24UNFE. Inlet Bottom Thread 3/8 Outlet Side Thread 3/8 NPT. Outlet Bottom Thread 3/8 NPT.', NULL, '149.15', 0, '2', NULL, '149.15', NULL, NULL, '2018-10-04 08:47:43', '2018-10-04 11:34:46', 25, 25, 0),
(24, 'VALVE SIT 630 EUROSIT 9/16', 'valve-sit-630-eurosit-916', '0630545', 'SIT 630 EUROSIT SERIES 9/16\", Thermostat range 13-48, Pressure regulator Po=3.5\" Pi=7\" Q=27,000 Btu/h for NG. (Drop out-Hold in current) >65<170, Thermocouple connection 11/32 ASA. Pilot connection 7/16 24UNS, 0 1/4, 21 mm long. Inlet Side Thread 9/16 24UNFE. Inlet Bottom Thread 9/16 24UNFE Outlet Side Thread 3/8 NPT. Outlet Bottom Thread 9/16 24UNEF.', 'dfbdfb', '159.66', 0, '1', NULL, '159.66', NULL, NULL, '2018-10-04 08:50:52', '2018-10-17 12:45:38', 25, 25, 0),
(25, '820MV HI-LO VALVE', '820mv-hi-lo-valve', '0820635', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual Hi-Lo 20% for N.G. (Set at 3.5\" Pi=7\" Q=30,000 Btu/h for NG. (Drop out-Hold in current) 125-285, Thermocouple connection 11/32 ASA. Pilot connection Standard 1/4\" (7/16UNS) Shear Off nut (0958042) . Inlet Thread 3/8\".  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '2', NULL, '122.17', NULL, NULL, '2018-10-04 08:53:53', '2018-10-04 11:35:26', 25, 25, 0),
(26, 'VALVE 820 MV HI-LO', 'valve-820-mv-hi-lo', '0820636', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual Hi-Lo 25% for L.P.G. (Set at 10\" Pi=12\" Q=30,000 Btu/h for LPG. (Drop out-Hold in current) 80-200, Thermocouple connection M 8X1. Pilot connection Special Screw (drilled 2x1.5)  3/16\"  Shear Off nut (0958042) . Inlet Thread 3/8\".  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '2', NULL, '122.17', NULL, NULL, '2018-10-04 08:57:07', '2018-10-04 11:35:49', 25, 25, 0),
(27, 'VALVE 820 TRUE HI-LO NG33%', 'valve-820-true-hi-lo-ng33', '0820640', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual Hi-Lo 33% for NG. (Set at 3.5\" Pi=7\" Q=30,000 Btu/h for NG. (Drop out-Hold in current) True version, Thermocouple connection 11/32 ASA. Pilot connection Standard Screw 1/4\"(7/16UNS)   Shear Off nut (0958042) . Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '2', NULL, '122.17', NULL, NULL, '2018-10-04 09:00:22', '2018-10-04 11:36:49', 25, 25, 0),
(28, 'VALVE 820MV HI-LO', 'valve-820mv-hi-lo', '0820642', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual Hi-Lo 25% for LPG. (Set at 10\" Pi=12\" Q=30,000 Btu/h for LPG. (Drop out-Hold in current) 80-200, Thermocouple connection M 8X1. Pilot connection Special Screw (Drilled 2x1.5) 3/16\" Tube without shear-Off Nut . Inlet Thread 1/2\" NPT.  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '2', NULL, '122.17', NULL, NULL, '2018-10-04 09:06:10', '2018-10-04 11:37:06', 25, 25, 0),
(29, 'VALVE 820MV HI-LO', 'valve-820mv-hi-lo-1', '0820643', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual Hi-Lo 33% for NG. (Set at 3.5\" Pi=7\" Q=30,000 Btu/h for NG. (Drop out-Hold in current) 80-200, Thermocouple connection M 8X1. Pilot connection Special Screw (Drilled 2x1.5)  3/16\" Tube without shear-Off Nut . Inlet Thread 1/2\" NPT.  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '2', NULL, '122.17', NULL, NULL, '2018-10-04 09:15:21', '2018-10-04 11:37:29', 25, 25, 0),
(30, 'VALVE 820 MV HI-LO NG', 'valve-820-mv-hi-lo-ng', '0820644', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual Hi-Lo  NG. (Set at 3.8\" Pi=7\" Q=30,000 Btu LO 1.1\". (Drop out-Hold in current) 125-285, Thermocouple connection 11/32\" ASA. Pilot connection Standard adjustable pilot outlet (7/16UNS) screw.  Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '2', NULL, '122.17', NULL, NULL, '2018-10-04 09:18:26', '2018-10-04 11:37:48', 25, 25, 0),
(31, 'VALVE 820 MV HI-LO', 'valve-820-mv-hi-lo-1', '0820651', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual Hi-Lo 25% for LPG. (Set at 10\" Pi=12\" Q=30,000 Btu . (Drop out-Hold in current) 125-285, Thermocouple connection 11/32 ASA. Pilot connection Standard = Adjustable pilot outlet (7/16UNS) screw. Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '5', NULL, '122.17', NULL, NULL, '2018-10-04 09:21:03', '2018-10-04 11:38:41', 25, 25, 0),
(32, 'VALVE 820 MV HI-LO', 'valve-820-mv-hi-lo-2', '0820652', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual Hi-Lo  33% for NG. (Set at 3.5\" Pi=7\" Q=30,000 Btu. (Drop out-Hold in current) 125-285, Thermocouple connection 11/32\" ASA. Pilot connection Standard adjustable pilot outlet (7/16UNS) screw.  Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '10', NULL, '122.17', NULL, NULL, '2018-10-04 09:23:45', '2018-10-04 11:39:04', 25, 25, 0),
(33, 'VALVE 820MV', 'valve-820mv', '0820656', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator NG version (Set at 3.5\" Pi=7\" Q=30,000 Btu. (Drop out-Hold in current) 125-285, Thermocouple connection 11/32\" ASA. Pilot connection Standard adjustable pilot outlet (7/16UNS) screw.  Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '2', NULL, '122.17', NULL, NULL, '2018-10-04 09:26:31', '2018-10-04 11:39:26', 25, 25, 0),
(34, 'VALVE 820 MV HI-LO', 'valve-820-mv-hi-lo-3', '0820662', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator NG version (Set at 3.5\" Pi=7\" Q=30,000 Btu Convertible to LPG. (Drop out-Hold in current) 125-285, Thermocouple connection 11/32 ASA. Pilot connection Standard = Adjustable pilot outlet (7/16UNS) screw. Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '122.17', 0, '2', NULL, '122.17', NULL, NULL, '2018-10-04 09:29:15', '2018-10-04 11:40:30', 25, 25, 0),
(35, 'VALVE 820 MV HI-LO', 'valve-820-mv-hi-lo-4', '0820707', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator Manual HI-LO LPG  (Set at 10\" Pi=12\" Q=30,000 Btu LO 3.6\". (Drop out-Hold in current) 125-285, Thermocouple connection 11/32 ASA. Pilot connection Standard = Adjustable pilot outlet (7/16UNS) screw. Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '142.90', 0, '3', NULL, '142.90', NULL, NULL, '2018-10-04 09:31:57', '2018-10-04 11:41:33', 25, 25, 0),
(36, 'VALVE 820MV HI-LO (225F)', 'valve-820mv-hi-lo-225f', '0820708', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator HI-LO NG (Set at 3.5\" Pi=7\" Q=30,000 Btu) LO 1.3\" . (Drop out-Hold in current) 125-285, Thermocouple connection 11/32 ASA. Pilot connection Standard = Adjustable pilot outlet (7/16UNS) screw. Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '142.90', 0, '3', NULL, '142.90', NULL, NULL, '2018-10-04 09:35:40', '2018-10-04 11:43:51', 25, 25, 0),
(37, 'VALVE 820 mV Convertibile HI-LO', 'valve-820-mv-convertibile-hi-lo', '0820844', 'SIT 820 MILLIVOLT SERIES, No step opening, Pressure regulator NG 3.8-1.1\" & LPG 11-2.9\". (Drop out-Hold in current) 125-285, Thermocouple connection 11/32 ASA. Pilot connection Standard = Adjustable pilot outlet (7/16UNS) screw. Inlet Thread 3/8\" NPT.  Outlet Thread 3/8 NPT.', NULL, '147.18', 0, '4', NULL, '147.18', NULL, NULL, '2018-10-04 09:43:17', '2018-10-04 11:44:15', 25, 25, 0),
(38, 'VALVE 0880 Proflame Valvola ON-OFF', 'valve-0880-proflame-valvola-on-off', '0880001', 'SIT Proflame valve 0880 series, On-Off gas type NG. Pressure outlet 3.5\" W.C. Pilot connection 7/16 24UNS. Inlet 3/8\" NPT', NULL, '131.25', 0, '1', NULL, '131.25', NULL, NULL, '2018-10-04 09:47:25', '2018-10-04 11:44:38', 25, 25, 0),
(39, 'VALVE 0885 Valvola Proflame Modulante', 'valve-0885-valvola-proflame-modulante', '0885001', 'SIT Proflame valve 0885 series Modulante, gas type NG. Pressure outlet 3.5-1.6\" W.C. Pilot connection 7/16 24UNS. Inlet 3/8\" NPT', NULL, '193.75', 0, '1', NULL, '193.75', NULL, NULL, '2018-10-04 09:51:26', '2018-10-04 11:45:24', 25, 25, 0),
(40, 'VALVE 0886 Proflame Hi Lo NG 3.5-1.6', 'valve-0886-proflame-hi-lo-ng-35-16', '0886002', 'SIT Proflame valve 0886 series, gas type NG. Pressure outlet 3.5-1.6\" W.C. Pilot connection 7/16 24UNS. Inlet 3/8\" NPT', NULL, '143.75', 0, '1', NULL, '143.75', NULL, NULL, '2018-10-04 09:54:49', '2018-10-04 11:45:43', 25, 25, 0),
(41, 'CONVERSION KIT (NG 3.5', 'conversion-kit-ng-35-16', '0907201', 'CONVERSION KIT Natural Gas, Pressure outlet 3.5-1.6\" W.C.', NULL, '40.54', 0, '2', NULL, '40.54', NULL, NULL, '2018-10-04 09:59:57', '2018-10-04 11:46:03', 25, 25, 0),
(42, 'CONVERSION KIT (LPG 10', 'conversion-kit-lpg-10-64', '0907202', 'CONVERSION KIT Propane Gas, Pressure Outlet  10\"-6.4\" W.C.', NULL, '40.54', 0, '2', NULL, '40.54', NULL, NULL, '2018-10-04 10:12:07', '2018-10-04 11:46:22', 25, 25, 0),
(43, 'ELECTRODE WITH CABLE', 'electrode-with-cable', '0915075', 'ELECTRODE WITH CABLE, TARGET TYPE, LENGTH 600 (mm) Diameter 2.4 (mm)', NULL, '12.90', 0, '10', NULL, '12.90', NULL, NULL, '2018-10-04 10:19:03', '2018-10-04 11:46:37', 25, 25, 0),
(44, 'ELECTRODE WITH CABLE', 'electrode-with-cable-1', '0915076', 'ELECTRODE WITH CABLE, TARGET TYPE, LENGTH 860 (mm) Diameter 2.4 (mm)', NULL, '12.90', 0, '10', NULL, '12.90', NULL, NULL, '2018-10-04 10:22:14', '2018-10-04 11:46:58', 25, 25, 0),
(45, 'THERMOGENERATOR', 'thermogenerator', '0940002', 'THERMOGENERATOR, LENGTH 580 (MM)', NULL, '39.75', 0, '10', NULL, '39.75', NULL, NULL, '2018-10-04 10:24:23', '2018-10-04 11:47:16', 25, 25, 0),
(46, 'NUT AND OLIVE 3/16', 'nut-and-olive-316', '0958048', 'NUT AND OLIVE 3/16, LENGTH 15 (mm) DIAMETER 3/16\" (mm) CONNECTION 7/16UNS', NULL, '2.40', 0, '10', NULL, '2.40', NULL, NULL, '2018-10-04 10:25:44', '2018-10-04 11:47:33', 25, 25, 0),
(47, '3 FL. CONVERTIBLE HOOD', '3-fl-convertible-hood', '0975063', '3 FLAME CONVERTIBLE HOOD', NULL, '18.45', 0, '10', NULL, '18.45', NULL, NULL, '2018-10-04 10:26:52', '2018-10-04 11:47:49', 25, 25, 0),
(48, '4 FL. CONVERTIBLE HOOD', '4-fl-convertible-hood', '0975065', '4 FLAME CONVERTIBLE HOOD', NULL, '18.45', 0, '10', NULL, '18.45', NULL, NULL, '2018-10-04 10:28:02', '2018-10-04 11:48:07', 25, 25, 0),
(49, 'INJECTOR CONVERT.NG(51)', 'injector-convertng51', '0977165', 'INJECTOR CONVERTION NATURAL GAS ORIFICE # 51', NULL, '7.88', 0, '10', NULL, '7.88', NULL, NULL, '2018-10-04 10:28:59', '2018-10-04 11:48:23', 25, 25, 0),
(50, 'INJECTOR CONVERT.NG(62)', 'injector-convertng62', '0977166', 'INJECTOR CONVERTION NATURAL GAS ORIFICE # 62', NULL, '7.88', 0, '10', NULL, '7.88', NULL, NULL, '2018-10-04 10:30:02', '2018-10-04 11:48:38', 25, 25, 0),
(51, 'INJECTOR CONVERT.LPG(30)', 'injector-convertlpg30', '0977167', 'INJECTOR CONVERTION PROPANE GAS ORIFICE # 30', NULL, '7.88', 0, '10', NULL, '7.88', NULL, NULL, '2018-10-04 10:33:39', '2018-10-04 11:48:54', 25, 25, 0),
(52, 'INJECTOR CONVERT.LPG(35)', 'injector-convertlpg35', '0977168', 'INJECTOR CONVERTION PROPANE GAS ORIFICE # 35', NULL, '7.88', 0, '10', NULL, '7.88', NULL, NULL, '2018-10-04 10:37:44', '2018-10-04 11:49:13', 25, 25, 0),
(53, 'Bracket Piezo', 'bracket-piezo', '0978099', 'Bracket Piezo (Support for piezo igniter)', NULL, '1.14', 0, '20', NULL, '1.14', NULL, NULL, '2018-10-04 10:40:32', '2018-10-04 11:49:28', 25, 25, 0),
(54, 'TUBO PILOTA 24', 'tubo-pilota-24-usa-71', '2182280', 'TUBO PILOTA 24\" USA 7/1', NULL, '16.80', 0, '10', NULL, '16.80', NULL, NULL, '2018-10-04 10:43:48', '2018-10-04 11:49:44', 25, 25, 0),
(55, 'test', 'test', '123', 'test', 'test', '150', 0, '1', NULL, '200', NULL, NULL, '2018-10-16 13:52:02', '2018-10-17 06:50:43', 25, 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products_cat_link`
--

CREATE TABLE `products_cat_link` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_cat_link`
--

INSERT INTO `products_cat_link` (`id`, `product_id`, `cat_id`) VALUES
(58, 2, 30),
(59, 3, 30),
(60, 4, 30),
(61, 5, 30),
(62, 6, 30),
(63, 7, 30),
(64, 8, 30),
(65, 9, 30),
(66, 10, 30),
(67, 11, 30),
(68, 12, 30),
(69, 13, 30),
(70, 14, 30),
(71, 15, 30),
(72, 16, 30),
(73, 17, 30),
(74, 19, 30),
(75, 20, 30),
(76, 21, 30),
(77, 22, 30),
(78, 23, 30),
(80, 25, 30),
(81, 26, 30),
(83, 27, 30),
(84, 28, 30),
(85, 29, 30),
(86, 30, 30),
(87, 31, 30),
(88, 32, 30),
(89, 33, 30),
(90, 34, 30),
(91, 35, 30),
(92, 36, 30),
(93, 37, 30),
(94, 38, 30),
(95, 39, 30),
(96, 40, 30),
(97, 41, 30),
(98, 42, 30),
(99, 43, 30),
(100, 44, 30),
(101, 45, 30),
(102, 46, 30),
(103, 47, 30),
(104, 48, 30),
(105, 49, 30),
(106, 50, 30),
(107, 51, 30),
(108, 52, 30),
(109, 53, 30),
(110, 54, 30),
(117, 1, 30),
(121, 55, 47),
(127, 24, 30);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `author`, `body`, `created_at`) VALUES
(1, 'Anthony Hopkins', '<p>&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '2018-09-03 06:36:33'),
(2, 'Charles Xavior', '<p>&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2018-09-03 06:41:02'),
(3, 'Fernando G', '<p>&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2018-09-03 06:41:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address_1` varchar(50) DEFAULT NULL,
  `address_2` varchar(50) DEFAULT NULL,
  `address_3` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `postcode` varchar(30) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `phone_verified` tinyint(1) DEFAULT '0',
  `user_type` tinyint(1) NOT NULL DEFAULT '0',
  `gender` enum('Male','Female','Trans') DEFAULT NULL,
  `biodata` varchar(255) DEFAULT NULL,
  `dateofbirth` varchar(255) DEFAULT NULL,
  `profileImage` varchar(255) DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `accessToken` varchar(100) DEFAULT NULL,
  `IsLogin` tinyint(1) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT '0',
  `fulladdress` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `address_1`, `address_2`, `address_3`, `city`, `postcode`, `country`, `state`, `phone_verified`, `user_type`, `gender`, `biodata`, `dateofbirth`, `profileImage`, `step`, `accessToken`, `IsLogin`, `latitude`, `longitude`, `vendor_id`, `fulladdress`) VALUES
(11, NULL, 'Test@gmail.com', '$2y$08$9xYjA.ZHgOMKi925j1iD9.LZsm5fOrm8UJ/743nhWv0epBRKD9Ec2', NULL, 'Test@gmail.com', NULL, NULL, NULL, NULL, 1524849201, 1524849231, 1, 'Test', NULL, NULL, '+923332052567', 'Test', 'Test', NULL, 'Karachi', 'Test', 'Pakistan', 'Sindh', 1, 0, 'Male', 'Testing', '2018-04-27', '6b79447842bb7e8eb7377eed2786a94a10.jpg', 2, 'rbX2mrGe9b5JZQ1', 1, '24.8739836', '67.06739689999999', 0, NULL),
(12, NULL, 'Charlestsmith88118@gmail.com', '$2y$08$vaMz6Qes0NDH4ROc84.IEO7FfOSpqPjhkiLGX0kUWlJ3iRyRXJZiK', NULL, 'Charlestsmith88118@gmail.com', NULL, NULL, NULL, NULL, 1525086950, 1534331762, 1, 'Charles', NULL, NULL, '+923082840133', 'Abc', 'Abc', NULL, 'Houston', 'Tyu', 'United States', 'Texas', 1, 0, 'Male', 'A', '2018-04-30', '56430baf37ae3575b454193e1cbb1f7a93.jpg', 2, '', 0, '29.760426700000004', '-95.3698028', 0, NULL),
(13, NULL, 'Test123@gmail.com', '$2y$08$sD38og5664UAxOw/ouGPhubXIFuK8fIRipbhUjjk8bNqs5Zi5XJI6', NULL, 'Test123@gmail.com', NULL, NULL, NULL, NULL, 1525090498, 1525093736, 1, 'Testing', NULL, NULL, '+923332052567', 'Test', 'Test', NULL, 'Cali', 'Test', 'United Arab Emirates', 'Abu Dhabi', 1, 0, 'Male', 'Testing account', '2018-04-30', '3dbb0540ed42f8ce46cad2bcc771afd553.jpg', 2, 'Napsl7SMZ7EoTw6', 1, '24.0913522', '54.1230348', 0, NULL),
(16, '::1', 'charlestsmith8818@gmail.com', '$2y$08$XhP.g.l7yhtfG3hBDeuSvu8D.lyWMzIKORNYqD685sQjBZq.Krqb6', NULL, 'charlestsmith8818@gmail.com', NULL, NULL, NULL, NULL, 1525858962, 1527077659, 1, 'Charles1', 'Smith', '', '323434', '', '', NULL, '', '', 'United States', 'FL', 1, 1, 'Male', 'sdfasfsffafdsfasfsd', '2018-04-30', 'tech-step-icon61.png', NULL, NULL, NULL, NULL, NULL, 0, NULL),
(19, '::1', 'vendor@test2.co', '$2y$08$lzxdfkIt6qq3GakYK/c5DOtn.a6IL7BqELzAIz5wabfFlV179jqXi', NULL, 'vendor@test2.co', NULL, NULL, NULL, NULL, 1525861491, NULL, 1, 'Vendor2', 'Test', '', '0468484', 'sdfdf564', 'Tariq Road', NULL, 'Karachi', '074489', 'Pakistan', 'Sindh', 0, 0, 'Male', NULL, '1991-11-02', 'tech-step-icon73.png', NULL, NULL, NULL, NULL, NULL, 0, NULL),
(25, '107.170.166.118', 'charlestsmith888@gmail.com', '$2y$08$97hMwsP4ZCM23gJJPgg2sePWK2/OHXnFsGsZiNIyZ3y5AzkeeoDTu', NULL, 'charlestsmith888@gmail.com', NULL, NULL, NULL, NULL, 1526382888, 1539784110, 1, 'Super', 'Admin', '', '00000000000', '', '', NULL, '', '', '', '', 0, 0, 'Male', NULL, '1994-06-16', 'tech-step-icon74.png', NULL, NULL, NULL, NULL, NULL, 0, NULL),
(27, '110.93.244.176', 'sykelevez@mailinator.net', '$2y$08$UR0LF1Q4YrK7TUHc1DFC2.thf2ZqKFkC4Ei8ET1wYApfJYAIB5Csm', NULL, 'sykelevez@mailinator.net', NULL, NULL, NULL, NULL, 1526468676, NULL, 1, 'Aurora', 'Burt', '', '96', '573', 'Et sint sunt aut aliquam reprehenderit earum iste ', NULL, 'Ut maxime ab aute ipsa et nostrud saepe', '98', 'Sed dignissimos perferendis voluptatem deserunt sint vitae q', 'Minima lorem dignissimos iste nostrud sint soluta ', 0, 0, '', NULL, '1981-03-03', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(28, '110.93.244.176', 'nagiwady@mailinator.net', '$2y$08$Dm60sn2lpiEe5iCDz3jeXedl8cNA4pOFGanCQOX0w9T6Y/6KtA8gm', NULL, 'nagiwady@mailinator.net', NULL, NULL, NULL, NULL, 1526469623, NULL, 1, 'Vielka', 'Paul', '', '89', '573', 'Pariatur Aliqua Illo incidunt corrupti tempore off', NULL, 'Eos voluptatem modi nihil aut eu molestiae hic fac', '56', 'Explicabo Cillum vel Nam vitae vel maxime est nihil nostrum ', 'Quod ullamco eum ea quia culpa ea dignissimos volu', 0, 0, 'Female', NULL, '1996-06-16', 'Selena-Marie-Gomez-HD-Wallpaper.jpg', NULL, NULL, NULL, NULL, NULL, 0, NULL),
(39, '103.82.122.47', 'charlestsmith999@gmail.com', '$2y$08$W4Yb8WEcdXnCvLRjTL/fVuSuZ28IOUW60R3Te/69arsWwDJm3NlaW', NULL, 'charlestsmith999@gmail.com', NULL, NULL, NULL, NULL, 1527529569, NULL, 1, 'Charles', 'smith', '', '001100000', '434433', 'test', NULL, 'test', '', 'United States', 'CA', 0, 0, 'Male', NULL, '2000-02-09', 'shutterstock_1830870082.jpg', NULL, NULL, NULL, NULL, NULL, 0, NULL),
(65, '111.88.111.252', 'vinmichael63@gmail.com', '$2y$08$y.1o39dIcwDCwx/VPGOCn.Ovq8lfiTDQbQtzDyRAxNqihkchlXnOS', NULL, 'vinmichael63@gmail.com', NULL, NULL, NULL, NULL, 1539263483, 1539264492, 1, 'Vin', 'Michael', NULL, NULL, 'Jackson street, Dallas - TX', NULL, NULL, 'Dallas', '23423', 'United States', 'TX', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(67, '110.93.214.36', 'jamesphilippp407@gmail.com', '$2y$08$KTxkkd/sHOnKS3mpXA.WI.uA3A5xnum.z3.vudqGMMFEgHStsbjP6', NULL, 'jamesphilippp407@gmail.com', NULL, NULL, NULL, NULL, 1539675092, 1539756192, 1, 'James', 'Philippp', '', '364636', 'ggsdg', '', NULL, 'gdfgdg', 'fgdfg', 'AL', 'gdgdf', 0, 0, 'Male', NULL, '', 'photo-1519940616396-2bb690a63a0b3.jpg', NULL, NULL, NULL, NULL, NULL, 0, NULL),
(68, '::1', 'dennyhopkin@gmail.com', '$2y$08$vPw0MXcG4Ikq43XKDPSyeumyuQA7.kPdDGXYlKxQjfltCref6TqDu', NULL, 'dennyhopkin@gmail.com', NULL, NULL, NULL, NULL, 1539775964, 1539794933, 1, 'lotihit', 'cefelix', NULL, NULL, '26 Hague Extension', NULL, NULL, 'Cillum quia nemo excepturi sint est fugiat velit', 'Alias impedit irure voluptatem', 'Tenetur magna dolor exercitation aliquam illum consequuntur ', 'Consequat Ex harum duis dolor ea explicabo Volupta', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(42, 16, 4),
(45, 19, 4),
(52, 25, 1),
(53, 25, 2),
(51, 25, 5),
(55, 27, 4),
(56, 28, 4),
(67, 39, 4),
(95, 65, 2),
(97, 67, 2),
(98, 68, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_permissions`
--

INSERT INTO `users_permissions` (`id`, `user_id`, `perm_id`, `value`, `created_at`, `updated_at`) VALUES
(34, 1, 1, 1, 1519224950, 1519224950),
(35, 1, 2, 1, 1519224950, 1519224950);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes_values`
--
ALTER TABLE `attributes_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brandings`
--
ALTER TABLE `brandings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_settings`
--
ALTER TABLE `cart_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roleID_2` (`group_id`,`perm_id`);

--
-- Indexes for table `homepagebanner`
--
ALTER TABLE `homepagebanner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage_elements`
--
ALTER TABLE `homepage_elements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `link_attributes_products`
--
ALTER TABLE `link_attributes_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`OrderDetailID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderID`);

--
-- Indexes for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permKey` (`perm_key`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_cat_link`
--
ALTER TABLE `products_cat_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userID` (`user_id`,`perm_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attributes_values`
--
ALTER TABLE `attributes_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `brandings`
--
ALTER TABLE `brandings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart_settings`
--
ALTER TABLE `cart_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=691;

--
-- AUTO_INCREMENT for table `homepagebanner`
--
ALTER TABLE `homepagebanner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `homepage_elements`
--
ALTER TABLE `homepage_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `link_attributes_products`
--
ALTER TABLE `link_attributes_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `OrderDetailID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `order_payments`
--
ALTER TABLE `order_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `products_cat_link`
--
ALTER TABLE `products_cat_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
