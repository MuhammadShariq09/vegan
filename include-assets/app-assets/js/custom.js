baseURL=$('meta[name=baseURL]').attr("content");
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview_image img').attr('src', e.target.result);
            $('#base64_cat_image').val(e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$('#all_attrs').on('change',function(){

    var id = $(this).val();
    $select = $('#all_attr_values');
    $('#all_attr_values').html('');
    $.ajax({
        type: "POST",
        url: baseURL+"products/load_attr_values",
        data: { id : id },
        dataType: "json",
        cache : false,
        success: function(data){
            $('#all_attr_values_wrapper').fadeIn();
            $.each(data, function(i, alldata) {
                $('#all_attr_values').append($('<option id="attr_value_id_'+alldata.id+'" value="'+alldata.id+'" data-value-name="'+alldata.name+'">'+alldata.name+'</option>'));
            });
        } ,error: function(xhr, status, error) {
            alert(error);
        }
    });
});

$('#add_attr').on('click', function(){
    if(!$(this).hasClass('disabled')){
        var attr_id = $('#all_attrs').val();
        var attr_name = $('#all_attrs').find(':selected').attr('data-attr-name');
        var error = false;
        var html ='';
        if($('#tbl_attr_id_'+attr_id).length === 0) {
            html += "<table class='table table-responsive table-bordered' id='tbl_attr_id_" + attr_id + "'>";
            html +="<tr><td colspan=\"3\" class=\"text-center\">"+attr_name+"</td> </tr>";
        }else{
            $('#tbl_attr_id_'+attr_id).html('');
            html +="<tr><td colspan=\"3\" class=\"text-center\">"+attr_name+"</td> </tr>";
        }


        $('#all_attr_values > option:selected').each(function() {

            if($('#val_id_'+$(this).val()).length === 0){
                html+='<tr id="val_id_'+$(this).val()+'">\n' +
                    ' <input type="hidden" name="attr_id[]" value="'+attr_id+'">    <input type="hidden" name="attr_value_id[]" value="'+$(this).val()+'">                        <td style="vertical-align: middle;">'+$(this).text()+'</td>\n' +
                    '                                                        <td><input type="number" class="form-control" name="attr_price[]" value="0" placeholder="Price"></td>\n' +
                    '                                                        <td><input type="file" class="form-control" name="attr_image[]"></td>\n' +
                    '                                                    </tr>\n';
                error = false;
            }else{
                //$('#val_id_'+$(this).val()).remove();
                html+='';
                error = true;
            }
        });

        //html += "</table>";
        if(error == false){
            if($('#tbl_attr_id_'+attr_id).length === 0) {
                $('.attribute_form').append(html);
            }else{
                $('.attribute_form #tbl_attr_id_'+attr_id).append(html);
            }
        }
    }


});

$(document).ready(function()
{
    var datepicker = $('.datepickerr');
    if (datepicker.length > 0) {
        datepicker.datepicker({
            format: "yyyy-mm-dd",
            startDate: new Date()
        });
    }
    //PRODUCTS

    $('#schedule').on('click', function(){
        $("#schedule_date").toggle();
    });
    $('#stock_management').on('ifChecked', function (event){
        $('#stock_qty_wrapper').css('display', 'block');
    });
    $('#stock_management').on('ifUnchecked', function (event) {
        $('#stock_qty_wrapper').css('display', 'none');
    });

    var typingTimer;                //timer identifier
    //var doneTypingInterval = 3000;  //time in ms (3 seconds)
    var doneTypingInterval = 3000;  //time in ms (3 seconds)

    $("#product_name").on('keyup', function () {
        clearTimeout(typingTimer);
        $('#slug_loading').fadeIn();
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    function doneTyping () {

        if($("#product_id").val() == '' || $("#product_id").val() == '0'){
            var dataString = {
                product_name : $("#product_name").val(),
                product_id : $("#product_id").val()
            };
            $.ajax({
                type: "POST",
                url: baseURL+"products/place_slug",
                data: dataString,
                dataType: "json",
                cache : false,
                success: function(data){
                    $("#product_slug").val(data);
                   // $('#slug_loading').fadeOut();
                } ,error: function(xhr, status, error) {
                    $("#product_slug").val('');
                    //$('#slug_loading').fadeOut();
                },
            });
        }
        $('#slug_loading').fadeOut();
    }

    //PRODUCTS
    //CATOGERIES
    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1,
        maxDepth: 3
    }).on('change', updateOutput);

    // activate Nestable for list 2

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));

    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });

    $('#updateCategoryOrder').on('click', function(){
        swal({
            title: "Confirm",
            text: "Are you sure you want to update categories order?",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
                },
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if(isConfirm){
                $('#updateCategoryStatus').removeClass('alert-danger');
                $('#updateCategoryStatus').removeClass('alert-success');

                var dataString = {
                    data : $("#nestable-output").val()
                };
                //alert($('#nestable-output').val());
                $.ajax({
                    type: "POST",
                    url: baseURL+"categories/update_categories",
                    data: dataString,
                    dataType: "json",
                    cache : false,
                    success: function(data){
                        if(data.success == 1){
                            //$('#updateCategoryStatus').html(data.msg);
                            $('#updateCategoryStatus').addClass('alert-success');
                            $('#updateCategoryStatus #statusMessage').html(data.msg);
                            $('#updateCategoryStatus').fadeIn();
                            swal("Success!", "Category reordered successfully!", "success");
                        }else{
                            $('#updateCategoryStatus').addClass('alert-danger');
                            $('#updateCategoryStatus #statusMessage').html(data.msg);
                            $('#updateCategoryStatus').fadeIn();
                            swal("Error!", data.msg, "error");
                        }

                        //alert(data.msg);
                        //alert('Data has been saved');
                        //console.log('aa: ' + data);

                    } ,error: function(xhr, status, error) {
                        alert('Error');
                    },
                });

            } else {
                swal("Cancelled", "Nothing change!.", "error");
    }
    });

    });


    $("#name").on('keyup', function () {
        if($("#id").val() == '' || $("#id").val() == '0'){
            var dataString = {
                name : $("#name").val(),
                id : $("#id").val()
            };
            $.ajax({
                type: "POST",
                url: baseURL+"categories/place_slug",
                data: dataString,
                dataType: "json",
                cache : false,
                success: function(data){
                    $("#slug").val(data);
                } ,error: function(xhr, status, error) {
                    $("#slug").val('')
                },
            });
        }

    });

    $("#submit").click(function(){
        var dataString = {
            name : $("#name").val(),
            slug : $("#slug").val(),
            base64_cat_image : $("#base64_cat_image").val(),
            id : $("#id").val()
        };

        $.ajax({
            type: "POST",
            url: baseURL+"categories/save_category",
            data: dataString,
            dataType: "json",
            cache : false,
            success: function(data){
                $('#updateCategoryStatus').removeClass('alert-danger');
                $('#updateCategoryStatus').removeClass('alert-success');
                $('#updateCategoryStatus').fadeIn();
                if(data.type == 'add'){
                    if(data.success == '1') {
                        if ($('#nestable .dd-list').length > 0) {
                            $("#nestable .dd-list:first-child").append(data.menu);
                            $('#updateCategoryStatus').addClass('alert-success');
                            $('#updateCategoryStatus #statusMessage').html(data.msg);
                            $('#name').val('');
                            $('#slug').val('');
                            $('#id').val('');
                            $('#cat_image').val('');
                            $('#base64_cat_image').val('');
                            $("#preview_image img").attr("src", '');
                        } else {
                            $('#no_cat_found').remove();
                            $("#nestable").append('<ol class="dd-list">' + data.menu + '</ol>');
                        }
                    }else if(data.error == '1'){
                        $('#updateCategoryStatus').addClass('alert-danger');
                        $('#updateCategoryStatus #statusMessage').html(data.form_error);
                    }
                } else if(data.type == 'edit') {
                    if (data.success == '1') {
                        $('#name_show' + data.id).html(data.name);
                        $('#slug_show' + data.id).html(data.slug);
                        $('#edit_button' + data.id).attr('data-name', data.name);
                        $('#edit_button' + data.id).attr('data-slug', data.slug);
                        if(data.image !=''){
                            $('#edit_button' + data.id).attr('data-image', data.image);
                        }

                        $('#updateCategoryStatus').addClass('alert-success');
                        $('#updateCategoryStatus #statusMessage').html(data.msg);
                        $('#name').val('');
                        $('#slug').val('');
                        $('#id').val('');
                        $('#cat_image').val('');
                        $('#base64_cat_image').val('');
                        $("#preview_image img").attr("src", '');
                    } else if (data.error == '1') {
                        $('#updateCategoryStatus').addClass('alert-danger');
                        $('#updateCategoryStatus #statusMessage').html(data.form_error);
                        //$('#updateCategoryStatus').html(data.form_error);
                    }

                }else if(data.error == '1'){
                    $('#updateCategoryStatus').addClass('alert-danger');
                    $('#updateCategoryStatus #statusMessage').html(data.msg);
                    //$('#updateCategoryStatus').html(data.form_error);
                }
            } ,error: function(xhr, status, error) {
                alert(error);
            },
        });
    });

    $(document).on("click",".edit-button",function() {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        var slug = $(this).attr('data-slug');
        var image = $(this).attr('data-image');
        $("#id").val(id);
        $("#name").val(name);
        $("#slug").val(slug);
        $("#preview_image img").attr("src", image);
    });

    $(document).on("click",".cat_status",function() {

        var id = $(this).attr('data-id');
        var thiss = $(this);
        var dataString = {
            id : id
        };
        $('#updateCategoryStatus').removeClass('alert-danger');
        $('#updateCategoryStatus').removeClass('alert-success');
        $.ajax({
            type: "POST",
            url: baseURL+"categories/save_category_status",
            data: dataString,
            dataType: "json",
            cache : false,
            success: function(data){
                //console.log(data);
                if(data.success == '1'){
                    $('#updateCategoryStatus').addClass('alert-success');
                    $('#updateCategoryStatus #statusMessage').html(data.msg);
                    $('#updateCategoryStatus').fadeIn();
                    $('#cat_status_'+id).html(data.status);

                }else if(data.success == '0'){
                    $('#updateCategoryStatus').addClass('alert-danger');
                    $('#updateCategoryStatus #statusMessage').html(data.msg);
                    $('#updateCategoryStatus').fadeIn();
                }else{
                    $('#updateCategoryStatus').addClass('alert-danger');
                    $('#updateCategoryStatus #statusMessage').html(data.msg);
                    $('#updateCategoryStatus').fadeIn();
                }
            } ,error: function(xhr, status, error) {
                alert(error);
            },
        });
        /*if(thiss.hasClass('cat_make_active')){
            $('#cat_status_'+id).removeClass('cat_make_active');
            $('#cat_status_'+id).addClass('cat_make_deactive');
            $('#cat_status_'+id).html('Active');
            //alert('Active');
        }

        if(thiss.hasClass('cat_make_deactive')){
            $('#cat_status_'+id).removeClass('cat_make_deactive');
            $('#cat_status_'+id).addClass('cat_make_active');
            $('#cat_status_'+id).html('Deactive');
            //alert('Deative');
        }*/

    });

    $(document).on("click",".del-button",function() {

        /* var x = confirm('Delete this menu?');
         var id = $(this).data('id');
         if(x){
             $.ajax({
                 type: "POST",
                 url: "<?php echo base_url();?>categories/category_delete",
                 data: { id : id },
                 dataType: "json",
                 cache : false,
                 success: function(data){
                     $('#updateCategoryStatus').removeClass('alert-danger');
                     $('#updateCategoryStatus').removeClass('alert-success');
                     $('#updateCategoryStatus').addClass('alert-success');
                    // if(data.success == '1'){
                         $('#updateCategoryStatus').fadeIn();
                     //}
                     $("#load").hide();
                     $("li[data-id='" + id +"']").remove();
                     $('#updateCategoryStatus #statusMessage').html(data.msg);
                     if(!$("#nestable .dd-list .dd-item").length > 0){
                         $('#nestable').html('<div class="text-center" id="no_cat_found"><b>No Category Found!</b></div>');
                     }
                 } ,error: function(xhr, status, error) {
                     alert(error);
                 },
             });
         }*/
        var id = $(this).data('id');
        swal({
            title: "Delete",
            text: "Are you sure you want to delete category?",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
                },
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if(isConfirm){
                $.ajax({
                    type: "POST",
                    url: baseURL+"categories/category_delete",
                    data: { id : id },
                    dataType: "json",
                    cache : false,
                    success: function(data){
                        if(data.success == 1){
                            $('#updateCategoryStatus').removeClass('alert-danger');
                            $('#updateCategoryStatus').removeClass('alert-success');
                            $('#updateCategoryStatus').addClass('alert-success');
                            if(data.success == '1'){
                                $('#updateCategoryStatus').fadeIn();
                            }
                            $("#load").hide();
                            $("li[data-id='" + id +"']").remove();
                            $('#updateCategoryStatus #statusMessage').html(data.msg);
                            if(!$("#nestable .dd-list .dd-item").length > 0){
                                $('#nestable').html('<div class="text-center" id="no_cat_found"><b>No Category Found!</b></div>');
                            }
                            swal("Success!", "Category deleted successfully!", "success");
                        }else{
                            $('#updateCategoryStatus').addClass('alert-danger');
                            $('#updateCategoryStatus #statusMessage').html(data.msg);
                            $('#updateCategoryStatus').fadeIn();
                            swal("Error!", data.msg, "error");
                        }

                    } ,error: function(xhr, status, error) {
                        alert(error);
                    },
                });

            } else {
                swal("Cancelled", "It's safe.", "error");
    }
    });
    });
    $(document).on("click","#reset",function() {
        $('#name').val('');
        $('#slug').val('');
        $('#id').val('');
        $('#cat_image').val('');
        $('#base64_cat_image').val('');
        /*$("#preview_image").css("display", "none");*/
        $("#preview_image img").attr("src", '');
    });



    //END CATEGORIES
    //$('#nestable-output').areYouSure();




});