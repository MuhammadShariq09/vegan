try {
  CKEDITOR.replace('description')
} catch {}

$(document).on('change', '#blog_title', function() {
  $.ajax({
    url: base_url + 'backend/settings/place_slug',
    data: {
      name: $(this).val()
    },
    type: 'post',
    success: function(response) {
      $("#blog_slug").val(response);
    }
  })
})


if (window.location.href.indexOf("blog") > -1) {
  $uploadCrop = $('#upload-demo-blog').croppie({
    enableExif: true,
    viewport: {
      width: 640,
      height: 391,
    },
    boundary: {
      width: 700,
      height: 420
    }
  });
}


$('#blog_image').on('change', function() {
  if (window.location.href.indexOf("blog") > -1) {
      var fileName = $(this).val();
      var ext = (/[^.]+$/.exec(fileName))[0];
      if (fileName == "") {
        toastr.error("Browse to upload a valid File with png extension","Warning!")
        return false;
      } 
      else if (ext.toUpperCase() == "PNG" || ext.toUpperCase() == "JPG" || ext.toUpperCase() == "JPEG" || ext.toUpperCase() == "BMP") {
        var reader = new FileReader();
        reader.onload = function(e) {
          $uploadCrop.croppie('bind', {
            url: e.target.result
          }).then(function() {
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
    
      } 
      else {
        toastr.error("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png, jpeg, jpg extensions","Warning!")
    //    alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png, jpeg, jpg extensions");
        $(this).val("");
        return false;
      }
  }
});




$('#save_blog').on('click', function(ev) {
  if (Number($("#blog_id").val()) == 0) {
    if ($("#blog_image").val() != '') {
      $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'original'
      }).then(function(resp) {
        $('#pi_b64').val(resp);
        $('form').submit();
      });
    }
  } else {
    if ($("#blog_image").val() != '') {
      $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'original'
      }).then(function(resp) {
        $('#pi_b64').val(resp);
        $('form').submit();
      });
    } else
      $('form').submit();
  }
});