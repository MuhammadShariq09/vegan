try {
  CKEDITOR.replace('description')
} catch {}

$(document).ready(function() {
  if (window.location.href.indexOf("catego") > -1) {
    $uploadCrop = $('#upload-demo-cat').croppie({
      enableExif: true,
      viewport: {
        width: 350,
        height: 280,
      },
      boundary: {
        width: 400,
        height: 400
      }
    });
  }

});


$('#image').on('change', function() {
  if (window.location.href.indexOf("catego") > -1) {
      var fileName = $(this).val();
      var ext = (/[^.]+$/.exec(fileName))[0];
      if (fileName == "") {
        toastr.error("Browse to upload a valid File with png extension","Warning!")
        //alert("Browse to upload a valid File with png extension");
        return false;
      } 
      else if (ext.toUpperCase() == "PNG" || ext.toUpperCase() == "JPG" || ext.toUpperCase() == "JPEG" || ext.toUpperCase() == "BMP") {
        var reader = new FileReader();
        reader.onload = function(e) {
          $uploadCrop.croppie('bind', {
            url: e.target.result
          }).then(function() {
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
    
      } 
      else {
        //alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png, jpeg, jpg extensions");
        toastr.error("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png, jpeg, jpg extensions","Warning!")
        $(this).val("");
        return false;
      }
  }
});



$('#submit').on('click', function(ev) {
  if ($("#image").val() != '') {
    $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'original'
    }).then(function(resp) {
      $('#ci_b64').val(resp);
      $('form').submit();
      console.log('here');
    });
  } else {
    $('#catform').submit();
    console.log('here');
  }
});