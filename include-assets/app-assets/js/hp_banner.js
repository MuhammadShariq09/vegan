$("#save_hp_banner").on('click', function() {

  var formData = new FormData($("#hp_banner_form")[0]);
  $.ajax({
    url: base_url + 'backend/settings/add_homepage_banner',
    data: formData,
    type: 'post',
    async: false,
    success: function(response) {
      //alert(response.msg);
      if(response.status == 200)
          toastr.success(response.msg,"Success !")
      else
          toastr.error(response.msg,"Error !")
      getBannerImages();
      clearInputs();
    },
    cache: false,
    contentType: false,
    processData: false
  })
})


$(document).ready(function() {
  getBannerImages();
})

function clearInputs() {
  $("#hp_image_preview").attr("src", '');
  $("#hp_banner_text").val('');
  $("#hp_banner_id").val(0);
  $("#hp_image").val("");
}

$("#cancel_hp_banner").on('click', function() {
  clearInputs();
})

function getBannerImages() {
  $("#hp_bn_res").load(base_url + 'backend/settings/getHomePageBannerDetails');
}

function editHpBanner(id) {
  $.ajax({
    url: base_url + 'backend/settings/getHpBannerInfo/' + id,
    success: function(data) {
      var response = data[0];
      $("#hp_image_preview").attr("src", base_url + 'uploads/hpbanners/' + response.image);
      $("#hp_banner_text").val(response.text);
      $("#hp_banner_id").val(id);
    }
  })
}

function deleteHpBanner(id) {
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure you really want to perform this action ?',
        buttons: {
            Ok: function(){
                  $.ajax({
                    url: base_url + 'backend/settings/deleteHpBanner/' + id,
                    success: function(response) {
                      getBannerImages();
                      clearInputs();
                    }
                  })
            },
            Cancel: function(){
    
            }
        }
    });
}