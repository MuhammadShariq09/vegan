try {
  CKEDITOR.replace('product_description')
} catch {}

$(document).on('change', '#product_name', function() {
  $.ajax({
    url: base_url + 'backend/products/place_slug',
    data: {
      name: $(this).val()
    },
    type: 'post',
    success: function(response) {
      $("#product_slug").val(response);
    }
  })
})

$uploadCrop = $('#upload-demo').croppie({
  enableExif: true,
  viewport: {
    width: 350,
    height: 280,
  },
  boundary: {
    width: 400,
    height: 400
  }
});

function reverseString(str) {
  return str.split("").reverse().join("");
}

$('#product_images').on('change', function() {
  var fileName = $(this).val();
  var ext = (/[^.]+$/.exec(fileName))[0];
  if (fileName == "") {
    toastr.error("Browse to upload a valid File with png extension","Warning!")
    return false;
  } else if (ext.toUpperCase() == "PNG" || ext.toUpperCase() == "JPG" || ext.toUpperCase() == "JPEG" || ext.toUpperCase() == "BMP") {
    var reader = new FileReader();
    reader.onload = function(e) {
      $uploadCrop.croppie('bind', {
        url: e.target.result
      }).then(function() {
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);

  } else {
      toastr.error("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png, jpeg, jpg extensions","Warning!")
      //alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png, jpeg, jpg extensions");
    $(this).val("");
    return false;
  }

});

$('#save_product').on('click', function(ev) {
  if (Number($("#product_id").val()) == 0) {
    if ($("#product_images").val() != '') {
      $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'original'
      }).then(function(resp) {
        $('#pi_b64').val(resp);
        $('form').submit();
      });
    }
  } else {
    if ($("#product_images").val() != '') {
      $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'original'
      }).then(function(resp) {
        $('#pi_b64').val(resp);
        $('form').submit();
      });
    } else
      $('form').submit();
  }
});