
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Attributes</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('backend/dashboard');?>">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('backend/products');?>">Products</a>
                  </li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('backend/products/attributes');?>">Attributes</a>
                  </li>
                <li class="breadcrumb-item active">Edit <?php echo (isset($attribute_id)?$attribute.' Value':'Attribute');?>
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->
            <?php if(isset($message)) { ?>
            <div class="row">
                <div class="col-md-12">
                    <?php $msg = $message; ?>
                    <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php echo $msg['message']; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        <section>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-colored-form-control">Add <?php echo (isset($attribute_id)?$attribute.' Value':'Attribute');?></h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <?php echo form_open();?>
                            <div class="form-body">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Attribute</label>
                                            <input type="text" <?php echo (($type == 'values')?'readonly':'');?> id="attribute" value="<?php echo $attribute;?>" name="attribute" class="form-control border-primary" placeholder="Attribute" value="<?php echo set_value('attribute'); ?>">
                                        </div>
                                    </div>
                                    <input type="hidden" name="attribute_id" value="<?php echo $attribute_id;?>">
                                    <?php if(isset($attribute_value)){?>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Attribute Value</label>
                                            <input type="text" id="attribute_value" name="attribute_value" value="<?php echo $attribute_value;?>" class="form-control border-primary" placeholder="Attribute Value" value="<?php echo set_value('attribute_value'); ?>">
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <input type="hidden" name="attribute_type" value="<?php echo ((isset($attribute_value))?'attribute_value':'attribute');?>">
                            </div>
                            <div class="form-actions right">
                                <?php echo (isset($attribute_id)?anchor(base_url().'backend/products/attributes/'.$attribute_id, 'Cancel','class="btn btn-warning mr-1"'):anchor(base_url().'products/attributes', 'Cancel','class="btn btn-warning mr-1"'));?>
                                <?php echo form_submit(array('type'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary'));?>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

