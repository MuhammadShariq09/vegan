
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Categories</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Categories
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


        <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add/Edit Category</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li> <button id="updateCategoryOrder" class="btn btn-outline-primary ">Update Categories</button></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        <div class="row">
                            <div class="col-md-4">
                                <div id="updateCategoryStatus"></div>

                                <?php echo form_open_multipart('backend/Categories/save_category', ['id'=>'catform']);?>
                                    <div class="form-body">
                                        <?php if(isset($message) &&$message['message']) { ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <?php $msg = $message; ?>
                                                    <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <?php echo $msg['message']; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Category Name</label>
                                                    <input type="text" id="name" name="name" <?php set_value('name'); ?> class="form-control border-primary" placeholder="Category Name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Category Slug</label>
                                                    <input type="text" id="slug" name="slug" value="<?php set_value('slug'); ?>" class="form-control border-primary" placeholder="Category Slug" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Category Description</label>
                                                    <textarea id="description" name="description" class="form-control border-primary" placeholder="Category Description" required><?php //set_value('description'); ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Category Image</label>
                                                    <input type="hidden" id="ci_b64" name="ci_b64">
                                                    <input type="file" id="image" accept='image/*' name="image" class="form-control border-primary" placeholder="Category Image" >
                                                    <br>
                                                    <img src="" id="old_image" style="width:350px; display: none" alt="">
                                                </div>
                                                <div class="form-group">
                                                    <label>Category Status</label>
                                                    <select id="status" name="status" class="form-control border-primary" required>
                                                        <option value="">Select Status</option>
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><strong>Featured Category</strong> &nbsp;</label>
                                                    <input type="checkbox" name="featured" id="featured">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button id="reset" type="button" class="btn btn-secondary mr-1">Reset</button>
                                        <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                                        <!-- <button type="button" class="btn btn-primary" id="submit">Save</button> -->
                                        <input type="hidden" name="id" id="id">
                                    </div>
                                <?php echo form_close();?>
                             </div>
                            <div class="col-md-8">
                                <div class="dd" id="nestable" style="padding: 15px;border: 1px solid">
                                    <?php if($categories){?>
                                        <?php echo $categories;?>
                                    <?php }else{ ?>
                                        <div class="text-center" id="no_cat_found"><b>No Category Found!</b></div>
                                    <?php } ?>
                                </div>
                                <div style="padding-left:20%" class="green-container form-group">
                                  <div class="text-center">
                                       <div id="upload-demo-cat" style="width:350px"></div>
                                  </div>
                                </div>
                            </div>
                       </div>
                      <div id="menu-form">
                        <textarea id="nestable-output" style="display: none;"></textarea>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  $(document).on('change', '#name', function(){
    $.ajax({
      url: '<?=base_url()?>backend/categories/place_slug',
      data: { name: $(this).val()},
      type: 'post',
      success: function(response){
        $("#slug").val(response);
      }
    })
  })
</script>
