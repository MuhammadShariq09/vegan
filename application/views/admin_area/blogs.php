
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Blogs</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Blogs
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


  <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">

              <?php if ($this->session->flashdata('message')): ?>
                <div class="alert <?=$this->session->flashdata('message')['class']?>">
                  <?=$this->session->flashdata('message')['message']?>
                </div>
              <?php endif; ?>

                <div class="card-header">
                    <h4 class="card-title">All Blogs</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'backend/settings/add_blog';?>"><i class="ft-plus"></i> Add Blog</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        <table class="table table-striped table-bordered  default-ordering">
                            <thead>
                                <tr>
                                    <th>Featured</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Created At</th>
                                    <th>Author</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($blogs){?>
                                    <?php foreach ($blogs as $blog){?>
                                        <tr>
                                            <td><input type="checkbox" <?=($blog->isfeatured) ? "checked" : ""?>
                                              class="isFeat" id="is_featured<?=$blog->id?>" data-id="<?=$blog->id?>"
                                              value="<?=$blog->id?>"></td>
                                            <td><?php echo htmlspecialchars($blog->blog_title,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo htmlspecialchars($blog->blog_slug,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo htmlspecialchars($blog->created_at,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo htmlspecialchars($blog->author,ENT_QUOTES,'UTF-8');?></td>
                                            <td>
                                                <a class="badge badge-secondary" href="<?php echo base_url();?>backend/settings/edit_blogs/<?php echo $blog->id; ?>">Edit</a>
                                                <a class="badge badge-danger" href="<?php echo base_url();?>backend/settings/deleteBlogs/<?php echo $blog->id; ?>" id="confirmbtn">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th>Featured</th>
                                  <th>Name</th>
                                  <th>Slug</th>
                                  <th>Created At</th>
                                  <th>Author</th>
                                  <th>Modify</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">

  $('.isFeat').on('click', function(){
    var id = $(this).data("id");
    var state = $('#is_featured'+id).is(":checked");
    $.ajax({
      url: '<?=base_url()?>backend/settings/setBlogFeatured/'+id,
      success: function(response){
        if(response.status == 200)
          toastr.success(response.msg,"Success!")
        else{
  //        alert(response.msg);
          toastr.error(response.msg,"Success!")
          $('#is_featured'+id).prop("checked", !state);
        }
      }
    })
  })


</script>
