
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">ORDERS</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item active">ORDERS
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


        <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        <h4 class="card-title">Order Details</h4>
                        <?php if($this->session->flashdata('message')) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $msg = $this->session->flashdata('message');?>
                                    <div class="alert alert-success alert-dismissible mb-2 ">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <?php echo $msg; ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">

                            <div class="col-sm-6">
                                <h5><b>Billing Details</b></h5>
                                <p><strong>Name:</strong> <?php echo($orders['billing_first_name']." ".$orders['billing_last_name']); ?></p>
                                <p><strong>Email:</strong> <?php echo($orders['billing_email']); ?></p>
                                <p><strong>Address 1:</strong> <?php echo($orders['billing_address_1']); ?></p>
                                <p><strong>Address 2:</strong> <?php echo($orders['billing_address_2']); ?></p>
                                <p><strong>City - State:</strong> <?php echo($orders['billing_city']." ".$orders['billing_state']); ?></p>
                                <p><strong>Country:</strong> <?php echo($orders['billing_country']." ".$orders['billing_postcode']); ?></p>
                                <p><strong>Phone:</strong> <?php echo($orders['billing_phone']); ?></p>
                            </div>
                            <div class="col-sm-6">
                                <h5><b>Shipping Details</b></h5>
                                <p><strong>Name:</strong> <?php echo($orders['shipping_first_name']." ".$orders['shipping_last_name']); ?></p>
                                <p><strong>Email:</strong> <?php echo($orders['shipping_email']); ?></p>
                                <p><strong>Address 1:</strong> <?php echo($orders['shipping_address_1']); ?></p>
                                <p><strong>Address 2:</strong> <?php echo($orders['shipping_address_2']); ?></p>
                                <p><strong>City - State:</strong> <?php echo($orders['shipping_city']." ".$orders['shipping_state']); ?></p>
                                <p><strong>Country:</strong> <?php echo($orders['shipping_country']." ".$orders['shipping_postcode']); ?></p>
                                <p><strong>Phone:</strong> <?php echo($orders['shipping_phone']); ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <form action="<?php echo base_url(); ?>orders/changestatus/<?php echo $id ?>" style="display: flex;width: 100%" method="POST">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <label for="Status">Change Status</label>
                                        <select name="Status" id="Status" class="form-control">
                                            <option value="0" <?php if($orders['Status'] == 0 ){ ?>Selected <?php } ?>>Pending</option>
                                            <option value="1" <?php if($orders['Status'] == 1 ){ ?>Selected <?php } ?>>Processing</option>
                                            <option value="2" <?php if($orders['Status'] == 2 ){ ?>Selected <?php } ?>>On hold</option>
                                            <option value="3" <?php if($orders['Status'] == 3 ){ ?>Selected <?php } ?>>Completed</option>
                                            <option value="4" <?php if($orders['Status'] == 4 ){ ?>Selected <?php } ?>>Cancelled</option>
                                            <option value="5" <?php if($orders['Status'] == 5 ){ ?>Selected <?php } ?>>Refunded</option>
                                            <option value="6" <?php if($orders['Status'] == 6 ){ ?>Selected <?php } ?>>Failed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="Status">&nbsp;</label>
                                        <button class="btn btn-primary form-control">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <h4 class="card-title">Ordered Products</h4>
                        <table class="table table-striped table-bordered default-ordering">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>SKU</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($ordersDetails){?>
                                    <?php foreach ($ordersDetails as $orderproduct){?>
                                        <tr>
                                            <td><?php echo $orderproduct->ProductID; ?></td>
                                            <td><?php echo htmlspecialchars($orderproduct->ProductName,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo $orderproduct->ProductPrice; ?></td>
                                            <td><?php echo htmlspecialchars($orderproduct->ProductSKU,ENT_QUOTES,'UTF-8'); ?></td>
                                            <td><?php echo htmlspecialchars($orderproduct->ProductQuantity,ENT_QUOTES,'UTF-8'); ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>SKU</th>
                                    <th>Quantity</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <br>    
                                <div class="well">
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name"> Sub Total: </div>
                                        <div class="col-md-3 value"> $<?php echo $orders['OrderAmount']; ?> </div>
                                    </div>
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name"> Shipping: </div>
                                        <div class="col-md-3 value"> $<?php echo $orders['OrderShipping']; ?> </div>
                                    </div>
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name"> Coupon Amount: </div>
                                        <div class="col-md-3 value"> $<?php echo $orders['OrderCouponAmount']; ?> </div>
                                    </div>
                                    <div class="row static-info align-reverse">
                                        <div class="col-md-8 name"> Total Amount: </div>
                                        <div class="col-md-3 value"> $<?php echo $orders['OrderTotal']; ?> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

