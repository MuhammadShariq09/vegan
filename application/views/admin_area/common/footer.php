<!--<footer class="footer footer-static footer-light navbar-border">-->
<!--    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 content"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank" class="text-bold-800 grey darken-2">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span></p>-->
<!--</footer>-->
<!-- BEGIN VENDOR JS-->
<script type="text/javascript">
  var base_url = "<?php echo base_url()?>";
</script>
<script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="<?php echo base_url();?>include-assets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/core/app.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/scripts/customizer.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/jquery.nestable.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/jquery.are-you-sure.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/ays-beforeunload-shim.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/scripts/extensions/toastr.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/extensions/toastr.min.js" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url();?>include-assets/app-assets/js/scripts/forms/checkbox-radio.js" type="text/javascript"></script>
<script src="https://ckeditor.com/assets/libs/ckeditor4/4.10.0/ckeditor.js"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/croppie.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>


    try {
      CKEDITOR.replace('body')
    } catch {}
                
// ------------------------------
    $(window).on("load", function(){

        Morris.Donut({
            element: 'products-sales',
            data: [{
                label: "Pending",
                value: "<?=$this->Home_Model->countResult_new('orders',['Status' => 0])?>"
            }, {
                label: "Processing",
                value: "<?=$this->Home_Model->countResult_new('orders',['Status' => 1])?>"
            }, {
                label: "Completed",
                value: "<?=$this->Home_Model->countResult_new('orders',['Status' => 3])?>"
            }],
            resize: true,
            colors: ['#00A5A8', '#FF7D4D', '#FF4558']
        });
    });

    $(document).ready(function()
    {

        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1,
            maxDepth: 3
        }).on('change', updateOutput);

        // activate Nestable for list 2

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $('#nestable-menu').on('click', function(e)
        {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });

        $('#updateCategoryOrder').on('click', function(){
            var dataString = {
                data : $("#nestable-output").val()
            };
            //alert($('#nestable-output').val());
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>backend/categories/update_categories",
                data: dataString,
                cache : false,
                success: function(data){
                    $('#updateCategoryStatus').html('Save successfully!');
                    //alert('Data has been saved');
                    console.log('aa: ' + data);

                } ,error: function(xhr, status, error) {
                    alert(error);
                },
            });
        });


        $("#submit").click(function(){

            /*var dataString = {
                name : $("#name").val(),
                slug : $("#slug").val(),
                status : $("#status").val(),
                id : $("#id").val()
            };*/
        //
        // var dataString = $("form#catform").serializeToJson();
        //
        //     $.ajax({
        //         type: "POST",
        //         url: "<?php echo base_url();?>backend/categories/save_category",
        //         data: dataString,
        //         dataType: "json",
        //         cache : false,
        //         success: function(data){
        //             if(data.type == 'add'){
        //                 if(data.success == '1') {
        //                     if ($('#nestable .dd-list').length > 0) {
        //                         $("#nestable .dd-list:first-child").append(data.menu);
        //                         $('#updateCategoryStatus').html(data.msg);
        //                         $('#name').val('');
        //                         $('#slug').val('');
        //                         $("#status").val('')
        //                         $('#id').val('');
        //                     } else {
        //                         $('#no_cat_found').remove();
        //                         $("#nestable").append('<ol class="dd-list">' + data.menu + '</ol>');
        //                     }
        //                 }else if(data.error == '1'){
        //                     $('#updateCategoryStatus').html(data.form_error);
        //                 }
        //             } else if(data.type == 'edit'){
        //                 if(data.success == '1'){
        //                     console.log(data);
        //                     $('#name_show'+data.id).html(data.name);
        //                     $('#slug_show'+data.id).html(data.slug);
        //                     if(data.status == 1) { var status = "Active"} else { var status = "Deactive"}
        //                     $('#status_show'+data.id).html(status);
        //                     $('#edit_button'+data.id).attr('data-name', data.name);
        //                     $('#edit_button'+data.id).attr('data-slug', data.slug);
        //                     $('#edit_button'+data.id).attr('data-status', data.status);
        //                     $('#updateCategoryStatus').html(data.msg);
        //                     $('#name').val('');
        //                     $('#slug').val('');
        //                     $('#status').val('');
        //                     $('#id').val('');
        //                 }else if(data.error == '1'){
        //                     $('#updateCategoryStatus').html(data.form_error);
        //                 }
        //
        //             }
        //         } ,error: function(xhr, status, error) {
        //             alert(error);
        //         },
        //     });
        });


        $(document).on("click",".edit-button",function() {
            var id = $(this).attr('data-id');
            var dataString = {
                id : id
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>backend/categories/get_category",
                data: dataString,
                dataType: "json",
                cache : false,
                success: function(data){
                    $("#id").val(data.id);
                    $("#name").val(data.name);
                    $("#slug").val(data.slug);
                    $("#description").val(data.description);
                    $("#slug").val(data.slug);
                    if(data.image != ""){
                        image = data.image;
                        $("#old_image").attr('src',image);
                        $("#old_image").show();
                    }else {
                        $("#old_image").attr('src',image);
                        $("#old_image").hide();
                    }

                    $('#status option[value='+data.status+']').attr('selected','selected');
                    if(Number(data.featured))
                      $('#featured').prop("checked",true);
                    else
                      $('#featured').prop("checked",false);
                }

            })

        });
        $("#name").on('keyup', function () {
            if($("#id").val() == '' || $("#id").val() == '0'){
                var dataString = {
                    name : $("#name").val(),
                    id : $("#id").val()
                };
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>backend/categories/place_slug",
                    data: dataString,
                    dataType: "json",
                    cache : false,
                    success: function(data){
                        $("#slug").val(data);
                    } ,error: function(xhr, status, error) {
                        $("#slug").val('')
                    },
                });
            }

        });
        $(document).on("click",".del-button",function() {
            var id = $(this).data('id');
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure you really want to perform this action ?',
                buttons: {
                    Ok: function(){
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url();?>backend/categories/category_delete",
                            data: { id : id },
                            cache : false,
                            success: function(data){

                                $("#load").hide();
                                $("li[data-id='" + id +"']").remove();
                                if(!$("#nestable .dd-list .dd-item").length > 0){
                                    $('#nestable').html('<div class="text-center" id="no_cat_found"><b>No Category Found!</b></div>');
                                }
                            } ,error: function(xhr, status, error) {
                                alert(error);
                            }
                        });
                    },
                    Cancel: function(){

                    }
                }
            });
            //
            //
            // var x = confirm('Delete this menu?');

            // if(x){
            //
            // }
        });
        $(document).on("click","#reset",function() {
            $('#name').val('');
            $('#slug').val('');
            $('#status').val('');
            $('#id').val('');
        });
        //$('#nestable-output').areYouSure();
        $(document).on("click",".cat_status",function() {

            var id = $(this).attr('data-id');
            var thiss = $(this);
            var dataString = {
                id : id
            };
            $('#updateCategoryStatus').removeClass('alert-danger');
            $('#updateCategoryStatus').removeClass('alert-success');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>backend/categories/save_category_status",
                data: dataString,
                dataType: "json",
                cache : false,
                success: function(data){
                    //console.log(data);
                    if(data.success == '1'){
                        $('#updateCategoryStatus').addClass('alert-success');
                        $('#updateCategoryStatus #statusMessage').html(data.msg);
                        $('#updateCategoryStatus').fadeIn();
                        $('#cat_status_'+id).html(data.status);

                    }else if(data.success == '0'){
                        $('#updateCategoryStatus').addClass('alert-danger');
                        $('#updateCategoryStatus #statusMessage').html(data.msg);
                        $('#updateCategoryStatus').fadeIn();
                    }else{
                        $('#updateCategoryStatus').addClass('alert-danger');
                        $('#updateCategoryStatus #statusMessage').html(data.msg);
                        $('#updateCategoryStatus').fadeIn();
                    }
                } ,error: function(xhr, status, error) {
                    alert(error);
                },
            });
            /*if(thiss.hasClass('cat_make_active')){
                $('#cat_status_'+id).removeClass('cat_make_active');
                $('#cat_status_'+id).addClass('cat_make_deactive');
                $('#cat_status_'+id).html('Active');
                //alert('Active');
            }

            if(thiss.hasClass('cat_make_deactive')){
                $('#cat_status_'+id).removeClass('cat_make_deactive');
                $('#cat_status_'+id).addClass('cat_make_active');
                $('#cat_status_'+id).html('Deactive');
                //alert('Deative');
            }*/

        });
    });
</script>


<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script>
    $('#daterangepicker').daterangepicker({
        timePicker: true,
        timePickerIncrement: 1,
        locale: {
            format: 'YYYY-MM-DD:HH:mm:ss'
        }
    });
</script>

<script>
    $('a#confirmbtn, a.confirmbtn').confirm({
        title: 'Confirm!',
        content: 'Are you sure you really want to perform this action ?',
        buttons: {
            Ok: function(){
                location.href = this.$target.attr('href');
            },
            Cancel: function(){

            }
        }
    });
</script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/add_products.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/add_category.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/add_blog.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>include-assets/app-assets/js/hp_banner.js" type="text/javascript"></script>
</body>
</html>
