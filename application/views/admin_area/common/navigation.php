<?php   $cuser = $this->ion_auth->user()->row();
// print_r($cuser);
$url =  $this->uri->segment(1).'/'.$this->uri->segment(2);
$url1 =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3);
$url2 =  $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4);
?>

<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item">
                    <a href="<?php echo base_url() .'dashboard';?>" class="navbar-brand">
                        <h2 style="color: #1175BB;font-family: fantasy;" class="brand-text">
                          <img style="width:18%;height: 40px;" src="<?php echo base_url();?>vegan/assets/images/footer-logo.png" alt="">
                          <text style="border-bottom: 2px solid #e9483a"><?=APP_NAME?></text>
                        </h2>
                    </a>
                </li>
                <li class="nav-item d-md-none"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div id="navbar-mobile" class="collapse navbar-collapse">
                <ul class="nav navbar-nav mr-auto float-left">

                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="user-name"><?php echo $cuser->first_name; ?></span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <?php if($this->ion_auth_acl->has_permission('users_view') || $this->ion_auth->is_admin()){?>
                                    <a href="<?php echo base_url() . 'backend/users';?>"  class="dropdown-item"><i class="fa fa-user"></i><span data-i18n="" class="menu-title">Users</span></a>
                            <?php } ?>

                            <?php if($this->ion_auth_acl->has_permission('products_view') || $this->ion_auth->is_admin()){?>
                                    <a href="<?php echo base_url() . 'backend/products';?>"  class="dropdown-item"><i class="fa fa-product-hunt"></i><span data-i18n="" class="menu-title">Products</span></a>
                            <?php } ?>

                            <?php if($this->ion_auth_acl->has_permission('categories_view') || $this->ion_auth->is_admin()){?>
                                    <a href="<?php echo base_url() . 'backend/categories';?>"  class="dropdown-item"><i class="fa fa-tag"></i><span data-i18n="" class="menu-title">Categories</span></a>
                            <?php } ?>

                            <?php if($this->ion_auth_acl->has_permission('order_view') || $this->ion_auth->is_admin()){?>
                                <a href="<?php echo base_url() . 'backend/orders';?>"  class="dropdown-item"><i class="fa fa-shopping-cart"></i><span data-i18n="" class="menu-title">Orders</span></a>
                            <?php } ?>

                            <div class="dropdown-divider"></div>
                            <a href="<?php echo base_url() . 'auth/logout';?>" class="dropdown-item"><i class="ft-power"></i>Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion">
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class=" navigation-header">
            </li>
            <?php if($this->ion_auth_acl->has_permission('dashboard_view') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item <?= (current_url() == base_url() .'backend/dashboard') ? 'active' : '' ?>">
                    <a href="<?php echo base_url() . 'backend/dashboard';?>"><i class="fa fa-dashboard"></i><span data-i18n="" class="menu-title">Dashboard</span></a>
                </li>
            <?php } ?>

            <?php if($this->ion_auth_acl->has_permission('users_view') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item
                <?= (current_url() == base_url() .'backend/users') ? 'active' : '' ?>
                <?= ($url == 'backend/users') ? 'active' : '' ?>
                ">
                    <a href="<?php echo base_url() . 'backend/users';?>"><i class="fa fa-user"></i><span data-i18n="" class="menu-title">Users</span></a>
                    <ul class="sub-menu">
                        <li class=" nav-item">
                            <a href="<?php echo base_url() . 'backend/users';?>"><i class="fa fa-users"></i><span data-i18n="" class="menu-title">Users</span></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>



            <?php if($this->ion_auth_acl->has_permission('products_view') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item <?= ($url == 'backend/products') ? 'active' : '' ?>">
                    <a href="<?php echo base_url() . 'backend/products';?>"><i class="fa fa-product-hunt"></i><span data-i18n="" class="menu-title">Items</span></a>
                </li>
            <?php } ?>

            <?php if($this->ion_auth_acl->has_permission('categories_view') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item <?= ($url == 'backend/categories') ? 'active' : '' ?>">
                    <a href="<?php echo base_url() . 'backend/categories';?>"><i class="fa fa-tag"></i><span data-i18n="" class="menu-title">Menu</span></a>
                </li>
            <?php } ?>

            <?php if($this->ion_auth_acl->has_permission('orders_view') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item <?= ($url == 'backend/orders') ? 'active' : '' ?>">
                    <a href="<?php echo base_url() . 'backend/orders';?>"><i class="fa fa-shopping-cart"></i><span data-i18n="" class="menu-title">Orders</span></a>
                </li>
            <?php } ?>

            <?php if($this->ion_auth_acl->has_permission('transactions_view') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item <?= ($url == 'backend/transactions') ? 'active' : '' ?>">
                    <a href="<?php echo base_url() . 'backend/transactions';?>"><i class="fa fa-money"></i><span data-i18n="" class="menu-title">Transactions</span></a>
                </li>
            <?php } ?>

            <?php if($this->ion_auth_acl->has_permission('coupon_view') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item <?= ($url == 'backend/coupons') ? 'active' : '' ?>">
                    <a href="<?php echo base_url() . 'backend/coupons';?>"><i class="fa fa-wpforms"></i><span data-i18n="" class="menu-title">Coupons</span></a>
                </li>
            <?php } ?>

            <?php if($this->ion_auth_acl->has_permission('pages_view') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item <?= ($url == 'backend/pages') ? 'active' : '' ?>">
                    <a href="<?php echo base_url() . 'backend/pages';?>"><i class="fa fa-file"></i><span data-i18n="" class="menu-title">Pages</span></a>
                </li>
            <?php } ?>

            <?php if($this->ion_auth_acl->has_permission('settings') || $this->ion_auth->is_admin()){?>
                <li class=" nav-item ">
                    <a href=""><i class="fa fa-cog"></i><span data-i18n="" class="menu-title">Settings</span></a>
                    <ul class="sub-menu">
                        <li class=" nav-item <?= ($url1 == 'backend/settings/brandings') ? 'active' : '' ?>">
                            <a href="<?php echo base_url() . 'backend/settings/brandings';?>"><i class="fa fa-th"></i><span data-i18n="" class="menu-title">Brandings</span></a>
                        </li>
                        <li class=" nav-item <?= ($url1 == 'backend/settings/blogs') ? 'active' : '' ?>">
                            <a href="<?php echo base_url() . 'backend/settings/blogs';?>"><i class="fa fa-th"></i><span data-i18n="" class="menu-title">Blogs</span></a>
                        </li>
                        <li class=" nav-item  <?= ($url1 == 'backend/settings/homepageBanner') ? 'active' : '' ?>">
                            <a href="<?php echo base_url() . 'backend/settings/homepageBanner';?>"><i class="fa fa-image"></i><span data-i18n="" class="menu-title">HomePage Banner</span></a>
                        </li>
                        <li class=" nav-item  <?= ($url1 == 'backend/settings/cartSettings') ? 'active' : '' ?>">
                            <a href="<?php echo base_url() . 'backend/settings/cartSettings';?>"><i class="fa fa-shopping-cart"></i><span data-i18n="" class="menu-title">Cart Settings</span></a>
                        </li>
                        <li class=" nav-item">
                            <a href=""><i class="fa fa-cog"></i><span data-i18n="" class="menu-title">Element Settings</span></a>
                            <ul class="sub-menu">
                                <li class=" nav-item <?= ($url2 == 'backend/settings/homePageSetting/1') ? 'active active1' : '' ?>">
                                  <a href="<?php echo base_url() . 'backend/settings/homePageSetting/1';?>"><i class="fa fa-circle-thin"></i><span data-i18n="" class="menu-title">Section 1</span></a>
                                </li>
                                <li class=" nav-item <?= ($url2 == 'backend/settings/homePageSetting/2') ? 'active active1' : '' ?>">
                                    <a href="<?php echo base_url() . 'backend/settings/homePageSetting/2';?>"><i class="fa fa-circle-thin"></i><span data-i18n="" class="menu-title">Section 2</span></a>
                                </li>
                                <li class=" nav-item <?= ($url2 == 'backend/settings/homePageSetting/3') ? 'active active1' : '' ?>">
                                  <a href="<?php echo base_url() . 'backend/settings/homePageSetting/3';?>"><i class="fa fa-circle-thin"></i><span data-i18n="" class="menu-title">Section 3</span></a>
                                </li>
                                <li class=" nav-item <?= ($url2 == 'backend/settings/homePageSetting/4') ? 'active active1' : '' ?>">
                                  <a href="<?php echo base_url() . 'backend/settings/homePageSetting/4';?>"><i class="fa fa-circle-thin"></i><span data-i18n="" class="menu-title">Section 4</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class=" nav-item <?= ($url1 == 'backend/settings/testimonials') ? 'active' : '' ?>">
                            <a href="<?php echo base_url() . 'backend/settings/testimonials';?>"><i class="fa fa-quote-left"></i><span data-i18n="" class="menu-title">Testimonials</span></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>

        </ul>
    </div>
</div>
