<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>


</script>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">COUPONS</h3>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="ordering">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Add Coupon</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <?php echo form_open_multipart("backend/coupons/add");?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if(isset($message) &&$message['message']) { ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <?php $msg = $message; ?>
                                                    <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <?php echo $msg['message']; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Coupon Name</label>
                                                            <input type="text" id="CouponName" name="CouponName" class="form-control border-primary" value="<?php echo set_value('CouponName'); ?>" placeholder="Coupon Name" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Coupon Code</label>
                                                            <input type="text" id="CouponCode" name="CouponCode" class="form-control border-primary" value="<?php echo set_value('CouponCode'); ?>" placeholder="Coupon Code" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Coupon Percentage</label>
                                                            <input type="number" min="1" max="100" id="CouponPercentage" name="CouponPercentage" class="form-control border-primary" value="<?php echo set_value('CouponPercentage'); ?>" placeholder="Coupon Percentage" >
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>From</label>
                                                                    <input type="date" id="CouponFrom" name="CouponFrom" class="form-control border-primary" value="<?php echo set_value('CouponFrom'); ?>" placeholder="From - To" >
                                                                </div>                                                                
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>To </label>
                                                                    <input type="date" id="CouponTo" name="CouponTo" class="form-control border-primary" value="<?php echo set_value('CouponTo'); ?>" placeholder="From - To" >
                                                                </div>                                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label >Status</label>
                                                            <select name="status" id="status" class="form-control border-primary">
                                                                <option value="0" <?php echo  set_select('status', '0'); ?>>Active</option>
                                                                <option value="1" <?php echo  set_select('status', '1'); ?>>InActive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                                <?php echo anchor(base_url().'coupons', 'Cancel','class="btn btn-warning mr-1"');?>
                                                <?php echo form_submit(array('type'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary'));?>

                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <style>
                span.select2 {
                    width: 100% !important;
                }
            </style>
