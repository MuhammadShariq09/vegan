
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Orders</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>users">Orders</a>
                        </li>
                        <li class="breadcrumb-item active"><?php
                            if(empty($data)){
                                echo "Add";
                            }else{
                                echo "Edit";
                            }
                            ?> Order
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->
            <?php if($message['message']) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php $msg = $message; ?>
                        <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $msg['message']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <section>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-colored-form-control"><?php
                                    if(empty($data)){
                                        echo "Add";
                                    }else{
                                        echo "Edit";
                                    }
                                    ?> Order</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">

                                    <?php
                                    if(empty($data)){
                                        echo form_open_multipart('backend/orders/req_add_order');
                                    }else{
                                        echo form_open_multipart('backend/orders/req_edit_order');
                                    }
                                    ?>
                                    <?php ?>

                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="productError alert alert-danger" style="display: none;"></div>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-info"></i>User Info</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Users</label>
                                                    <select name="users" class="select2 form-control" id="users">
                                                        <option value="">SELECT</option>
                                                        <?php
                                                        $users = $this->ion_auth->users([2])->result();
                                                        foreach ($users as $k => $user)
                                                        {
                                                            $cuser = $this->ion_auth->user()->row();
                                                            if($user->id == $cuser->user_id || $user->id == "25" || $user->id == "1"){
                                                                unset($users[$k]);
                                                            }
                                                        }
                                                        //$users = json_decode(json_encode($users),true);
                                                        foreach ($users as $user) { ?>
                                                            <option value="<?php echo $user->id; ?>"><?php echo $user->first_name." ".$user->last_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-info"></i>Product Info</h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Products</label>
                                                    <select name="products" class="select2 form-control" id="products">
                                                        <option value="">SELECT</option>
                                                        <?php foreach ($products as $product) { ?>
                                                            <option value="<?php echo $product['id']."_".$product['price']."_".$product['name']; ?>"><?php echo $product['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Quantity</label>
                                                    <input type="number" class="form-control" id="qty" name="qty" min="1">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <input type="button" class="btn btn-primary form-control" style="color:#fff" value="ADD" id="addbtn">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table class="table table-bordered" id="productstable">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Name</th>
                                                            <th>Quantity</th>
                                                            <th>Price</th>
                                                            <th>Total</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="2"></th>
                                                            <th style="text-align:center"><a href="#" id="updateqty" class="btn btn-default">Update Quantity</a> </th>
                                                            <th colspan="2" style="text-align: right">Total</th>
                                                            <th colspan="1">$ <span class="sum"></span><input type="hidden" class="sum" name="sum"></th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h4 class="form-section"><i class="ft-info"></i>Billing Info</h4>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="billingError alert alert-danger" style="display: none;"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="address_1">Enter Address</label>
                                                            <input name="full_billing_address" id="autocomplete" placeholder="Enter your address" class="form-control border-primary"
                                                                   onFocus="geolocate()" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address_2">Apt/Unit</label>
                                                            <input  class="form-control border-primary" value="<?php set_value('billing_address_1') ?>" id="street_number" name="billing_address_1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address_2">Street</label>
                                                            <input class="form-control border-primary" id="route" name="billing_address_2" value="<?php set_value('billing_address_2') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="city">City</label>
                                                            <input class="form-control border-primary" id="locality" name="billing_city" value="<?php set_value('billing_city') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="state">State / Province</label>
                                                            <input class="form-control border-primary" id="administrative_area_level_1" name="billing_state" value="<?php set_value('billing_state') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Country</label>
                                                            <input class="form-control border-primary" id="country" name="billing_country" value="<?php set_value('billing_country') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="postcode">Postcode / ZIP</label>
                                                            <input class="form-control border-primary" id="postal_code" type="number" name="billing_postcode" value="<?php set_value('billing_postcode') ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4 class="form-section"><i class="ft-info"></i>Shipping Info</h4>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="shippingError alert alert-danger" style="display: none;"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="address_1">Enter Address</label>
                                                            <input id="autocomplete2" placeholder="Enter your address" class="form-control border-primary"
                                                                   onFocus="geolocate2()" type="text" autocomplete="false">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address_2">Apt/Unit</label>
                                                            <input  class="form-control border-primary" value="<?php set_value('shipping_address_1') ?>" id="street_number2" name="shipping_address_1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address_2">Street</label>
                                                            <input class="form-control border-primary " id="route2" name="shipping_address_2" value="<?php set_value('shipping_address_2') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="city">City</label>
                                                            <input class="form-control border-primary" id="locality2" name="shipping_city" value="<?php set_value('shipping_city') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="state">State / Province</label>
                                                            <input class="form-control border-primary" id="administrative_area_level_12" name="shipping_state" value="<?php set_value('shipping_state') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Country</label>
                                                            <input class="form-control border-primary" id="country2" name="shipping_country" value="<?php set_value('shipping_country') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="postcode">Postcode / ZIP</label>
                                                            <input class="form-control border-primary" id="postal_code2" type="number" name="shipping_postcode" value="<?php set_value('shipping_postcode') ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">

                                        <?php echo anchor(base_url().'users', 'Cancel','class="btn btn-secondary mr-1"');?>

                                        <?php echo form_submit(array('type'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary hidden', 'id' =>'submitBtn'));?>

                                        <a href="#" id="paymentBox" class="btn btn-primary mr-1">Order Now</a>

                                        <!-- Large modal -->
<!--                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>-->

                                        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Payment Info </h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="paymentError alert alert-danger" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="firstname" class="col-form-label">First Name:</label>
                                                                    <input type="text" class="form-control" name="firstname" id="firstname">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="lastname" class="col-form-label">Last Name:</label>
                                                                    <input type="text" class="form-control" name="lastname" id="lastname">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="cc_number" class="col-form-label">Card Number:</label>
                                                                    <input type="number" class="form-control" name="cc_number" id="cc_number">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="cc_cvv" class="col-form-label">CVV:</label>
                                                                    <input type="number" class="form-control" name="cc_cvv" id="cc_cvv">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="cc_month" class="col-form-label">Expiry Month:</label>
                                                                    <select name="cc_month" class="form-control" id="cc_month">
                                                                        <?php $count = 1;
                                                                        while ($count < 13){ ?>
                                                                            <option><?php echo $count; ?></option>
                                                                        <?php $count++;  } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="cc_year" class="col-form-label">Expiry Year:</label>
                                                                    <select name="cc_year" id="cc_year" class="form-control">
                                                                        <option value="2018">2018</option>
                                                                        <option value="2019">2019</option>
                                                                        <option value="2020">2020</option>
                                                                        <option value="2021">2021</option>
                                                                        <option value="2022">2022</option>
                                                                        <option value="2023">2023</option>
                                                                        <option value="2024">2024</option>
                                                                        <option value="2025">2025</option>
                                                                        <option value="2026">2026</option>
                                                                        <option value="2027">2027</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="button" id="paynow" class="btn btn-primary">Pay Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete"
                    async defer></script>
            <script>
                $(document).ready(function() {
                    $('select.select2').select2();
                });
                var inc = 0;
                $("#addbtn").click(function(e){
                    e.preventDefault();
                    var productId = $("#products").val();
                    var qty = $("#qty").val();

                    if(qty > 1 && productId  != ""){
                        productId = productId.split("_");
                        var pId = productId[0];
                        var price = productId[1];
                        var name = productId[2];

                        var total = parseInt(price)*qty;


                        var htmlRow = '<tr id="tr_'+inc+'">' +
                            '<td>'+pId+'<input type="hidden" name="ids[]" value="'+pId+'"></td>' +
                            '<td>'+name+'<input type="hidden" name="names[]" value="'+name+'"></td>' +
                            '<td><input type="number" value="'+qty+'" name="qtys[]" min="1" class="qtyfield"></td>' +
                            '<td>$ <span class="prices">'+price+'</span><input type="hidden" name="prices[]" value="'+price+'"></td>' +
                            '<td>$ <span class="total">'+total+'</span><input type="hidden" name="totals[]" value="'+total+'"></td> ' +
                            '<td><a href="#" id="deleteRow'+inc+'" onclick="removeRow(this.id)"><i class="fa fa-trash"></i> </a></td>' +
                            '</tr>';

                        $("#productstable").append(htmlRow);

                        var sum = 0;
                        $("#productstable input[name='totals[]']").each( function() {
                            sum += +this.value;
                        });
                        $(".sum").text(sum);
                        $(".sum").val(sum);

                        inc++;

                        $("#products").val('').trigger('change')
                        $("#qty").val("");

                        $(".productError").text("").hide();
                    }
                });


                function removeRow(id){
                    // alert("#"+id);
                    $("#"+id).closest("tr").remove();

                    var sum = 0;
                    $("#productstable input[name='totals[]']").each( function() {
                        sum += +this.value;
                    });
                    $(".sum").text(sum);
                    $(".sum").val(sum);
                }


                $("#updateqty").click(function(e){
                    e.preventDefault();
                    $("#productstable input[name='qtys[]']").each( function() {
                        var qty = this.value;
                        var tr = $(this).closest("tr");
                        var price =  $("#"+tr[0].id+" input[name='prices[]").val();
                        if(qty < 1){
                            tr.remove();
                        }
                        var total =  price * qty;
                        // alert(total+"#"+tr[0].id+" .total");
                        $("#"+tr[0].id+" input[name='totals[]").val(total);
                        $("#"+tr[0].id+" .total").text(total);
                    });

                    var sum = 0;
                    $("#productstable input[name='totals[]']").each( function() {
                        sum += +this.value;
                    });
                    $(".sum").text(sum);
                    $(".sum").val(sum);

                })


                $("#paymentBox").click(function(e){
                    e.preventDefault();

                    var userId = $("#users").val();



                    var billing_address_1 = $("input[name='billing_address_1'").val();
                    var billing_address_2 = $("input[name='billing_address_2'").val();
                    var billing_city = $("input[name='billing_city'").val();
                    var billing_state = $("input[name='billing_state'").val();
                    var billing_country = $("input[name='billing_country'").val();

                    var shipping_address_1 = $("input[name='shipping_address_1'").val();
                    var shipping_address_2 = $("input[name='shipping_address_2'").val();
                    var shipping_city = $("input[name='shipping_city'").val();
                    var shipping_state = $("input[name='vstate'").val();
                    var shipping_country = $("input[name='shipping_country'").val();

                    $(".productError").text("").hide();
                    $(".shippingError").text("").hide();
                    $(".billingError").text("").hide();

                    if(userId == ""){
                        $(".productError").text("User Required").show();
                    }
                    else if(inc < 1){
                        $(".productError").text("Please Select One Product to Order").show();
                    }
                    else if(billing_address_1 == ""){
                        $(".billingError").text("Apt/Unit Required").show();
                    }
                    else if(billing_address_2 == ""){
                        $(".billingError").text("Street Required").show();
                    }
                    else if(billing_city == ""){
                        $(".billingError").text("City Required").show();
                    }
                    else if(billing_state == ""){
                        $(".billingError").text("State Required").show();
                    }
                    else if(billing_country == ""){
                        $(".billingError").text("Country Required").show();
                    }
                    else if(shipping_address_1 == ""){
                        $(".shippingError").text("Apt/Unit Required").show();
                    }
                    else if(shipping_address_2 == ""){
                        $(".shippingError").text("Street Required").show();
                    }
                    else if(shipping_city == ""){
                        $(".shippingError").text("City Required").show();
                    }
                    else if(shipping_state == ""){
                        $(".shippingError").text("State Required").show();
                    }
                    else if(shipping_country == ""){
                        $(".shippingError").text("Country Required").show();
                    }else{
                        $('.bd-example-modal-lg').modal();
                    }

                });

                $("#paynow").click(function(){
                    var firstname = $("#firstname").val();
                    var lastname = $("#lastname").val();
                    var cc_number = $("#cc_number").val();
                    var cc_cvv = $("#cc_cvv").val();
                    var cc_month = $("#cc_month").val();
                    var cc_year = $("#cc_year").val();

                    $(".paymentError").text("").hide();
                    if(firstname == ""){
                        $(".paymentError").text("First Name Required").show();
                    }
                    else if(lastname == ""){
                        $(".paymentError").text("Last Name Required").show();
                    }
                    else if(cc_number == ""){
                        $(".paymentError").text("Number Required").show();
                    }
                    else if(cc_cvv == ""){
                        $(".paymentError").text("CVV Required").show();
                    }
                    else if(cc_month == ""){
                        $(".paymentError").text("Month Required").show();
                    }
                    else if(cc_year == ""){
                        $(".paymentError").text("Year Required").show();
                    }else{
                        $("#submitBtn").trigger('click');
                    }
                });

                // This example displays an address form, using the autocomplete feature
                // of the Google Places API to help users fill in the information.

                // This example requires the Places library. Include the libraries=places
                // parameter when you first load the API. For example:
                // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                var placeSearch, autocomplete;

                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                var componentForm2 = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                function initAutocomplete() {
                    // Create the autocomplete object, restricting the search to geographical
                    // location types.
                    autocomplete = new google.maps.places.Autocomplete(
                        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                        {types: ['geocode']});

                    // When the user selects an address from the dropdown, populate the address
                    // fields in the form.
                    autocomplete.addListener('place_changed', fillInAddress);



                    autocomplete2 = new google.maps.places.Autocomplete(
                        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete2')),
                        {types: ['geocode']});

                    // When the user selects an address from the dropdown, populate the address
                    // fields in the form.
                    autocomplete2.addListener('place_changed', fillInAddress2);
                }

                function fillInAddress() {
                    // Get the place details from the autocomplete object.
                    var place = autocomplete.getPlace();
                    console.log(place.geometry.location.lat());
                    console.log(place.geometry.location.lng());
                    for (var component in componentForm) {
                        document.getElementById(component).value = '';
                        document.getElementById(component).disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        console.log(addressType)
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType).value = val;
                        }
                    }
                }

                function fillInAddress2() {
                    // Get the place details from the autocomplete object.
                    var place = autocomplete2.getPlace();
                    console.log(place.geometry.location.lat());
                    console.log(place.geometry.location.lng());
                    for (var component in componentForm2) {
                        document.getElementById(component+"2").value = '';
                        document.getElementById(component+"2").disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        console.log(addressType)
                        if (componentForm2[addressType]) {
                            var val = place.address_components[i][componentForm2[addressType]];
                            document.getElementById(addressType+"2").value = val;
                        }
                    }
                }

                // Bias the autocomplete object to the user's geographical location,
                // as supplied by the browser's 'navigator.geolocation' object.
                function geolocate() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var geolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var circle = new google.maps.Circle({
                                center: geolocation,
                                radius: position.coords.accuracy
                            });
                            autocomplete.setBounds(circle.getBounds());
                        });
                    }
                }

                function geolocate2() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var geolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var circle = new google.maps.Circle({
                                center: geolocation,
                                radius: position.coords.accuracy
                            });
                            autocomplete2.setBounds(circle.getBounds());
                        });
                    }
                }





            </script>
            <style>
                thead,tfoot {
                    background: #f3f3f3;
                }
                a#updateqty {
                    border: 1px solid #ccc;
                }

                a#updateqty:hover {
                    border: 1px solid #58a5f1;
                    color: #58a5f1;
                }
            </style>