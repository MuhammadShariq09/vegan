
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Pages</h3>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


        <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">All Pages</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'backend/pages/add';?>"><i class="ft-plus"></i> Add Pages</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard table-responsive">
                      <?php if ($this->session->flashdata('message')): ?>
                        <div class="alert <?=$this->session->flashdata('message')['class']?>">
                          <?=$this->session->flashdata('message')['message']?>
                        </div>
                      <?php endif; ?>
                        <table class="table table-striped table-bordered default-ordering">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($pages){?>
                                    <?php foreach ($pages as $p){?>
                                        <tr>
                                            <td><?php echo $p['id']; ?></td>
                                            <td><?php echo htmlspecialchars($p['title'],ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo $p['slug']; ?></td>
                                            <td><?php echo date('M d,Y',strtotime($p['created_at']));?></td>
                                            <td><a href="#!" class="badge badge-<?=($p['status']) ? "danger" : "primary" ?>"><?=($p['status']) ? "Inactive" : "Active" ?></a></td>
                                            <td>
                                                <a class="badge badge-primary"
                                                  href="<?php echo base_url();?>backend/pages/edit/<?php echo $p['id']; ?>">Edit</a>
                                                <a class="badge badge-danger"
                                                  href="<?php echo base_url();?>backend/pages/delete/<?php echo $p['id']; ?>" id="confirmbtn">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                    <th>Modify</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


