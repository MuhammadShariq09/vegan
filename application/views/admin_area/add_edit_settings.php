<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Settings</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Edit Settings
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="ordering">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Edit Setting</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <?php echo form_open('settings/update_settings');?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-info"></i>Shipping Settings</h4>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Shipping Amount</label>
                                                            <input type="number" id="shipping_amount" name="shipping_amount" class="form-control border-primary" placeholder="Shipping Amount" value="<?php echo $settings->shipping_amount ?>"  >
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="form-section"><i class="ft-info"></i>Currency Settings</h4>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Base Currency Symbol</label>
                                                            <select id="base_currency" name="base_currency" class="form-control border-primary select2">
                                                                <option value="USD" <?php echo ($settings->base_currency == "USD")? "SELECTED":""; ?>>USD US Dollar</option>
                                                                <option value="EUR" <?php echo ($settings->base_currency == "EUR")? "SELECTED":""; ?>>EUR Euro</option>
                                                                <option value="GBP" <?php echo ($settings->base_currency == "GBP")? "SELECTED":""; ?>>GBP Pound Sterling</option>
                                                                <option value="PKR" <?php echo ($settings->base_currency == "PKR")? "SELECTED":""; ?>>PKR Pakistan Rupee</option>
                                                                <option value="JPY" <?php echo ($settings->base_currency == "JPY")? "SELECTED":""; ?>>JPY Yen</option>
                                                                <option value="AUD" <?php echo ($settings->base_currency == "AUD")? "SELECTED":""; ?>>AUD Australian Dollar</option>
                                                                <option value="CAD" <?php echo ($settings->base_currency == "CAD")? "SELECTED":""; ?>>CAD Canadian Dollar</option>
                                                                <option value="HKD" <?php echo ($settings->base_currency == "HKD")? "SELECTED":""; ?>>HKD Hong Kong Dollar</option>
                                                                <option value="NZD" <?php echo ($settings->base_currency == "NZD")? "SELECTED":""; ?>>NZD New Zealand Dollar</option>
                                                                <option value="AED" <?php echo ($settings->base_currency == "AED")? "SELECTED":""; ?>>AED UAE Dirham</option>
                                                                <option value="BHD" <?php echo ($settings->base_currency == "BHD")? "SELECTED":""; ?>>BHD Bahraini Dinar</option>
                                                                <option value="INR" <?php echo ($settings->base_currency == "INR")? "SELECTED":""; ?>>INR Indian Rupee</option>
                                                                <option value="IRR" <?php echo ($settings->base_currency == "IRR")? "SELECTED":""; ?>>IRR Iranian Rial</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                                <?php echo form_submit(array('type'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary'));?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>