
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Attributes</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/dashboard';?>">Home</a>
                </li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/products';?>">Products</a>
                  </li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/products/attributes';?>">Attributes</a>
                  </li>
                <li class="breadcrumb-item active"><?php echo $attribute;?> Values
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->
            <?php if(isset($message)) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php $msg = $message; ?>
                        <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $msg['message']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('message')){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php $msg = $this->session->flashdata('message'); ?>
                        <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $msg['message']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

        <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?php echo $attribute;?> Values</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'backend/products/add_attribute/'.$attribute_id;?>"><i class="ft-plus"></i> Add <?php echo $attribute;?> Value</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        <table class="table table-striped table-bordered  default-ordering">
                            <thead>
                                <tr>
                                    <th style="width:3%">#</th>
                                    <th style="width:60%">Value Name</th>
                                    <th>Create Date</th>
                                    <th style="width:15%">Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($attributes_values){?>
                                    <?php $i=1;?>
                                    <?php foreach ($attributes_values as $attributes_value){?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $attributes_value['name'];?></td>
                                            <td><?php echo convertDateTimeToLocalDateTime($attributes_value['createdate']);?></td>
                                            <td>
                                                <a class="badge badge-secondary" href="<?php echo base_url();?>backend/products/edit_attribute/<?php echo $attributes_value['id']; ?>/value">Edit</a>
                                                <a class="badge badge-danger" href="<?php echo base_url();?>backend/products/delete_attribute_value/<?php echo $attribute_id;?>/<?php echo $attributes_value['id']; ?>">Delete</a>
                                            </td>
                                        </tr>
                                        <?php $i++;?>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="width:3%">#</th>
                                    <th>Value Name</th>
                                    <th>Create Date</th>
                                    <th>Modify</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

