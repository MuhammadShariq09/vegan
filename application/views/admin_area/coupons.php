
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Coupons</h3>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


        <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">All Coupons</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'backend/coupons/add';?>"><i class="ft-plus"></i> Add Coupon</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard table-responsive">
                      <?php if ($this->session->flashdata('message')): ?>
                        <div class="alert <?=$this->session->flashdata('message')['class']?>">
                          <?=$this->session->flashdata('message')['message']?>
                        </div>
                      <?php endif; ?>
                        <table class="table table-striped table-bordered default-ordering">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Percentage</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($coupons){?>
                                    <?php foreach ($coupons as $coupon){?>
                                        <tr>
                                            <td><?php echo $coupon['id']; ?></td>
                                            <td><?php echo htmlspecialchars($coupon['CouponName'],ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo $coupon['CouponCode']; ?></td>
                                            <td><?php echo $coupon['CouponPercentage']; ?>%</td>
                                            <td><?php echo date('M d,Y',strtotime($coupon['CouponFrom']));?></td>
                                            <td><?php echo date('M d,Y',strtotime($coupon['CouponTo']));?></td>
                                            <td><a href="#!" class="badge badge-<?=($coupon['status']) ? "danger" : "primary" ?>"><?=($coupon['status']) ? "Inactive" : "Active" ?></a></td>
                                            <td><?php echo date('M d,Y',strtotime($coupon['createdOn']));?></td>
                                            <td>
                                                <a class="badge badge-primary"
                                                  href="<?php echo base_url();?>backend/coupons/edit/<?php echo $coupon['id']; ?>">Edit</a>
                                                <a class="badge badge-danger"
                                                  href="<?php echo base_url();?>backend/coupons/delete/<?php echo $coupon['id']; ?>" id="confirmbtn">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Percentage</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Modify</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
            <script>
                $('a#confirmbtn').confirm({
                    title: 'Are You Sure?',
                    content: 'Do you want to delete this Coupon?',
                    buttons: {
                        Ok: function(){
                            location.href = this.$target.attr('href');
                        },
                        Cancel: function(){

                        }
                    }
                });
            </script>
