<style media="screen">
.green-container{
  padding: 15px;
  border: 1px solid #00B5B8 !important;
}
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">HomePage Banners</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'settings/homepageBanner';?>">HomePage Banners</a>
                        </li>
                        <li class="breadcrumb-item active">Add Blog
                        </li>
                    </ol>
                </div>
            </div>
        </div>
<div class="content-body">
    <?php if(isset($message)){ ?>
        <div class="row">
            <div class="col-md-12">
                <?php $msg = $message; ?>
                <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $msg['message']; ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <section id="ordering">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add Banner Image</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <?php echo form_open_multipart('',['id'=>'hp_banner_form']);?>
                            <input type="hidden" id="hp_banner_id" name="hp_banner_id" value="0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Text *</label>
                                                    <input type="text" required id="hp_banner_text" name="hp_banner_text" class="form-control border-primary" placeholder="Name" value="<?php echo set_value('banner_text'); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Image (1440x550) pixels</label>
                                                    <img style="width: 200px;" id="hp_image_preview" src="">
                                                    <input type="file" accept="image/*" required id="hp_image" name="hp_image" class="form-control border-primary" multiple>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button type="button" class="btn btn-warning" id="cancel_hp_banner">Cancel</button>
                                        <button type="button" class="btn btn-primary" id="save_hp_banner">Save</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="ordering">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add Banner Image</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table">
                                        <tr>
                                          <th>Image</th>
                                          <th>Text</th>
                                          <th></th>
                                          <th></th>
                                        </tr>
                                        <tbody id="hp_bn_res"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
