<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="PIXINVENT">
        <title><?php echo APP_NAME;?></title>
        <link rel="apple-touch-icon" href="<?php echo base_url();?>include-assets/app-assets/images/ico/apple-icon-120.png">
        <link rel="icon" href="<?php echo base_url();?>include-assets/app-assets/images/ico/favicon.png" sizes="192x192">
        <link rel="icon" href="<?php echo base_url();?>include-assets/app-assets/images/ico/favicon.png" sizes="32x32">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="icon" href="<?php echo base_url();?>vegan/assets/images/favicon.ico" sizes="32x32">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>include-assets/app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>include-assets/app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>include-assets/app-assets/vendors/css/forms/icheck/custom.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN STACK CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>include-assets/app-assets/css/app.css">
        <!-- END STACK CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>include-assets/app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>include-assets/app-assets/css/pages/login-register.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>include-assets/assets/css/style.css">
        <!-- END Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().APP_CUSTOM_CSS;?>">
    </head>
    <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column   menu-expanded blank-page blank-page">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="flexbox-container" style="background: url('<?php echo base_url();?>include-assets/app-assets/images/logo/aukee-login-bg.jpg');  background-size: cover;">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="col-md-4 col-10 box-shadow-2 p-0">
                                <div class="card border-grey border-lighten-3 m-0">
                                    <div class="card-header border-0">
                                        <div class="card-title text-center">
                                            <div class="p-1"><img alt="stack admin logo" style="width: 120px;" src="<?php echo base_url().APP_LOGO;?>" class="brand-logo"></h1></div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <?php if($message) { ?>
                                            <div class="alert alert-info"><?php echo $message;?></div>
                                            <?php } ?>
                                            <?php echo form_open("auth/login", array('class' => 'form-horizontal form-simple', 'novalidate' => ''));?>
                                            <fieldset class="form-group position-relative has-icon-left mb-0">
                                                <?php echo form_input($identity);?>
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <?php echo form_input($password);?>
                                                <div class="form-control-position">
                                                    <i class="fa fa-key"></i>
                                                </div>
                                            </fieldset>

                                            <?php echo form_submit( array('value'=> lang('login_submit_btn'), 'class' => 'btn btn-primary btn-lg btn-block'));?>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- <div class="card-footer">
                                        <div class="">
                                            <p class="float-sm-left text-center m-0"><a href="recover-password.html" class="card-link">Recover password</a></p>
                                            <p class="float-sm-right text-center m-0">New to Stack? <a href="register-simple.html" class="card-link">Sign Up</a></p>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- BEGIN VENDOR JS-->
        <script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>include-assets/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN STACK JS-->
        <script src="<?php echo base_url();?>include-assets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>include-assets/app-assets/js/core/app.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>include-assets/app-assets/js/scripts/customizer.js" type="text/javascript"></script>
        <!-- END STACK JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="<?php echo base_url();?>include-assets/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
    </body>
</html>
