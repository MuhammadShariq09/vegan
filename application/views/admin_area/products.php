
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Products</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Products
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


  <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">

              <?php if ($this->session->flashdata('message')): ?>
                <div class="alert <?=$this->session->flashdata('message')['class']?>">
                  <?=$this->session->flashdata('message')['message']?>
                </div>
              <?php endif; ?>

                <div class="card-header">
                    <h4 class="card-title">All Products</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'backend/products/add_product';?>"><i class="ft-plus"></i> Add Product</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard table-responsive">

                        <table class="table table-striped table-bordered  default-ordering">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>SKU</th>
                                    <th>Rating</th>
                                    <th>Featured</th>
                                    <th>Categories</th>
                                    <th>Sales Price</th>
                                    <th>Reg Price</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($products){?>
                                    <?php foreach ($products as $product){?>
                                        <tr>
                                            <td><?php echo htmlspecialchars($product->name,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo htmlspecialchars($product->slug,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo htmlspecialchars($product->sku,ENT_QUOTES,'UTF-8');?></td>
                                            <?php
                                                  $rev = $this->Home_Model->selectWhereResult('feedback',['product_id'=>$product->id]);
                                                  $sum = $count = 0;
                                                  if(count($rev)){
                                                    foreach ($rev as $key => $value) {
                                                      $sum += $value['review'];
                                                      $count++;
                                                    }
                                                    $rating = $sum/$count;
                                                  }else{
                                                    $rating = 0;
                                                  }                                            
                                            ?>
                                            <td><span class="badge badge-secondary"><?=round($rating,2)?></span></td>
                                            <td> <input onclick="setFeatured('<?=$product->id?>')" type="checkbox"
                                              class="is_featured<?=$product->id?>" name="is_featured" <?=($product->featured) ? "checked" : "" ?>> </td>
                                            <td>
                                                <?php $cats='';?>
                                                <?php foreach ($product->cats as $cat):?>
                                                    <?php $cats .= htmlspecialchars($cat->name,ENT_QUOTES,'UTF-8').', ' ;?>
                                                <?php endforeach?>
                                                <?php echo substr($cats, 0, -2);?>
                                            </td>
                                            <td>$<?php echo $product->price; ?></td>
                                            <td>$<?php echo $product->reg_price;?></td>
                                            <td>
                                                <a class="badge badge-secondary" href="<?php echo base_url();?>backend/products/edit_product/<?php echo $product->id; ?>">Edit</a>
                                                <a class="badge badge-danger" href="<?php echo base_url();?>backend/products/deleteProduct/<?php echo $product->id; ?>" id="confirmbtn">Delete</a>
                                                <a class="badge badge-primary" href="<?php echo base_url();?>backend/products/review/<?php echo $product->id; ?>" >Review</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th>Name</th>
                                  <th>Slug</th>
                                  <th>SKU</th>
                                  <th>Featured</th>
                                  <th>Categories</th>
                                  <th>Regular Price</th>
                                  <th>Sales Price</th>
                                  <th>Modify</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">

  function setFeatured(id){
    var state = $('.is_featured'+id).is(":checked");
    $.ajax({
      url: '<?=base_url()?>backend/products/setFeatured/'+id,
      success: function(response){
          if(response.status == 200)
            toastr.success(response.msg, "Success!")
          else{
            toastr.error(response.msg,"Error!")
            $('.is_featured'+id).prop("checked", !state);
          }
      }
    })
  }

</script>
