
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Testimonials</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Testimonials
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


  <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">

              <?php if ($this->session->flashdata('message')): ?>
                <div class="alert <?=$this->session->flashdata('message')['class']?>">
                  <?=$this->session->flashdata('message')['message']?>
                </div>
              <?php endif; ?>

                <div class="card-header">
                    <h4 class="card-title">All Testimonials</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'backend/settings/add_testimonials';?>"><i class="ft-plus"></i> Add Testimonials</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        <table class="table table-striped table-bordered  default-ordering">
                            <thead>
                                <tr>
                                  <th>id</th>
                                  <th>Author</th>
                                  <th>Body</th>
                                  <th>Created at</th>
                                  <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($testimonials){?>
                                    <?php foreach ($testimonials as $test){?>
                                        <tr>
                                            <td><?=$test['id']?></td>
                                            <td><?php echo htmlspecialchars($test['author'],ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo substr($test['body'],0,100);?>...</td>
                                            <td><?php echo date('M d, Y', strtotime($test['created_at']));?></td>
                                            <td>
                                                <a class="badge badge-secondary" href="<?php echo base_url();?>backend/settings/edit_testimonials/<?php echo $test['id']; ?>">Edit</a>
                                                <a class="badge badge-danger" href="<?php echo base_url();?>backend/settings/deleteTestimonials/<?php echo $test['id']; ?>" id="confirmbtn">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <th>id</th>
                                  <th>Author</th>
                                  <th>Body</th>
                                  <th>Created at</th>
                                  <th>Modify</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">

</script>
