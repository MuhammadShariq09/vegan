<style media="screen">
.green-container{
  padding: 15px;
  border: 1px solid #00B5B8 !important;
}
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Testimonial</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'products';?>">Testimonial</a>
                        </li>
                        <li class="breadcrumb-item active">Edit Testimonials
                        </li>
                    </ol>
                </div>
            </div>
        </div>
<div class="content-body">
    <?php if(isset($message)){ ?>
        <div class="row">
            <div class="col-md-12">
                <?php $msg = $message; ?>
                <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $msg['message']; ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <section id="ordering">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Testimonials</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <?php echo form_open_multipart();?>
                            <input type="hidden" id="test_id" name="test_id" value="<?php echo $testimonials['id'];?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Author</label>
                                                    <input type="text" required id="author" name="author"
                                                    class="form-control border-primary" placeholder="Author"
                                                    value="<?php echo (($_POST)?set_value('author'):$testimonials['author']); ?>">
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label>Body *</label>
                                                    <textarea id="description"  required name="body"
                                                    class="form-control border-primary" placeholder="Description"
                                                    rows="9"><?php echo (($_POST)?set_value('body'):$testimonials['body']); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <?php echo anchor(base_url().'backend/settings/testimonials', 'Cancel','class="btn btn-warning mr-1"');?>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
