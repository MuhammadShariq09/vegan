
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Groups</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index-2.html">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Users</a>
                </li>
                  <li class="breadcrumb-item"><a href="#">Groups</a>
                  </li>
                <li class="breadcrumb-item active">Add Group
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->
            <?php if($message['message']) { ?>
            <div class="row">
                <div class="col-md-12">
                    <?php $msg = $message; ?>
                    <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php echo $msg['message']; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        <section>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-colored-form-control">Add Group</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">


                        <?php echo form_open(current_url());?>
                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label >Group Name</label>
                                            <?php echo form_input($group_name);?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Group Description</label>
                                            <?php echo form_input($group_description);?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions right">

                                <?php echo anchor(base_url().'users/groups', 'Cancel','class="btn btn-warning mr-1"');?>

                                <?php echo form_submit(array('type'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary'));?>
                            </div>
                        <?php echo form_close();?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

