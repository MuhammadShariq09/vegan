<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Edit Homepage Element</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Edit HomePage Elements
                        </li>
                    </ol>
                </div>
            </div>
        </div>
<div class="content-body">
    <?php if(isset($message)){ ?>
        <div class="row">
            <div class="col-md-12">
                <?php $msg = $message; ?>
                <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $msg['message']; ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('message')): ?>
      <div class="alert <?=$this->session->flashdata('message')['class']?>">
        <?=$this->session->flashdata('message')['message']?>
      </div>
    <?php endif; ?>

    <section id="ordering">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <?php echo form_open_multipart('backend/settings/update_hompage_element');?>
                            <input type="hidden" id="hp_e_id" name="hp_e_id" value="<?=$element['id']?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Title </label>
                                                    <input type="text" id="title" name="title" class="form-control border-primary" placeholder="Name"
                                                    value="<?php echo (($_POST)?set_value('title'):$element['title']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Subtitle </label>
                                                    <textarea id="subtitle" rows=4 name="subtitle" class="form-control border-primary" placeholder="subtitle"><?php echo (($_POST)?set_value('subtitle'):$element['subtitle']); ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Button Text </label>
                                                    <input type="text" id="btn_text" name="btn_text" class="form-control border-primary" placeholder="Button text"
                                                    value="<?php echo (($_POST)?set_value('btn_text'):$element['btn_text']); ?>">
                                                </div>
                                                <div class="form-group">
                                                  <label>Button Link </label>
                                                  <input type="text" id="btn_link" name="btn_link" class="form-control border-primary" placeholder="Button link"
                                                  value="<?php echo (($_POST)?set_value('btn_link'):$element['btn_link']); ?>">
                                                </div>
                                                <div class="form-group">
                                                  <label>Image </label>&nbsp;&nbsp;<a class="badge badge-secondary" href="<?=base_url().'uploads/'.$element['image']?>">Upoaded Image</a>
                                                  <input type="file" id="Image" accept="image/*" name="image" class="form-control border-primary" multiple>
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label>Extra Text</label>
                                                    <textarea id="description"  name="description" class="form-control border-primary" placeholder="Description"  rows="9"><?php echo (($_POST)?set_value('on_hover_text'):$element['on_hover_text']); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button type="submit" class="btn btn-primary" id="">Save</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
