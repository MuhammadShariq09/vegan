
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Attributes</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/dashboard';?>">Home</a>
                </li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/products';?>">Products</a>
                  </li>
                <li class="breadcrumb-item active">Attributes
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->
            <?php if(isset($message)) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php $msg = $message; ?>
                        <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $msg['message']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('message')){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php $msg = $this->session->flashdata('message'); ?>
                        <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $msg['message']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

        <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Attributes</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'backend/products/add_attribute';?>"><i class="ft-plus"></i> Add Attribute</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        <table class="table table-striped table-bordered  default-ordering">
                            <thead>
                                <tr>
                                    <th>Attribute Name</th>
                                    <th>Attribute Value Count</th>
                                    <th>Create Date</th>
                                    <th style="width:15%">Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($attributes){?>
                                    <?php foreach ($attributes as $attribute){?>
                                        <tr>
                                            <td><?php echo $attribute['name'];?></td>
                                            <td><?php echo $attribute['count_values'];?></td>
                                            <td><?php echo convertDateTimeToLocalDateTime($attribute['createdate']);?></td>
                                            <td>
                                                <a class="badge badge-secondary" href="<?php echo base_url();?>backend/products/edit_attribute/<?php echo $attribute['id']; ?>">Edit</a>
                                                <a class="badge badge-primary" href="<?php echo base_url();?>backend/products/attributes/<?php echo $attribute['id']; ?>">Attr Values</a>
                                                <a class="badge badge-danger" href="<?php echo base_url();?>backend/products/delete_attribute/<?php echo $attribute['id']; ?>">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Attribute Name</th>
                                    <th>Attribute Value Count</th>
                                    <th>Create Date</th>
                                    <th>Modify</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

