
		<!-- ////////////////////////////////////////////////////////////////////////////-->
		<!-- - var menuBorder = true-->

		<div class="app-content content">
			<div class="content-wrapper">
				<div class="content-header row">
				</div>
				<div class="content-body">
					<!-- Stats -->
					<div class="row">
						<div class="col-xl-4 col-lg-6 col-12">
							<div class="card">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2 text-center bg-primary bg-darken-2">
											<i class="icon-camera font-large-2 white"></i>
										</div>
										<div class="p-2 bg-gradient-x-primary white media-body">
											<h5>Products</h5>
											<h5 class="text-bold-400 mb-0"><i class="ft-plus"></i>
												<?=$this->Home_Model->countResult('products')?>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-6 col-12">
							<div class="card">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2 text-center bg-danger bg-darken-2">
											<i class="icon-user font-large-2 white"></i>
										</div>
										<div class="p-2 bg-gradient-x-danger white media-body">
											<h5>New Users</h5>
											<h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i>
												<?=$no_of_users?></h5>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-6 col-12">
							<div class="card">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2 text-center bg-warning bg-darken-2">
											<i class="icon-basket-loaded font-large-2 white"></i>
										</div>
										<div class="p-2 bg-gradient-x-warning white media-body">
											<h5>Orders</h5>
											<h5 class="text-bold-400 mb-0"><i class="ft-arrow-down"></i>
												<?php $no_of_orders = $this->Home_Model->countResult('orders'); ?>
												<?=($no_of_orders) ? $no_of_orders : 0 ?>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/ Stats -->
					<!--Product sale & buyers -->
					<div class="row match-height">
						<div class="col-xl-8 col-lg-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Products Sales</h4>
									<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
									<div class="heading-elements">
										<ul class="list-inline mb-0">
											<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
											<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-content">
									<div class="card-body">
										<div id="products-sales" class="height-300"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Recent Buyers</h4>
									<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
									<div class="heading-elements">
										<ul class="list-inline mb-0">
											<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-content px-1">
									<div id="recent-buyers" class="media-list position-relative">
										<?php
										foreach ($rec_buy as $key => $value): ?>
											<a href="<?=base_url().'/users/edit_user/'.$value['CustomerID']?>" class="media border-0">
											<div class="media-left pr-1">
											    <span class="avatar avatar-md avatar-online">
														<?php if($value['profileImage']){ ?>
    														<img class="media-object rounded-circle" src="<?=base_url().'uploads/'.$value['profileImage']?>" alt="Generic placeholder image">
    													<?php }else{ ?>
    														<img class="media-object rounded-circle" src="<?=base_url().'assets/user.jpg'?>" alt="Generic placeholder image">
    													<?php } ?>
											    <i></i>
											    </span>
											</div>
											<div>
											    <h6 class="mt-1"><?=$value['first_name'].' '.$value['last_name']?></h6>
											</div>
											</a>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/ Product sale & buyers -->
					<!--Recent Orders & Monthly Salse -->
					<div class="row match-height">
						<div class="col-xl-8 col-lg-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Recent Orders</h4>
									<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
									<div class="heading-elements">
										<ul class="list-inline mb-0">
											<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
											<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-content">
									<div class="table-responsive">
										<table id="recent-orders" class="table table-hover mb-0 ps-container ps-theme-default">
											<thead>
												<tr>
													<th>OrderID</th>
													<th>Guest</th>
													<th>Customer Name</th>
													<th>Status</th>
													<th>Payment Status</th>
													<th>Amount</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($orders as $key => $o): ?>
													<tr>
														<td class="text-truncate"><?=$o['OrderID']?></td>
														<td><?php echo $o['OrderTrackingNo']; ?></td>
														<?php if ($o['CustomerID']): ?>
															<td class="text-truncate">No</td>
														<?php else: ?>
															<td class="text-truncate">Yes</td>
														<?php endif; ?>
														<?php
														$status = array(
															 '0' => 'Pending',
															 '1' => 'Processing',
															 '2' => 'On hold',
															 '3' => 'Completed',
															 '4' => 'Cancelled',
															 '6' => 'Refunded',
															 '7' => 'Failed',
														)
														 ?>
														<td class="text-truncate"><?=$o['billing_first_name'].' '.$o['billing_last_name']?></td>
														<td class="text-truncate"><?php echo $status[$o['Status']] ?></td>
														<td class="text-truncate"><?php echo ($o['TransactionStatus'])?"Paid":"Unpaid"; ?></td>
														<td class="text-truncate">$ <?=$o['OrderTotal']?></td>
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Recent Users</h4>
									<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
									<div class="heading-elements">
										<ul class="list-inline mb-0">
											<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="card-content px-1">
									<div id="recent-buyers" class="media-list position-relative">
										<?php foreach ($users as $key => $u): ?>
											<a href="<?=base_url().'/users/edit_user/'.$u->id?>" class="media border-0">
											<div class="media-left pr-1">
											    <span class="avatar avatar-md avatar-online">
														<img class="media-object rounded-circle"
														<?php if ($u->profileImage): ?>
															<img class="media-object rounded-circle"
															src="<?=base_url().'uploads/'.$u->profileImage?>" alt="">
														<?php else: ?>
															<img class="media-object rounded-circle"
															src="<?=base_url().'assets/user.jpg'?>" alt="Generic placeholder image">
														<?php endif; ?>
											    <i></i>
											    </span>
											</div>
											<div>
											    <h6 class="mt-1"><?=$u->first_name.' '.$u->last_name?></h6>
											</div>
											</a>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
