
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Permissions</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="<?php echo base_url().'users';?>">Users</a>
                </li>
                <li class="breadcrumb-item active">Permissions
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


        <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">All Permissions</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'users/add_permission';?>"><i class="ft-plus"></i> Add Permission</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        <table class="table table-striped table-bordered  default-ordering">
                            <thead>
                                <tr>
                                    <th>Permissions Name</th>
                                    <th>Permissions Key</th>
                                    <th>Added Date</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($permissions){?>
                                    <?php foreach ($permissions as $permission){?>
                                        <tr>
                                            <td><?php echo htmlspecialchars($permission['name'],ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo htmlspecialchars($permission['key'],ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo date('M d, Y ',strtotime($permission['created_on']));?></td>
                                            <td>
                                                <a class="badge badge-secondary" href="<?php echo base_url();?>users/update_permission/<?php echo $permission['id']; ?>">Edit</a>
                                                <a id="confirmbtn" class="badge badge-danger" href="<?php echo base_url();?>users/delete_permission/<?php echo $permission['id']; ?>">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Permissions Name</th>
                                    <th>Permissions Key</th>
                                    <th>Added Date</th>
                                    <th>Modify</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
