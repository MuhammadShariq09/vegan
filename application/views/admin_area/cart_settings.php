<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Manage Cart Settings</h3>
            </div>
        </div>
            <div class="content-body">
                <section id="ordering">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Manage Settings</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                      <?php if ($this->session->flashdata('message')): ?>
                                        <div class="alert <?=$this->session->flashdata('message')['class']?>">
                                          <?=$this->session->flashdata('message')['message']?>
                                        </div>
                                      <?php endif; ?>
                                      <?php if(isset($message) &&$message['message']) { ?>
                                      <div class="row">
                                          <div class="col-md-12">
                                              <?php $msg = $message; ?>
                                              <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                  </button>
                                                  <?php echo $msg['message']; ?>
                                              </div>
                                          </div>
                                      </div>
                                      <?php } ?>

                                      <?php echo form_open_multipart("backend/settings/updateCartSettings");?>
                                      <?php foreach ($cartSettings as $key => $value): ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-body">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                           <div class="col-md-4">
                                                             <div class="form-group">
                                                               <label>Attribute Name</label>
                                                               <input type="text" disabled name="attr_name" class="form-control border-primary" value="<?=$value['name']?>" >
                                                             </div>
                                                           </div>
                                                           <div class="col-md-4">
                                                              <div class="form-group">
                                                                  <label>Attribute Rate</label>
                                                                  <input type="text" name="attr_rate<?=$value['id']?>" class="form-control border-primary" value="<?=$value['rate']?>" >
                                                              </div>
                                                           </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      <?php endforeach; ?>
                                      <div class="col-md-12">
                                         <div class="form-group pull-right" style="margin-top:7%">
                                           <?php echo form_submit(array('type'=>'submit', 'value'=>'Update', 'class'=>'btn btn-primary'));?>
                                         </div>
                                      </div>

                                      <?php echo form_close();?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
