<style media="screen">
.green-container{
  padding: 15px;
  border: 1px solid #00B5B8 !important;
}
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Blogs</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'products';?>">Blogs</a>
                        </li>
                        <li class="breadcrumb-item active">Add Blog
                        </li>
                    </ol>
                </div>
            </div>
        </div>
<div class="content-body">
    <?php if(isset($message)){ ?>
        <div class="row">
            <div class="col-md-12">
                <?php $msg = $message; ?>
                <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $msg['message']; ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <section id="ordering">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add Blog</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <?php echo form_open_multipart();?>
                            <input type="hidden" id="blog_id" name="blog_id" value="0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Name *</label>
                                                    <input type="text" required id="blog_title" name="blog_title" class="form-control border-primary" placeholder="Name" value="<?php echo set_value('blog_title'); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Slug  *<i id="slug_loading" class="fa fa-spinner fa-spin" style="display:none;"></i></label>
                                                    <input type="text" required id="blog_slug" name="blog_slug" class="form-control border-primary" placeholder="Slug" value="<?php echo set_value('blog_slug'); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Author</label>
                                                    <input type="text" required id="author" name="author" class="form-control border-primary" placeholder="Author" value="<?php echo set_value('author'); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Keywords</label>
                                                    <input type="text" id="keywords" name="keywords"
                                                    class="form-control border-primary" placeholder="Enter keywords separated by commas "
                                                    value="<?php echo set_value('keywords'); ?>">
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label>Blog Description *</label>
                                                    <textarea id="description"  required name="description" class="form-control border-primary" placeholder="Description"  rows="9"><?php echo set_value('description'); ?></textarea>
                                                </div>

                                                <hr />
                                                <div class="form-group">
                                                    <label>Blog Images *</label>
                                                    <input type="hidden" id="pi_b64" name="pi_b64">
                                                    <input type="file" accept="image/*" required id="blog_image" name="blog_image" class="form-control border-primary" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <?php echo anchor(base_url().'backend/settings/blogs', 'Cancel','class="btn btn-warning mr-1"');?>
                                        <button type="button" class="btn btn-primary" id="save_blog">Save</button>
                                    </div>
                                </div>

                                <div style="padding-top:2%" class="col-md-12">
                                    <div class="form-group">
                                      <div class="text-center">
                              				     <div id="upload-demo-blog" style="width:350px"></div>
                              	  		</div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
