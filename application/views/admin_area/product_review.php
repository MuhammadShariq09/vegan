
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Products Review</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Products Review
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


  <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">

              <?php if ($this->session->flashdata('message')): ?>
                <div class="alert <?=$this->session->flashdata('message')['class']?>">
                  <?=$this->session->flashdata('message')['message']?>
                </div>
              <?php endif; ?>

                <div class="card-header">
                    <h4 class="card-title"><?php echo $name[0]->name; ?></h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard table-responsive">

                        <table class="table table-striped table-bordered  default-ordering">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Msg</th>
                                    <th>Created At</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($reviews){?>
                                    <?php foreach ($reviews as $r){?>
                                        <tr>
                                            <td><?php echo htmlspecialchars($r->name,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo htmlspecialchars($r->email,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo htmlspecialchars($r->body,ENT_QUOTES,'UTF-8');?></td>
                                            <td><?php echo date('Y-m-d', strtotime($r->created_at)); ?></td>
                                            <td>
                                                <a class="badge badge-danger" href="<?php echo base_url();?>backend/products/deleteReview/<?php echo $r->id; ?>" id="confirmbtn">Delete</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Msg</th>
                                    <th>Created At</th>
                                    <th>Modify</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">

  function setFeatured(id){
    var state = $('.is_featured'+id).is(":checked");
    $.ajax({
      url: '<?=base_url()?>backend/products/setFeatured/'+id,
      success: function(response){
          if(response == 200)
            alert(response.msg);
          else{
            alert(response.msg);
            $('.is_featured'+id).prop("checked", !state);
          }
      }
    })
  }

</script>
