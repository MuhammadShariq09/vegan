<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Import and Export</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Import and Export
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="ordering">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Import and Export</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <?php echo form_open('users/importExport');?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-info"></i>Import Data</h4>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="upload-btn-wrapper">
                                                                <button class="btn" name="import">Upload a file</button>
                                                                <input type="file" name="myfile"  accept=".csv" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <h4 class="form-section"><i class="ft-info"></i>Export Data</h4>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="upload-btn-wrapper">
                                                                <button class="btn" name="export">Export To CSV</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <style>
                .upload-btn-wrapper {
                    position: relative;
                    overflow: hidden;
                    display: block;
                }

                .upload-btn-wrapper .btn {
                    border: 2px solid #58a5f1;
                    color: #58a5f1;
                    background-color: white;
                    padding: 45px 20px;
                    border-radius: 8px;
                    font-size: 20px;
                    width: 100%;
                    font-weight: ;
                }

                .upload-btn-wrapper input[type=file] {
                    font-size: 100px;
                    position: absolute;
                    left: 0;
                    top: 0;
                    opacity: 0;
                    cursor: pointer;
                }
                .upload-btn-wrapper:hover .btn {
                    background: #58a5f1 !important;
                    color: #fff;
                    cursor: pointer;
                }

            </style>