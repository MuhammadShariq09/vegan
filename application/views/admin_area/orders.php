<style>
    table#DataTables_Table_0 a {
        margin: 2px;
    }
</style>   
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Orders</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Orders
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


    <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">All Orders</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a href="<?php echo base_url().'backend/orders/add_order';?>"><i class="ft-plus"></i> Add Order</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body table-responsive card-dashboard">

                        <table class="table table-striped table-bordered default-ordering">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tracking #</th>
                                    <th>Is Guest</th>
                                    <th>Full Name</th>
                                    <th>Address</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Paid/Unpaid</th>
                                    <th>Order Date</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($orders){?>
                                    <?php foreach ($orders as $order){?>
                                        <tr>
                                            <td><?php echo $order->OrderID; ?></td>
                                            <td><?php echo $order->OrderTrackingNo; ?></td>
                                            <?php if ($order->CustomerID): ?>
                                              <td>No</td>
                                              <td><?php echo htmlspecialchars($order->UserFirstName,ENT_QUOTES,'UTF-8');?> <?php echo htmlspecialchars($order->UserLastName,ENT_QUOTES,'UTF-8');?></td>
                                            <?php else: ?>
                                              <td>Yes</td>
                                              <td><?=$order->billing_first_name.' '.$order->billing_last_name?></td>
                                            <?php endif; ?>
                                            <td><?php echo $order->billing_address_1?></td>
                                            <td><?php echo $order->OrderTotal; ?></td>
                                            <?php
                                            $status = array(
                                               '0' => 'Pending',
                                               '1' => 'Processing',
                                               '2' => 'On hold',
                                               '3' => 'Completed',
                                               '4' => 'Cancelled',
                                               '6' => 'Refunded',
                                               '7' => 'Failed',
                                            )
                                             ?>
                                            <td><?php echo $status[$order->Status] ?></td>
                                            <td><?php if($order->TransactionStatus) { ?>
                                                <?php 
                                                $t = $this->Home_Model->selectWhere('order_payments',['OrderId' => $order->OrderID]); 
                                                if($t['payment_type'] == 'Paypal Pro') { ?>
                                                    Paid - Paypal
                                                <?php } else {  ?>
                                                    Cash On Delivery
                                                <?php } ?>
                                            <?php } else {  ?>
                                                    Unpaid
                                            <?php } ?></td>
                                            <td><?php echo date('M d, Y',strtotime($order->createdate));?></td>
                                            <td>
                                                <a style="font-size:15px;" class="badge badge-primary" href="<?php echo base_url();?>backend/orders/view_order/<?php echo $order->OrderID; ?>">View</a>
                                                <a style="font-size:15px;" class="badge badge-warning" href="<?php echo base_url();?>backend/orders/edit_order/<?php echo $order->OrderID; ?>">Edit</a>
                                                <a style="font-size:15px;" class="badge badge-danger" href="<?php echo base_url();?>backend/orders/delete_order/<?php echo $order->OrderID; ?>" id="confirmbtn">Delete</a>
                                            </td>

                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Tracking #</th>
                                    <th>Full Name</th>
                                    <th>Address</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Paid/Unpaid</th>
                                    <th>Order Date</th>
                                    <th>Modify</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

