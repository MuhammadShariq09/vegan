
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Users</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>users">Users</a>
                        </li>
                        <li class="breadcrumb-item active">Add User
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->
            <?php if($message['message']) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php $msg = $message; ?>
                        <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $msg['message']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <section>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-colored-form-control">Add User</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">


                                    <?php echo form_open_multipart("backend/users/create_user");?>
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="ft-info"></i>Personal Info</h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <?php echo form_input($first_name);?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Last Name</label>
                                                    <?php echo form_input($last_name);?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Date Of Birth</label>
                                                    <?php echo form_input($dateofbirth);?>
                                                </div>
                                            </div>
                                            <?php if($identity_column!=='email') { ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <?php echo form_error('identity');?>
                                                        <?php echo form_input($identity);?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <?php echo form_input($email);?>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="display: none">
                                                <div class="form-group">
                                                    <label>Company</label>
                                                    <?php echo form_input($company);?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <?php echo form_input($password);?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Confirm Password</label>
                                                    <?php echo form_input($password_confirm);?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Profile Image</label>
                                                    <?php echo form_input($profileImage);?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Gender</label>
                                                    <?php echo form_dropdown($gender,$gender_options);?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Phone</label>
                                                    <?php echo form_input($phone);?>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><i class="ft-info"></i>Address Info</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="address_1">Enter Address</label>
                                                    <input id="autocomplete" placeholder="Enter your address" class="form-control border-primary"
                                                           onFocus="geolocate()" type="text" value="<?php set_value('fulladdress') ?>" name="fulladdress" autocomplete="false">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address_2">Apt/Unit</label>
                                                    <input  class="form-control border-primary" value="<?php set_value('address_1') ?>" id="street_number" name="address_1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address_2">Street</label>
                                                    <input class="form-control border-primary" id="route" name="address_2" value="<?php set_value('address_2') ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="city">City</label>
                                                    <input class="form-control border-primary" id="locality" name="city" value="<?php set_value('city') ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="state">State / Province</label>
                                                    <input class="form-control border-primary" id="administrative_area_level_1" name="state" value="<?php set_value('state') ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country">Country</label>
                                                    <input class="form-control border-primary" id="country" name="country" value="<?php set_value('country') ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="postcode">Postcode / ZIP</label>
                                                    <input class="form-control border-primary" id="postal_code" type="number" name="postcode" value="<?php set_value('postcode') ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">

                                        <?php echo anchor(base_url().'users', 'Cancel','class="btn btn-secondary mr-1"');?>

                                        <?php echo form_submit(array('type'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary'));?>
                                    </div>
                                    <?php echo form_close();?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete"
                    async defer></script>
            <script>
                // This example displays an address form, using the autocomplete feature
                // of the Google Places API to help users fill in the information.

                // This example requires the Places library. Include the libraries=places
                // parameter when you first load the API. For example:
                // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                var placeSearch, autocomplete;
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                function initAutocomplete() {
                    // Create the autocomplete object, restricting the search to geographical
                    // location types.
                    autocomplete = new google.maps.places.Autocomplete(
                        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                        {types: ['geocode']});

                    // When the user selects an address from the dropdown, populate the address
                    // fields in the form.
                    autocomplete.addListener('place_changed', fillInAddress);
                }

                function fillInAddress() {
                    // Get the place details from the autocomplete object.
                    var place = autocomplete.getPlace();
                    console.log(place.geometry.location.lat());
                    console.log(place.geometry.location.lng());
                    for (var component in componentForm) {
                        document.getElementById(component).value = '';
                        document.getElementById(component).disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        console.log(addressType)
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType).value = val;
                        }
                    }
                }

                // Bias the autocomplete object to the user's geographical location,
                // as supplied by the browser's 'navigator.geolocation' object.
                function geolocate() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var geolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var circle = new google.maps.Circle({
                                center: geolocation,
                                radius: position.coords.accuracy
                            });
                            autocomplete.setBounds(circle.getBounds());
                        });
                    }
                }
            </script>
