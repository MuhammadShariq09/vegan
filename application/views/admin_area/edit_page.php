<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">COUPONS</h3>
            </div>
        </div>
        <div class="content-body">
            <!-- Zero configuration table -->
            <section id="ordering">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Edit Page</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <?php echo form_open_multipart("backend/pages/edit/".$id);?>
                                    <input type="hidden" name="page_id" id="page_id" value="<?=$id?>">   
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php if(isset($message) &&$message['message']) { ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?php $msg = $message; ?>
                                                        <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            <?php echo $msg['message']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                            ?>
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Page Name</label>
                                                            <input type="text" id="title" name="title" class="form-control border-primary" 
                                                            value="<?php echo set_value('title',$page['title']); ?>" placeholder="Page Name" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Page Slug</label>
                                                            <input type="text" id="slug" name="slug" class="form-control border-primary" 
                                                            value="<?php echo set_value('slug',$page['slug']); ?>" placeholder="Page Slug" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label >Status</label>
                                                            <select name="status" id="status" class="form-control border-primary">
                                                                <option value="0" <?php echo  set_select('status', '0',$page['status'] == "0"); ?>>Active</option>
                                                                <option value="1" <?php echo  set_select('status', '1',$page['status'] == "1"); ?>>Inactive</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Body</label>
                                                            <textarea id="body" name="body" class="form-control border-primary" placeholder="Page Body"><?php echo set_value('body',$page['body']); ?></textarea>
                                                        </div>                                                           
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                                <?php echo anchor(base_url().'pages', 'Cancel','class="btn btn-warning mr-1"');?>
                                                <?php echo form_submit(array('type'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary'));?>

                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <script>
            </script>
            <style>
                span.select2 {
                    width: 100% !important;
                }
            </style>

            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
            <script>
                try {
                  CKEDITOR.replace('body')
                } catch {}
                
                $(document).on('change', '#title', function() {
                  $.ajax({
                    url: base_url + 'backend/pages/place_slug',
                    data: {
                      title: $(this).val(),
                      id: $("#page_id").val()
                    },
                    type: 'post',
                    success: function(response) {
                      $("#slug").val(response);
                    }
                  })
                })

            </script>            
