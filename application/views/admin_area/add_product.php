<style media="screen">
.green-container{
  padding: 15px;
  border: 1px solid #00B5B8 !important;
}
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Products</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/products';?>">Products</a>
                        </li>
                        <li class="breadcrumb-item active">Add Product
                        </li>
                    </ol>
                </div>
            </div>
        </div>
<div class="content-body">
    <?php if(isset($message)){ ?>
        <div class="row">
            <div class="col-md-12">
                <?php $msg = $message; ?>
                <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $msg['message']; ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <section id="ordering">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add Product</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <?php echo form_open_multipart();?>
                            <input type="hidden" id="product_id" name="product_id" value="0">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Name *</label>
                                                    <input type="text" id="product_name" name="product_name" class="form-control border-primary" placeholder="Name" value="<?php echo set_value('product_name'); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Slug  *<i id="slug_loading" class="fa fa-spinner fa-spin" style="display:none;"></i></label>
                                                    <input type="text" id="product_slug" name="product_slug" class="form-control border-primary" placeholder="Slug" value="<?php echo set_value('product_slug'); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>S.K.U</label>
                                                    <input type="text" id="product_sku" name="product_sku" class="form-control border-primary" placeholder="S.K.U" value="<?php echo set_value('product_sku'); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Regular Price *</label>
                                                    <input type="number" id="reg_price" name="reg_price" class="form-control border-primary" placeholder="Sale Price" value="<?php echo set_value('reg_price'); ?>">
                                                </div>
                                                <div class="form-group">
                                                  <label>Sales Price *</label>
                                                  <input type="number" id="product_price" name="product_price" class="form-control border-primary" placeholder="Reguler Price" value="<?php echo set_value('product_price'); ?>" >
                                                </div>

                                                <div class="form-group" >
                                                    <label>Stock Qty *</label>
                                                    <input type="number" id="stock_qty" min="0" name="stock_qty" class="form-control border-primary" placeholder="Stock Qty" value="<?php echo set_value('stock_qty'); ?>">
                                                </div>

                                                <div class="col-md-12 form-group">
                                                    <label>Product Description *</label>
                                                    <textarea id="product_description" name="product_description" class="form-control border-primary" placeholder="Description"  rows="9"><?php echo set_value('product_description'); ?></textarea>
                                                </div>

                                                <div class="col-md-12 form-group">
                                                    <label>Product Specifications *</label>
                                                    <textarea id="product_specifications" name="product_specifications" class="form-control border-primary" placeholder="Product Specifications"  rows="9"><?php echo set_value('product_specifications'); ?></textarea>
                                                </div>

                                                <hr />
                                                <div class="form-group">
                                                    <label>Product Images *</label>
                                                    <input type="hidden" id="pi_b64" name="pi_b64">
                                                    <input type="file" accept='image/*' id="product_images" name="product_images" class="form-control border-primary" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <?php echo anchor(base_url().'backend/products', 'Cancel','class="btn btn-warning mr-1"');?>
                                        <button type="button" class="btn btn-primary" id="save_product">Save</button>
                                    </div>
                                </div>

                                <div style="padding-top:2%" class="col-md-6">
                                    <div class="form-group skin skin-square select_categories" style="padding: 15px; border: 1px solid #00B5B8 !important;">
                                        <label>Product Categories</label>
                                        <?php if($categories){?>
                                            <?php echo $categories;?>
                                        <?php }else{ ?>
                                            <div class="text-center" id="no_cat_found"><b>No Category Found!</b></div>
                                        <?php } ?>
                                    </div>
                                    <div class="green-container form-group">
                                      <div class="text-center">
                              				     <div id="upload-demo" style="width:350px"></div>
                              	  		</div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
