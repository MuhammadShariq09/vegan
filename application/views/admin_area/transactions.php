
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Transactions</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Transactions
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->


        <section id="ordering">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">All Transactions</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">

                        <table class="table table-striped table-bordered default-ordering">
                            <thead>
                                <tr>
                                    <th>Transaction ID</th>
                                    <th>Order Id</th>
                                    <th>Amount</th>
                                    <th>Paid By</th>
                                    <th>Transaction Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($transactions){?>
                                    <?php foreach ($transactions as $transaction){?>
                                        <tr>
                                            <td><?php echo $transaction['transactionid']; ?></td>
                                            <td><?php echo $transaction['OrderId']; ?></td>
                                            <td><?php echo $transaction['currencycode']; ?> <?php echo $transaction['amt']; ?></td>
                                            <td><?php echo $transaction['payment_type']; ?></td>
                                            <td><?php echo date('M d,Y',strtotime($transaction['date_added'] ));?></td>
                                            <td>
                                                <a class="badge badge-secondary" href="<?php echo base_url();?>backend/orders/view_order/<?php echo $transaction['OrderId']; ?>">View Order</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Transaction ID</th>
                                    <th>Order Id</th>
                                    <th>Amount</th>
                                    <th>Paid By</th>
                                    <th>Transaction Date</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
            <script>
                $('a#confirmbtn').confirm({
                    buttons: {
                        Ok: function(){
                            location.href = this.$target.attr('href');
                        },
                        Cancel: function(){

                        }
                    }
                });
            </script>
