<style media="screen">
.green-container{
  padding: 15px;
  border: 1px solid #00B5B8 !important;
}
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Brandings</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'products';?>">Brandings</a>
                        </li>
                        <li class="breadcrumb-item active">Update Brandings
                        </li>
                    </ol>
                </div>
            </div>
        </div>
<div class="content-body">
    <?php if(isset($message)){ ?>
        <div class="row">
            <div class="col-md-12">
                <?php $msg = $message; ?>
                <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $msg['message']; ?>
                </div>
            </div>
        </div>
    <?php } ?>
              <?php if ($this->session->flashdata('message')): ?>
                <div class="alert <?=$this->session->flashdata('message')['class']?>">
                  <?=$this->session->flashdata('message')['message']?>
                </div>
              <?php endif; ?>
    <section id="ordering">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Update Brandings</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <?php
                            $b = $brandings[0];
                            echo form_open_multipart(base_url().'backend/settings/updateBranding');?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Footer Text</label>
                                                    <input type="text" maxlength="30" required id="footer_col_title" name="footer_col_title"
                                                    class="form-control border-primary" placeholder="Footer Column Title"
                                                    value="<?php echo set_value('footer_col_title', $b['footer_col_title']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Section 2 Heading</label>
                                                    <input type="text"  maxlength="35" required id="fp_title" name="fp_title"
                                                    class="form-control border-primary" placeholder="Section 2 Heading"
                                                    value="<?php echo set_value('fp_title', $b['fp_title']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Section 2 Sub Heading</label>
                                                    <input type="text"  maxlength="35" required id="fp_subtitle" name="fp_subtitle"
                                                    class="form-control border-primary" placeholder="Section 2 Sub Heading"
                                                    value="<?php echo set_value('fp_subtitle', $b['fp_subtitle']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Facebook Link</label>
                                                    <input type="text" required id="fb_link" name="fb_link"
                                                    class="form-control border-primary" placeholder="Facebook Link"
                                                    value="<?php echo set_value('fb_link', $b['fb_link']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Twitter Link</label>
                                                    <input type="text" required id="tw_link" name="tw_link"
                                                    class="form-control border-primary" placeholder="Twitter Link"
                                                    value="<?php echo set_value('tw_link', $b['tw_link']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Youtube Link</label>
                                                    <input type="text" required id="lk_link" name="lk_link"
                                                    class="form-control border-primary" placeholder="Footer Column Title"
                                                    value="<?php echo set_value('lk_link', $b['lk_link']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Google+ Link</label>
                                                    <input type="text" required id="gp_link" name="gp_link"
                                                    class="form-control border-primary" placeholder="Footer Column Title"
                                                    value="<?php echo set_value('gp_link', $b['gp_link']); ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Instagram Link</label>
                                                    <input type="text" required id="inst_link" name="inst_link"
                                                    class="form-control border-primary" placeholder="Footer Column Title"
                                                    value="<?php echo set_value('inst_link', $b['inst_link']); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
