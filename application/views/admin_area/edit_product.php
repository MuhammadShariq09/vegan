<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-12 mb-1">
        <h3 class="content-header-title">Products</h3>
      </div>
      <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/dashboard';?>">Home</a>
            </li>
            <li class="breadcrumb-item"><a href="<?php echo base_url().'backend/products';?>">Products</a>
            </li>
            <li class="breadcrumb-item active">Add Product
            </li>
          </ol>
        </div>
      </div>
    </div>
    <div class="content-body">
      <?php if(isset($message)){ ?>
      <div class="row">
        <div class="col-md-12">
          <?php $msg = $message; ?>
          <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
            <?php echo $msg['message']; ?>
          </div>
        </div>
      </div>
      <?php } ?>
      <!-- Zero configuration table -->
      <?php //echo validation_errors(); ?><br />
      <?php //if(isset($message)){ ?>
      <?php //print_r($message);?>
      <?php //} ?>
      <section id="ordering">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Product</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
              </div>
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  <?php echo form_open_multipart();?>
                  <input type="hidden" id="product_id" name="product_id" value="<?php echo $product['id'];?>">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-body">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Name</label>
                              <input type="text" id="product_name" name="product_name" class="form-control border-primary" placeholder="Name" value="<?php echo (($_POST)?set_value('product_name'):$product['name']); ?>">
                            </div>
                            <div class="form-group">
                              <label>Slug <i id="slug_loading" class="fa fa-spinner fa-spin" style="display:none;"></i></label>
                              <input type="text" id="product_slug" name="product_slug" class="form-control border-primary" placeholder="Slug" value="<?php echo (($_POST)?set_value('product_slug'):$product['slug']); ?>">
                            </div>
                            <div class="form-group">
                              <label>S.K.U</label>
                              <input type="text" id="product_sku" name="product_sku" class="form-control border-primary" placeholder="S.K.U" value="<?php echo (($_POST)?set_value('product_sku'):$product['sku']); ?>">
                            </div>
                            <div class="form-group">
                              <label>Regualr Price </label>
                              <input type="number" id="reg_price" name="reg_price" class="form-control border-primary" placeholder="Sale Price" value="<?php echo (($_POST)?set_value('reg_price'):$product['reg_price']); ?>">
                            </div>
                            <div class="form-group">
                              <label>Sales Price</label>
                              <input type="number" id="product_price" name="product_price" class="form-control border-primary" placeholder="Sales Price" value="<?php echo (($_POST)?set_value('product_price'):$product['price']); ?>">
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group" id="stock_qty_wrapper">
                                  <label>Stock Qty</label>
                                  <input type="number" id="stock_qty" min="0" name="stock_qty" class="form-control border-primary" placeholder="Stock Qty" value="<?php echo (($_POST)?set_value('stock_qty'):$product['stock_qty']); ?>">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 form-group">
                              <label>Product Description</label>
                              <textarea id="product_description" name="product_description" class="form-control border-primary" placeholder="Description" rows="9"><?php echo (($_POST)?set_value('product_description'):$product['description']); ?></textarea>
                            </div>
                            <div class="col-md-12 form-group">
                              <label>Product Specifications</label>
                              <textarea id="product_specifications" name="product_specifications" class="form-control border-primary" placeholder="Product Specifications" 
                              rows="9"><?php echo (($_POST)?set_value('product_specifications'):$product['product_specifications']); ?></textarea>
                            </div>

                            <hr />
                            <div class="form-group">
                              <label>Product Images</label>
                              <input type="hidden" id="pi_b64" name="pi_b64">
                              <input type="file" accept="image/*" id="product_images" name="product_images" class="form-control border-primary" value="">
                            </div>
                          </div>
                        </div>
                      </div>

                      <?php if($images){?>
                      <div class="masonry-grid my-gallery " itemscope itemtype="http://schema.org/ImageGallery">
                        <!-- width of .grid-sizer used for columnWidth -->
                        <div class="grid-sizer"></div>
                        <div class="row">
                          <?php foreach($images as $img){?>
                            <div class="col-md-3 i<?=$img['id']?> grid-item">
                              <figure class="card border-grey border-lighten-2" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                <a href="<?php echo base_url('uploads/'.$img['image_path']);?>" itemprop="contentUrl" data-size="600x441">
                                  <img style="border: 1px solid black" class="gallery-thumbnail card-img-top" src="<?php echo base_url('uploads/'.$img['image_path']);?>" itemprop="thumbnail" alt="Image description" />
                                </a>
                                <a class="btn btn-danger" onclick="deleteProduct('<?=$img['id']?>')" href="#!">Del</a>
                              </figure>
                            </div>
                          <?php } ?>
                        </div>
                      </div>
                      <?php }else{?>
                      <div class="col-md-12 text-center">
                        <b>No Images Found!</b>
                      </div>
                      <?php } ?>


                      <div class="form-actions right">
                        <?php echo anchor(base_url().'backend/products', 'Cancel','class="btn btn-warning mr-1"');?>
                        <button type="button" class="btn btn-primary" id="save_product">Save</button>
                        <?php //echo form_submit(array('type'=>'submit', 'value'=>'Save', 'class'=>'btn btn-primary'));
                        ?>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group skin skin-square select_categories" style="padding: 15px; border: 1px solid #00B5B8 !important;">
                        <label>Product Categories</label>
                        <?php if($categories){?>
                        <?php echo $categories;?>
                        <?php }else{ ?>
                        <div class="text-center" id="no_cat_found"><b>No Category Found!</b></div>
                        <?php } ?>
                      </div>
                      <div class="green-container form-group">
                        <div class="text-center">
                             <div id="upload-demo" style="width:350px"></div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <?php echo form_close();?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <script type="text/javascript">
        $(document).on('change', '#product_name', function(){
          $.ajax({
            url: '<?=base_url()?>backend/products/place_slug',
            data: { name: $(this).val()},
            type: 'post',
            success: function(response){
              $("#product_slug").val(response);
            }
          })
        })

        function deleteProduct(id){
          if(id){
            $.ajax({
              url: '<?=base_url()?>backend/products/deleteImage/'+id,
              success: function(response){
                if(response.status == 200){
                  $(".i"+id).remove();
                }
              }
            })
          }
        }









      </script>
