<style media="screen">

</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <h3 class="content-header-title">Elements Settings</h3>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'dashboard';?>">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Elements Settings
                        </li>
                    </ol>
                </div>
            </div>
        </div>
<div class="content-body">
    <?php if(isset($message)){ ?>
        <div class="row">
            <div class="col-md-12">
                <?php $msg = $message; ?>
                <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $msg['message']; ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <section id="ordering">
      <div class="row">
          <div class="col-12">
              <div class="card">

                <?php if ($this->session->flashdata('message')): ?>
                  <div class="alert <?=$this->session->flashdata('message')['class']?>">
                    <?=$this->session->flashdata('message')['message']?>
                  </div>
                <?php endif; ?>

                  <div class="card-header">
                      <h4 class="card-title">
                      <?php
                        if($elements[0]->type == 0){
                           echo 'Below Banner';
                        }
                        else if($elements[0]->type == 1){
                           echo 'Above Feat. Products';
                        }
                        else{
                           echo 'Below Feat. Products';
                        }
                      ?>
                      </h4>
                      <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                      </div>
                  </div>
                  <div class="card-content collapse show">
                      <div class="card-body table-responsive card-dashboard">
                          <table class="table table-striped table-bordered  default-ordering">
                              <thead>
                                  <tr>
                                      <th>Image</th>
                                      <th>Title</th>
                                      <th>Subtitle</th>
                                      <th>Extra Text</th>
                                      <th>Button Text</th>
                                      <th>Button Link</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php foreach ($elements as $e){?>
                                      <tr>
                                         <td><img width="100" src="<?=base_url().'uploads/'.$e->image?>" alt=""></td>
                                          <td><?=$e->title?></td>
                                          <td><?=$e->subtitle?></td>
                                          <td><?=$e->on_hover_text?></td>
                                          <td><?=$e->btn_text?></td>
                                          <td><?=$e->btn_link?></td>
                                          <td>
                                              <a class="badge badge-secondary" href="<?php echo base_url();?>backend/settings/hompage_element_edit/<?php echo $e->id; ?>">Edit</a>
                                          </td>
                                      </tr>
                                  <?php } ?>
                              </tbody>
                              <tfoot>
                                  <tr>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Subtitle</th>
                                    <th>Extra Text</th>
                                    <th>Button Text</th>
                                    <th>Button Link</th>
                                    <th></th>
                              </tfoot>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
