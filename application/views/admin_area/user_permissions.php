
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">Users</h3>
          </div>
          <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-12">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Users</a>
                </li>
                <li class="breadcrumb-item active">User Permissions
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Zero configuration table -->
            <?php if($this->session->flashdata('message')) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php $msg = $this->session->flashdata('message'); ?>
                        <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo $msg['message']; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <section>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-colored-form-control">User Permissions (<?php echo $user->first_name . ' ' . $user->last_name;?> - <?php echo $user->email;?>)</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">

                        <?php echo form_open(); ?>
                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Permission</th>
                                                    <th>Allow</th>
                                                    <th>Deny</th>
                                                    <th>Ignore</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if($permissions) : ?>
                                                    <?php foreach($permissions as $k => $v) : ?>
                                                        <tr>
                                                            <td><?php echo $v['name']; ?></td>
                                                            <td><?php echo form_radio("perm_{$v['id']}", '1', set_radio("perm_{$v['id']}", '1', $this->ion_auth_acl->is_allowed($v['key'], $users_permissions))); ?></td>
                                                            <td><?php echo form_radio("perm_{$v['id']}", '0', set_radio("perm_{$v['id']}", '0', $this->ion_auth_acl->is_denied($v['key'], $users_permissions))) ?></td>
                                                            <td><?php echo form_radio("perm_{$v['id']}", 'X', set_radio("perm_{$v['id']}", 'X', ( $this->ion_auth_acl->is_inherited($v['key'], $users_permissions) || ! array_key_exists($v['key'], $users_permissions)) ? TRUE : FALSE)); ?> (Inherit <?php echo ($this->ion_auth_acl->is_inherited($v['key'], $group_permissions, 'value')) ? "Allow" : "Deny"; ?>)</td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                <?php else: ?>
                                                    <tr>
                                                        <td colspan="4">There are currently no permissions to manage, please add some permissions</td>
                                                    </tr>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions right">

                                <?php echo anchor(base_url().'users', 'Cancel','class="btn btn-warning mr-1"');?>

                                <?php echo form_submit('save', 'Save', array('class'=>'btn btn-primary'));?>
                            </div>
                        <?php echo form_close();?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

