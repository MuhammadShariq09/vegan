<section class="main-header" style="background-image:url(assets/images/gallery-2.jpg)">
  <header>
    <div class="container text-center">
      <h2 class="h2 title">Password Reset</h2>
      <ol class="breadcrumb breadcrumb-inverted">
        <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
        <li><a class="active" href="#">Password Reset</a></li>
      </ol>
    </div>
  </header>
</section>

<section class="login-wrapper login-wrapper-page">
  <div class="container">

    <header class="hidden">
      <h3 class="h3 title">Sign in</h3>
    </header>

    <div class="row">

      <!-- === left content === -->

      <div class="col-md-6 col-md-offset-3">
        <?php if ($this->session->flashdata('message')): ?>
          <div class="alert <?=$this->session->flashdata('message')['class']?>">
            <?=$this->session->flashdata('message')['message']?>
          </div>
        <?php endif; ?>
        <!-- === login-wrapper === -->
        <div class="login-wrapper">
          <div class="white-block">
            <!--signin-->
            <div class="login-block login-block-signup">
              <form action="<?=base_url()?>users/submitResetPassword" method="post">
              <input type="hidden" name="token" value="<?=$token?>">
              <input type="hidden" name="user_id" value="<?=$user_id?>">
              <div class="h4">Reset Password
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Enter New Password">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="password" name="confirm_password" class="form-control" placeholder="Confirm New Password">
                  </div>
                </div>
                <div class="col-xs-12 text-center">
                  <button type="submit" class="btn btn-main">Reset Password</button>
                </div>
              </div>
            </form>
            </div>

          </div>
        </div>
      </div>

    </div>

  </div>
</section>
