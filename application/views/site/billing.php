<!-- ========================  Main header ======================== -->

<section class="main-header" style="background-image:url(assets/images/gallery-2.jpg)">
  <header>
    <div class="container text-center">
      <h2 class="h2 title">Checkout</h2>
      <ol class="breadcrumb breadcrumb-inverted">
        <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
        <li><a href="<?=base_url().'checkout'?>">Cart items</a></li>
        <li><a class="active" href="<?=base_url().'billing'?>">Billings</a></li>
        <li><a href="<?=base_url().'payment'?>">Payment</a></li>
        <li><a href="#">Receipt</a></li>
      </ol>
    </div>
  </header>
</section>

<!-- ========================  Step wrapper ======================== -->

<div class="step-wrapper">
  <div class="container">

    <div class="stepper">
      <ul class="row">
        <li class="col-md-3 active">
          <span data-text="Cart items"></span>
        </li>
        <li class="col-md-3 active">
          <span data-text="Billings"></span>
        </li>
        <li class="col-md-3">
          <span data-text="Payment"></span>
        </li>
        <li class="col-md-3">
          <span data-text="Receipt"></span>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- ========================  Checkout ======================== -->

<section class="checkout">
  <div class="container">

    <div class="clearfix">
      <div class="row">
        <div class="col-xs-6">
          <a href="<?=base_url().'checkout'?>" class="btn btn-clean-dark"><span class="icon icon-chevron-left"></span> Back to cart</a>
        </div>
        <div class="col-xs-6 text-right">
          <a class="processBilling btn btn-main"><span class="icon icon-cart"></span> Go to payment</a>
        </div>
      </div>
    </div>


    <?php

    $first_name = $last_name = $bill_email = $phone = $c = $zip_code = $city = $address = '';
    if( $this->ion_auth->logged_in() ){
      $user_id = $this->session->userdata('user_id');
      $data = $this->Home_Model->selectWhere("users",["id" => $user_id]);
      $first_name = $data['first_name'];
      $last_name = $data['last_name'];
      $bill_email = $data['email'];
      $phone = $data['phone'];
      $c = $data['country'];
      $zip_code = $data['postcode'];
      $city = $data['city'];
      $address = $data['address_1'];
    }

    if($this->session->userdata('first_name'))
      $first_name = $this->session->userdata('first_name');
    if($this->session->userdata('last_name'))
      $last_name = $this->session->userdata('last_name');
    if($this->session->userdata('bill_email'))
      $bill_email = $this->session->userdata('bill_email');
    if($this->session->userdata('phone'))
      $phone = $this->session->userdata('phone');
    if($this->session->userdata('countries'))
      $c = $this->session->userdata('countries');
    if($this->session->userdata('zip_code'))
      $zip_code = $this->session->userdata('zip_code');
    if($this->session->userdata('city'))
      $city = $this->session->userdata('city');
    if($this->session->userdata('address'))
      $address = $this->session->userdata('address');
    ?>

    <div class="cart-wrapper">
      <div class="alert b-m hide alert-success text-center" role="alert">
      </div>
      <div class="alert hide alert-danger text-center" role="alert">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong class="a-msg"></strong>
      </div>
      <div class="note-block">
        <div class="row">
          <!-- === left content === -->
          <div class="col-md-6">
            <!-- === login-wrapper === -->
            <div class="login-wrapper">
              <div class="white-block">
                <div class="login-block login-block-signup">
                  <div class="h4">Billing Details </div>
                  <hr />
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" value="<?=$first_name?>" name="first_name" id="first_name" class="form-control" placeholder="First name: *">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" value="<?=$last_name?>" name="last_name" id="last_name" class="form-control" placeholder="Last name: *">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" value="<?=$bill_email?>" name="bill_email" id="bill_email" class="form-control" placeholder="Email: *">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" value="<?=$phone?>" name="phone" id="phone" class="form-control" placeholder="Phone: *">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <select class="form-control" id="countries" name="countries">
                          <option value="">Select Country</option>
                          <?php foreach ($countries as $key => $cont): ?>
                            <?php if ($c == $cont['code']): ?>
                              <option selected value="<?=$cont['code']?>"><?=$cont['name']?></option>
                            <?php else: ?>
                              <option value="<?=$cont['code']?>"><?=$cont['name']?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <input type="text" value="<?=$zip_code?>" id="zip_code" name="zip_code" class="form-control" placeholder="Zip code: *">
                      </div>
                    </div>
                    <div class="col-md-8">
                      <div class="form-group">
                        <input type="text" value="<?=$city?>" id="city" name="city" class="form-control" placeholder="City: *">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" value="<?=$address?>" id="address" name="address" class="form-control" placeholder="Address: *">
                      </div>
                    </div>

                    <?php if(!$this->ion_auth->logged_in() ): ?>
                      <div class="col-md-12">
                        <span class="checkbox">
                          <input type="checkbox" id="create_an_account" name="create_an_account">
                          <label for="create_an_account">Create an Account ?
                          </label>
                        </span>
                      </div>

                      <div class="col-md-12 cr_an_ac hide">
                        <div class="form-group">
                          <p>Create an account by entering the information below. If you are a returning customer please login at the top of the page.</p>
                          <label for="">Account Password</label>
                          <input type="password" value="" id="bill_password" name="bill_password" class="form-control" placeholder="*************">
                        </div>
                      </div>
                    <?php endif; ?>
                  </div>

                </div>
                <!--/signup-->
              </div>
            </div>
            <!--/login-wrapper-->
          </div>
          <!--/col-md-6-->
          <!-- === right content === -->
          <div class="col-md-6">
            <div class="white-block">
              <?php if(!$this->ion_auth->logged_in() ): ?>
              <div class="h4">Returning Customer ? Sign in here.</div>
              <hr />
              <div class="login-block login-block-signin">
                <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" value="" class="form-control" placeholder="User ID">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="password" value="" class="form-control" placeholder="Password">
                    </div>
                  </div>
                  <div class="col-xs-12 text-right">
                    <a href="#" class="btn btn-main">Login</a>
                  </div>
                </div>
              </div>
            <?php endif ?>
              <hr />
              <div class="clearfix">
                <p><strong>Order Notes:</strong></p>
                <textarea name="name" rows="3" class="form-control" cols="80"></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ========================  Cart wrapper ======================== -->

    <div class="cart-wrapper">
      <!--cart header -->
      <div class="cart-block cart-block-header clearfix">
        <div>
          <span>Product</span>
        </div>
        <div>
          <span>&nbsp;</span>
        </div>
        <div>
          <span>Quantity</span>
        </div>
        <div class="text-center">
          <span>Price</span>
        </div>
        <div class="text-right">
          <span>Total</span>
        </div>
      </div>
      <!--cart items-->
      <div class="clearfix">
        <?php foreach ($this->cart->contents() as $key => $value): ?>
          <?php
              $slug  = $this->Home_Model->selectWhere('products',['id' => $value['id'] ]);
              $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$value['id'] ], 'image_path');
              $path = base_url().'uploads/'.$images[0]['image_path'];
           ?>
          <div class="cart-block cart-block-item clearfix">
            <div class="">
              <a href="<?=$path?>"><img style="width:100px" src="<?=$path?>" alt="" /></a>
            </div>
            <div class="title">
              <div class="h5"><a href="<?=base_url().'products/'.$slug['slug']?>"><?=$value['name']?></a></div>
            </div>
            <div class="quantity">
              <strong><?=$value['qty']?></strong>
            </div>
            <div class="text-center">
              <span class="h5">$<?=$value['price']?></span>
            </div>
            <div class="text-right">
              <span class="h5">$<?=$value['price']*$value['qty']?></span>
            </div>
          </div>
        <?php endforeach; ?>

                    </div>
                    <div class="cart-block cart-block-footer clearfix">
                      <div>
                        <strong>Sub-total</strong>
                      </div>
                      <div>
                          $<span class="subtotal"><?=$this->cart->total();?></span>
                      </div>
                    </div>
                    <?php
                    $cs_total=0;
                    $total = $this->cart->total();
                    $cartS = $this->Home_Model->simpleSelect('cart_settings');
                    foreach ($cartS as $key => $cs): ?>
                      <div class="cart-block cart-block-footer clearfix">
                        <div>
                          <strong><?=$cs['name']?> ( <?=$cs['rate']?> %)</strong>
                        </div>
                        <div>
                          <?php $cs_total += $this->cart->total()*$cs['rate']/100?>
                          $<span class="cartSettings"><?=$this->cart->total()*$cs['rate']/100?></span>
                        </div>
                      </div>
                    <?php endforeach; ?>
                      <div class="cartFinal cart-block cart-block-footer cart-block-footer-price clearfix">
                      <div id="couponblock"></div>
                      <div>
                        <?php
                        $total = $cs_total+$total;
                        if ($this->session->userdata('promocode')){
                          $total = ($total) * $this->session->userdata('promocode')['disc'] / 100;
                        }
                        ?>
                        <div class="netTotal pull-right h2 title">$ <?=$total  ?></div>
                      </div>
                    </div>

                </div>

    <!-- ========================  Cart navigation ======================== -->
    <div class="clearfix">
      <div class="row">
        <div class="col-xs-6">
          <a href="<?=base_url().'checkout'?>" class="btn btn-clean-dark"><span class="icon icon-chevron-left"></span> Back to cart</a>
        </div>
        <div class="col-xs-6 text-right">
          <a class="processBilling btn btn-main"><span class="icon icon-cart"></span> Go to payment</a>
        </div>
      </div>
    </div>
  </div>
  <!--/container-->
</section>
