<section class="main-header main-header-blog" style="background-image:url(assets/images/gallery-1.jpg)">
        <header>
            <div class="container text-center">
                <h2 class="h2 title">Blog</h2>
                <ol class="breadcrumb breadcrumb-inverted">
                    <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
                    <li><a href="<?=base_url()?>blogs">Blogs</a></li>
                    <li><a class="active" href="<?=base_url()?>blogs/<?=$blog['blog_slug']?>"><?=$blog['blog_title']?></a></li>
                </ol>
            </div>
        </header>
    </section>

    <section class="blog">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                            <div class="blog-post">
                                <!-- === blog main image & entry info === -->
                                <div class="blog-image-main">
                                    <img src="<?=base_url()?>uploads/blogs/<?=$blog['featured_image']?>" alt="">
                                </div>

                                <div class="blog-post-content">

                                    <!-- === blog post title === -->

                                    <div class="blog-post-title">
                                        <h1 class="blog-title">
                                          <?=$blog['blog_title']?>
                                        </h1>

                                        <div class="blog-info blog-info-top">
                                            <div class="entry">
                                                <i class="fa fa-user"></i>
                                                <span><?=$blog['author']?></span>
                                            </div>
                                            <div class="entry">
                                                <i class="fa fa-calendar"></i>
                                                <span><?=$blog['created_at']?></span>
                                            </div>
                                        </div> <!--/blog-info-->
                                    </div>

                                    <!-- === blog post text === -->

                                    <div class="blog-post-text">
                                        <?=$blog['body']?>
                                    </div>

                                    <!-- === blog info === -->

                                    <div class="blog-info blog-info-bottom">
                                        <ul>
                                            <li class="divider"></li>
                                            <li>
                                                <i class="fa fa-tag"></i>
                                                <span>
                                                    <?php foreach (explode(',',$blog['keywords']) as $tags): ?>
                                                      <?php if ($tags): ?>
                                                        <a href="#"><?=$tags?></a>,
                                                      <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </span>
                                            </li>
                                        </ul>
                                    </div> <!--/blog-info-->
                                </div>

                                </div><!--blog-post-->
                        </div><!--col-sm-8-->
                    </div> <!--/row-->
                </div><!--/container-->
            </section>
