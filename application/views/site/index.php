
        <!-- ========================  Header content ======================== -->

        <section class="header-content">

            <div class="clearifx">

                <div class="owl-slider">
                    <!-- === slide item === -->
                    <?php foreach ($banners as $key => $b): ?>
                      <div class="item" style="background-image:url(<?php echo base_url().'uploads/hpbanners/'.$b['image'];?>)">
                        <div class="box box-left">
                          <h2 class="title animated h1" data-animation="fadeInDown"><?=$b['text']?></h2>
                        </div>
                      </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>

        <!-- ========================  Blog Block ======================== -->

        <section class="blog blog-block blog-intro">

            <div class="container">

                <!-- === blog header === -->

                <header class="hidden">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 text-center">
                            <h2 class="title">Check out our featured news</h2>
                            <div class="text">
                                <p>Keeping things minimal</p>
                            </div>
                        </div>
                    </div>
                </header>

                <div class="row row-clean top--boxs">
                    
                    <!-- === blog item === -->
                    <?php foreach ($elements as $key => $value): ?>
                      <?php if ($value->type == 0): ?>
                        <div class="col-md-4 col-sm-12">
                          <article>
                            <a href="<?=$value->btn_link?>">
                              <div class="image">
                                <img src="<?php echo base_url();?>uploads/<?=$value->image?>" alt="" />
                              </div>
                              <div class="entry entry-block">
                                <div class="date"><?=$value->title?> </div>
                                <div class="title">
                                  <h2 class="h3"><?=$value->subtitle?> </h2>
                                </div>
                                <div class="description">
                                  <p>
                                      <?=$value->on_hover_text?>
                                  </p>
                                </div>
                              </div>
                              <div class="show-more">
                                <span class="btn btn-clean"><?=$value->btn_text?></span>
                              </div>
                            </a>
                          </article>
                        </div>
                      <?php endif; ?>
                    <?php endforeach; ?>

                </div> <!--/row-->

                <!-- === button more === -->

                <div class="wrapper-more">
                    <a href="<?=base_url().'categories'?>" class="btn btn-lg">View all categories</a>
                </div>

            </div> <!--/container-->
        </section>

        <!-- ========================  Block banner category ======================== -->

        <section class="blog blog-block cntr-two">

            <!-- === header === -->
            <header class="hidden">
                <div class="text-center"><h2 class="title">Categories</h2></div>
            </header>
            <div class="row row-clean">
                <!-- === blog item === -->
                <?php foreach ($elements as $key => $value): ?>
                  <?php if ($value->type == 1): ?>
                    <div class="col-md-6">
                        <article>
                            <div>
                                <div class="image">
                                    <img src="<?php echo base_url();?>uploads/<?=$value->image?>" alt="" class="hidden-xs" />
                                </div>
                                <div class="entry entry-block">
                                    <div class="date"><?=$value->title?></div>
                                    <div class="title">
                                        <h2 class="h3"><?=$value->subtitle?></h2>
                                    </div>
                                    <div class="description">
                                      <?=$value->on_hover_text?>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                  <?php endif; ?>
                <?php endforeach; ?>

            </div> <!--/row-->
        </section>

        <section class="products">

            <!-- === header title === -->

            <header>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 text-center">
                        <h2 class="title"><?=$brandings[0]->fp_title?></h2>
                        <div class="text">
                            <p><?=$brandings[0]->fp_subtitle?></p>
                        </div>
                    </div>
                </div>
            </header>
            <div style="padding: 10px;" class="col-md-12">
              <div class="alert hide alert-success" role="alert">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong class="a-msg">Item is added to cart</strong>
              </div>
              <div class="alert hide alert-danger" role="alert">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong class="a-msg">Item can not be added to cart</strong>
              </div>
            </div>

            <div class="row row-clean">

                <?php $j=1; foreach ($products as $key => $pro): ?>
                    <?php if ($j<=6): ?>
                      <div class="col-xs-6 col-sm-4 col-lg-2">
                        <article>
                      <div class="info">
                          <span class="add-favorite">
                              <a href="javascript:void(0);" data-title="Add to favorites" data-title-added="Added to favorites list"><i class="icon icon-heart"></i></a>
                          </span>
                          <span>
                              <a href="#productid<?=$pro['id']?>" class="mfp-open" data-title="Quick wiew"><i class="icon icon-eye"></i></a>
                          </span>
                      </div>
                      <div data-id="<?= $pro['id']; ?>" class="atc_pdp btn btn-add">
                          <i class="icon icon-cart"></i>
                      </div>
                      <div class="figure-grid">
                          <span class="label label-danger">-<?=round(($pro['price']/$pro['reg_price'])*100,2)?>%</span>
                          <div class="image">
                              <a href="#productid<?=$pro['id']?>" class="mfp-open">
                                  <img src="<?=base_url().'uploads/'.$pro['images'][0]['image_path']?>" alt="" width="360">
                              </a>
                          </div>
                          <div class="text">
                              <h2 class="title h5">
                                 <a href="<?=base_url().$pro['slug']?>"><?=$pro['name']?></a>
                              </h2>
                            <?php if(trim($pro['price']) == trim($pro['reg_price'])): ?>
                                <sup>$ <?= $pro['price']; ?></sup>
                            <?php else: ?> 
                                <sub>$ <?= $pro['reg_price']; ?></sub>
                                <sup>$ <?= $pro['price']; ?></sup>
                            <?php endif ?> 
                          </div>
                      </div>
                  </article>
                      </div>
                    <?php endif; ?>
                <?php $j++; endforeach; ?>

              </div>
            <!--/row-->
            <!-- === button more === -->

            <div class="wrapper-more">
                <a href="<?=base_url().'products'?>" class="btn btn-lg">View all</a>
            </div>

            <!-- ========================  Product info popup - quick view ======================== -->
            <?php
              echo count($products);
             ?>

            <?php $i=1; foreach ($products as $key => $prod): ?>
                  <div class="popup-main mfp-hide" id="productid<?=$prod['id']?>">
                      <div class="product">
                          <div class="popup-title">
                              <div class="h1 title">
                                  <?=$prod['name']?>
                              </div>
                          </div>
                          <div class="owl-product-gallery">
                            <?php foreach ($prod['images'] as $key => $i): ?>
                              <img src="<?php echo base_url();?>uploads/<?=$i['image_path']?>" alt="" width="640" />
                            <?php endforeach; ?>
                          </div>
                          <div class="popup-content">
                              <div class="product-info-wrapper">
                                  <div class="row">
                                      <div class="col-sm-6">
                                          <div class="info-box">
                                              <strong>SKU</strong>
                                              <span><?=$prod['sku']?></span>
                                          </div>
                                          <div class="info-box">
                                              <strong>Availability</strong>
                                              <span><i class="fa fa-check-square-o"></i> <?=($prod['stock_qty']) ? "Yes": "No"?></span>
                                          </div>
                                      </div>
                                  </div> <!--/row-->
                              </div> <!--/product-info-wrapper-->
                          </div> <!--/popup-content-->
                          <!-- === product-popup-footer === -->
                          <div class="popup-table">
                              <div class="popup-cell">
                                  <div class="price">
                                    <?php if(trim($prod['price']) == trim($prod['reg_price'])): ?>
                                      <span class="h3">$ <?=$prod['price']?> </span>
                                    <?php else: ?> 
                                      <span class="h3">$ <?=$prod['price']?> <small>$ <?=$prod['reg_price']?></small></span>
                                    <?php endif ?> 
                                  </div>
                              </div>
                              <div class="popup-cell">
                                  <div class="popup-buttons">
                                      <a href="<?=base_url().'products/'.$prod['slug']?>"><span class="icon icon-eye"></span> <span class="hidden-xs">View more</span></a>
                                      <a class="atc_pdp" data-id="<?= $prod['id']; ?> href="javascript:void(0);"><span class="icon icon-cart"></span> <span class="hidden-xs">Buy</span></a>
                                  </div>
                              </div>
                          </div>
                      </div> <!--/product-->
                  </div> <!--popup-main--> <!--/container-->
            <?php endforeach; ?>


        </section>



        <section class="blog blog-block">

            <!-- === header === -->

            <header class="hidden">
                <div class="text-center"><h2 class="title">Banner</h2></div>
            </header>

            <div class="row row-clean">

                <!-- === blog item === -->
                <?php $i=1; foreach ($elements1 as $key => $value): ?>
                  <?php if ($value->type == 2): ?>
                    <div class="col-lg-6 col-md-12 home-main-center">
                      <img src="<?php echo base_url();?>uploads/<?=$value->image?>" class="full-img" alt="" />
                      <?php if ($i==1):  ?>
                        <div class="left1">
                          <h3><?=$value->title?><strong><?=$value->subtitle?></strong></h3>
                          <span><?=$value->percentoff?></span>
                          <div class="clearfix"></div>
                          <a href="<?=$value->btn_link?>"><?=$value->btn_text?></a>
                        </div>
                      <?php else: ?>
                        <div class="left2">
                          <span><?=$value->percentoff?></span>
                          <h3><?=$value->title?><strong><?=$value->subtitle?></strong></h3>
                          <div class="clearfix"></div>
                          <a href="<?=$value->btn_link?>"><?=$value->btn_text?></a>
                        </div>
                      <?php endif; ?>
                    </div>
                  <?php endif; ?>
                <?php $i++;  endforeach; ?>



            </div> <!--/row-->
        </section>


        <!-- ========================  Stretcher widget ======================== -->

        <section class="stretcher-wrapper">

            <!-- === stretcher header === -->

            <header>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 text-center">
                        <h1 class="h2 title"><?=$brandings[0]->fc_title?></h1>
                        <div class="text">
                            <p><?=$brandings[0]->fc_subtitle?></p>
                        </div>
                    </div>
                </div>
            </header>

            <!-- === stretcher === -->

            <ul class="stretcher">

                <!-- === stretcher item === -->

                <?php $i=1;foreach ($categories as $key => $value): ?>
                  <?php if ($i<=4): ?>
                    <li class="stretcher-item" style="background-image:url(<?php echo base_url();?>uploads/categories/thumb/<?=$value['image']?>);">
                        <!--logo-item-->
                        <div class="stretcher-logo">
                            <div class="text">
                                <span class="text-intro">
                                  <?=$value['name']?>
                                </span>
                            </div>
                        </div>
                        <!--main text-->
                        <figure class="fc">
                            <h4><?=$value['name']?></h4>
                            <figcaption><?=$value['description']?></figcaption>
                        </figure>
                        <!--anchor-->
                        <a href="<?=base_url()?>products/category/<?=$value['slug']?>">Anchor link</a>
                    </li>
                  <?php endif; $i++; ?>
                <?php endforeach; ?>


                <li class="stretcher-item more">
                    <div class="more-icon">
                        <span data-title-show="Show more" data-title-hide="+"></span>
                    </div>
                    <a href="<?=base_url()?>categories"></a>
                </li>

            </ul>
        </section>


        <section class="blog bottomm-blog blog-widget blog-animation">

            <!-- === blog header === -->

            <header>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 text-center">
                        <h1 class="h2 title"><?=$brandings[0]->bl_title?></h1>
                        <div class="text">
                            <p><?=$brandings[0]->bl_subtitle?></p>
                        </div>
                    </div>
                </div>
            </header>

            <div class="container">

                <div class="row">
                    <?php foreach ($blogs as $key => $b): ?>
                      <div class="col-sm-3">
                          <article>
                              <a href="<?=base_url().'blogs/'.$b['blog_slug']?>">
                                  <div class="image" style="background-image:url(<?=base_url()?>uploads/blogs/<?=$b['featured_image']?>)">
                                      <img src="<?=base_url()?>uploads/blogs/<?=$b['featured_image']?>" alt="" />
                                  </div>
                                  <div class="entry entry-table">
                                      <div class="date-wrapper">
                                          <div class="date">
                                            <span><?=date('M',strtotime($b['created_at']))?></span>
                                            <strong><?=date('d',strtotime($b['created_at']))?></strong>
                                            <span><?=date('Y',strtotime($b['created_at']))?></span>
                                          </div>
                                      </div>
                                      <div class="title">
                                          <h2 class="h5"><?=substr($b['blog_title'], 0, 46) . '...'?></h2>
                                      </div>
                                  </div>
                              </a>
                          </article>
                      </div>
                    <?php endforeach; ?>
                </div>
            </div>


            <div class="wrapper-more">
                <a href="<?=base_url().'blogs'?>" class="btn btn-lg">View all Blogs</a>
            </div> <!--/container-->
        </section>


        <!-- ========================  Banner ======================== -->

        <section class="banner bottom-story">

            <!-- === header === -->

            <h2 class="hidden">Banner</h2>

            <div class="container">
                <div class="row">
                  <div class="col-md-offset-2 col-md-8 text-center">
                  <h2 class="title">Testimonials</h2>
                    <div class="owl-slider">
                    <?php foreach ($testimonials as $key => $test): ?>
                      <div class="item">
                        <div class="box box-left">
                          <div class="text-center">
                            <h2 class="title animated h4" data-animation="fadeInDown"><?=$test['author']?></h2>
                            <div class="animated" data-animation="fadeInUp">
                                <?=$test['body']?>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
          </section>

        <!-- ========================  Instagram ======================== -->

        <section class="instagram">

            <!-- === instagram header === -->

            <header>
                <h2 class="h3 title">
                    Follow us<br />
                    <i class="fa fa-instagram fa-3x"></i> <br />
                    Instagram
                </h2>
                <div class="text">
                    <p>@LagerShop</p>
                </div>
            </header>

            <!-- === instagram gallery === -->

            <div class="gallery clearfix">
                <a class="item" href="#">
                    <img src="<?php echo base_url();?>assets/assets/images/insta_07.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                   <img src="<?php echo base_url();?>assets/assets/images/insta_09.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                  <img src="<?php echo base_url();?>assets/assets/images/insta_10.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                    <img src="<?php echo base_url();?>assets/assets/images/insta_11.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                   <img src="<?php echo base_url();?>assets/assets/images/insta_12.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                   <img src="<?php echo base_url();?>assets/assets/images/insta_13.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                   <img src="<?php echo base_url();?>assets/assets/images/insta_14.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                     <img src="<?php echo base_url();?>assets/assets/images/insta_15.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                     <img src="<?php echo base_url();?>assets/assets/images/insta_16.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                   <img src="<?php echo base_url();?>assets/assets/images/insta_17.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                    <img src="<?php echo base_url();?>assets/assets/images/insta_18.jpg" alt="Alternate Text" />
                </a>
                <a class="item" href="#">
                    <img src="<?php echo base_url();?>assets/assets/images/insta_19.jpg" alt="Alternate Text" />
                </a>
            </div> <!--/gallery-->

        </section>
