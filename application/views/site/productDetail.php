      <!-- ========================  Main header ======================== -->

        <section class="main-header text-center" style="background-image:url(assets/images/gallery-2.jpg)">
            <header>
                <div class="container">
                    <h1 class="h2 title">Product Details</h1>
                    <ol class="breadcrumb breadcrumb-inverted">
                        <li>
                            <a href="index.html"><span class="icon icon-home"></span></a>
                        </li>
                        <li>
                            <a href="<?=base_url()?>products">Product</a>
                        </li>
                        <li>
                            <a class="active" href="<?=base_url().$product['slug']?>">Product overview</a>
                        </li>
                    </ol>
                </div>
            </header>
        </section>

        <!-- ========================  Product ======================== -->
        <!-- ========================  Product ======================== -->

        <section class="product">
            <div class="main">
                <div class="container">
                    <div class="row product-flex">
                      <div class="alert hide alert-success" role="alert">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong class="a-msg">Item is added to cart</strong>
                      </div>
                      <div class="alert hide alert-danger" role="alert">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <strong class="a-msg">Item can not be added to cart</strong>
                      </div>
                        <!-- product flex is used only for mobile order -->
                        <!-- on mobile 'product-flex-info' goes bellow gallery 'product-flex-gallery' -->

                        <div class="col-md-4 col-sm-12 product-flex-info">
                            <div class="clearfix">
                               <input type="hidden" id="pid" value="<?=$product['id']?>">
                                <!-- === product-title === -->

                                <h1 class="title">
                                    <?=$product['name']?>
                                </h1>

                                <div class="clearfix">
                                    <!-- === price wrapper === -->
                                    <div class="price">
                                    <?php if(trim($product['price']) == trim($product['reg_price'])): ?>
                                        <span class="h3">
                                            $ <?=$product['price']?>
                                        </span>
                                    <?php else: ?> 
                                        <span class="h3">
                                            $ <?=$product['price']?>
                                            <small>$ <?=$product['reg_price']?></small>
                                        </span>
                                    <?php endif ?>

                                    </div>
                                    <hr />
                                    <!-- === info-box === -->
                                    <div class="info-box">
                                        <span><strong>SKU</strong></span>
                                        <span><?=$product['sku']?></span>
                                    </div>

                                    <!-- === info-box === -->

                                    <div class="info-box">
                                        <span><strong>Availability</strong></span>
                                        <?php if($product['stock_qty']) { ?>
                                          <span><i class="fa fa-check-square-o"></i> In stock</span>
                                        <?php } else { ?>
                                        <span class="hidden"><i class="fa fa-truck"></i> Out of stock</span>
                                      <?php } ?>
                                    </div>

                                      <div class="info-box">
                                          <span><strong>Rating</strong></span>
                                          <div class="rateit fixedrate" data-rate-value=<?=$rating?>></div>
                                      </div>
                                    <hr />


                                  <?php
                                      $fav = "";
                                      if($this->ion_auth->logged_in()){
                                        $user_id = $this->session->userdata('user_id');
                                        $this->db->where("user_id", $user_id);
                                        $data = $this->db->get("favourites")->result_array();
                                        if(count($data)){
                                          $arr = explode(',',$data[0]['product_id']);
                                            if (in_array($product['id'], $arr))
                                              $fav = "added";
                                            else
                                              $fav = "";
                                        }
                                        else
                                          $fav = "";
                                      }
                                      else{
                                        $fav = "";
                                      }
                                  ?>
                                  <div data-id="<?=$product['id']?>" class="atf<?=$product['id']?> info-box info-box-addto <?=$fav?>">
                                      <span>
                                          <i class="add"><i class="fa fa-heart-o"></i> Add to favorites</i>
                                          <i class="added"><i class="fa fa-heart"></i> Remove from favorites</i>
                                      </span>
                                  </div>
                                    <hr />

                                </div> <!--/clearfix-->
                            </div> <!--/product-info-wrapper-->
                        </div> <!--/col-md-4-->
                        <!-- === product item gallery === -->

                        <div class="col-md-8 col-sm-12 product-flex-gallery">

                            <!-- === add to cart === -->

                            <button type="button" data-id="<?=$product['id']?>" class="atc_pdp btn btn-buy" data-text="Buy"></button>


                            <!-- === product gallery === -->

                            <div class="owl-product-gallery open-popup-gallery">
                                <?php foreach ($product['images'] as $key => $value) { ?>
                                  <a href="<?=base_url().'uploads/'.$value['image_path']?>">
                                    <img src="<?=base_url().'uploads/'.$value['image_path']?>" alt="" height="500" />
                                  </a>
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- === product-info === -->

            <div class="info">
                <div class="container">
                    <div class="row">

                        <!-- === product-designer === -->

                        <div class="col-md-9">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#about" aria-controls="about" role="tab" data-toggle="tab">
                                        <i class="icon icon-history"></i>
                                        <span>About</span>
                                    </a>
                                </li>

                                <li role="presentation">
                                    <a href="#rating" aria-controls="rating" role="tab" data-toggle="tab">
                                        <i class="icon icon-thumbs-up"></i>
                                        <span>Rating</span>
                                    </a>
                                </li>
                            </ul>

                            <!-- === tab-panes === -->

                            <div class="tab-content">

                                <!-- ============ tab #1 ============ -->

                                <div role="tabpanel" class="tab-pane active" id="about">
                                    <div class="content">
                                        <h3>About this Item</h3>
                                        <p>
                                            <?=$product['description']?>
                                        </p>
                                    </div> <!--/content-->
                                </div> <!--/tab-pane-->


                                <div role="tabpanel" class="tab-pane" id="rating">

                                    <!-- ============ ratings ============ -->

                                    <div class="content">

                                        <h3>Rating</h3>
                                        <div class="row">
                                            <!-- === comments === -->
                                            <div class="col-md-12">
                                                <div class="comments">
                                                    <div style="padding-bottom: 20px;"></div>
                                                    <div class="comment-wrapper">
                                                        <!-- === comment === -->
                                                          <div id="loadfeedback"></div>
                                                    </div><!--/comment-wrapper-->

                                                    <div class="comment-add">

                                                        <div class="comment-reply-message">
                                                            <div class="h3 title">Leave a Reply </div>
                                                            <p>Your email address will not be published.</p>
                                                        </div>
                                                        <form>
                                                            <div class="form-group">
                                                              <label>Name *</label>
                                                              <input type="text" class="form-control" id="name" value="" placeholder="Your Name" />
                                                            </div>
                                                            <div class="form-group">
                                                              <label>Email *</label>
                                                              <input type="text" class="form-control" id="email" value="" placeholder="Your Email" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Rating *</label>
                                                                <div class="rateit rateitfeed" data-rate-value=5></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Message *</label>
                                                                <textarea rows="10" id="msg" class="form-control" placeholder="Your comment"></textarea>
                                                            </div>
                                                            <div class="clearfix text-center">
                                                                <a data-id="<?=$product['id']?>" class="addFeedback btn btn-main">Add comment</a>
                                                            </div>
                                                        </form>

                                                    </div><!--/comment-add-->
                                                </div> <!--/comments-->
                                            </div>


                                        </div> <!--/row-->
                                    </div> <!--/content-->
                                </div> <!--/tab-pane-->
                            </div> <!--/tab-content-->
                        </div>
                    </div> <!--/row-->
                </div> <!--/container-->
            </div> <!--/info-->
        </section>
