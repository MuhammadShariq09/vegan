<?php foreach ($feedback as $key => $value): ?>
  <div class="comment-block">
    <div class="comment-user">
      <div>
        <img src="http://localhost/gas-control-parts/assets/user.jpg" alt="Alternate Text" width="70">
      </div>
      <div>
        <h5>
          <span><?=$value['name']?></span>
          <div class="rateit" data-rate-value=<?=$value['review']?>></div>
          <small><?=date('Y-m-d',strtotime($value['created_at']))?></small>
        </h5>
      </div>
    </div>
    <!-- comment description -->
    <div class="comment-desc">
      <p>
          <?=$value['body']?>
      </p>
    </div>
  </div>

<?php endforeach; ?>
