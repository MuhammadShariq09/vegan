<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Mobile Web-app fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" href="<?php echo base_url();?>assets/favicon.ico" type="image/x-icon">

    <!-- Meta tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <!--Title-->
    <title><?=(isset($title)) ? $title : "Gas Control Parts"?></title>

    <!--CSS bundle -->
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/bootstrap.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/animate.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/font-awesome.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/linear-icons.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/magnific-popup.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/owl.carousel.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/ion-range-slider.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/theme.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/style.css" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/css/custom-style.css" />
    <link rel="stylesheet" href="https://sciactive.com/pnotify/src/pnotify.css">
    <link rel="stylesheet" href="https://sciactive.com/pnotify/src/pnotify.brighttheme.css">
    <link rel="stylesheet" href="http://auxiliary.github.io/rater/stylesheets/github-light.css">
    <!--Google fonts-->
   <link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700,800" rel="stylesheet">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!--<div class="page-loader"></div>-->

    <div class="wrapper">
