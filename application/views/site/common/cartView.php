
<div class="checkout">
    <div class="clearfix">
        <div class="row">
          <?php foreach ($this->cart->contents() as $key => $value): ?>
            <div class="cb<?=$value['rowid']?> cart-block cart-block-item clearfix">
                <div class="image">
                   <?php
                     $p = $this->Home_Model->selectWhere('products',['id' =>  $value['id']]);
                     $img = $this->Home_Model->selectWhere('images',['product_id' =>  $value['id']]);
                    ?>
                    <a href="<?=base_url()?>products/<?=$p['slug']?>">
                      <img style="width:80px; height:80px;" src="<?=base_url()?>uploads/<?=$img['image_path']?>" onerror="this.src='<?=base_url()?>/vegan/assets/images/product-1.png'" alt=""></a>
                </div>
                <div class="title">
                    <div><a href="#"><?=$value['name']?></a></div>
                </div>
                <div class="quantity">
                    <input type="number" data-id="<?=$value['rowid']?>" value="<?=$value['qty']?>" class="qty form-control form-quantity">
                    <small class="hide stock_alert" style="font-size:10px; color:red;"></small>
                </div>
                <div class="price">
                    <span class="final">$ <?=$value['price']?></span>
                </div>
                <!--delete-this-item-->
                <span data-id="<?=$value['rowid']?>" class="dfc icon icon-cross icon-delete"></span>
            </div>
          <?php endforeach; ?>
        </div>
        <hr>

        <hr>
        <?php if ($this->cart->contents()){ ?>
        <div class="clearfix">
          <div class="cart-block cart-block-footer clearfix">
              <div>
                  <strong>Sub-total</strong>
              </div>
              <div>
                  <span>$ <?=$this->cart->total()?></span>
              </div>
          </div>
          <?php
          $cs_total=0;
          $total = $this->cart->total();
          $cartS = $this->Home_Model->simpleSelect('cart_settings');
          foreach ($cartS as $key => $cs): ?>
            <div class="cart-block cart-block-footer clearfix">
              <div>
                <strong><?=$cs['name']?> (<?=$cs['rate']?>%)</strong>
              </div>
              <div>
                <?php $cs_total += $this->cart->total()*$cs['rate']/100?>
                $<span class="cartSettings"><?=$this->cart->total()*$cs['rate']/100?></span>
              </div>
            </div>
          <?php endforeach; ?>
          <?php
          if ($this->session->userdata('promocode')){
            $coupon = $this->session->userdata('promocode');
            ?>
            <div class="cart-block cart-block-footer clearfix">
              <div>
                <strong>Promo code <br><span class="green"><?=$coupon['code']?> (<?=$coupon['disc']?>%)</span></strong>
              </div>
              <div>
                <?php if ($this->session->userdata('promocode')['disc']): ?>
                  <div class="h4 title">$ <?=($total+$cs_total)*($this->session->userdata('promocode')['disc'])/100?></div>
                <?php else: ?>
                  <div class="h4 title">$ <?=($total+$cs_total)?></div>
                <?php endif; ?>
              </div>
            </div>
          <?php } else { ?>
              <div class="cart-block cart-block-footer clearfix">

                  <div>



                  </div>

                  <div>

                      <div class="h4 title">$ <?=($total+$cs_total)?></div>

                  </div>

              </div>
              <?php
          }
            ?>
          </div>


        <!--cart navigation -->
        <div class="cart-block-buttons clearfix">
            <div class="row">
                <div class="col-xs-6">
                    <a href="<?=base_url()?>products" class="btn btn-clean-dark">Continue shopping</a>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="<?=base_url()?>checkout" class="btn btn-main"><span class="icon icon-cart"></span> Checkout</a>
                </div>
            </div>
        </div>
        <?php } else{
            echo '<div class="cart-block-buttons clearfix">
            <div class="row">
                <div class="col-xs-6">
                    Your cart is empty.
                </div>
                <div class="col-xs-6 text-right">
                    
                </div>
            </div>
        </div>';
        } ?>

    </div>
</div>
