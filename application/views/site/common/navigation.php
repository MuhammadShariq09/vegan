<?php
$brandings = $this->db->query('select * from brandings')->result();

?>
        <!-- ======================== Navigation ======================== -->

        <nav>

            <div class="clearfix">

                <a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url();?>assets/assets/images/logo.png" alt="" /></a>

                <div class="navigation navigation-top clearfix">
                    <ul>
                        <!--add active class for current page-->
                        <li class="left-side"><a href="<?php echo base_url();?>" class="logo-icon"><img src="<?php echo base_url();?>assets/assets/images/icon.png" alt="Alternate Text" /></a></li>
                        <li class="left-side"><a href="<?=base_url().$brandings[0]->fb_link?>"><img src="<?php echo base_url();?>assets/assets/images/top-icon1.png" alt="" /></a></li>
                        <li class="left-side"><a href="<?=base_url().$brandings[0]->tw_link?>"><img src="<?php echo base_url();?>assets/assets/images/top-icon2.png" alt="" /></a></li>
                        <li class="left-side"><a href="<?=base_url().$brandings[0]->lk_link?>"><img src="<?php echo base_url();?>assets/assets/images/top-icon3.png" alt="" /></a></li>
                        <?php if( ! $this->ion_auth->logged_in() ): ?>
                          <li><a href="<?=base_url().'create-account'?>" ><i class="icon icon-user"></i></a></li>
                        <?php else: ?>
                          <li><a href="<?=base_url().'my-account'?>" ><i class="icon icon-user"></i></a></li>
                        <?php endif; ?>
                        <li><a href="javascript:void(0);" class="open-search"><i class="icon icon-magnifier"></i></a></li>
                        <li><a href="javascript:void(0);" class="open-cart"><i class="icon icon-cart"></i></a></li>
                    </ul>
                </div>

                <!-- ==========  Main navigation ========== -->

                <div class="navigation navigation-main">
                    <a href="#" class="open-search"><i class="icon icon-magnifier"></i></a>
                    <a href="#" class="open-cart"><i class="icon icon-cart"></i></a>
                    <a href="#" class="open-menu"><i class="icon icon-menu"></i></a>
                    <div class="floating-menu">
                        <!--mobile toggle menu trigger-->
                        <div class="close-menu-wrapper">
                            <span class="close-menu"><i class="icon icon-cross"></i></span>
                        </div>
                        <ul>
                            <li> <a href="<?php echo base_url(); ?>">Home </a> </li>
                            <li> <a href="<?php echo base_url(); ?>page/about-us">About Us </a> </li>
                            <li> <a href="<?php echo base_url(); ?>categories">Categories</a> </li>
                            <li> <a href="<?php echo base_url(); ?>products">Products</a> </li>
                            <li> <a href="<?php echo base_url(); ?>blogs">Blogs</a> </li>
                            <li> <a href="<?php echo base_url(); ?>contact">Contact Us</a> </li>
                        </ul>
                    </div>
                </div>

                <!-- ==========  Search wrapper ========== -->

                <div class="search-wrapper">
                    <form action="<?=base_url()?>product/search">
                      <input class="form-control" name="q" placeholder="Search..." />
                      <button onclik="this.form.submit()" class="btn btn-main">Go!</button>
                    </form>
                </div>

                <!-- ==========  Login wrapper ========== -->

                <div class="login-wrapper">
                    <div class="h4">Sign in</div>
                    <form>
                        <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <a href="#forgotpassword" class="open-popup">Forgot password?</a>
                            <a href="#createaccount" class="open-popup">Don't have an account?</a>
                        </div>
                        <button type="submit" class="btn btn-block btn-main">Submit</button>
                    </form>
                </div>

                <!-- ==========  Cart wrapper ========== -->
                <div id="cart-wrapper" class="cart-wrapper"></div>
            </div>
        </nav>
