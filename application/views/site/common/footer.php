<?php
$brandings = $this->db->query('select * from brandings')->result();

?>

        <!-- ================== Footer  ================== -->

        <footer>
            <!--footer showroom-->
            <div class="footer-showroom hidden">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>Visit our showroom</h2>
                        <p>200 12th Ave, New York, NY 10001, USA</p>
                        <p>Mon - Sat: 10 am - 6 pm &nbsp; &nbsp; | &nbsp; &nbsp; Sun: 12pm - 2 pm</p>
                    </div>
                    <div class="col-sm-4 text-center">
                        <a href="#" class="btn btn-clean"><span class="icon icon-map-marker"></span> Get directions</a>
                        <div class="call-us h4"><span class="icon icon-phone-handset"></span> 333.278.06622</div>
                    </div>
                </div>
            </div>

            <!--footer links-->
            <div class="footer-links">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <h5><?=$brandings[0]->footer_col_title?></h5>
                        <p class="text-justify">
                            <?=$brandings[0]->footer_col_body?>
                        </p>
                    </div>
                    <div class="col-md-offset-1 col-md-3">
                        <h5>Quick links</h5>
                        <ul>
                            <li><a href="<?=base_url().'page/about-us'?>">About Us</a></li>
                            <li><a href="<?=base_url().'blogs'?>">Blogs</a></li>
                            <li><a href="<?=base_url().'page/privacy-policy'?>">Privacy Policy</a></li>
                            <li><a href="<?=base_url().'page/refund-policy'?>">Refund Policy</a></li>
                            <li><a href="<?=base_url().'page/terms-and-conditions'?>">Terms and Conditions</a></li>
                            <li><a href="<?=base_url().'page/faqs'?>">FAQ</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <h5>Sign up for our newsletter</h5>
                        <p><i>Add your email address to sign up for our monthly emails and to receive promotional offers.</i></p>
                        <div class="form-group form-newsletter">
                            <input class="form-control" type="text" name="email" value="" placeholder="Email address" />
                            <input type="submit" class="btn btn-main btn-sm" value="Subscribe" />
                        </div>
                    </div>
                </div>
            </div>

            <!--footer social-->

            <div class="footer-social">
                <div class="row">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6 links">
                        <ul>
                            <li><a href="<?=$brandings[0]->fb_link?>"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="<?=$brandings[0]->tw_link?>"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="<?=$brandings[0]->gp_link?>"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="<?=$brandings[0]->lk_link?>"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="<?=$brandings[0]->inst_link?>"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

    </div> <!--/wrapper-->

    <!--JS files-->
    <script>var base_url = '<?php echo base_url() ?>';</script>
    <script>var is_logged = '<?php echo ($this->ion_auth->logged_in()) ? 1 : 0 ?>';</script>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.owl.carousel.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.ion.rangeSlider.js"></script>
    <script src="<?php echo base_url();?>assets/js/pnotify.custom.min.js"></script>
    <!--<script src="https://sciactive.com/pnotify/src/pnotify.js"></script>-->
    <!--<script type="text/javascript" src="https://sciactive.com/pnotify/src/pnotify.buttons.js"></script>-->
    <!--<link href="https://sciactive.com/pnotify/src/pnotify.buttons.css" rel="stylesheet" type="text/css" />-->
    <script src="http://auxiliary.github.io/rater/scripts/rater.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script src="<?php echo base_url();?>assets/js/checkout.js"></script>
    <script src="<?php echo base_url();?>assets/js/cart.js"></script>
    <script src="<?php echo base_url();?>assets/js/validation.js" charset="utf-8"></script>
    <script src="<?php echo base_url();?>assets/js/account.js" charset="utf-8"></script>
    <script src="<?php echo base_url();?>assets/js/feedback.js" charset="utf-8"></script>
    <script type="text/javascript">
      $(".rateit").rate();
      //or for example
      var options = {
          max_value: 5,
          step_size: 0.5,
      }
      $(".rateit").rate(options);

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4&libraries=places&callback=initAutocomplete"
                    async defer></script>
            <script>
                // This example displays an address form, using the autocomplete feature
                // of the Google Places API to help users fill in the information.

                // This example requires the Places library. Include the libraries=places
                // parameter when you first load the API. For example:
                // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                var placeSearch, autocomplete;
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                function initAutocomplete() {
                    // Create the autocomplete object, restricting the search to geographical
                    // location types.
                    autocomplete = new google.maps.places.Autocomplete(
                        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                        {types: ['geocode']});

                    // When the user selects an address from the dropdown, populate the address
                    // fields in the form.
                    autocomplete.addListener('place_changed', fillInAddress);
                }

                function fillInAddress() {
                    // Get the place details from the autocomplete object.
                    var place = autocomplete.getPlace();
                    console.log(place.geometry.location.lat());
                    console.log(place.geometry.location.lng());
                    for (var component in componentForm) {
                        try{
                            document.getElementById(component).value = '';
                            document.getElementById(component).disabled = false;
                        }catch(e){
                            
                        }    
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        console.log(addressType)
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType).value = val;
                        }
                    }
                }

                // Bias the autocomplete object to the user's geographical location,
                // as supplied by the browser's 'navigator.geolocation' object.
                function geolocate() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var geolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var circle = new google.maps.Circle({
                                center: geolocation,
                                radius: position.coords.accuracy
                            });
                            autocomplete.setBounds(circle.getBounds());
                        });
                    }
                }
            </script>

</body>

</html>
