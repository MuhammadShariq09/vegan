<section class="main-header text-center" style="background-image:url(assets/images/gallery-2.jpg)">
      <header>
          <div class="container">
              <h1 class="h2 title">Blogs</h1>
              <ol class="breadcrumb breadcrumb-inverted">
                  <li><a href="<?=base_url()?>"><span class="icon icon-home"></span>&nbsp;Home</a></li>
                  <li><a class="active" href="<?=base_url().'blogs'?>">Blog</a></li>
              </ol>
          </div>
      </header>
  </section>

  <section class="blog blog-category blog-animation">
              <div class="container">
                  <div class="pre-header hidden">
                      <div>
                          <h2 class="h3 title">
                              Category name
                          </h2>
                      </div>
                      <div>
                          <div class="sort-bar pull-right">
                              <div class="sort-results">
                                  <!--Items counter-->
                                  <span>Showing all <strong>50</strong> of <strong>3,250</strong> items</span>
                                  <!--Showing result per page-->
                                  <select>
                                      <option value="1">10</option>
                                      <option value="2">50</option>
                                      <option value="3">100</option>
                                      <option value="4">All</option>
                                  </select>
                                  <!--Grid-list view-->
                                  <span class="grid-list">
                                      <a href="blog-grid.html"><i class="fa fa-th-large"></i></a>
                                      <a href="blog-list.html"><i class="fa fa-align-justify"></i></a>
                                      <a href="javascript:void(0);" class="toggle-filters-mobile"><i class="fa fa-search"></i></a>
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-9">
                          <div class="row">
                              <?php foreach ($blogs as $key => $value): ?>
                                <div class="col-sm-6">
                                  <article>
                                      <a href="<?=base_url().'blogs/'.$value['blog_slug']?>">
                                          <div class="image" style="background-image:url(<?=base_url()?>uploads/blogs/<?=$value['featured_image']?>)">
                                              <img src="<?=base_url()?>uploads/blogs/<?=$value['featured_image']?>" alt="">
                                          </div>
                                          <div class="entry entry-table">
                                              <div class="date-wrapper">
                                                  <div class="date">
                                                      <span><?=date('M',strtotime($value['created_at']))?></span>
                                                      <strong><?=date('d',strtotime($value['created_at']))?></strong>
                                                      <span><?=date('Y',strtotime($value['created_at']))?></span>
                                                  </div>
                                              </div>
                                              <div class="title">
                                                  <h2 class="h5"><?=$value['blog_title']?></h2>
                                              </div>
                                          </div>
                                      </a>
                                  </article>
                              </div>
                              <?php endforeach; ?>
                          </div>
                          <div class="pagination-wrapper">
                              <?=$pagination?>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <aside>
                              <div class="box box-search">
                                  <form action="<?=base_url()?>blogs" method="get">
                                    <input type="text" name="q" class="form-control" placeholder="Search the blog">
                                    <button onclick="this.form.submit()" class="btn btn-main btn-sm">Go!</button>

                                  </form>
                              </div>
                              <div class="box box-posts">
                                  <h5 class="title">Recent Posts</h5>
                                  <ul>
                                      <?php foreach ($recent_blogs as $rec): ?>
                                        <li>
                                          <a href="<?=base_url().'blogs/'.$rec['blog_slug']?>">
                                            <span class="date">
                                              <span><?=date('M',strtotime($rec['created_at']))?></span>
                                              <span><?=date('d',strtotime($rec['created_at']))?></span>
                                            </span>
                                            <span class="text"><?=$rec['blog_title']?></span>
                                          </a>
                                        </li>
                                      <?php endforeach; ?>
                                  </ul>
                              </div>

                          </aside>
                      </div>


                  </div> <!--/row-->


              </div><!--/container-->
          </section>
