<!-- ========================  Main header ======================== -->

<section class="main-header" style="background-image:url(assets/images/gallery-2.jpg)">
  <header>
    <div class="container text-center">
      <h2 class="h2 title">Dashboard</h2>
      <ol class="breadcrumb breadcrumb-inverted">
        <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
        <li><a href="<?=base_url()?>">Home</a></li>
        <li><a class="active" href="<?=base_url()?>my-account">My Account</a></li>
      </ol>
    </div>
  </header>
</section>

<!-- ========================  Step wrapper ======================== -->
<div class="container">
    <div class="row">

        <div style="padding: 40px 0;" class="col-md-12">
            <h3>User Dashboard</h3>
            <!-- tabs left -->
            <div class="tabbable">
                <ul class="nav nav-pills nav-stacked col-md-3">
                    <li class="active"><a href="#my-profile" data-toggle="tab">My Profile</a></li>
                    <li ><a href="#order-history" data-toggle="tab">Order History</a></li>
                    <li ><a href="#my-favourites" data-toggle="tab">My Favourites</a></li>
                    <li ><a href="#logout" data-toggle="tab">Logout</a></li>
                </ul>
                <div class="tab-content col-md-9">
                    <div class="tab-pane active" id="my-profile">
                      <div class="panel panel-default">
                          <div class="panel-heading">Profile Settings</div>
                          <div class="panel-body">
                          <form id="updateProfile" class="form-horizontal">
                            <div class="col-md-6">
                              <div class="form-group" style="padding-right: 10px;">
                                <input type="text" name="first_name" value="<?=$profile['first_name']?>" class="form-control" placeholder="First name: ">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" name="last_name" value="<?=$profile['last_name']?>" class="form-control" placeholder="Last name: ">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group" style="padding-right: 10px;">
                                <input type="text" readonly value="<?=$profile['email']?>" class="form-control" placeholder="Email : ">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" name="phone" value="<?=$profile['phone']?>" class="form-control" placeholder="Phone : ">
                              </div>
                            </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <input type="password" name="password" value="" class="form-control" placeholder="Update Password or Leave Blank">
                                </div>
                              </div>

                              <div class="col-md-12">
                                <div class="form-group">
                                  <select class="form-control" name="country">
                                      <option value="">Select Country</option>
                                    <?php foreach ($countries as $key => $value): ?>
                                      <?php if ($profile['country'] == $value['code']): ?>
                                        <option selected value="<?=$value['code']?>"><?=$value['name']?></option>
                                      <?php else: ?>
                                        <option value="<?=$value['code']?>"><?=$value['name']?></option>
                                      <?php endif; ?>
                                    <?php endforeach; ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4" >
                                <div class="form-group" style="padding-right: 10px;">
                                  <input type="text" name="postcode" value="<?=$profile['postcode']?>" class="form-control" placeholder="Zip code: ">
                                </div>
                              </div>

                              <div class="col-md-4" >
                                <div class="form-group" style="padding-right: 10px;">
                                  <input type="text" name="city" value="<?=$profile['city']?>" class="form-control" placeholder="City: ">
                                </div>
                              </div>

                              <div class="col-md-4">
                                <div class="form-group">
                                  <select class="form-control" name="gender">
                                        <option value="">Select Gender</option>
                                      <?php foreach ($gender as $key => $value): ?>
                                        <?php if ($profile['gender'] == $key): ?>
                                          <option selected value="<?=$key?>"><?=$value?></option>
                                        <?php else: ?>
                                          <option value="<?=$key?>"><?=$value?></option>
                                        <?php endif; ?>
                                      <?php endforeach; ?>
                                  </select>
                                </div>
                              </div>

                              <div class="col-md-12">
                                <div class="form-group">
                                  <input type="text" name="address_1" value="<?=$profile['address_1']?>" class="form-control" placeholder="Address: ">
                                </div>
                              </div>
                              <button id="uppf" type="button" class="btn btn-main pull-right">Update</button>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="order-history">
                      <div class="panel panel-default">
                            <div class="panel-heading">Order History</div>
                          <div class="panel-body">
                          <div class="form-horizontal">
                            <div class="col-md-12">
                                <table class="table">
                                    <tr>
                                      <th>Order No</th>
                                      <th>Created At</th>
                                      <th>Amount</th>
                                      <th>Status</th>
                                      <th>View</th>
                                      <th>Action</th>
                                    </tr>
                                    <?php foreach ($orders as $key => $o): ?>
                                    <?php

                                    $status = array(
                                       '0' => 'Pending',
                                       '1' => 'Processing',
                                       '2' => 'On hold',
                                       '3' => 'Completed',
                                       '4' => 'Cancelled',
                                       '6' => 'Refunded',
                                       '7' => 'Failed',
                                    )
                                     ?>
                                     <tr>
                                       <td><?=$o['OrderID']?></td>
                                       <td><?=date('Y-m-d', strtotime($o['createdate']))?></td>
                                       <td><?=$o['OrderTotal']?></td>
                                       <td id="orderStatus<?=$o['OrderID']?>"><?=$status[$o['Status']]?></td>
                                       <td><a href="<?=base_url()?>order/order-details/<?=$o['OrderID']?>" class="btn btn-info">View</a></td>
                                       <?php if ($o['Status'] == 0): ?>
                                         <td><button id="cancelOrder<?=$o['OrderID']?>" type="button" onclick="cancelorder('<?=$o['OrderID']?>')" class="btn btn-danger">Cancel</a></td>
                                       <?php endif; ?>
                                     </tr>

                                    <?php endforeach; ?>
                                </table>
                            </div>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="my-favourites">
                      <div class="panel panel-default">
                            <div class="panel-heading">Favourites</div>
                          <div class="panel-body">
                          <form class="form-horizontal">
                            <div class="col-md-12">
                                <table class="table">
                                    <tr>
                                      <th>Image</th>
                                      <th>Product</th>
                                      <th>Remove</th>
                                      <th>View</th>
                                    </tr>
                                    <?php
                                      $fav = explode(',',$favourites['product_id']);
                                      foreach (array_filter($fav) as $key => $value) :
                                      $product = $this->Home_Model->selectWhere('products',['id' => $value]);
                                      $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$value], 'image_path');
                                     ?>
                                     <tr id="frow<?=$value?>">
                                       <td><img style="width:80px; height:80px;" src="<?php echo base_url(); ?>uploads/<?=$images[0]['image_path']?>" alt=""></td>
                                       <td><?=$product['name']?></td>
                                       <td><a data-id="<?=$value?>" class="rf btn btn-danger" >Remove</a></td>
                                       <td><a class="btn btn-info" href="<?=base_url().'products/'.$product['slug']?>">View</a></td>
                                     </tr>
                                   <?php endforeach ?>
                                </table>
                            </div>
                        </form>
                      </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="logout">
                      <div class="panel panel-default">
                          <div class="panel-body">
                            <h3>Hello <?=$profile['first_name'].' '.$profile['last_name']?></h3>
                            <p><a href="<?=base_url()?>/users/logout"><b>Click Here</b> </a> to logout</p>
                          </div>
                    </div>
                </div>
            </div>
            <!-- /tabs -->
        </div>

    </div>
    <!-- /row -->
</div>
</div>
