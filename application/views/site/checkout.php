<!-- ========================  Main header ======================== -->

<section class="main-header" style="background-image:url(assets/images/gallery-2.jpg)">
  <header>
      <div class="container text-center">
          <h2 class="h2 title">Checkout</h2>
          <ol class="breadcrumb breadcrumb-inverted">
            <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
            <li><a class="active" href="<?=base_url().'checkout'?>">Cart items</a></li>
            <li><a href="<?=base_url().'billing'?>">Billings</a></li>
            <li><a href="<?=base_url().'payment'?>">Payment</a></li>
            <li><a href="#">Receipt</a></li>
          </ol>
      </div>
  </header>
</section>


<div class="step-wrapper">
    <div class="container">

        <div class="stepper">
            <ul class="row">
                <li class="col-md-3 active">
                    <span data-text="Cart items"></span>
                </li>
                <li class="col-md-3">
                    <span data-text="Billings"></span>
                </li>
                <li class="col-md-3">
                    <span data-text="Payment"></span>
                </li>
                <li class="col-md-3">
                    <span data-text="Receipt"></span>
                </li>
            </ul>
        </div>
    </div>
</div>

<section class="checkout">

    <div class="container">

        <header class="hidden">
            <h3 class="h3 title">Checkout - Step 1</h3>
        </header>

        <!-- ========================  Cart wrapper ======================== -->

        <div class="cart-wrapper">
            <!--cart header -->

            <div class="cart-block cart-block-header clearfix">
                <div>
                    <span>Product</span>
                </div>
                <div>
                    <span>&nbsp;</span>
                </div>
                <div class="text-center">
                    <span>Quantity</span>
                </div>
                <div class="text-center">
                    <span>Price</span>
                </div>
                <div class="text-right">
                    <span>Total</span>
                </div>
            </div>

            <!--cart items-->

            <div class="clearfix">
              <?php foreach ($this->cart->contents() as $key => $value): ?>
                <?php
                    $slug  = $this->Home_Model->selectWhere('products',['id' => $value['id'] ]);
                    $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$value['id'] ], 'image_path');
                    $path = base_url().'uploads/'.$images[0]['image_path'];
                 ?>
                <div class="cb<?=$value['rowid']?> cart-block cart-block-item clearfix">
                    <div class="">
                        <a href="<?=$path?>">
                          <img style="width:100px" src="<?=$path?>" alt="" /></a>
                    </div>
                    <div class="title">
                        <div class="h5"><a href="<?=base_url().'products/'.$slug['slug']?>"><?=$value['name']?></a></div>
                    </div>
                    <div class="quantity">
                        <input style="width: 100px;" type="number" data-price="<?=$value['price']?>" data-id="<?=$value['rowid']?>" value="<?=$value['qty']?>" class="qty form-
                        rol form-quantity" />
                        <small class="hide stock_alert" style="font-size:10px; color:red;"></small>
                    </div>
                    <div class="text-center">
                        <span class="h5">$ <?=$value['price']?></span>
                    </div>
                    <div class="text-right">
                        <span class="total<?=$value['rowid']?> h5">$<?=$value['price']*$value['qty']?></span>
                    </div>
                    <!--delete-this-item-->
                    <span data-id="<?=$value['rowid']?>" class="dfc icon icon-cross icon-delete"></span>
                </div>
              <?php endforeach; ?>

            </div>
            <div class="cart-block cart-block-footer clearfix">
              <div>
                <strong>Sub-total</strong>
              </div>
              <div>
                  $<span class="subtotal"><?=$this->cart->total();?></span>
              </div>
            </div>
            <?php
            $cs_total=0;
            $total = $this->cart->total();
            $cartS = $this->Home_Model->simpleSelect('cart_settings');
            foreach ($cartS as $key => $cs): ?>
              <div class="cart-block cart-block-footer clearfix">
                <div>
                  <strong><?=$cs['name']?> ( <?=$cs['rate']?> %)</strong>
                </div>
                <div>
                  <?php $cs_total += $this->cart->total()*$cs['rate']/100?>
                  $<span class="cartSettings"><?=$this->cart->total()*$cs['rate']/100?></span>
                </div>
              </div>
            <?php endforeach; ?>
              <div class="cartFinal cart-block cart-block-footer cart-block-footer-price clearfix">
              <div id="couponblock"></div>
              <div>
                <?php
                $total = $cs_total+$total;
                if ($this->session->userdata('promocode')){
                  $total = ($total) * $this->session->userdata('promocode')['disc'] / 100;
                }
                ?>
                <div class="netTotal pull-right h2 title">$ <?=$total  ?></div>
              </div>
            </div>

        </div>

        <!-- ========================  Cart navigation ======================== -->

        <div class="clearfix">
            <div class="row">
                <div class="col-xs-6">
                    <a href="#" class="btn btn-clean-dark"><span class="icon icon-chevron-left"></span> Shop more</a>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="<?=base_url().'/billing'?>" class="btn btn-main"><span class="icon icon-cart"></span> Proceed to Billings</a>
                </div>
            </div>
        </div>

    </div> <!--/container-->

</section>
