<section class="main-header text-center" style="background-image:url(assets/images/gallery-2.jpg)">
      <header>
          <div class="container">
              <h1 class="h2 title"><?=$page->title?></h1>
              <ol class="breadcrumb breadcrumb-inverted">
                  <li><a href="<?=base_url()?>"><span class="icon icon-home"></span>&nbsp;Home</a></li>
                  <li><a class="active" href="<?=base_url().$page->slug?>"><?=$page->title?></a></li>
              </ol>
          </div>
      </header>
  </section>

  <section class="blog blog-category blog-animation">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                        <?=$page->body?>  
                      </div>

                  </div>

              </div> <!--/row-->

      </section>
