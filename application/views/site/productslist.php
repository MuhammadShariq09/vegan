<section class="main-header">
    <header>
        <div class="container">
            <h1 class="h2 title">All Products</h1>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
                <li><a class="active" href="<?=base_url()?>products">Product</a></li>
            </ol>
        </div>
    </header>
</section>

<!-- ======================== Products ======================== -->

<section class="products">
    <header class="hidden">
        <h3 class="h3 title">Product category grid</h3>
    </header>

    <div class="row row-clean">

        <!--product items-->

        <div class="col-md-12 col-xs-12">

            <div class="sort-bar clearfix">
              <div class="results_per_page sort-results pull-left">
                    <!--Showing result per page-->
                    <form method="get">
                      <select onchange="this.form.submit()" name="no_of_results">
                        <option <?=(@$_GET['no_of_results']) == 20 ? "selected" : '' ?> value="20">20</option>
                        <option <?=(@$_GET['no_of_results']) == 50 ? "selected" : '' ?> value="50">50</option>
                        <option <?=(@$_GET['no_of_results']) == 100 ? "selected" : '' ?> value="100">100</option>
                        <option <?=(@$_GET['no_of_results']) == $products_count ? "selected" : '' ?> value="<?=$products_count?>">All</option>
                    </select>
                    <!--Items counter-->
                    <span>Showing all <strong><?= $products_count; ?></strong> of <strong><?= $products_count; ?></strong> items</span>
                </div>
                <!--Sort options-->
                <div class="sort-options pull-right">
                    <span class="hidden-xs">Sort by</span>
                    <select onchange="this.form.submit()"  name="price">
                        <option <?=(@$_GET['price']) == "" ? "selected" : '' ?> value="">Default</option>
                        <option <?=(@$_GET['price']) == "low" ? "selected" : '' ?> value="low">Price: lowest</option>
                        <option <?=(@$_GET['price']) == "high" ? "selected" : '' ?> value="high">Price: highest</option>
                    </select>
                    </form>
                    <!--Grid-list view-->
                </div>
            </div>

            <div style="padding: 10px;" class="col-md-12">
              <div class="alert hide alert-success" role="alert">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong class="a-msg">Item is added to cart</strong>
              </div>
              <div class="alert hide alert-danger" role="alert">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong class="a-msg">Item can not be added to cart</strong>
              </div>
            </div>

            <div class="row row-clean">

                <?php foreach ($products as $product) : ?>

                <!-- === product-item === -->
                <div class="col-xs-6 col-sm-4 col-lg-3">
                    <article>
                        <div class="info">
                            <?php
                              $fav = "";
                              if($this->ion_auth->logged_in()){
                                $user_id = $this->session->userdata('user_id');
                                $this->db->where("user_id", $user_id);
                                $data = $this->db->get("favourites")->result_array();
                                if(count($data)){
                                  $arr = explode(',',$data[0]['product_id']);
                                  if (in_array($product['id'], $arr))
                                  $fav = "added";
                                }
                              }
                            ?>
                              <span data-id="<?= $product['id']; ?>" class="af<?= $product['id']; ?> <?=$fav?> add-favorite">
                                  <a href="javascript:void(0);" data-title="Add to favorites"
                                     data-title-added="Added to favorites list"><i
                                              class="icon icon-heart"></i></a>
                              </span>
                            <span>
                              <a href="#productid<?= $product['id']; ?>" class="mfp-open" data-title="Quick wiew"><i
                                          class="icon icon-eye"></i></a>
                          </span>
                        </div>
                        <div data-id="<?= $product['id']; ?>" class="atc_pdp btn btn-add">
                            <i class="icon icon-cart"></i>
                        </div>
                        <div class="figure-grid">
                            <div class="image">
                                <a href="#productid<?= $product['id']; ?>" class="mfp-open">
                                    <img src="<?php echo base_url(); ?>uploads/<?= $product['images'][0]['image_path']; ?>" alt=""
                                         width="360"/>
                                </a>
                            </div>
                            <div class="text">
                                <h2 class="title h4">
                                    <a href="<?=base_url()?>products/<?=$product['slug']?>"><?= $product['name']; ?></a>
                                </h2>
                                <?php if(trim($product['price']) == trim($product['reg_price'])): ?>
                                    <sup>$ <?= $product['price']; ?></sup>
                                <?php else: ?> 
                                    <sub>$ <?= $product['reg_price']; ?></sub>
                                    <sup>$ <?= $product['price']; ?></sup>
                                <?php endif ?> 
                                <span class="description clearfix"><?= $product['description']; ?></span>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="popup-main mfp-hide" id="productid<?= $product['id']; ?>">

                    <!-- === product popup === -->
                    <div class="product">

                        <!-- === popup-title === -->

                        <div class="popup-title">
                            <div class="h1 title">
                                <?= $product['name']; ?>
                                <small>slug: <?= $product['slug']; ?></small>
                            </div>
                        </div>

                        <!-- === product gallery === -->

                        <div class="owl-product-gallery">
                            <?php foreach ($product['images'] as $key => $image) { ?>
                                <img src="<?php echo base_url(); ?>uploads/<?= $image['image_path'] ?>" alt="" width="640"/>
                            <?php } ?>


                        </div>

                        <!-- === product-popup-info === -->

                        <div class="popup-content">
                            <div class="product-info-wrapper">
                                <div class="row">

                                    <!-- === left-column === -->

                                    <div class="col-sm-6">
                                        <div class="info-box">
                                            <strong>Availability</strong>
                                            <span><i class="fa fa-check-square-o"></i> in stock</span>
                                        </div>
                                    </div>
                                </div> <!--/row-->
                            </div> <!--/product-info-wrapper-->
                        </div> <!--/popup-content-->
                        <!-- === product-popup-footer === -->

                        <div class="popup-table">
                            <div class="popup-cell">
                                <div class="price">
                                    <?php if(trim($product['price']) == trim($product['reg_price'])): ?>
                                        <span class="h3">$ <?= $product['price']; ?></span>
                                    <?php else: ?> 
                                        <span class="h3">$ <?= $product['price']; ?> <small>$ <?= $product['reg_price']; ?></small></span>
                                    <?php endif ?> 
                                </div>
                            </div>
                            <div class="popup-cell">
                                <div class="popup-buttons">
                                    <a href="<?=base_url().'products/'.$product['slug']?>"><span class="icon icon-eye"></span> <span
                                                class="hidden-xs">View more</span></a>
                                    <a data-id="<?= $product['id']; ?>" class="atc_pdp" href="javascript:void(0);"><span class="icon icon-cart"></span> <span
                                                class="hidden-xs">Buy</span></a>
                                </div>
                            </div>
                        </div>

                    </div> <!--/product-->
                </div> <!--popup-main-->
                <?php endforeach; ?>

            </div><!--/row-->
            <!--Pagination-->
            <div class="pagination-wrapper">

                <ul class="pagination">
                  <?=@$pagination?>
                </ul>
            </div>

        </div> <!--/product items-->

    </div><!--/row-->


</section>
