
        <!-- ========================  Main header ======================== -->

        <section class="main-header" style="background-image:url(<?php echo base_url() ?>assets/assets/images/gallery-2.jpg)">
            <header class="hidden">
                <div class="container">
                    <h1 class="h2 title">Category</h1>
                </div>
            </header>
        </section>

        <!-- ========================  Products ======================== -->

        <section class="products main-header">
            <header>
                <div class="row ">
                    <div class="col-md-offset-2 col-md-8 text-center">
                        <h2 class="title">Product Categories</h2>
                        <div class="text">
                            <p>Select category</p>
                        </div>
                    </div>
                </div>
            </header>

            <div class="row row-clean">
                <?php foreach ($categories as $key => $category) { ?>
                    <!-- === product-item === -->
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <article>
                            <div class="figure-block cat-block">
                                <div class="image">
                                    <a href="<?php echo base_url() ?>products/category/<?= $category['slug']; ?>">
                                        <img src="<?php echo base_url() ?>uploads/categories/thumb/<?= $category['image']; ?>" alt="" width="360" />
                                    </a>
                                </div>
                                <div class="text">
                                    <h2 class="title h4"><a href="<?php echo base_url() ?>products/category/thumb/<?= $category['slug']; ?>">
                                        <?= substr($category['name'],0,34); ?><?=  (strlen($category['name']) > 34) ? "..." : ''  ?></a></h2>
                                    <span class="description clearfix"><?= $category['description']; ?></span>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>


            </div><!--/row--><!--/container-->
        </section>
