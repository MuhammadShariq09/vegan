<!-- ========================  Main header ======================== -->

<section class="main-header" style="background-image:url(assets/images/gallery-2.jpg)">
  <header>
    <div class="container text-center">
      <h2 class="h2 title">Checkout</h2>
      <ol class="breadcrumb breadcrumb-inverted">
        <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
        <li><a href="<?=base_url().'checkout'?>">Cart items</a></li>
        <li><a href="<?=base_url().'billing'?>">Billings</a></li>
        <li><a class="active" href="<?=base_url().'payment'?>">Payment</a></li>
        <li><a href="#">Receipt</a></li>
      </ol>
    </div>
  </header>
</section>

<!-- ========================  Step wrapper ======================== -->

<div class="step-wrapper">
  <div class="container">

    <div class="stepper">
      <ul class="row">
        <li class="col-md-3 active">
          <span data-text="Cart items"></span>
        </li>
        <li class="col-md-3 active">
          <span data-text="Billings"></span>
        </li>
        <li class="col-md-3 active">
          <span data-text="Payment"></span>
        </li>
        <li class="col-md-3">
          <span data-text="Receipt"></span>
        </li>
      </ul>
    </div>
  </div>
</div>


<!-- ========================  Checkout ======================== -->

<section class="checkout">
  <div class="container">

    <header class="hidden">
      <h3 class="h3 title">Checkout - Step 3</h3>
    </header>

    <!-- ========================  Cart navigation ======================== -->

    <div class="clearfix">
      <div class="row">
        <div class="col-xs-6">
          <a href="<?=base_url().'/billing'?>" class="btn btn-clean-dark"><span class="icon icon-chevron-left"></span> Back to Billings</a>
        </div>
      </div>
    </div>

    <!-- ========================  Payment ======================== -->

    <div class="cart-wrapper">

      <?php $message = $this->session->flashdata('message');
      if($message) { ?>
          <div class="row">
              <div class="col-md-12">
                  <?php $msg = $message; ?>
                  <div class="alert <?php echo $msg['class'] ?> alert-dismissible mb-2 ">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php echo $msg['message']; ?>
                  </div>
              </div>
          </div>
      <?php } ?>

      <div class="note-block">

        <div class="row">
          <!-- === left content === -->

          <div class="col-md-6">

            <div class="white-block">

              <div class="h4">Billing details</div>
              <hr />
              <div class="row">

                <div class="col-md-6">
                  <div class="form-group">
                    <strong>Name</strong> <br />
                    <span><?=$this->session->userdata('first_name')?>  <?=$this->session->userdata('last_name')?></span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <strong>Email</strong><br />
                    <span><?=$this->session->userdata('bill_email')?></span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <strong>Phone</strong><br />
                    <span><?=$this->session->userdata('phone')?></span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <strong>Country</strong><br />
                    <span><?=$this->Home_Model->selectWhere('countries',['code' => $this->session->userdata('countries')])['name']?></span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <strong>Zip</strong><br />
                    <span><?=$this->session->userdata('zip_code')?></span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <strong>City</strong><br />
                    <span><?=$this->session->userdata('city')?></span>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <strong>Address</strong><br />
                    <span><?=$this->session->userdata('address')?></span>
                  </div>
                </div>
              </div>

            </div>
            <!--/col-md-6-->

          </div>

          <!-- === right content === -->

          <div class="col-md-6">
            <div class="white-block">
              <form action="<?=base_url()?>shopping/process-payment" method="post" novalidate>
              <div class="h4">Choose payment</div>
              <hr />
              <div class="row">
                <span class="checkbox">
                  <input type="radio" class="pay_via" id="pay_via1" value="0" name="pay_via">
                  <label for="pay_via1">
                    <strong>Cash On Delivery</strong> <br />
                    <small>(Get your products delivered with cash on delivery service)</small>
                  </label>
                </span>
              </div>
              <div class="row">
                <span class="checkbox">
                  <input type="radio"  checked="checked" class="pay_via" id="pay_via2" value="1" name="pay_via">
                  <label for="pay_via2">
                    <strong>Pay with Credit Card</strong> <br />
                    <small>MasterCard, Maestro, Visa, Visa Electron, JCB and American Express)</small>
                  </label>
                </span>
              </div>
              <div class="payment-form">
                <div class="form-group">
                  <label class="black">CARD NUMBER</label>
                  <div class="input-group">
                      <input type="text" required maxlength="19" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.which == 8 || event.keyCode == 46' class="number form-control" name="cardNumber" placeholder="Valid Card Number" autocomplete="cc-number" required="" autofocus="">
                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-7 col-md-7">
                    <div class="form-group">
                      <label class="black"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                      <input type="text" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.which == 8 || event.keyCode == 46' required class="form-control expire" placeholder="MM / YYYY" name="cardExpiry"  autocomplete="cc-exp" required="">
                    </div>
                  </div>
                  <div class="col-xs-5 col-md-5 pull-right">
                    <div class="form-group">
                      <label class="black">CV CODE</label>
                      <input type="text" class="ccv form-control" required name="cardCVC" placeholder="CVC" maxlength="3" 
                      onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.which == 8 || event.keyCode == 46' autocomplete="cc-csc" required="">
                    </div>
                  </div>
                </div>
              </div>
                <div class="row">
                  <button type="submit" class="btn btn-main pull-right"><span class="icon icon-cart"></span> Checkout</button>
                </div>
              <hr/>
              <p>Please allow three working days for the payment confirmation to reflect in your <a href="#">online account</a>. Once your payment is confirmed, we will generate your e-invoice, which you can view/print from your account or email.</p>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>

    <!-- ========================  Cart wrapper ======================== -->

    <div class="cart-wrapper">
      <!--cart header -->
      <div class="cart-block cart-block-header clearfix">
        <div>
          <span>Product</span>
        </div>
        <div>
          <span>&nbsp;</span>
        </div>
        <div>
          <span>Quantity</span>
        </div>
        <div class="text-center">
          <span>Price</span>
        </div>
        <div class="text-right">
          <span>Total</span>
        </div>
      </div>
      <!--cart items-->
      <div class="clearfix">
        <?php foreach ($this->cart->contents() as $key => $value): ?>
          <?php
              $slug  = $this->Home_Model->selectWhere('products',['id' => $value['id'] ]);
              $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$value['id'] ], 'image_path');
              $path = base_url().'uploads/'.$images[0]['image_path'];
           ?>
          <div class="cart-block cart-block-item clearfix">
            <div class="">
              <a href="<?=$path?>"><img style="width:100px" src="<?=$path?>" alt="" /></a>
            </div>
            <div class="title">
              <div class="h5"><a href="<?=base_url().'products/'.$slug['slug']?>"><?=$value['name']?></a></div>
            </div>
            <div class="quantity">
              <strong><?=$value['qty']?></strong>
            </div>
            <div class="text-center">
              <span class="h5">$<?=$value['price']?></span>
            </div>
            <div class="text-right">
              <span class="h5">$<?=$value['price']*$value['qty']?></span>
            </div>
          </div>
        <?php endforeach; ?>

      </div>
      <!--cart final price -->
      <div class="clearfix">
        <div class="cart-block cart-block-footer clearfix">
          <div class="row pull-right">
              <div class="pull-right h2 title">$<?=$this->cart->total()?></div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix">
      <div class="row">
        <div class="col-xs-6">
          <a href="<?=base_url().'/billing'?>" class="btn btn-clean-dark"><span class="icon icon-chevron-left"></span> Back to Billings</a>
        </div>
      </div>
    </div>


  </div>
  <!--/container-->

</section>
