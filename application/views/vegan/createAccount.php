<section class="main-header" style="background-image:url(assets/images/gallery-2.jpg)">
  <header>
    <div class="container text-center">
      <h2 class="h2 title">Customer Account</h2>
      <ol class="breadcrumb breadcrumb-inverted">
        <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
        <li><a class="active" href="<?=base_url().'create-account'?>">Customer Account</a></li>
      </ol>
    </div>
  </header>
</section>

<section class="about-top1-sec  clearfix">
  <div class="container">



    <header class="hidden">
      <h3 class="h3 title">Sign in</h3>
    </header>

    <div class="row">

      <!-- === left content === -->

      <div class="col-md-6 col-md-offset-3">
        <?php if ($this->session->flashdata('message')): ?>
          <div class="alert <?=$this->session->flashdata('message')['class']?>">
            <?=$this->session->flashdata('message')['message']?>
          </div>
        <?php endif; ?>
        <!-- === login-wrapper === -->
        <div class="login-wrapper">
          <div class="white-block">
            <!--signin-->
            <div class="login-block login-block-forgetpass" style="display: none">
              <form action="<?=base_url()?>users/forgotpassword" method="post">
              <div class="h4">Forgot Password
                <a href="javascript:void(0);" class="btn btn-main btn-xs btn-register pull-right neww-btnn">
                Sign In</a>
              </div>
              <hr />
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="email" name="identity" class="form-control" placeholder="Enter Email Address">
                  </div>
                </div>
                <div class="col-xs-12 text-center">
                  <button type="submit" class="btn btn-main neww-btnn lg-btnn-new">Send Password Reset Link</button>
                </div>
              </div>
            </form>
            </div>

            <div class="login-block login-block-signin">
              <form action="<?=base_url()?>users/login" method="post">
              <div class="h4">Sign in
                <a href="javascript:void(0);" class="btn btn-main btn-xs btn-register pull-right neww-btnn">
                create an account</a>
                <a style="margin-right:10px" href="javascript:void(0);" class="btn btn-main btn-xs btn-forgotpassword pull-right neww-btnn">
                  Forgot Password ?</a>
              </div>
              <hr />
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="email" name="identity" class="form-control" placeholder="Email">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                  </div>
                </div>
                <div class="col-xs-6">
                  <span class="checkbox">
                        <input type="checkbox" id="checkBoxId3">
                        <label for="checkBoxId3">Remember me</label>
                    </span>
                </div>
                <div class="col-xs-6 text-right">
                  <button type="submit" class="btn btn-main  neww-btnn">Login</button>
                </div>
              </div>
            </form>
            </div>

            <!--/signin-->
            <!--signup-->
            <div class="login-block login-block-signup">
              <form  action="<?=base_url()?>users/create_customer" method="post">
              <div class="h4">Register now <a href="javascript:void(0);" class="btn btn-main btn-xs btn-login pull-right neww-btnn">Log in</a></div>
              <hr />

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="first_name" value="" class="form-control" placeholder="First name: *">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text"  name="last_name" value="" class="form-control" placeholder="Last name: *">
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="email" value="" class="form-control" placeholder="Email : *">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="password" name="password" value="" class="form-control" placeholder="Password: *">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="password" name="password_confirm" value="" class="form-control" placeholder="Confirm Password: *">
                  </div>
                </div>
                
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" id="autocomplete" autocomplete="off" onfocus="geolocate()" name="address_1" value="" class="form-control" placeholder="Address: *">
                  </div>
                </div>

                <!--<div class="col-md-12">-->
                <!--  <div class="form-group">-->
                <!--    <select class="form-control" name="country">-->
                <!--          <option value="">Select Country</option>-->
                <!--  </select>-->
                <!--  </div>-->
                <!--</div>-->
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" id="country" name="country" value="" class="form-control" placeholder="Country: *">
                  </div>
                </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" id="administrative_area_level_1" name="state" value="" class="form-control" placeholder="State: *">
                  </div>
                </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" id="locality" name="city"value="" class="form-control" placeholder="City: *">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" id="postal_code" name="postcode" value="" class="form-control" placeholder="Zip code: *">
                  </div>
                </div>


                <div class="col-md-12">
                  <span class="checkbox">
                        <input type="checkbox" name="agree_terms_and_conditions" id="checkBoxId1">
                        <label for="checkBoxId1">I have read and accepted the <a href="<?=base_url()?>page/terms-and-conditions">terms</a>, as well as read and understood our terms of
                          <a href="<?=base_url()?>page/terms-and-conditions">business conditions</a></label>
                    </span>
                  <hr />
                </div>
                <div class="col-md-12">
                  <button type="submit" class="btn btn-main btn-block neww-btnn lg-btnn-new">Create account</button>
                </div>
              </div>
              </form>
            </div>
            <!--/signup-->
          </div>
        </div>
        <!--/login-wrapper-->
      </div>
      <!--/col-md-6-->

    </div>

  </div>
</section>
