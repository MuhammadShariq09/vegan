<!--cart out start here-->
<div class="cart">
    <div class="container">
        <h1>Delivery</h1>
        <section class="checkout">

            <div class="container">

                <header class="hidden">
                    <h3 class="h3 title">Checkout - Step 1</h3>
                </header>

                <!-- ========================  Cart wrapper ======================== -->
                <div class="clearfix">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="<?=base_url().'checkout'?>" class="btn green-btn">   Back to cart</a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a class="btn btn-main green-btn processBilling"><span class="icon icon-cart"></span> Go to payment</a>
                        </div>
                    </div>
                </div>
                <?php

                $first_name = $last_name = $bill_email = $phone = $c = $zip_code = $city = $address = '';
                if( $this->ion_auth->logged_in() ){
                    $user_id = $this->session->userdata('user_id');
                    $data = $this->Home_Model->selectWhere("users",["id" => $user_id]);
                    $first_name = $data['first_name'];
                    $last_name = $data['last_name'];
                    $bill_email = $data['email'];
                    $phone = $data['phone'];
                    $c = $data['country'];
                    $zip_code = $data['postcode'];
                    $city = $data['city'];
                    $address = $data['address_1'];
                }

                if($this->session->userdata('first_name'))
                    $first_name = $this->session->userdata('first_name');
                if($this->session->userdata('last_name'))
                    $last_name = $this->session->userdata('last_name');
                if($this->session->userdata('bill_email'))
                    $bill_email = $this->session->userdata('bill_email');
                if($this->session->userdata('phone'))
                    $phone = $this->session->userdata('phone');
                if($this->session->userdata('countries'))
                    $c = $this->session->userdata('countries');
                if($this->session->userdata('zip_code'))
                    $zip_code = $this->session->userdata('zip_code');
                if($this->session->userdata('city'))
                    $city = $this->session->userdata('city');
                if($this->session->userdata('address'))
                    $address = $this->session->userdata('address');
                ?>

                <div class="cart-wrapper">
                    <div class="alert b-m hide alert-success text-center" role="alert">
                    </div>
                    <div class="alert hide alert-danger text-center" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong class="a-msg"></strong>
                    </div>
                    <div class="note-block">
                        <div class="row">

                            <!-- === left content === -->

                            <div class="col-md-6">

                                <!-- === login-wrapper === -->

                                <div class="login-wrapper">

                                    <div class="white-block">

                                        <!--signin-->

                                        <div class="login-block login-block-signin">

                                            <div class="h4">Sign in <a href="javascript:void(0);" class="btn btn-main btn-xs btn-register pull-right">create an account</a></div>

                                            <hr>

                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text" value="" class="form-control" placeholder="User ID">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="password" value="" class="form-control" placeholder="Password">
                                                    </div>
                                                </div>

                                                <div class="col-xs-6">
                                                    <span class="checkbox">
                                                        <input type="checkbox" id="checkBoxId3">
                                                        <label for="checkBoxId3">Remember me</label>
                                                    </span>
                                                </div>

                                                <div class="col-xs-6 text-right">
                                                    <a href="#" class="btn btn-main ">Login</a>
                                                </div>
                                            </div>
                                        </div> <!--/signin-->
                                        <!--signup-->

                                        <div class="login-block login-block-signup">

                                            <div class="h4">Register now <!--<a href="javascript:void(0);" class="login-btn btn btn-xs btn-login pull-right">Log in</a>--></div>

                                            <hr>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" value="<?=$first_name?>" name="first_name" id="first_name" class="form-control" placeholder="First name: *">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" value="<?=$last_name?>" name="last_name" id="last_name" class="form-control" placeholder="Last name: *">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <input type="text" value="<?=$bill_email?>" name="bill_email" id="bill_email" class="form-control" placeholder="Email: *">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" value="<?=$phone?>" name="phone" id="phone" class="form-control" placeholder="Phone: *">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select class="form-control" id="countries" name="countries">
                                                            <option value="">Select Country</option>
                                                            <?php foreach ($countries as $key => $cont): ?>
                                                                <?php if ($c == $cont['code']): ?>
                                                                    <option selected value="<?=$cont['code']?>"><?=$cont['name']?></option>
                                                                <?php else: ?>
                                                                    <option value="<?=$cont['code']?>"><?=$cont['name']?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" value="<?=$city?>" id="city" name="city" class="form-control" placeholder="City: *">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" value="<?=$zip_code?>" id="zip_code" name="zip_code" class="form-control" placeholder="Zip code: *">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" value="<?=$address?>" id="address" name="address" class="form-control" placeholder="Address: *">
                                                    </div>
                                                </div>

                                                <?php if(!$this->ion_auth->logged_in() ): ?>

                                                <div class="col-md-12">
                                                    <hr>
                                                    <span class="checkbox">
                                                        <input type="checkbox" id="create_an_account" name="create_an_account">
                                                        <label for="create_an_account">Create an Account ?</label>
                                                        <!--<label for="checkBoxId1">I have read and accepted the <a href="#">terms</a>, as well as read and understood our terms of
                                                            <a href="#">business contidions</a></label>-->


                                                    </span>
                                                    <!--<span class="checkbox">
                                                        <input type="checkbox" id="checkBoxId2">
                                                        <label for="checkBoxId2">Subscribe to exciting newsletters and great tips</label>
                                                    </span>
                                                    <hr>-->
                                                </div>
                                                    <div class="col-md-12 cr_an_ac hide">
                                                        <div class="form-group">
                                                            <p>Create an account by entering the information below. If you are a returning customer please login at the top of the page.</p>
                                                            <label for="">Account Password</label>
                                                            <input type="password" value="" id="bill_password" name="bill_password" class="form-control" placeholder="*************">
                                                        </div>
                                                    </div>
                                                <?php endif; ?>

                                                <!--<div class="col-md-12">
                                                    <a href="#" class="btn btn-main btn-block">Create account</a>
                                                </div>-->

                                            </div>
                                        </div> <!--/signup-->
                                    </div>
                                </div> <!--/login-wrapper-->
                            </div> <!--/col-md-6-->
                            <!-- === right content === -->

                            <div class="col-md-6">

                                <div class="white-block">

                                    <?php if(!$this->ion_auth->logged_in() ): ?>
                                        <div class="h4">Returning Customer ? Sign in here.</div>
                                        <hr />
                                        <div class="login-block login-block-signin">
                                            <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
                                            <form action="<?=base_url()?>users/login" method="post">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="email" name="identity" class="form-control" placeholder="Email / User ID">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="password" name="password" class="form-control" placeholder="Password">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 text-right">
                                                    <!--<a href="#" class="btn btn-main">Login</a>-->
                                                    <button type="submit" class="btn btn-main">Login</button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <hr />
                                    <div class="clearfix">
                                        <p><strong>Order Notes:</strong></p>
                                        <textarea name="name" rows="3" class="form-control" cols="80"></textarea>
                                    </div>

                                    <div class="clearfix">
                                        <p>A frequently overlooked, powerful fulfillment option is offering local pick-up. If you have a physical location and can allow your customers to forgo paying shipping costs altogether, you should!</p>                            <p><strong>Benefits:</strong></p>
                                        <ul>
                                            <li>Avoid both shipping and packaging costs</li>
                                            <li>Develop a face-to-face relationship with your customers</li>
                                            <li>Potential for additional purchases while customers are at your store</li>
                                        </ul>
                                        <p><strong>Challenges:</strong></p>
                                        <ul>
                                            <li>Limited business hours can sometimes make it difficult to coordinate pickup</li>
                                            <li>Shoppers who cross state lines or ZIP codes may not know the sales tax rates in your area</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="cart-wrapper">
                    <!--cart header -->

                    <div class="cart-block cart-block-header clearfix">
                        <div>
                            <span>Product</span>
                        </div>
                        <div>
                            <span>&nbsp;</span>
                        </div>
                        <div>
                            <span>Quantity</span>
                        </div>
                        <div class="text-right">
                            <span>Price</span>
                        </div>
                    </div>

                    <div class="cart-info">
                        <!--cart items-->

                        <div class="clearfix">
                            <?php foreach ($this->cart->contents() as $key => $value): ?>
                            <?php
                            $slug  = $this->Home_Model->selectWhere('products',['id' => $value['id'] ]);
                            $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$value['id'] ], 'image_path');
                            $path = base_url().'uploads/'.$images[0]['image_path'];
                            ?>

                            <div class="cart-block cart-block-item clearfix">
                                <div class="image">
                                    <a href="<?=$path?>"><img src="<?=$path?>" onerror="this.src='<?=base_url()?>/vegan/assets/images/cart-1.png'" alt=""></a>
                                </div>
                                <div class="title">
                                    <div class="h4"><a href="<?=base_url().'products/'.$slug['slug']?>"><?=$value['name']?></a></div>
                                    <!--<div>Electronics</div>-->
                                </div>
                                <div class="quantity">
                                    <!--<input type="number" value="2" class="form-control form-quantity">-->
                                    <strong><?=$value['qty']?></strong>
                                </div>
                                <div class="price">
                                    <span class="final h3">$ <?=$value['price']?></span>
                                    <!--<span class="discount">$ 190,00</span>-->
                                </div>
                                <!--delete-this-item-->

                            </div>
                            <?php endforeach; ?>
                        </div>

                        <!--cart prices -->
                        <div class="dis">
                            <div class="clearfix">
                                <div class="cart-block cart-block-footer clearfix">
                                    <div>
                                        <strong>Sub-total</strong>
                                    </div>
                                    <div>
                                        <span>$ <?=$this->cart->total();?></span>
                                    </div>
                                </div>
                                <?php
                                $cs_total=0;
                                $total = $this->cart->total();
                                $cartS = $this->Home_Model->simpleSelect('cart_settings');
                                foreach ($cartS as $key => $cs): ?>

                                <div class="cart-block cart-block-footer clearfix">
                                    <div>
                                        <strong><?=$cs['name']?> ( <?=$cs['rate']?> %)</strong>
                                    </div>
                                    <div>
                                        <?php $cs_total += $this->cart->total()*$cs['rate']/100?>
                                        <span>$ <?=$this->cart->total()*$cs['rate']/100?></span>
                                    </div>
                                </div>
                                <?php endforeach; ?>


                            </div>
                        </div>
                        <!--cart final price -->

                        <div class="clearfix">
                            <div class="cart-block cart-block-footer cart-block-footer-price clearfix">
                                <!--<div>
                                <span class="checkbox">
                                    <label for="couponCodeID" class="l1">Promo code include</label>
                                </span>
                                </div>
                                <div>
                                    <div class="h2 title">$ 1259,00</div>
                                </div>-->
                                <div id="couponblock"></div>
                                <div>
                                    <?php
                                    $total = $cs_total+$total;
                                    if ($this->session->userdata('promocode')){
                                        $total = ($total) * $this->session->userdata('promocode')['disc'] / 100;
                                    }
                                    ?>
                                    <div class="netTotal pull-right h2 title">$ <?=$total  ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ========================  Cart navigation ======================== -->
                <div class="de">
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="<?=base_url().'checkout'?>" class="btn green-btn"> Shop more</a>
                            </div>
                            <div class="col-xs-6 text-right">
                                <a class="processBilling btn btn-main green-btn"><span class="icon icon-cart"></span> Proceed to delivery</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--/container-->

        </section>


    </div>
</div>
