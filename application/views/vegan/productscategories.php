
     <!--   <div class="col-md-3 col-sm-12">


            <div id="tabs">
                <h5>Vegan Soul Food</h5>
                <ul>
                    <li><a href="#tab1">Caribbean Menu Selection</a></li>
                    <li><a href="#tab2">Oriental Menu</a></li>
                    <li><a href="#tab3">Spanish Menu</a></li>
                    <li><a href="#tab4">Italian Cuisine</a></li>
                    <li><a href="#tab5">Off the grill &amp; Fryer</a></li>
                    <li><a href="#tab6">Sandwiches</a></li>
                    <li><a href="#tab7">Deserts</a></li>
                    <li><a href="#tab8">Drinks</a></li>
                </ul>
            </div>


        </div>
        -->
    <div class="menu-second">
     <div class="top1-sec sec clearfix">
         <div class="top1-sec-sec-inner"></div>
         <div class="container">
             <h1 class="menu-h1">Our Menu</h1>

            <div id="">
                <div id="" class="row" style="display: block;">
                    <?php $i=0;?>
                    <?php foreach ($categories as $key => $category) { ?>

                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 menu-boxs">
                        <a href="<?php echo base_url() ?>products/category/<?= $category['slug']; ?>">
                            <img src="<?php echo base_url() ?>uploads/categories/thumb/<?= $category['image']; ?>" alt="" />
                        </a>
                        <h5><?= substr($category['name'],0,34); ?><?=  (strlen($category['name']) > 34) ? "..." : ''  ?></h5>
                        <p><?= substr($category['description'],0,100); ?><?=  (strlen($category['description']) > 100) ? "..." : ''  ?></p>
                    </div>
                        <?php $i++;?>
                        <?php
                        if($i%4==0)
                        {
                            echo '</div><div class="row" style="display: block;">';
                        }
                        ?>
                    <?php } ?>

                </div><!--  end tab1   -->
            </div><!--  end content   -->
        <!--right col end-->


</div><!--top1 sec end-->

     </div>
    </div>