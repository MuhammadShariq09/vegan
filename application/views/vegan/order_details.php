<!--cart out start here-->
<div class="cart">
    <div class="container">
        <h1>Receipt</h1>
        <section class="checkout">

            <div class="container">

                <header class="hidden">
                    <h3 class="h3 title">Checkout - Step 1</h3>
                </header>

                <!-- ========================  Cart wrapper ======================== -->
                <div class="clearfix">
                    <div class="row">
                        <div class="col-xs-6">
                            <span class="h2 title order-comp">Your order is completed!</span>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a onclick="window.print()" class="btn btn-main green-btn"><span class="icon icon-printer"></span> Print</a>
                        </div>
                    </div>
                </div>

                <!--           recipt start here     -->
                <div class="cart-wrapper">

                    <div class="note-block">

                        <div class="row">
                            <!-- === left content === -->

                            <div class="col-md-6">

                                <div class="white-block">

                                    <div class="h4">Shipping info</div>

                                    <hr>

                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Name</strong> <br>
                                                <span><?=$orders['billing_first_name'].' '.$orders['billing_last_name']?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Email</strong><br>
                                                <span><?=$orders['billing_email']?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Phone</strong><br>
                                                <span><?=$orders['billing_phone']?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Zip</strong><br>
                                                <span><?=$orders['billing_postcode']?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>City</strong><br>
                                                <span><?=$orders['billing_city']?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Country</strong><br>
                                                <span><?=$this->Home_Model->selectWhere('countries',['code' => $orders['billing_country']])['name']?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Address</strong><br>
                                                <span><?=$orders['billing_address_1']?></span>
                                            </div>
                                        </div>

                                        <!--<div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Company name</strong><br>
                                                <span>Mobel Inc</span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Company phone</strong><br>
                                                <span>+122 333 6665</span>
                                            </div>
                                        </div>-->

                                    </div>

                                </div> <!--/col-md-6-->

                            </div>

                            <!-- === right content === -->

                            <div class="col-md-6">
                                <div class="white-block">

                                    <div class="h4">Order details</div>

                                    <hr>

                                    <div class="row">


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Order no.</strong> <br>
                                                <span><?=$orders['OrderID']?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Transaction ID</strong> <br>
                                                <span><?=$payment_detail['transactionid']?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Order date</strong> <br>
                                                <span><?=date('Y-m-d', strtotime($orders['createdate']))?></span>
                                            </div>
                                        </div>

                                        <!--<div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Shipping arrival</strong> <br>
                                                <span>07/30/2017</span>
                                            </div>
                                        </div>-->

                                    </div>

                                    <div class="h4">Payment details</div>

                                    <hr>

                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Transaction Date</strong> <br>
                                                <span><?=date('Y-m-d', strtotime($payment_detail['paypal_timestamp']))?></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Amount</strong><br>
                                                <span>$ <?=$payment_detail['amt']?></span>
                                            </div>
                                        </div>



                                        <!--<div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Cart details</strong><br>
                                                <span>**** **** **** 5446</span>
                                            </div>
                                        </div>-->

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Items in cart</strong><br>
                                                <span><?=count($orderdetails)?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <strong>Payment Type</strong><br>
                                                <span><?=$payment_detail['payment_type']?></span>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--           recipt end here     -->
                <div class="cart-wrapper">
                    <!--cart header -->

                    <div class="cart-block cart-block-header clearfix">
                        <div>
                            <span>Product</span>
                        </div>
                        <div>
                            <span>&nbsp;</span>
                        </div>
                        <div>
                            <span>Quantity</span>
                        </div>
                        <div class="text-right">
                            <span>Price</span>
                        </div>
                    </div>

                    <div class="cart-info">
                        <!--cart items-->

                        <div class="clearfix">
                            <?php $subtotal=0; foreach ($orderdetails as $key => $value): ?>
                            <?php
                            $slug  = $this->Home_Model->selectWhere('products',['id' => $value['ProductID'] ]);
                            $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$value['ProductID'] ], 'image_path');
                            $path = base_url().'uploads/'.$images[0]['image_path'];
                            ?>

                            <div class="cart-block cart-block-item clearfix">
                                <div class="image">
                                    <a href="<?=$path?>"><img src="<?=$path?>" onerror="this.src='<?=base_url()?>/vegan/assets/images/cart-1.png'" alt=""></a>
                                </div>
                                <div class="title">
                                    <div class="h4"><a href="<?=base_url().'products/'.$slug['slug']?>"><?=$value['ProductName']?></a></div>
                                    <!--<div>Electronics</div>-->
                                </div>
                                <div class="quantity">
                                    <!--<input type="number" value="2" class="form-control form-quantity">-->
                                    <strong><?=$value['ProductQuantity']?></strong>
                                </div>
                                <div class="price">
                                    <span class="final h3">$ <?=$value['ProductPrice']?></span>
                                    <!--<span class="discount">$ 190,00</span>-->
                                </div>
                                <!--delete-this-item-->

                            </div>
                                <?php $subtotal+=$value['ProductQuantity']*$value['ProductPrice']; endforeach; ?>

                        </div>

                        <!--cart prices -->
                        <div class="dis">
                            <div class="clearfix">
                                <div class="cart-block cart-block-footer clearfix">
                                    <div>
                                        <strong>Sub Total</strong>
                                    </div>
                                    <div>
                                        <span>$ <?=$subtotal?></span>
                                    </div>
                                </div>
                                <?php
                                $cs_total=0;
                                $total = $orders['OrderTotal'];
                                if ($orders['cart_settings']) :
                                $settings = unserialize(($orders['cart_settings']));
                                foreach ($settings as $key => $cs) :
                                ?>
                                <div class="cart-block cart-block-footer clearfix">
                                    <div>
                                        <strong><?=$cs['name']?> ( <?=$cs['rate']?> %)</strong>
                                    </div>
                                    <div>
                                        <?php $cs_total += $total*$cs['rate']/100?>
                                        <span>$ <?=$total*$cs['rate']/100?></span>
                                    </div>
                                </div>
                                <?php endforeach; endif; ?>



                            </div>
                        </div>
                        <!--cart final price -->

                        <div class="clearfix">
                            <div class="cart-block cart-block-footer cart-block-footer-price clearfix">


                                <?php
                                if ($orders['coupon']):
                                    $coupon = unserialize($orders['coupon']);
                                    ?>
                                    <div class="pull-left">
                                        <label for="couponCodeID">Promo code <strong><?=$coupon['code']?></strong> Added</label>
                                        <span class="coupon-text-fix green bold">(<?=$coupon['disc']?>% Discount)</span>
                                    </div>
                                <?php endif; ?>

                                <div class="new-subtot">
                                    <div class="h2 title">$ <?=$payment_detail['amt']?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ========================  Cart navigation ======================== -->
                <div class="de">
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-xs-6">
                                <!--<a href="<?/*=base_url().'checkout'*/?>" class="btn green-btn">  Back to cart</a>-->
                            </div>
                            <div class="col-xs-6 text-right">
                                <a onclick="window.print()"  class="btn btn-main green-btn"><span class="icon icon-cart"></span> Print</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--/container-->

        </section>


    </div>
</div>
