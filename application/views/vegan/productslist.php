<section class="food-2-inner top1-sec sec">
    <div class="container">
        <div class="col-md-12">
            <h1>food</h1>
        </div>

        <div class="my-box">



            <section class="products">


                <div style="padding: 10px;" class="col-md-12">
                    <div class="alert hide alert-success" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong class="a-msg">Item is added to cart</strong>
                    </div>
                    <div class="alert hide alert-danger" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong class="a-msg">Item can not be added to cart</strong>
                    </div>
                </div>
                <div class="row row-clean">

                    <!-- === product-item === -->
                    <?php foreach ($products as $product) : ?>
                        <?php
                        $fav = "";
                        if($this->ion_auth->logged_in()){
                            $user_id = $this->session->userdata('user_id');
                            $this->db->where("user_id", $user_id);
                            $data = $this->db->get("favourites")->result_array();
                            if(count($data)){
                                $arr = explode(',',$data[0]['product_id']);
                                if (in_array($product['id'], $arr))
                                    $fav = "added";
                            }
                        }
                        ?>
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <!--<h2><?/*= $product['name']; */?></h2>-->
                            <h2><?= substr($product['name'],0,22); ?><?=  (strlen($product['name']) > 22) ? "..." : ''  ?></h2>
                            <span>
                            <?php if(trim($product['price']) == trim($product['reg_price'])): ?>
                                <sup>$ <?= $product['price']; ?></sup>
                            <?php else: ?>
                                <del>$ <?= $product['reg_price']; ?></del>
                                <sup>$ <?= $product['price']; ?></sup>
                            <?php endif ?>

                                <!--<del>$ 159</del>$139-->

                        </span>
                            <article>
                                <div class="info">
                            <span data-id="<?= $product['id']; ?>" class="af<?= $product['id']; ?> <?=$fav?> add-favorite">
                                <a href="javascript:void(0);" data-title="Add to favorites" data-title-added="Added to favorites list" data-id="<?= $product['id']; ?>" class="af<?= $product['id']; ?> <?=$fav?> add-favorite"><i class="icon icon-heart"></i></a>
                            </span>
                                    <span>
                                <a href="#productid<?= $product['id']; ?>" class="mfp-open" data-title="Quick wiew"><i class="icon icon-eye"></i></a>
                            </span>
                                </div>
                                <div class="">
                                    <a data-id="<?= $product['id']; ?>" class="atc_pdp btn btn-add" style="color: white;"><i class="icon icon-cart"></i></a>
                                </div>
                                <div class="figure-grid">
                                    <div class="image">
                                        <a href="#productid<?= $product['id']; ?>" class="mfp-open">
                                            <img src="<?php echo base_url(); ?>uploads/<?= $product['images'][0]['image_path']; ?>" onerror="this.src='<?=base_url('vegan/assets/images/food-3.png')?>'" class="img-responsive" alt="" width="360">
                                        </a>
                                    </div>

                                </div>
                            </article>
                        </div>
                        <!-- ========================  Product info popup - quick view ======================== -->
                        <div class="popup-main mfp-hide" id="productid<?= $product['id']; ?>">

                            <!-- === product popup === -->

                            <div class="product">

                                <!-- === popup-title === -->

                                <div class="popup-title">
                                    <div class="h1 title">  <?= $product['name']; ?>   </div>
                                </div>

                                <!-- === product gallery === -->

                                <div class="owl-product-gallery">
                                    <!--<img src="<?/*=base_url()*/?>/vegan/assets/images/food-10.png" alt="" width="640" />
                                <img src="<?/*=base_url()*/?>/vegan/assets/images/food-1.png" alt="" width="640" />-->
                                    <?php foreach ($product['images'] as $key => $image) { ?>
                                        <img src="<?php echo base_url(); ?>uploads/<?= $image['image_path'] ?>" onerror="this.src='<?=base_url('vegan/assets/images/food-3.png')?>'" alt="" width="640"/>
                                    <?php } ?>
                                </div>

                                <!-- === product-popup-info === -->

                                <div class="popup-content">
                                    <div class="product-info-wrapper">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="info-box">
                                                    <!--<h2>With or Without gravy</h2>-->
                                                    <p>
                                                        <?= substr($product['description'],0,100); ?><?=  (strlen($product['description']) > 100) ? "..." : ''  ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div> <!--/row-->
                                    </div> <!--/product-info-wrapper-->
                                </div> <!--/popup-content-->
                                <!-- === product-popup-footer === -->

                                <div class="popup-table">
                                    <div class="popup-cell">
                                        <div class="price">
                                            <?php if(trim($product['price']) == trim($product['reg_price'])): ?>
                                                <span class="h3">$ <?= $product['price']; ?></span>
                                            <?php else: ?>
                                                <span class="h3">$ <?= $product['price']; ?> <small>$ <?= $product['reg_price']; ?></small></span>
                                            <?php endif ?>
                                            <!--<span class="h3">$ 1999,00 <small>$ 2999,00</small></span>-->
                                        </div>
                                    </div>
                                    <div class="popup-cell">
                                        <div class="popup-buttons">
                                            <a href="<?=base_url().'products/'.$product['slug']?>"><span class="icon icon-eye"></span> <span class="hidden-xs">View more</span></a>
                                            <a data-id="<?= $product['id']; ?>" class="atc_pdp" href="javascript:void(0);"><span class="icon icon-cart"></span> <span class="hidden-xs">Buy</span></a>
                                        </div>
                                    </div>
                                </div>

                            </div> <!--/product-->
                        </div> <!--popup-main--> <!--/container-->
                        <!--popup-main--> <!--/container-->
                    <?php endforeach; ?>


                </div>





            </section>
        </div>

        <!--pagination start here-->


        <div class="row">
            <div class="col-md-12 text-center">
                <div class="pagi">
                    <ul class="pagination">
                        <?=@$pagination?>
                    </ul>
                </div>
            </div>
        </div>

        <!--pagination end here-->

    </div>
</section>
