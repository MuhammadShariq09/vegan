
<section class="contact">
    <div class="container">
        <div class="row"><div class="col-md-12"><h1>contact us</h1></div></div>


        <!-- === Goolge map === -->

        <div id="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.9147703544!2d-74.11976315700824!3d40.697403442216945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY!5e0!3m2!1sen!2s!4v1537256560714" width="100%" height="563" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>



        <div class="row">

            <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">

                <div class="contact-block">

                    <div class="contact-info">
                        <div class="row">
                            <div class="col-sm-4">
                                <figure class="text-center">
                                    <span class="icon icon-map-marker"></span>
                                    <figcaption>
                                        <strong>Where are we?</strong>
                                        <span>200 12th Ave, New York, <br />NY 10001, USA</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-sm-4">
                                <figure class="text-center">
                                    <span class="icon icon-phone"></span>
                                    <figcaption>
                                        <strong>Call us</strong>
                                        <span>
                                                    <strong>T</strong> +1 222 333 4444 <br />
                                                    <strong>F</strong> +1 222 333 5555
                                                </span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-sm-4">
                                <figure class="text-center">
                                    <span class="icon icon-clock"></span>
                                    <figcaption>
                                        <strong>Working hours</strong>
                                        <span>
                                                    <strong>Mon</strong> - Sat: 10 am - 6 pm <br />
                                                    <strong>Sun</strong> 12pm - 2 pm
                                                </span>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>

                    <div class="contact-info">
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10 text-center">
                                <h2 class="title">Send an email</h2>
                                <p>
                                    Thanks for your interest in Mobel Theme. We believe in creativity as one of the major forces of progress.
                                    Please use this form if you have any questions about our products and we'll get back with you very soon.
                                </p>

                                <div class="contact-form-wrapper">

                                    <a class="btn   open-form green-btn" data-text-open="Contact us via form" data-text-close="Close form">Contact us  </a>

                                    <div class="contact-form clearfix">
                                        <form action="#" method="post">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" value="" class="form-control" placeholder="Your name" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="email" value="" class="form-control" placeholder="Your email" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <input type="text" value="" class="form-control" placeholder="Subject" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea class="form-control" placeholder="Your message" rows="10"></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 text-center">
                                                    <input type="submit" class="btn btn-main" value="Send message" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div> <!--/contact-block-->
            </div><!--col-sm-8-->
        </div> <!--/row-->


    </div>
</section>
