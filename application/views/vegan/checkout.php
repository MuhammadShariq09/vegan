<!--cart out start here-->
<div class="cart">
    <div class="container">
        <h1>cart item</h1>
        <section class="checkout">

            <div class="container">

                <header class="hidden">
                    <h3 class="h3 title">Checkout - Step 1</h3>
                </header>

                <!-- ========================  Cart wrapper ======================== -->

                <div class="cart-wrapper">
                    <!--cart header -->

                    <div class="cart-block cart-block-header clearfix">
                        <div>
                            <span>Product</span>
                        </div>
                        <div>
                            <span>&nbsp;</span>
                        </div>
                        <div>
                            <span>Quantity</span>
                        </div>
                        <div class="text-right">
                            <span>Price</span>
                        </div>
                    </div>

                    <div class="cart-info">
                        <!--cart items-->

                        <div class="clearfix">
                            <?php foreach ($this->cart->contents() as $key => $value): ?>
                                <?php
                                $slug  = $this->Home_Model->selectWhere('products',['id' => $value['id'] ]);
                                $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$value['id'] ], 'image_path');
                                $path = base_url().'uploads/'.$images[0]['image_path'];
                                ?>
                            <div class="cb<?=$value['rowid']?> cart-block cart-block-item clearfix">
                                <div class="image">
                                    <a href="<?=$path?>"><img src="<?=$path?>" onerror="this.src='<?=base_url('vegan/assets/images/cart-1.png')?>'" alt=""></a>
                                </div>
                                <div class="title">
                                    <div class="h4"><a href="<?=base_url().'products/'.$slug['slug']?>"><?=$value['name']?></a></div>
                                    <!--<div>Electronics</div>-->
                                </div>
                                <div class="quantity">
                                    <input type="number" data-price="<?=$value['price']?>" data-id="<?=$value['rowid']?>" value="<?=$value['qty']?>" class="qty form-control form-quantity">
                                    <small class="hide stock_alert" style="font-size:10px; color:red;"></small>
                                </div>
                                <div class="price">
                                    <span class="final h3">$ <?=$value['price']?></span>
                                    <!--<span class="total<?/*=$value['rowid']*/?> discount">$ <?/*=$value['price']*$value['qty']*/?></span>-->
                                </div>
                                <span data-id="<?=$value['rowid']?>" class="dfc icon icon-cross icon-delete"></span>
                                <!--delete-this-item-->

                            </div>
                            <?php endforeach; ?>
                        </div>

                        <!--cart prices -->
                        <div class="dis">
                            <div class="clearfix">
                                <div class="cart-block cart-block-footer clearfix">
                                    <div>
                                        <strong>Sub Total</strong>
                                    </div>
                                    <div>
                                        <span class="subtotal">$ <?=$this->cart->total();?></span>
                                    </div>
                                </div>
                                <?php
                                $cs_total=0;
                                $total = $this->cart->total();
                                $cartS = $this->Home_Model->simpleSelect('cart_settings');
                                foreach ($cartS as $key => $cs): ?>
                                <div class="cart-block cart-block-footer clearfix">
                                    <div>
                                        <strong><?=$cs['name']?> ( <?=$cs['rate']?> %)</strong>
                                    </div>
                                    <div>
                                        <?php $cs_total += $this->cart->total()*$cs['rate']/100?>
                                        <span>$ <?=$this->cart->total()*$cs['rate']/100?></span>
                                    </div>
                                </div>
                                <?php endforeach; ?>

                                <!--<div class="cart-block cart-block-footer clearfix">
                                    <div>
                                        <strong>VAT</strong>
                                    </div>
                                    <div>
                                        <span>$ 59,00</span>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        <!--cart final price -->

                        <div class="clearfix">
                            <div class="cart-block cart-block-footer cart-block-footer-price clearfix">
                                <!--<div>
                                    <?php
/*                                    $total = $cs_total+$total;
                                    if ($this->session->userdata('promocode')){
                                        $total = ($total) * $this->session->userdata('promocode')['disc'] / 100;
                                    }
                                    */?>
                                <span class="checkbox">
                                    <input type="checkbox" id="couponCodeID">
                                    <label for="couponCodeID" class="ll">Promo code</label>
                                    <input type="text" class="form-control form-coupon" value="" placeholder="Enter your coupon code" style="display: none;">
                                </span>
                                </div>-->
                                <div id="couponblock"></div>
                                <div>
                                    <?php
                                    $total = $cs_total+$total;
                                    if ($this->session->userdata('promocode')){
                                        $total = ($total) * $this->session->userdata('promocode')['disc'] / 100;
                                    }
                                    ?>
                                    <div class="netTotal pull-right h2 title">$ <?=$total  ?></div>
                                </div>
                                <!--<div>
                                    <div class="netTotal pull-right h2 title">$ <?/*=$total  */?></div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ========================  Cart navigation ======================== -->
                <div class="de">
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="<?=base_url('categories')?>" class="btn green-btn">  Shop more</a>
                            </div>
                            <div class="col-xs-6 text-right">
                                <a href="<?=base_url('billing')?>" class="btn btn-main green-btn"><span class="icon icon-cart"></span> Proceed to delivery</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--/container-->

        </section>


    </div>
</div>
