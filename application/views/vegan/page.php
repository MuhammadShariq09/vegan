
<div class="about-top1-sec  clearfix">
    <div class="about-top1-sec-inner"></div>
    <div class="container">
        <div class="col-md-7 col-sm-12 left1">
            <?=$page->body?>

        </div><!--left col end-->

        <div class="col-md-5 col-sm-12 right1">
            <div class="box">
                <span class="top"></span>
                <span class="right"></span>
                <span class="bottom"></span>
                <span class="left"></span>
                <img src="<?=base_url()?>/vegan/assets/images/about.png" class="img-responsive" alt="" />
            </div>

        </div><!--right col end-->
    </div><!--container end-->

</div><!--top1 sec end-->
