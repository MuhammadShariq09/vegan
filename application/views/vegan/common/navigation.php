<div id="menu" class="clearfix">
    <ul>
        <li><a href="<?=base_url()?>"  class="active">Home</a></li>
        <li><a href="<?=base_url('page/about-us')?>">About Us</a></li>
        <li><a href="<?=base_url('categories')?>">Menu</a></li>
        <li><a href="#">blog</a></li>
        <li><a href="<?=base_url('contact')?>">Contact Us</a></li>
    </ul>



    <nav>
        <ul class="end">
            <?php if( ! $this->ion_auth->logged_in() ): ?>
            <li><a href="<?=base_url('create-account')?>"><i class="icon icon-user"></i> </a></li>
            <?php else: ?>
            <li><a href="<?=base_url('my-account')?>"><i class="icon icon-user"></i> </a></li>
            <?php endif;?>
            <li><a href="javascript:void(0);" class="open-cart"><i class="icon icon-cart"></i> <!--<span>4</span>--></a></li>
        </ul>
        <div class="cart-wrapper new-cartt-open" id="cart-wrapper">
        </div>
    </nav>



</div>
