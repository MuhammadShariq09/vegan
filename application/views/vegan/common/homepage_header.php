<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Vegan Day And Night</title>
    <link rel="shortcut icon" href="images/favicon.ico" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>/vegan/assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>/vegan/assets/css/custom-styles.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>/vegan/assets/css/slicknav.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>/vegan/assets/linear-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Barlow:200,300,400,500,600,700,800" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<div class="top-banner clearfix"> <img src="<?=base_url()?>/vegan/assets/images/elements_new1.png" data-animate-scroll='{"x": "-300", "y": "-200", "scaleX": "0.85","scaleY": "0.85", "alpha": "0", "duration": "2","rotationY":"720","rotationX":"45","rotation":"45","z":"-30"}' class="elmnt-new1" alt="" /> <img src="<?=base_url()?>/vegan/assets/images/elements_new2.png" data-animate-scroll='{"x": "-300", "y": "-200", "scaleX": "0.85","scaleY": "0.85", "alpha": "0", "duration": "1","rotationY":"720","rotationX":"45","rotation":"45","z":"-30"}' class="elmnt-new2" alt="" /> <img src="<?=base_url()?>/vegan/assets/images/elements_new3.png" data-animate-scroll='{"x": "-300", "y": "-200", "scaleX": "0.85","scaleY": "0.85", "alpha": "0", "duration": "3","rotationY":"720","rotationX":"45","rotation":"45","z":"-30"}' class="elmnt-new3" alt="" /> <img src="<?=base_url()?>/vegan/assets/images/elements_new4.png" data-animate-scroll='{"x": "-300", "y": "-200", "scaleX": "0.85","scaleY": "0.85", "alpha": "0", "duration": "3","rotationY":"720","rotationX":"45","rotation":"45","z":"-30"}' class="elmnt-new4" alt="" /> <img src="<?=base_url()?>/vegan/assets/images/elements_new5.png" data-animate-scroll='{"x": "-300", "y": "-200", "scaleX": "1","scaleY": "0.85", "alpha": "0", "duration": "2","rotationY":"720","rotationX":"45","rotation":"45","z":"-30"}' class="elmnt-new5" alt="" /> <img src="<?=base_url()?>/vegan/assets/images/elements_06.png" data-animate-scroll='{"x": "300", "y": "-200", "scaleX": "1","scaleY": "0.85", "alpha": "0", "duration": "1","rotationY":"720","rotationX":"45","rotation":"45","z":"-30"}'  class="elmnt2" alt="" /> <img src="<?=base_url()?>/vegan/assets/images/elements_07.png" data-animate-scroll='{"x": "300", "y": "-200", "scaleX": "1","scaleY": "0.85", "alpha": "0", "duration": "2","rotationY":"720","rotationX":"45","rotation":"45","z":"-30"}' class="elmnt03" alt="" /> <img src="<?=base_url()?>/vegan/assets/images/elements_09.png" data-animate-scroll='{"x": "300", "y": "-200", "scaleX": "1","scaleY": "0.85", "alpha": "0", "duration": "3","rotationY":"720","rotationX":"45","rotation":"45","z":"-30"}' class="elmnt3" alt="" /> <img src="<?=base_url()?>/vegan/assets/images/logo.png" data-animate-scroll='{"scaleX": "1.2","scaleY": "1.2", "alpha": "0", "duration": "4"}'class="logo" alt="" /> </div>
<!--top banner end-->
