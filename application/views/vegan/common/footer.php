<?php
$brandings = $this->db->query('select * from brandings')->result();

?>
<div class="footer clearfix">
    <div class="container"> <img src="<?=base_url()?>/vegan/assets/images/footer-logo.png" class="footer-logo" alt="" />
        <ul>
            <li><a href="<?=base_url()?>">Home</a></li>
            <li><a href="<?=base_url('page/about-us')?>">About Us</a></li>
            <li><a href="<?=base_url('categories')?>">Menu</a></li>
            <li><a href="#">blog</a></li>
            <li><a href="<?=base_url('contact')?>">Contact Us</a></li>
        </ul>
        <div class="footer-social">
          <a href="<?=$brandings[0]->fb_link?>"><i class="fa fa-facebook"></i></a>
          <a href="<?=$brandings[0]->tw_link?>"><i class="fa fa-twitter"></i></a>
          <a href="<?=$brandings[0]->gp_link?>"><i class="fa fa-google-plus"></i></a>
          <a href="<?=$brandings[0]->lk_link?>"><i class="fa fa-youtube"></i></a>
          <a href="<?=$brandings[0]->inst_link?>"><i class="fa fa-instagram"></i></a>
      </div>
        <div class="clearfix"></div>
        <div class="footer-bottom clearfix">
            <p><?=$brandings[0]->footer_col_title?></p>
        </div>
    </div>
    <!--container end-->

</div>
<!--footer end-->

<?php $this->load->view('vegan/common/footer_files');?>
</body>
</html>
