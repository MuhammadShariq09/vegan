<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Vegan Day And Night- Menu</title>
    <link rel="shortcut icon" href="<?=base_url()?>/vegan/assets/images/favicon.ico" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>/vegan/assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>/vegan/assets/css/custom-styles.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>/vegan/assets/css/slicknav.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Barlow:200,300,400,500,600,700,800" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


    <link rel="stylesheet" href="https://sciactive.com/pnotify/src/pnotify.css">
    <link rel="stylesheet" href="https://sciactive.com/pnotify/src/pnotify.brighttheme.css">
    <link rel="stylesheet" href="http://auxiliary.github.io/rater/stylesheets/github-light.css">



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>/vegan/assets/linear-icons.css">
    <link rel="stylesheet" media="all" href="<?=base_url()?>/vegan/assets/css/animate.css" />
    <link rel="stylesheet" media="all" href="<?=base_url()?>/vegan/assets/css/magnific-popup.css"/>
    <link rel="stylesheet" media="all" href="<?=base_url()?>/vegan/assets/css/owl.carousel.css" />


</head>
<body>
<div class="top-banner-inner clearfix">



    <img src="<?=base_url()?>/vegan/assets/images/logo.png" class="logo" alt="" />



</div><!--top banner end-->
