
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?=base_url()?>/vegan/assets/js/main.js"></script>
<script src="<?=base_url()?>/vegan/assets/js/animate-scroll.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.1/js/foundation.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/plugins/ScrollToPlugin.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/plugins/EaselPlugin.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

--><script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>/vegan/assets/js/jquery.slicknav.min.js"></script>
<script src="<?=base_url()?>/vegan/assets/js/jquery.magnific-popup.js"></script>
<script src="<?=base_url()?>/vegan/assets/js/jquery.owl.carousel.js"></script>
<script src="<?=base_url()?>/vegan/assets/js/jquery.ion.rangeSlider.js"></script>


<script type="text/javascript">
    $(function () {
        $('#menu').slicknav();
        $(document).ready(function(){
            $("#content > div").hide(); // Initially hide all content
            $("#tabs li:first").attr("id","current"); // Activate first tab
            $("#content > div:first").fadeIn(); // Show first tab content
            $('#tabs li a').click(function(e) {
                e.preventDefault();
                if ($(this).attr("id") == "current"){ //detection for current tab
                    return
                }
                else{
                    $("#content > div").hide(); //Hide all content
                    $("#tabs li").attr("id",""); //Reset id's
                    $(this).parent().attr("id","current"); // Activate this
                    $( $(this).attr('href')).fadeIn(); // Show content for current tab
                }
            });
        });
    });
</script>
<script>
    $(document).foundation();
    $(document).animateScroll();
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
</script>





