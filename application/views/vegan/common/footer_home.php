<div class="footer clearfix">
    <div class="container"> <img src="<?=base_url()?>/vegan/assets/images/footer-logo.png" class="footer-logo" alt="" />
        <ul>
            <li><a href="index.html">Home</a></li>
            <li><a href="about.html">About Us</a></li>
            <li><a href="menu.html">Menu</a></li>
            <li><a href="blog.html">blog</a></li>
            <li><a href="contact.html">Contact Us</a></li>
        </ul>
        <div class="footer-social"> <a target="_blank" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a> </div>
        <div class="clearfix"></div>
        <div class="footer-bottom clearfix">
            <p>© 2018 Vegan Day & Night. All Rights Reserved</p>
        </div>
    </div>
    <!--container end-->

</div>
<!--footer end-->

<?php $this->load->view('vegan/common/footer_files_home');?>
</body>
</html>
