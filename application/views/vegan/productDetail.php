<!--food detail start here-->
<section class="product top1-sec sec">
    <div class="container">
        <div class="product-inner">
            <div class="main">


                <div class="row product-flex">

                    <!-- product flex is used only for mobile order -->
                    <!-- on mobile 'product-flex-info' goes bellow gallery 'product-flex-gallery' -->
                    <div class="alert hide alert-success" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong class="a-msg">Item is added to cart</strong>
                    </div>
                    <div class="alert hide alert-danger" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong class="a-msg">Item can not be added to cart</strong>
                    </div>

                    <div class="col-md-5 col-sm-12 product-flex-info">
                        <div class="clearfix">
                          <input type="hidden" id="pid" value="<?=$product['id']?>">

                            <!-- === product-title === -->


                            <h2><?=$product['name']?></h2>
                            </h1>

                            <div class="clearfix">

                                <!-- === price wrapper === -->

                                <div class="price">

                                    <?php if(trim($product['price']) == trim($product['reg_price'])): ?>
                                        <span class="h3">
                                            $ <?=$product['price']?>
                                        </span>
                                    <?php else: ?>
                                        <span class="h3">
                                            $ <?=$product['price']?>
                                            <small>$ <?=$product['reg_price']?></small>
                                        </span>
                                    <?php endif ?>
                                </div>
                                <hr>

                                <div class="info-box">
                                    <span><strong>Rating</strong></span>
                                    <div class="rateit fixedrate" data-rate-value=<?=$rating?>></div>
                                </div>
                                <!-- === info-box === -->

                                <div class="info-box">
                                    <strong>Receipes:</strong>
                                    <p><?=$product['description']?></p>
                                </div>



                                <hr>
                                <?php
                                $fav = "";
                                if($this->ion_auth->logged_in()){
                                    $user_id = $this->session->userdata('user_id');
                                    $this->db->where("user_id", $user_id);
                                    $data = $this->db->get("favourites")->result_array();
                                    if(count($data)){
                                        $arr = explode(',',$data[0]['product_id']);
                                        if (in_array($product['id'], $arr))
                                            $fav = "added";
                                        else
                                            $fav = "";
                                    }
                                    else
                                        $fav = "";
                                }
                                else{
                                    $fav = "";
                                }
                                ?>

                                <div data-id="<?=$product['id']?>" class="atf<?=$product['id']?> info-box info-box-addto <?=$fav?>">
                                      <span>
                                          <i class="add"><i class="fa fa-heart-o"></i> Add to favorites</i>
                                          <i class="added"><i class="fa fa-heart"></i> Remove from favorites</i>
                                      </span>
                                </div>

                                <!--<div data-id="<?/*=$product['id']*/?>" class="atf<?/*=$product['id']*/?> info-box info-box-addto <?/*=$fav*/?> added">
                                        <span>
                                            <i class="add"><i class="fa fa-heart-o"></i> Add to favorites</i>
                                            <i class="added"><i class="fa fa-heart"></i> Remove from favorites</i>
                                        </span>
                                </div>-->

                                <!--<div class="info-box info-box-addto">
                                        <span>
                                            <i class="add"><i class="fa fa-eye-slash"></i> Add to Watch list</i>
                                            <i class="added"><i class="fa fa-eye"></i> Remove from Watch list</i>
                                        </span>
                                </div>

                                <div class="info-box info-box-addto">
                                        <span>
                                            <i class="add"><i class="fa fa-star-o"></i> Add to Collection</i>
                                            <i class="added"><i class="fa fa-star"></i> Remove from Collection</i>
                                        </span>
                                </div>-->



                            </div> <!--/clearfix-->
                        </div> <!--/product-info-wrapper-->
                    </div> <!--/col-md-4-->
                    <!-- === product item gallery === -->

                    <div class="col-md-7 col-sm-12 product-flex-gallery">

                        <!-- === add to cart === -->

                        <a data-id="<?=$product['id']?>" class="atc_pdp btn btn-buy" data-text="Buy"></a>


                        <!-- === product gallery img === -->
                        <div class="product-image">
                            <div class="row">
                                <div class="col-md-12">
                                    <!--<img src="<?/*=base_url()*/?>/vegan/assets/images/pro-image.png" class="img-fluid" alt="">-->

                                    <?php foreach ($product['images'] as $key => $value) { if($key==0){?>
                                        <a href="<?=base_url().'uploads/'.$value['image_path']?>" target="_blank">
                                            <img src="<?=base_url().'uploads/'.$value['image_path']?>" alt="" height="574" style="width:574px; height: 381px;" />
                                        </a>
                                    <?php }} ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>


            <!-- === product-info === -->

            <div class="info">

                <div class="info-inner">
                    <div class="row">


                        <!-- === product-designer === -->

                        <!--<div class="col-md-3">
                            <div class="brands">
                                <div class="box">
                                    <div class="image">
                                        <img src="<?/*=base_url()*/?>/vegan/assets/images/logo-2.png" alt="Alternate Text">
                                    </div>
                                    <div class="name">
                                        <p>Brand name</p>
                                        <p><a href="#">Brand Url</a></p>
                                        <p><a href="#">More about brand</a></p>
                                    </div>
                                </div>
                            </div>
                        </div> -->


                        <div class="col-md-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#about" aria-controls="about" role="tab" data-toggle="tab">
                                        <i class="icon icon-history"></i>
                                        <span>About</span>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#specs" aria-controls="specs" role="tab" data-toggle="tab">
                                        <i class="icon icon-sort-alpha-asc"></i>
                                        <span>Specification</span>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#rating" aria-controls="rating" role="tab" data-toggle="tab">
                                        <i class="icon icon-thumbs-up"></i>
                                        <span>Rating</span>
                                    </a>
                                </li>
                            </ul>



                            <div class="tab-content">



                                <div role="tabpanel" class="tab-pane active" id="about">
                                    <div class="content">
                                        <h3>About this Item</h3>
                                        <p>
                                            <?=$product['description']?>
                                        </p>


                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="specs">
                                    <div class="content">
                                        <h3>Specification</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>
                                                    <?=$product['product_specifications']?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="rating">



                                    <div class="content">

                                        <h3>Rating</h3>
                                        <div class="row">
                                            <!-- === comments === -->
                                            <div class="col-md-12">
                                                <div class="comments">
                                                    <div style="padding-bottom: 20px;"></div>
                                                    <div class="comment-wrapper">
                                                        <!-- === comment === -->
                                                        <div id="loadfeedback"></div>
                                                    </div><!--/comment-wrapper-->

                                                    <div class="comment-add">

                                                        <div class="comment-reply-message">
                                                            <div class="h3 title">Leave a Reply </div>
                                                            <p>Your email address will not be published.</p>
                                                        </div>
                                                        <form>
                                                            <div class="form-group">
                                                                <label>Name *</label>
                                                                <input type="text" class="form-control" id="name" value="" placeholder="Your Name" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Email *</label>
                                                                <input type="text" class="form-control" id="email" value="" placeholder="Your Email" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Rating *</label>
                                                                <div class="rateit rateitfeed" data-rate-value=5></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Message *</label>
                                                                <textarea rows="10" id="msg" class="form-control" placeholder="Your comment"></textarea>
                                                            </div>
                                                            <div class="clearfix text-center">
                                                                <a data-id="<?=$product['id']?>" class="addFeedback btn btn-main">Add comment</a>
                                                            </div>
                                                        </form>

                                                    </div><!--/comment-add-->
                                                </div> <!--/comments-->
                                            </div>


                                        </div> <!--/row-->

                                    </div>
                                </div>

                            </div>
                        </div> <!--/row-->
                    </div> <!--/container-->


                </div>
            </div><!--/info-->




        </div>
        <!--<section class="products">


            <header>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 text-center">
                        <div class="text">
                            <p>Check out our latest collections</p>
                        </div>
                        <h1 class="title">Best seller</h1>

                    </div>
                </div>
            </header>

            <div class="row row-clean my-box">

                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text">
                        <h2 class="title h5">
                            <a href="product.html">Shepherds pie</a>
                        </h2>
                        <span><del>$139</del> $159</span>


                    </div>
                    <article>
                        <div class="info">
                            <span class="add-favorite">
                                <a href="javascript:void(0);" data-title="Add to favorites" data-title-added="Added to favorites list"><i class="icon icon-heart"></i></a>
                            </span>
                            <span>
                                <a href="#productid1" class="mfp-open" data-title="Quick wiew"><i class="icon icon-eye"></i></a>
                            </span>
                        </div>
                        <div class="btn btn-add">
                            <i class="icon icon-cart"></i>
                        </div>
                        <div class="figure-grid">
                            <div class="image">
                                <a href="#productid1" class="mfp-open">
                                    <img src="<?/*=base_url()*/?>/vegan/assets/images/seller-1.png" class="image-fluid" alt="" width="360">
                                </a>
                            </div>

                        </div>
                    </article>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text">
                        <h2 class="title h5">
                            <a href="product.html">Cornbread stuffing</a>
                        </h2>
                        <span><del>$139</del> $159</span>


                    </div>
                    <article>
                        <div class="info">
                            <span class="add-favorite">
                                <a href="javascript:void(0);" data-title="Add to favorites" data-title-added="Added to favorites list"><i class="icon icon-heart"></i></a>
                            </span>
                            <span>
                                <a href="#productid1" class="mfp-open" data-title="Quick wiew"><i class="icon icon-eye"></i></a>
                            </span>
                        </div>
                        <div class="btn btn-add">
                            <i class="icon icon-cart"></i>
                        </div>
                        <div class="figure-grid">
                            <div class="image">
                                <a href="#productid1" class="mfp-open">
                                    <img src="<?/*=base_url()*/?>/vegan/assets/images/seller-2.png" class="img-fluid" alt="" width="360">
                                </a>
                            </div>

                        </div>
                    </article>
                </div>


                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text">
                        <h2 class="title h5">
                            <a href="product.html">Collard Greens</a>
                        </h2>
                        <span><del>$139</del>$159 </span>


                    </div>
                    <article>
                        <div class="info">
                            <span class="add-favorite">
                                <a href="javascript:void(0);" data-title="Add to favorites" data-title-added="Added to favorites list"><i class="icon icon-heart"></i></a>
                            </span>
                            <span>
                                <a href="#productid1" class="mfp-open" data-title="Quick wiew"><i class="icon icon-eye"></i></a>
                            </span>
                        </div>
                        <div class="btn btn-add">
                            <i class="icon icon-cart"></i>
                        </div>
                        <div class="figure-grid">
                            <div class="image">
                                <a href="#productid1" class="mfp-open">
                                    <img src="<?/*=base_url()*/?>/vegan/assets/images/seller-3.png" class="img-fluid" alt="" width="360">
                                </a>
                            </div>

                        </div>
                    </article>
                </div>


                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text">
                        <h2 class="title h5">
                            <a href="product.html">Biscuits and Gravy</a>
                        </h2>
                        <span><del>$139</del>$159 </span>


                    </div>
                    <article>
                        <div class="info">
                            <span class="add-favorite">
                                <a href="javascript:void(0);" data-title="Add to favorites" data-title-added="Added to favorites list"><i class="icon icon-heart"></i></a>
                            </span>
                            <span>
                                <a href="#productid1" class="mfp-open" data-title="Quick wiew"><i class="icon icon-eye"></i></a>
                            </span>
                        </div>
                        <div class="btn btn-add">
                            <i class="icon icon-cart"></i>
                        </div>
                        <div class="figure-grid">
                            <div class="image">
                                <a href="#productid1" class="mfp-open">
                                    <img src="<?/*=base_url()*/?>/vegan/assets/images/seller-4.png" class="img-fluid" alt="" width="360">
                                </a>
                            </div>

                        </div>
                    </article>
                </div>




            </div>

            <div class="row"><div class="col-md-12 text-center">
                    <div class="wrapper-more ">
                        <a href="food-detail.html" class="btn btn-lg green-btn">View all</a>
                    </div></div>
            </div>


            <div class="popup-main mfp-hide" id="productid1">
                <div class="product">

                    <div class="popup-title">
                        <div class="h1 title">  Fried Chick’n Mushrooms   </div>
                    </div>


                    <div class="owl-product-gallery">
                        <img src="<?/*=base_url()*/?>/vegan/assets/images/food-10.png" alt="" width="640" />
                        <img src="<?/*=base_url()*/?>/vegan/assets/images/food-1.png" alt="" width="640" />
                    </div>
                    <div class="popup-content">
                        <div class="product-info-wrapper">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="info-box">
                                        <h2>With or Without gravy</h2>
                                        <p>Lorem ipsum dolor sit amet, dolorem nolui sse inimicus cu per, te eos utin am ancillae volutpat. No cetero tim eam democritum. his, an exerci munere propriae eos. Et eius dolores has. Et iudico prodesset sit, veniam docendi ius in, mentitum ponderum lucilius sea ne. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="popup-table">
                        <div class="popup-cell">
                            <div class="price">
                                <span class="h3">$ 1999,00 <small>$ 2999,00</small></span>
                            </div>
                        </div>
                        <div class="popup-cell">
                            <div class="popup-buttons">
                                <a href="checkout-1.html"><span class="icon icon-eye"></span> <span class="hidden-xs">View more</span></a>
                                <a href="javascript:void(0);"><span class="icon icon-cart"></span> <span class="hidden-xs">Buy</span></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </section>-->
    </div>
</section>
