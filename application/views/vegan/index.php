
<div class="top1-sec clearfix">
    <div class="top1-sec-inner"></div>
    <div class="container">
        <div class="col-md-7 col-sm-12 left1">
            <h1><?=$elements1[0]->title?></h1>
            <p class="upar"><?=$elements1[0]->subtitle?></p>
        </div>
        <!--left col end-->

        <div class="col-md-5 col-sm-12 right1">
            <div class="box"> <span class="top"></span> <span class="right"></span> <span class="bottom"></span> <span class="left"></span>
              <img src="<?=base_url()?>/uploads/<?=$elements1[0]->image?>" class="img-responsive" alt="" /> </div>
        </div>
        <!--right col end-->
    </div>
    <!--container end-->

</div>
<!--top1 sec end-->

<div class="wht-we-offer clearfix">
    <div class="container">
        <h1><span><?=$brandings[0]->fp_title?></span><?=$brandings[0]->fp_subtitle?></h1>
        <div class="col-md-6 col-sm-12">
            <div class="book-slide-1 clearfix">
                <div class="col-md-6 col-lg-6  col-sm-12 book-slide-box">
                  <img src="<?=base_url().'/uploads/'.$elements2[0]->image?>" alt="" />
                    <h4><?=$elements2[0]->title?></h4>
                    <p><?=$elements2[0]->subtitle?></p>
                    <a href="<?=$elements2[0]->btn_link?>"><?=$elements2[0]->btn_text?></a> </div>
                <div class="col-md-6 col-lg-6  col-sm-12 book-slide-box">
                  <img src="<?=base_url().'/uploads/'.$elements2[1]->image?>" alt="" />
                    <h4><?=$elements2[1]->title?></h4>
                    <p><?=$elements2[1]->subtitle?></p>
                    <a href="<?=$elements2[1]->btn_link?>"><?=$elements2[1]->btn_text?></a> </div>
            </div>
            <!--book slide end-->
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="book-slide-1 clearfix">
                <div class="col-md-6 col-lg-6  col-sm-12 book-slide-box">
                  <img src="<?=base_url().'/uploads/'.$elements2[2]->image?>" alt="" />
                    <h4><?=$elements2[2]->title?></h4>
                    <p><?=$elements2[2]->subtitle?></p>
                    <a href="<?=$elements2[2]->btn_link?>"><?=$elements2[2]->btn_text?></a> </div>
                <!--inner box end-->

                <div class="col-md-6 col-lg-6  col-sm-12 book-slide-box">
                  <img src="<?=base_url().'/uploads/'.$elements2[3]->image?>" alt="" />
                    <h4><?=$elements2[3]->title?></h4>
                    <p><?=$elements2[3]->subtitle?></p>
                    <a href="<?=$elements2[3]->btn_link?>"><?=$elements2[3]->btn_text?></a> </div>
                <!--inner box end-->

            </div>
            <!--book slide end-->
        </div>
        <div class="clearfix"></div>
        <a href="menu.html" class="green-btn">View All</a> </div>
    <!--container end-->

</div>
<!--wht we offer end-->

<div style="background: url(<?=base_url().'/uploads/'.$elements3[0]->image?>) no -repeat center" class="tents-sec clearfix">
    <div class="container">
        <h1><?=$elements3[0]->title?></h1>
        <h2><?=$elements3[0]->subtitle?></h2>
        <p><?=$elements3[0]->on_hover_text?></p>
        <a href="<?=$elements3[0]->btn_link?>" class="green-btn"><?=$elements3[0]->btn_text?></a> </div>
    <!--container end-->
</div>
<!--tents sec end-->

<div style="background: url(<?=base_url().'/uploads/'.$elements4[0]->image?>) no -repeat center" class="events-sec clearfix">
    <div class="container">
        <div class="col-md-7 col-sm-12"></div>
        <div class="col-md-5 col-sm-12">
          <h1><?=$elements4[0]->title?></h1>
          <h2><?=$elements4[0]->subtitle?></h2>
          <p><?=$elements4[0]->on_hover_text?></p>
          <a href="<?=$elements4[0]->btn_link?>" class="green-btn"><?=$elements4[0]->btn_text?></a> </div>

    </div>
    <!--container end-->
</div>
<!--events-sec end-->

<div class="bottom-testi-sec clearfix">
    <div class="row">
        <div class="col-md-3 col-sm-12"><img src="<?=base_url()?>/vegan/assets/images/Vegan-Day-&-Night_37.png" class="img-full" alt="" /></div>
        <div class="col-md-6 col-sm-12">
            <div class="testi-inner clearfix">
                <h1>What Our Client</h1>
                <h2>Say</h2>
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                       <?php foreach ($testimonials as $key => $test): ?>
                         <div class="item <?=($key == 0) ? 'active' : '' ?>">
                           <h3><?=$test['author']?></h3>
                           <p><?=$test['body']?></p>
                         </div>
                       <?php endforeach; ?>
                    </div>
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                </div>
            </div>
            <!--bottom testi sec end-->

        </div>
        <div class="col-md-3 col-sm-12"><img src="<?=base_url()?>/vegan/assets/images/Vegan-Day-&-Night_39.png" class="img-full" alt="" /></div>
    </div>
    <!--row end-->

</div>
<!--bottom testi sec end-->
