<h1>Manage</h1>

<?php echo anchor(base_url(). '/dashboard', "Dashboard"); ?>

<ul>
    <li><?php echo anchor(base_url(). '/admin/permissions', "Manage Permission's"); ?></li>
    <li><?php echo anchor(base_url(). '/admin/groups', "Manage Group's"); ?></li>
    <li><?php echo anchor(base_url(). '/admin/users', "Manage User's"); ?></li>
</ul>
