<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'index';
$route['404_override'] = '';
$route['login'] = 'auth/login';
$route['admin/add-permission'] = 'admin/add_permission';


$route['request-login'] = 'services/login';
$route['request-logout'] = 'services/logout';
$route['request-forgot-password'] = 'services/forgotYourPass';
$route['request-verify-token'] = 'services/verifyToken';
$route['request-update-password'] = 'services/updatePassword';
$route['request-create-user'] = 'services/userRegister';

$route['request-profile'] = 'services/viewProfile';
$route['request-edit-profile'] = 'services/editProfile';

$route['request-add-review'] = 'services/addReview';
$route['request-product-categories'] = 'services/getAllProductCategories';
$route['request-products'] = 'services/getAllProducts';
$route['request-single-product'] = 'services/getSingleProduct';
$route['request-search-product'] = 'services/searchProduct';

$route['request-submit-order'] = 'services/submitOrder';
$route['request-my-orders'] = 'services/getOrders';
$route['request-single-order'] = 'services/singleOrder';
$route['request-apply-coupon'] = 'services/applyCoupon';
$route['request-contact'] = 'services/contact';


$route['password-reset/(:any)'] = "users/getPasswordReset/$1";
$route['contact'] = "settings/contactUs";
$route['blogs'] = "settings/getBlogs";
$route['blogs/page_no/(:num)'] = "settings/getBlogs/$1";
$route['blogs/(:any)'] = "settings/getBlogsDetail/$1";
$route['products/getfeedback'] = "products/getfeedback";
$route['products/addfeedback'] = "products/addfeedback";
$route['product/search'] = "products/searchProduct";
$route['products/page_no/(:num)'] = "products/index/$1";
$route['products/(:any)'] = "products/index/null/$1";
$route['product/add-to-favourites'] = "products/addToFavourites";
$route['shopping/delete-item-from-cart'] = "shopping/deleteItemFromCart";
$route['shopping/add-to-cart'] = "shopping/add";
$route['shopping/get-coupon-block'] = "shopping/getCouponBlock";
$route['shopping/get-promo-discount'] = "shopping/getPromoCodeDisc";
$route['shopping/apply-promo-code'] = "shopping/applyPromoCode";
$route['shopping/remove-promo-code'] = "shopping/removePromoCode";
$route['shopping/process-cart-settings'] = "shopping/processCartSettings";
$route['shopping/get-cart'] = "shopping/getCartView";
$route['checkout'] = "shopping/getCheckOut";
$route['billing'] = "shopping/getBillings";
$route['payment'] = "shopping/getPayment";
$route['shopping/update-cart'] = "shopping/updateCart";
$route['shopping/process-billings'] = "shopping/processBilling";
$route['shopping/process-payment'] = "shopping/processPayment";
$route['order/order-details'] = "order/orderDetails";
$route['order/order-details/(:any)'] = "order/orderDetails/$1";
$route['create-account'] = "index/createAccount";
$route['my-account'] = "index/customerDashboard";
$route['user/update-profile'] = "users/updateProfile";
$route['user/cancel-order/(:any)'] = "users/cancelOrder/$1";
$route['page'] = "Settings/getPage";
$route['page/(:any)'] = "Settings/getPage/$1";

$route['web'] = 'web/index';

$route['translate_uri_dashes'] = TRUE;
