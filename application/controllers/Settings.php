<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Users.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Settings extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');

        // if( ! $this->ion_auth->logged_in() )
        //     redirect('/login');

        /*if( ! $this->ion_auth_acl->has_permission('access_admin') )
            redirect('/dashboard');*/
    }

    public function index()
    {
        $data['settings'] = $this->Home_Model->selectRowWhere('settings',['id'=>'1']);
        $data['home_masterview'] = 'add_edit_settings';
        $this->load->view(HOME_GENERALVIEW, $data);
    }

    public function getPage($slug=null){

        if(!$slug)
            redirect('/');
        $data['page'] = $this->Home_Model->selectRowWhere('pages',['slug'=>$slug]);
        $data['home_masterview'] = 'page';
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }

    public function getBlogs($page=null)
    {

        if($this->input->get('q'))
            $this->db->like('blog_title', $this->input->get('q'));
        $blogs = $this->db->get('blogs')->result_array();;

        $count = count($blogs);
        $per_page=6;
        $config['base_url'] = base_url()."blogs/page_no";
        $config['total_rows'] = $count;
        $config['per_page'] = $per_page;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = true;
        //Adding Enclosing Markup
        $config['full_tag_open'] = '<p><ul  class="pagination">';
        $config['full_tag_close'] = '</ul></p><!--pagination-->';
        //Customizing the First Link
        $config['first_link'] = '<<';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        //Customizing the Last Link
        $config['last_link'] = '>>';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        //Customizing the "Next" Link
        $config['next_link'] = '>';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        //Customizing the "Previous" Link
        $config['prev_link'] = '<';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        //Customizing the "Current Page" Link
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li></li>";
        //Customizing the "Digit" Link
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        if($this->uri->segment(3) > 0)
            $offset = ($this->uri->segment(3) + 0)*$per_page - $per_page;
        else
            $offset = $this->uri->segment(3);

        $this->db->limit($config['per_page'], $offset);
        $this->db->order_by('id', 'desc');
        if($this->input->get('q'))
            $this->db->like('blog_title', $this->input->get('q'));
        $blogs = $this->db->get("blogs")->result_array();
        $data['blogs'] = $blogs;

        $data['recent_blogs'] = $this->Home_Model->selectLimitOrderBy('blogs',5,'id','desc');
        $data['home_masterview'] = 'blogs';
        $data['title'] = APP_NAME.' - Blogs';
        $data['pagination'] =$this->pagination->create_links();
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }

    public function contactUs()
    {
      $data['home_masterview'] = 'contactus';
      $data['title'] = APP_NAME.' - Contact Us';
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }

    public function getBlogsDetail($slug){
      $blog = $this->Home_Model->selectWhere('blogs',['blog_slug' => $slug]);
      if(!$blog)
        redirect('/blogs');

      $data['blog'] = $blog;
      $data['home_masterview'] = 'blogDetail';
      $data['title'] = APP_NAME.' - Blogs - '.$blog['blog_title'];
      if(theme=='vegan')
          $this->load->view(siteview, $data);
      else
          $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }




    public function update_settings()
    {

        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $this->Home_Model->updateDB('settings',['id'=>'1'],
                [
                    'shipping_amount'=>$this->input->post('shipping_amount'),
                    'base_currency'=>$this->input->post('base_currency'),
                ]

            );
            redirect("/settings", 'refresh');
        }
    }

}
