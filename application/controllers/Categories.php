<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Categories extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

//        if( ! $this->ion_auth->logged_in() )
//            redirect('/login');
    }

    public function index()
    {

        $categories = $this->Home_Model->selectWhereResult('categories',['active' => "1"]);
        $data['title'] = APP_NAME.' - Categories';
        $data['categories'] = $categories;
        $data['home_masterview'] = 'productscategories';
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }

}
