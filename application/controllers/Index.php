<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Index extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

//        if( ! $this->ion_auth->logged_in() )
//            redirect('/login');
    }

    public function index()
    {
        $data['home_masterview'] = 'index';
        $data['elements'] = $this->db->query('select * from homepage_elements')->result();
        //echo '<pre>';print_r($data['elements']);exit;
        $data['brandings'] = $this->db->query('select * from brandings')->result();
        $data['elements1'] = $this->db->query('select * from homepage_elements WHERE type = 1')->result();
        $data['elements2'] = $this->db->query('select * from homepage_elements WHERE type = 2')->result();
        $data['elements3'] = $this->db->query('select * from homepage_elements WHERE type = 3')->result();
        $data['elements4'] = $this->db->query('select * from homepage_elements WHERE type = 4')->result();
        $data['categories'] = $this->Home_Model->selectWhereResult('categories',['featured' => 1]);
        $products = $this->Home_Model->selectWhereResult('products',['featured' => 1]);
        foreach ($products as $key => $product) {
            $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$product['id']], 'image_path');
            $products[$key]['images'] = $images;
        }
        $data['products'] = $products;
        $data['banners'] = $this->Home_Model->simpleSelect('homepagebanner');
        $data['testimonials'] = $this->Home_Model->simpleSelect('testimonials');
        $data['blogs'] = $this->Home_Model->selectWhereResult('blogs',['isfeatured' => 1]);

        if(theme=='vegan')
            $this->load->view(homeview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }

    public function createAccount(){

      if ($this->ion_auth->logged_in())
          redirect('/my-account', 'refresh');

      $data['home_masterview'] = 'createAccount';
      $data['countries'] = $this->Home_Model->simpleSelect('countries');
      $data['gender'] = array('male' => 'Male', 'female' => 'Female');
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }

    public function customerDashboard(){
        if( ! $this->ion_auth->logged_in() ):
            redirect('create-account');
        endif;
      $user_id = $this->session->userdata('user_id');
      $data['countries'] = $this->Home_Model->simpleSelect('countries');
      $data['profile'] = $this->Home_Model->selectWhere('users',['id'=>$user_id]);
      $data['favourites'] = $this->Home_Model->selectWhere('favourites',['user_id'=>$user_id]);
      $data['orders'] = $this->Home_Model->selectWhereResult('orders',['CustomerID'=>$user_id], 'OrderID', 'desc');
      $data['gender'] = array('Male' => 'Male', 'Female' => 'Female');
      $data['home_masterview'] = 'myAccount';
      $data['title'] = APP_NAME.' - My Account';
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);

    }

}
