<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shopping extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('cart');
    $this->load->helper('string');
  }

  public function checkIfItemExistUpdateQty($id){
      foreach ($this->cart->contents() as $key => $value) {
          if($value['id'] == $id){
            $prod = $this->Home_Model->selectWhere('products',['id' =>  $value['id']]);
            if($value['qty']+1 > $prod['stock_qty']){
              return $this->output
                      ->set_content_type('application/json')
                      ->set_output(json_encode(['status'=>500,'msg'=>"Not Enough stock available"]));
            }
            else{
              $data = array(
                'rowid' => $value['rowid'],
                'qty'   => $value['qty']+1,
              );
              $this->cart->update($data);
              return 1;
            }
          }
      }
      return 0;
  }

  public function processCartSettings(){
    $total = $this->cart->total();
    $tax=0;
    $cartS = $this->Home_Model->simpleSelect('cart_settings');
    foreach ($cartS as $key => $cs){
       $tax += $total*$cs['rate']/100;
    }

    $total = $total+$tax;
    if($this->session->userdata('promocode')){
      $disc =  $total*$this->session->userdata('promocode')['disc']/100;
      $total = $total - $disc;
    }
    echo $total;
  }

  public function getTotalAmount(){
    $total = $this->cart->total();
    $tax=0;
    $cartS = $this->Home_Model->simpleSelect('cart_settings');
    foreach ($cartS as $key => $cs){
       $tax += $total*$cs['rate']/100;
    }

    $total = $total+$tax;
    if($this->session->userdata('promocode')){
      $disc =  $total*$this->session->userdata('promocode')['disc']/100;
      $total = $total - $disc;
    }
    $total = $this->truncate_number($total);
    return $total;
  }

  public function truncate_number( $number, $precision = 2) {
        // Zero causes issues, and no need to truncate
        if ( 0 == (int)$number ) {
            return $number;
        }
        // Are we negative?
        $negative = $number / abs($number);
        // Cast the number to a positive to solve rounding
        $number = abs($number);
        // Calculate precision number for dividing / multiplying
        $precision = pow(10, $precision);
        // Run the math, re-applying the negative value to ensure returns correctly negative / positive
        return floor( $number * $precision ) / $precision * $negative;
    }

  public function getPromoCodeDisc(){
    if($this->session->userdata('promocode'))
        echo $this->session->userdata('promocode')['disc'];
    else
        echo 0;
  }

  public function removePromoCode(){
    $this->session->unset_userdata('promocode');
  }

  public function getCouponBlock(){
    $ret = "";
    if($this->session->userdata('promocode')){
      $promo = $this->session->userdata('promocode');
      $ret .='<span class="checkbox">
                <a class="removePromoCode"><i class="fa fa-times-circle red"></i></a>
                <label for="couponCodeID">Promo code <strong>'.$promo['code'].'</strong> Added</label>
                <span class="coupon-text-fix green bold">('.$promo['disc'].'% Discount)</span>
              </span>';
    }
    else{
      $ret .='<span class="checkbox">
        <input type="checkbox" id="couponCodeID">
        <label for="couponCodeID">Promo code</label>
        <input type="text" id="promocode" class="form-control form-coupon" style="display:none" value="" placeholder="Enter coupon code" style="">
        <span class="coupon-text"></span>
      </span>';
    }
    echo $ret;
  }

  public function applyPromoCode(){
    $promocode = $this->input->get('promocode');
    $date = date('Y-m-d');
    $where = "'$date' BETWEEN DATE(CouponFrom) AND DATE(CouponTo) AND status = 0 AND  CouponCode = BINARY '$promocode'";
    $record = $this->db->where($where)->get('coupons')->result_array();
    if(count($record)){
      $array = array(
        'code' => $record[0]['CouponCode'],
        'disc' => $record[0]['CouponPercentage'],
      );
      $this->session->set_userdata('promocode',$array);
      return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(['status'=>200,'msg'=>"Copupon Code is Valid"]));
    }
    else{
      return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(['status'=>500,'msg'=>"Coupon Code is Invalid"]));
    }
  }


  public function getCheckOut(){
      $data['home_masterview'] = 'checkout';
        $data['title'] = APP_NAME.' - Checkout';
      if(theme=='vegan')
          $this->load->view(siteview, $data);
      else
          $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
  }
  public function getBillings(){
      if(!count($this->cart->contents())){
        redirect('/checkout');
      }
      $data['home_masterview'] = 'billing';
        $data['title'] = APP_NAME.' - Billings';
      $data['countries'] = $this->Home_Model->simpleSelect('countries');
      if(theme=='vegan')
          $this->load->view(siteview, $data);
      else
          $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
  }


  public function getPayment(){
    if(
       !$this->session->userdata('first_name') ||
       !$this->session->userdata('last_name') ||
       !$this->session->userdata('bill_email') ||
       !$this->session->userdata('phone') ||
       !$this->session->userdata('countries') ||
       !$this->session->userdata('zip_code') ||
       !$this->session->userdata('city') ||
       !$this->session->userdata('address')
    ){
        redirect('/billing');
    }
    else if(!count($this->cart->contents())){
      redirect('/billing');
    }
    else{
      $data['home_masterview'] = 'payment';
        $data['title'] = APP_NAME.' - Payment';
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }
  }

  public function deleteItemFromCart(){
    $this->cart->remove($this->input->post('id'));
  }

  public function processPayment(){
      if($this->input->post('pay_via')){
          if($this->input->post('cardNumber') == ''){
            $messge = array('message' => 'Card Number is invalid' ,'class' => 'alert-danger');
            $this->session->set_flashdata('message', $messge);
            redirect("/payment",'refresh');
          }
          if($this->input->post('cardExpiry') == ''){
            $messge = array('message' => 'Card Expiry is invalid' ,'class' => 'alert-danger');
            $this->session->set_flashdata('message', $messge);
            redirect("/payment",'refresh');
          }
          if($this->input->post('cardCVC') == ''){
            $messge = array('message' => 'Card CVC is invalid' ,'class' => 'alert-danger');
            $this->session->set_flashdata('message', $messge);
            redirect("/payment",'refresh');

          }
          /*if(strlen($this->input->post('cardNumber')) != '19'){
            $messge = array('message' => 'Card is invalid','class' => 'alert-danger');
            $this->session->set_flashdata('message', $messge);
            redirect("/payment",'refresh');
          }*/
          if(strlen($this->input->post('cardExpiry')) != '9'){
            $messge = array('message' => 'Card Expiry is invalid' ,'class' => 'alert-danger');
            $this->session->set_flashdata('message', $messge);
            redirect("/payment",'refresh');
          }
          if(strlen($this->input->post('cardCVC')) != '3'){
            $messge = array('message' => 'Card CVC is invalid' ,'class' => 'alert-danger');
            $this->session->set_flashdata('message', $messge);
            redirect("/payment",'refresh');
          }

          $cardNumber = trim(preg_replace('/\s+/', '', $this->input->post('cardNumber')));
          $cardExpiry = trim(preg_replace('/\s+/', '', $this->input->post('cardExpiry')));
          $cardCVC = trim(preg_replace('/\s+/', '', $this->input->post('cardCVC')));
          $exp = explode('/',$cardExpiry);
          $month = trim($exp[0]);
          $year = trim($exp[1]);
          /*
            dummy card
            'creditCardNumber' => '4080 7124 5906 1839',
            'expMonth' => '10',
            'expYear' => '2026',
            'cvv' => '561',
          */
          $amount = $this->getTotalAmount();
          //echo 'amount:'.$amount;exit;

          $paypalParams = array(
              'paymentAction' => 'Sale',
              'amount' => $this->getTotalAmount(),
              'currencyCode' => 'USD',
              'creditCardType' => '',
              'creditCardNumber' => $cardNumber,
              'expMonth' => $month,
              'expYear' => $year,
              'cvv' => $cardCVC,
              'firstName' => "",
              'lastName' => "",
              'city' => "",
              'zip'  => "",
              'countryCode' => "",
          );
        $this->load->model('paypal_model');
        $response = $this->paypal_model->paypalCall($paypalParams);
        $paymentStatus = strtoupper($response["ACK"]);
        //echo '<pre>';print_r($paymentStatus);exit;
      }
      else{
          $amount = $this->getTotalAmount();
          $paymentStatus = 'SUCCESS';
      }

    if($paymentStatus == 'SUCCESS'){
      $customer_id='';
      if($this->session->userdata('create_an_account')){
          $pass = $this->ion_auth_model->hash_password($this->session->userdata('bill_password'));
          $customer_id = $this->Home_Model->insertDB('users',array(
            'username' => $this->session->userdata('bill_email'),
            'password' => $pass,
            'email' => $this->session->userdata('bill_email'),
            'created_on' => strtotime(date('Y-m-d')),
            'active' => 1,
            'first_name' => $this->session->userdata('first_name'),
            'last_name' => $this->session->userdata('last_name'),
            'phone' => $this->session->userdata('phone'),
            'address_1' => $this->session->userdata('address'),
            'address_2' => $this->session->userdata('address'),
            'address_3' => $this->session->userdata('address'),
            'city' => $this->session->userdata('city'),
            'postcode' => $this->session->userdata('zip_code'),
            'country' => $this->Home_Model->selectWhere('countries',['code' => $this->session->userdata('countries')])['name'],
          ));
          $this->Home_Model->insertDB('users_groups',array(
            'user_id' => $customer_id,
            'group_id' => 2,
          ));
      }

      if( $this->ion_auth->logged_in() ){
        $customer_id = $this->session->userdata('user_id');
      }


      $order = array();
      $order['billing_first_name'] = $this->session->userdata('first_name');
      $order['billing_email'] = $this->session->userdata('bill_email');
      $order['billing_phone'] = $this->session->userdata('phone');
      $order['billing_last_name'] = $this->session->userdata('last_name');
      $order['billing_address_1'] = $this->session->userdata('address');
      $order['billing_address_2'] = $this->session->userdata('address');
      $order['billing_city'] = $this->session->userdata('city');
      $order['billing_state'] = $this->session->userdata('city');
      $order['billing_country'] = $this->session->userdata('countries');
      $order['billing_postcode'] = $this->session->userdata('zip_code');
      $order['shipping_address_1'] = $this->session->userdata('address');
      $order['shipping_address_2'] = $this->session->userdata('address');
      $order['shipping_city'] = $this->session->userdata('city');
      $order['shipping_state'] = $this->session->userdata('city');
      $order['shipping_country'] = $this->session->userdata('countries');
      $order['shipping_postcode'] = $this->session->userdata('zip_code');
      $order['OrderAmount'] = $amount;
      $order['OrderTotal'] = $amount;
      $order['CustomerID'] = $customer_id;
      $order['cart_settings'] = serialize($this->Home_Model->simpleSelect('cart_settings'));
      $order['coupon'] = ($this->session->userdata('promocode')) ? serialize($this->session->userdata('promocode')) : "";

      $OrderId = $this->Home_Model->insertDB('orders',$order);

      $ids = $this->input->post('ids');
      $names = $this->input->post('names');
      $qtys = $this->input->post('qtys');
      $prices = $this->input->post('prices');
      $totals = $this->input->post('totals');

      foreach ($this->cart->contents() as $index => $cart){
          $orderDetails = array();
          $orderDetails['ProductID'] = $cart['id'];
          $orderDetails['ProductName'] = $cart['name'];
          $orderDetails['ProductPrice'] = $cart['price'];
          $orderDetails['ProductQuantity'] = $cart['qty'];
          $orderDetails['ProductTotal'] = $cart['price']*$cart['qty'];
          $orderDetails['OrderID'] = $OrderId;
          $this->Home_Model->insertDB('orderdetails',$orderDetails);
          $prod = $this->Home_Model->selectWhere('products',['id' =>  $cart['id']]);
          $this->Home_Model->updateDB('products',['id' =>  $cart['id']],['stock_qty' => ($prod['stock_qty']-$cart['qty']) ]);
      }


      if($this->input->post('pay_via')){
          $transactionID = $response['TRANSACTIONID'];
          $timestamp = $response['TIMESTAMP'];
          $correlation_id = $response['CORRELATIONID'];
          $ack = $response['ACK'];
          $version = $response['VERSION'];
          $build = $response['BUILD'];
          $amount = $response['AMT'];
          $currency_code = $response['CURRENCYCODE'];
          $avs_code = $response['AVSCODE'];
          $cvv2 = $response['CVV2MATCH'];
          $trtime = date('Y-m-d H:i:s');
          $payment_type = 'Pay with Credit Card';
      }
      else{
          $transactionID = random_string('alnum', 17);
          $timestamp = date('Y-m-d H:i:s');
          $correlation_id = '';
          $ack = 'Success';
          $version = '';
          $build = '';
          $amount = $amount;
          $currency_code = '';
          $avs_code = '';
          $cvv2 = '';
          $trtime = date('Y-m-d H:i:s');
          $payment_type = 'Cash On Delievery';

      }
      $this->Home_Model->insertDB('order_payments', array(
              'fkUserId'=>$customer_id,
              'OrderId' => $OrderId,
              'paypal_timestamp'=>$timestamp,
              'correlation_id'=>$correlation_id,
              'ack'=>$ack,
              'version'=>$version,
              'build'=>$build,
              'amt'=>$amount,
              'currencycode'=>$currency_code,
              'avscode'=>$avs_code,
              'cvv2match'=>$cvv2,
              'transactionid'=>$transactionID,
              'payment_type'=> $payment_type,
              'date_added'=>$trtime
          )
      );
      $this->Home_Model->updateDB('orders',['OrderID'=>$OrderId],['TransactionStatus'=>"1"]);
      $this->session->set_userdata('order_id', $OrderId);

      $data = array(
        'invoice_no' => 'INV-'.$OrderId,
        'name' => $this->session->userdata('first_name').' '.$this->session->userdata('last_name'),
        'phone' => $this->session->userdata('phone'),
        'email' => $this->session->userdata('bill_email'),
        'address' => $this->session->userdata('address'),
        'city' => $this->session->userdata('city'),
        'zip' => $this->session->userdata('zip_code'),
        'order_no' => $OrderId,
        'order_date' => date('Y-m-d'),
        'transaction_id' => $transactionID,
        'transaction_time' => $trtime,
        'invoice_total' => $amount,
        'payment_type'=> $payment_type,
      );
      $data['isadmin'] = 0;
      $this->Home_Model->sendMail(APP_EMAIL,
                                  $this->session->userdata('bill_email'),
                                  APP_NAME,
                                  'Order Reciept Email',
                                  'emails/order_recipt',
                                  $data);
      $us = $this->Home_Model->simpleSelect('users');
      $data['isadmin'] = 1;
      foreach ($us as $key => $u) {
        if($this->ion_auth->is_admin($u['id'])){
          $this->Home_Model->sendMail(APP_EMAIL,
                                      $u['email'],
                                      APP_NAME,
                                      'Order Reciept Email',
                                      'emails/order_recipt',
                                      $data);
        }
      }

      $this->cart->destroy();
      $this->session->unset_userdata('first_name');
      $this->session->unset_userdata('last_name');
      $this->session->unset_userdata('bill_email');
      $this->session->unset_userdata('phone');
      $this->session->unset_userdata('countries');
      $this->session->unset_userdata('zip_code');
      $this->session->unset_userdata('city');
      $this->session->unset_userdata('address');
      $this->session->unset_userdata('create_an_account');
      $this->session->unset_userdata('bill_password');
      $this->session->unset_userdata('promocode');


      redirect('/order/order-details');
    }
    else{
      $messge = array('message' => $response['L_LONGMESSAGE0'] ,'class' => 'alert-danger');
      $this->session->set_flashdata('message', $messge);
      redirect("/payment",'refresh');
    }
  }

  public function updateCart(){

    $data = $this->cart->get_item($this->input->post('id'));
    $prod = $this->Home_Model->selectWhere('products',['id' =>  $data['id']]);
    if($this->input->post('qty') > $prod['stock_qty']){
      return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status'=>500,'msg'=>"Not Enough stock available"]));
    }
    else{
      $data = array(
        'rowid' => $this->input->post('id'),
        'qty'   => $this->input->post('qty'),
      );
      $this->cart->update($data);
      return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(['status'=>200,'msg'=>"Cart is updated"]));
    }

  }

  public function processBilling(){
    $first_name = $this->input->post('first_name');
    $last_name = $this->input->post('last_name');
    $bill_email = $this->input->post('bill_email');
    $phone = $this->input->post('phone');
    $countries = $this->input->post('countries');
    $zip_code = $this->input->post('zip_code');
    $city = $this->input->post('city');
    $address = $this->input->post('address');
    $create_an_account = $this->input->post('create_an_account');
    $bill_password = $this->input->post('bill_password');
    if(!count($this->cart->contents())){
      return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status'=>500,'msg'=>"Atleast add one item in cart to proceed"]));
    }
    if( $first_name == '' || $last_name == '' || $bill_email == '' || $phone == '' || $countries == ''
      || $zip_code == '' || $city == '' || $address == ''){
        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status'=>500,'msg'=>"Please fill all the required fields"]));
      }
    if($create_an_account == 1){
      if($bill_password == '' || strlen($bill_password) < 8){
        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status'=>500,'msg'=>"Password should be atleast 8 characters long"]));
      }
    }

    $this->session->set_userdata('first_name', $first_name);
    $this->session->set_userdata('last_name', $last_name);
    $this->session->set_userdata('bill_email', $bill_email);
    $this->session->set_userdata('phone', $phone);
    $this->session->set_userdata('countries', $countries);
    $this->session->set_userdata('zip_code', $zip_code);
    $this->session->set_userdata('city', $city);
    $this->session->set_userdata('address', $address);
    $this->session->set_userdata('create_an_account', $create_an_account);
    $this->session->set_userdata('bill_password', $bill_password);

    return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode([
                'status'=>200,
                'url'=>base_url().'payment',
                'msg'=>"You can now proceed for order payment "]));

  }

  public function displayCart(){
    echo '<pre/>';
    // $this->session->unset_userdata('first_name');
    // $this->session->unset_userdata('last_name');
    // $this->session->unset_userdata('bill_email');
    // $this->session->unset_userdata('phone');
    // $this->session->unset_userdata('countries');
    // $this->session->unset_userdata('zip_code');
    // $this->session->unset_userdata('city');
    // $this->session->unset_userdata('address');
    // $this->session->unset_userdata('create_an_account');
    // $this->session->unset_userdata('bill_password');
    // $this->cart->destroy();
    print_r($this->cart->contents());
  }

  public function getCartView(){
      $this->load->view("site/common/cartView");
  }

  public function add()
  {
    $product = $this->Home_Model->selectWhere('products',['id' => $this->input->post('id')]);
    $chk = $this->checkIfItemExistUpdateQty($this->input->post('id'));
    if($chk){
      return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status'=>200,'msg'=>"Item is updated in cart"]));
    }
    if($product['stock_qty'] == 0){
      return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status'=>500,'msg'=>"Not Enough Item qunatity in stock"]));

    }
    else{
      $insert_data = array(
        'id' => $this->input->post('id'),
        'name' => $product['name'],
        'price' => $product['price'],
        'qty' => 1
      );
      $this->cart->insert($insert_data);
      //echo 'query:'.$this->db->last_query();exit;
      return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode(['status'=>200,'msg'=>"Item is added to cart"]));
    }
  }


}
