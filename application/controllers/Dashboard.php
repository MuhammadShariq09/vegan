<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Dashboard.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');
    }

    /*public function index()
    {
        $data['users_groups']           =   $this->ion_auth->get_users_groups()->result();
        $data['users_permissions']      =   $this->ion_auth_acl->build_acl();

        $data['home_masterview'] = 'dashboard';
        $this->load->view(HOME_GENERALVIEW, $data);
    }*/

    public function index()
    {
        redirect("users", 'refresh');
        $data['home_masterview'] = 'dashboard';
        $this->load->view(HOME_GENERALVIEW, $data);
    }



    public function openCSV(){


        $csv = array_map('str_getcsv', file(base_url()."file.csv"));
        $headers =   $csv[0];

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });

        array_shift($csv); # remove column header
        // echo "<pre>";//        echo count($csv);//        print_r($csv); // echo $csv[19][$header[3]];

        $salt = $this->ion_auth->salt();
        $password = $this->ion_auth->hash_password("123456", $salt);

        $count = 0;
        foreach ($csv as $row) {
            $data = array();
            $exsist = false;
            foreach ($headers as $index => $header) {
                if($header == "email"){
                    $data['username'] = $row[$header];
                    $data['password'] = $password;
                    $ret = $this->Home_Model->selectWhere("users", ['email'=> $row[$header]]);
                    if(!$ret){
                        $exsist = true;
                    }
                }
                if($header == "dateofbirth"){
                    $row[$header] = date("Y-m-d", strtotime($row[$header]));
                }
                $data[$header] = $row[$header];
            }

            if($exsist) {
                $ret = $this->Home_Model->insertDB('users', $data);
                $ret = $this->Home_Model->insertDB('users_groups', ['user_id'=>$ret , "group_id" => 2 ]);
                if($ret ){ $count++; }
            }
        }

        echo "<h3 style='text-align: center'>Total Rows Inserted : ". $count."</h3>";

    }

    public function exportCSV(){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');
        $output = fopen("php://output", "w");
        $fields = $this->db->list_fields('users');

        unset($fields[1]);unset($fields[2]);unset($fields[0]);unset($fields[3]);unset($fields[4]);unset($fields[6]);unset($fields[7]);unset($fields[8]);unset($fields[9]);unset($fields[10]);unset($fields[11]);unset($fields[12]);unset($fields[19]);unset($fields[25]);unset($fields[29]);unset($fields[30]);unset($fields[31]);unset($fields[32]);unset($fields[33]);unset($fields[34]);unset($fields[24]);

        fputcsv($output, $fields);

        $this->data['users'] = json_decode(json_encode($this->ion_auth->users([2])->result() ), true );
        foreach ($this->data['users'] as $k => $user)
        {
            $cuser = $this->ion_auth->user()->row();
            if($user['id'] == "25" || $user['id'] == "1"){

            }else {
                unset($user['user_id']);
                fputcsv($output, $user);
            }

        }

        fclose($output);
    }

    public function importCSV($type){
        $target_dir = "./uploads/imports";
        $upload_cv = md5(date('Y-m-d H:i:s:u')) . rand(10, 99);

        $target_file = $target_dir . basename($_FILES["file"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $file = $target_dir . $upload_cv . "." . $imageFileType;

        if (move_uploaded_file($_FILES["file"]["tmp_name"], $file)) {
            $csv = array_map('str_getcsv', file($file));
            $headers =   $csv[0];

            array_walk($csv, function(&$a) use ($csv) {
                $a = array_combine($csv[0], $a);
            });

            array_shift($csv); # remove column header
            // echo "<pre>";//        echo count($csv);//        print_r($csv); // echo $csv[19][$header[3]];

            $salt = $this->ion_auth->salt();
            $password = $this->ion_auth->hash_password("123456", $salt);

            $count = 0;
            $total = count($csv);
            $data['response'] = "";
            foreach ($csv as $row) {
                $data = array();
                $exsist = false;
                foreach ($headers as $index => $header) {
                    if($header == "email"){
                        $data['username'] = $row[$header];
                        $data['password'] = $password;
                        if($type == "users") {
                            $ret = $this->Home_Model->selectWhere("users", ['email' => $row[$header]]);
                        }
                        if(!$ret){
                            $exsist = true;
                        }
                    }
                    if($header == "dateofbirth"){
                        $row[$header] = date("Y-m-d", strtotime($row[$header]));
                    }

                    if($type == "products" && $header == "sku") {
                        $ret = $this->Home_Model->selectWhere("products", ['sku' => $row[$header] ]);
                        if(!$ret){
                            $exsist = true;
                        }
                    }
                    $data[$header] = $row[$header];
                }

                if($exsist && $type == "users") {
                    $ret = $this->Home_Model->insertDB('users', $data);
                    $ret = $this->Home_Model->insertDB('users_groups', ['user_id'=>$ret , "group_id" => 2 ]);
                    if($ret){ $count++; }
                }else if($type == "products") {

                    $ret = $this->Home_Model->insertDB('products', $data);

                    if($ret){ $count++; }
                }
            }

            return "<h3 style='text-align: center;border: 1px solid #30485d;padding: 10px;background: #30485d;color: #fff;margin-bottom: 40px;'>Total ".ucfirst($type)." Inserted : ". $count." out of ".$total."</h3>";
        }
    }

    public function exportProductsCSV(){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=product_data.csv');
        $output = fopen("php://output", "w");
        $fields = $this->db->list_fields('products');

        unset($fields[0]);unset($fields[8]);unset($fields[10]);unset($fields[11]);unset($fields[12]);unset($fields[13]);unset($fields[14]);

        fputcsv($output, $fields);

        fclose($output);
    }

    public function importerExporter()
    {
        if(isset($_POST['import']) ){
            $data['response'] = $this->importCSV('users');
        }
        else if(isset($_POST['importproduct']) ){
            $data['response'] = $this->importCSV('products');
        }
        else if(isset($_POST['export']) ){
            $this->exportCSV();
            exit();
        }
        else if(isset($_POST['exportproduct']) ){
            $this->exportProductsCSV();
            exit();
        }
        $data['home_masterview'] = 'importExport';
        $this->load->view(HOME_GENERALVIEW, $data);
    }
}