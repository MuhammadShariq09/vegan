<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Dashboard.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Transactions extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else {
            $required_rights = "transactions_view";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
                $cuser = $this->ion_auth->user()->row();
                if ($this->ion_auth->is_admin() || $cuser->user_id == 1) {
                    $data['transactions'] = $this->Home_Model->simpleSelect('order_payments');
                } else {
                    $data['transactions'] = $this->Home_Model->selectWhereResult('order_payments', ['fkUserId' => $cuser->user_id]);
                }

                $data['home_masterview'] = 'transactions';
                $this->load->view(HOME_GENERALVIEW, $data);

            } else {
                return show_error('You must be have {' . strtoupper($required_rights) . '} permission to view this page.');
            }
        }
    }

}
