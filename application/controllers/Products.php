<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Products extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');

    }

    public function addToFavourites(){
      if(!$this->input->post('id')){
        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status'=>500,'msg'=>"Operation could not be performed"]));
      }
      $id = $this->input->post('id');
      $user_id = $this->session->userdata('user_id');
      $this->db->where("user_id", $user_id);
      $data = $this->db->get("favourites")->result_array();

      if(!count($data)){
        $this->Home_Model->insertDB('favourites',['product_id' => $id, 'user_id'=>$user_id]);
        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status'=>200,'msg'=>"Item is added to favourites"]));
      }
      else{
        $arr = explode(',',$data[0]['product_id']);
        if (in_array($id, $arr)){
          $arr = array_diff($arr, array($id));
          $this->Home_Model->updateDB('favourites',['user_id'=>$user_id],['product_id' => implode(',',$arr)]);
          return $this->output
                  ->set_content_type('application/json')
                  ->set_output(json_encode(['status'=>200,'msg'=>"Item is removed from favourites"]));

        }
        else{
          array_push($arr, $id);
          $this->Home_Model->updateDB('favourites',['user_id'=>$user_id], ['product_id' => implode(',',$arr)]);
          return $this->output
                  ->set_content_type('application/json')
                  ->set_output(json_encode(['status'=>200,'msg'=>"Item is added to favourites"]));
        }
      }
    }

    public function getfeedback(){
      $data['feedback'] = $this->Home_Model->selectWhereResult('feedback',['product_id'=>$this->input->post('id')]);
      $this->load->view("site/common/feedback", $data);
    }

    public function addfeedback(){
      $chk = $this->Home_Model->selectWhere('feedback',['product_id'=>$this->input->post('id'), 'email' => $this->input->post('email')]);
      if($chk){
        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status'=>500,'class'=>'error', 'msg'=>"You've already given a feedback for this product"]));
      }
      $this->Home_Model->insertDB('feedback', [
        'product_id' => $this->input->post('id'),
        'name' => $this->input->post('name'),
        'email' => $this->input->post('email'),
        'body' => $this->input->post('msg'),
        'approved' => 1,
        'review' => $this->input->post('rate'),
      ]);
      return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status'=>200,'class'=>'success', 'msg'=>"Feedback is added"]));

    }

    public function searchProduct(){
      $query =  $this->input->get('q');
      if($this->input->get('no_of_results'))
        $per_page = $this->input->get('no_of_results');
      else
        $per_page = 20;

      if($query && $query != ""){

        $this->db->like('name', $query);
        $res = $this->db->get('products')->result_array();
        $count = count($res);

        $data['products_count'] = $count;
        $config['base_url'] = base_url()."products/page_no";
        $config['total_rows'] = $count;
        $config['per_page'] = $per_page;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = true;
        //Adding Enclosing Markup
        $config['full_tag_open'] = '<p><ul  class="pagination">';
        $config['full_tag_close'] = '</ul></p><!--pagination-->';
        //Customizing the First Link
        $config['first_link'] = '<<';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        //Customizing the Last Link
        $config['last_link'] = '>>';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        //Customizing the "Next" Link
        $config['next_link'] = '>';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        //Customizing the "Previous" Link
        $config['prev_link'] = '<';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        //Customizing the "Current Page" Link
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li></li>";
        //Customizing the "Digit" Link
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        if($this->uri->segment(3) > 0)
            $offset = ($this->uri->segment(3) + 0)*$per_page - $per_page;
        else
            $offset = $this->uri->segment(3);

        $this->db->limit($config['per_page'], $offset);
        $this->db->like('name', $query);
        $products = $this->db->get('products')->result_array();
        foreach ($products as $key => $product) {
            $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$product['id']], 'image_path');
            $products[$key]['images'] = $images;
        }
        $data['products'] = $products;
        $data['home_masterview'] = 'productslist';
        $data['pagination'] =$this->pagination->create_links();
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
      }
      else{
        redirect('/products');
      }

    }

    public function index($page=null,$slug=null)
    {
        if($this->input->get('no_of_results'))
          $per_page = $this->input->get('no_of_results');
        else
          $per_page = 20;

        if($slug && $slug != "page_no"){
          $product = $this->Home_Model->selectWhere('products',['slug' => $slug]);
          if(!$product){
            if(theme=='vegan')
                redirect('/products');
            else
                redirect('/products');
            die;
          }
          $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$product['id']], 'image_path');
          $product['images'] = $images;
          $data['product'] = $product;
          $data['home_masterview'] = 'productDetail';
          $rev = $this->Home_Model->selectWhereResult('feedback',['product_id'=>$product['id']]);
          $sum = $count = 0;
          if(count($rev)){
            foreach ($rev as $key => $value) {
              $sum += $value['review'];
              $count++;
            }
            $data['rating'] = $sum/$count;
            //echo $sum.'/'.$count;
          }else{
            $data['rating'] = 0;
          }
          //die;
          $data['title'] = APP_NAME.' - Products - '.$product['name'];
            if(theme=='vegan')
                $this->load->view(siteview, $data);
            else
                $this->load->view(HOME_GENERAL_SITE_VIEW, $data);

        }
        else{
          $data['products_count'] = $count = $this->Home_Model->countResult('products');
          $config['base_url'] = base_url()."products/page_no";
          $config['total_rows'] = $count;
          $config['per_page'] = $per_page;
          $config["uri_segment"] = 3;
          $config['use_page_numbers'] = TRUE;
          $config['reuse_query_string'] = true;
          //Adding Enclosing Markup
          $config['full_tag_open'] = '<p><ul  class="pagination">';
          $config['full_tag_close'] = '</ul></p><!--pagination-->';
          //Customizing the First Link
          $config['first_link'] = '<<';
          $config['first_tag_open'] = '<li class="prev page">';
          $config['first_tag_close'] = '</li>';
          //Customizing the Last Link
          $config['last_link'] = '>>';
          $config['last_tag_open'] = '<li class="next page">';
          $config['last_tag_close'] = '</li>';
          //Customizing the "Next" Link
          $config['next_link'] = '>';
          $config['next_tag_open'] = '<li class="next page">';
          $config['next_tag_close'] = '</li>';
          //Customizing the "Previous" Link
          $config['prev_link'] = '<';
          $config['prev_tag_open'] = '<li class="prev page">';
          $config['prev_tag_close'] = '</li>';
          //Customizing the "Current Page" Link
          $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
          $config['cur_tag_close'] = "<span class='sr-only'></span></a></li></li>";
          //Customizing the "Digit" Link
          $config['num_tag_open'] = '<li class="page">';
          $config['num_tag_close'] = '</li>';

          $this->pagination->initialize($config);
          if($this->uri->segment(3) > 0)
              $offset = ($this->uri->segment(3) + 0)*$per_page - $per_page;
          else
              $offset = $this->uri->segment(3);

          $this->db->limit($config['per_page'], $offset);
          if($this->input->get('price')){
            if($this->input->get('price') == "low")
              $this->db->order_by('price', 'asc');
            else
              $this->db->order_by('price', 'desc');
          }
          $products = $this->db->get("products")->result_array();
          foreach ($products as $key => $product) {
              $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$product['id']], 'image_path');
              $products[$key]['images'] = $images;
          }

          $data['products'] = $products;
          $data['home_masterview'] = 'productslist';
          $data['title'] = APP_NAME.' - Products';
          $data['pagination'] =$this->pagination->create_links();
          if(theme=='vegan')
              $this->load->view(siteview, $data);
          else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
        }

    }

    public function category($slug){
        $slug = htmlspecialchars($slug);
        $category = $this->Home_Model->selectWhere('categories',['slug' => $slug]);
        $categoryId = $category['id'];
        $products_in_category = $this->Home_Model->selectWhereResult('products_cat_link',['cat_id' => $categoryId]);
//        print_r($products_in_category);
        $products = array();
        $productsCount = 0;
        foreach ($products_in_category as $productId_in_category) {
            $product = $this->Home_Model->selectWhere('products',['id' => $productId_in_category['product_id']]);
            $images = $this->Home_Model->selectWhereResultWithFields('images', ['product_id'=>$product['id']], 'image_path');
            $product['images'] = $images;
            if(!empty($product)){
                array_push($products, $product);
                $productsCount++;
            }
        }
        $data['products_count'] = $productsCount;
        $data['products'] = $products;
        $data['home_masterview'] = 'productslist';
        $data['title'] = APP_NAME.' - Products';
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);

    }

}
