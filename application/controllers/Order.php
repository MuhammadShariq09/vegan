<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('cart');
    $this->load->helper('string');
  }

  public function orderDetails($oi = null){
    if(! $this->ion_auth->logged_in() ){
      $order_id = $this->session->userdata('order_id');
      $data['orders'] = $this->Home_Model->selectWhere('orders',['OrderID'=>$order_id]);
      $data['payment_detail'] = $this->Home_Model->selectWhere('order_payments',['OrderId'=>$order_id]);
      $data['orderdetails'] = $this->Home_Model->selectWhereResult('orderdetails',['OrderId'=>$order_id]);
      $data['home_masterview'] = 'order_details';
        $data['title'] = APP_NAME.' - Order Detail';
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }
    else{
      $user_id = $this->session->userdata('user_id');
      if($oi)
        $order_id = $oi;
      else
        $order_id = $this->session->userdata('order_id');
      $data['orders'] = $this->Home_Model->selectWhere('orders',['OrderID'=>$order_id, 'CustomerID' => $user_id]);
      $data['payment_detail'] = $this->Home_Model->selectWhere('order_payments',['OrderId'=>$order_id]);
      $data['orderdetails'] = $this->Home_Model->selectWhereResult('orderdetails',['OrderId'=>$order_id]);
      $data['home_masterview'] = 'order_details';
        $data['title'] = APP_NAME.' - Order Detail';
        if(theme=='vegan')
            $this->load->view(siteview, $data);
        else
            $this->load->view(HOME_GENERAL_SITE_VIEW, $data);
    }

  }

}
