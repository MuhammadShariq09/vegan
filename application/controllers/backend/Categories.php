<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Users.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Categories extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');

        /*if( ! $this->ion_auth_acl->has_permission('access_admin') )
            redirect('/dashboard');*/
    }
    function check_slug($str)
    {
        $id = $this->input->post('id');
        $condition = array('id !='=>$id,'slug'=>$str);
        $ret = $this->Home_Model->selectWhere('categories', $condition);
        if($ret==true){
            $this->form_validation->set_message('check_slug', '%s already exist!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function check_featured($str)
    {
        $count = $this->Home_Model->countResult('categories',['featured'=>1]);
        if($count>=4){
            $this->form_validation->set_message('check_featured', 'Only 4 categories can be set to featured!');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    public function create_slug($name, $id=null)
    {
        $count = 0;
        $name = url_title($name);
        $slug_name = $name;             // Create temp name
        while(true)
        {
            $this->db->select('id');
//            $this->db->where('id !=', $id);
            $this->db->where('slug', $slug_name);   // Test temp name
            $query = $this->db->get('categories');
            if ($query->num_rows() == 0) break;
            $slug_name = $name . '-' . (++$count);  // Recreate new temp name
        }
        return strtolower($slug_name);      // Return temp name
    }
    public function place_slug(){
      return $this->output
              ->set_output($this->create_slug($this->input->post('name')));
    }

    public function index()
    {

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        /*else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }*/
        else
        {
            $required_rights = "categories_view";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
                //$this->load->view('auth/index', $this->data);
                $this->data['categories'] = $this->build_nestable_menu(0);
                $this->data['home_masterview'] = 'categories';
                //echo "<pre>";
                //print_r( $this->data);die;
                $this->load->view(HOME_GENERALVIEW, $this->data);
            }else{
                return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
            }


        }
    }

    public function update_categories(){

        $required_rights = "categories_update";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $readbleArray = $this->parseJsonArray(json_decode($this->input->post('data')));
            $i=0;
            foreach($readbleArray as $row){
                $i++;
                $updateArray = array(
                    'parent_id'=>$row['parent_id'],
                    'sorting_id'=>$i
                );
                $this->db->update('categories', $updateArray, array('id'=>$row['id']));
                //print_r($updateArray);
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }



    }

    public function parseJsonArray($jsonArray, $parent_id = 0) {
        $return = array();
        foreach ($jsonArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray->children)) {
                $returnSubSubArray = $this->parseJsonArray($subArray->children, $subArray->id);
            }
            $return[] = array('id' => $subArray->id, 'parent_id' => $parent_id);
            $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }


    public function image_resize($image_data, $destPath, $width, $height) {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $image_data['full_path'];
        $config['new_image'] = $destPath . $image_data['file_name'];
        $config['width'] = 350;
        $config['height'] = 280;
        $config['maintain_ratio'] = FALSE;

        //send config array to image_lib's  initialize function
        $this->image_lib->initialize($config);
        $src = $config['new_image'];
        $data['new_image'] = substr($src, 2);
        $data['img_src'] = base_url() . $data['new_image'];
        // Call resize function in image library.
        $this->image_lib->resize();
        // Return new image contains above properties and also store in "upload" folder.
        return $data;
    }


    public function save_category_status(){
        $id = $this->input->post('id');
        $cat = $this->Home_Model->selectWhere("categories", ["id"=>$id]);
        if($cat['active'] == 0){
            $ret = $this->Home_Model->updateDB("categories", ["id"=>$id], ["active"=>"1"]);
            if($ret){
                $status['success'] = 1;
                $status['msg'] = $cat['name'] . " (".$cat['slug'].") Active Successfully!";
                $status['status'] = "Click to Deactive";
            }
        }elseif($cat['active'] == 1){
            $ret = $this->Home_Model->updateDB("categories", ["id"=>$id], ["active"=>"0"]);
            if($ret){
                $status['success'] = 1;
                $status['msg'] = $cat['name'] . " (".$cat['slug'].") Deactive Successfully!";
                $status['status'] = "Click to Active";
            }
        }else{
            $status['success'] = 0;
            $status['msg'] = "Something Went Wrong";
        }
        echo json_encode($status);
        //print_r($cat);
        //echo $cat['name'];


    }



    public function get_category(){

        $required_rights = "categories_view";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $id = $this->input->post('id');
            $cat = $this->Home_Model->selectWhere("categories", ["id"=>$id]);

            $arr['success'] = '1';
            $arr['msg'] = 'Category Fetched!';
            $arr['name'] = $cat['name'];
            $arr['slug'] = $cat['slug'];
            $arr['description'] = $cat['description'];
            $arr['image'] = base_url('uploads/categories/thumb/') . $cat['image'];
            $arr['icon'] = base_url('uploads/categories/thumb/') . $cat['icon'];
            $arr['status'] = $cat['active'];
            $arr['id'] = $cat['id'];
            $arr['featured'] = $cat['featured'];

            echo json_encode($arr);
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function save_category(){
        $required_rights = "categories_add_edit";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            if($this->input->post('id') != ''){
                $validate = array(
                    array(
                        'field' => 'name',
                        'label' => 'Category Name',
                        'rules' => 'trim|required|max_length[255]',
                    ),
                    array(
                        'field' => 'slug',
                        'label' => 'Category Slug',
                        'rules' => 'trim|max_length[255]|callback_check_slug',
                    ),
                    array(
                        'field' => 'description',
                        'label' => 'Category Description',
                        'rules' => 'trim|max_length[255]',
                    ),
                    array(
                        'field' => 'status',
                        'label' => 'Category Status',
                        'rules' => 'trim|required|max_length[1]',
                    )
                );
                $arr['type'] = 'edit';
                if(($this->input->post('featured') == "on")){
                  $validate[] = array(
                      'field' => 'featured',
                      'label' => 'Featured',
                      'rules' => 'callback_check_featured',
                  );
                }

                $this->form_validation->set_message('required', 'Please enter %s');
                $this->form_validation->set_rules($validate);
                $this->form_validation->set_error_delimiters('', '');
                if ($this->form_validation->run() == TRUE) {
                    $updateArray = array(
                        'name' => $this->input->post('name'),
                        'description' => $this->input->post('description'),
                        'slug' => $this->input->post('slug'),
                        'active' => $this->input->post('status'),
                        'featured' => ($this->input->post('featured')=="on") ? 1:0,
                    );


                    if($this->input->post('ci_b64')){
                        $image64 = $this->input->post('ci_b64');
                        if(!empty($image64) || $image64 != ''){
                          list($type, $image64) = explode(';', $image64);
                          list(,$extension) = explode('/',$type);
                          list(, $image64)      = explode(',', $image64);
                        }
                        $image64 = base64_decode($image64);
                        $time = time();
                        $path = './uploads/categories';
                        $path1 = './uploads/categories/thumb/';
                        $filename = md5(rand(11111111,99999999)).'.'.$extension;
                        file_put_contents($path.$filename, $image64);
                        file_put_contents($path1.$filename, $image64);
                        $updateArray['image'] = $filename;
                    }

                    $this->db->update('categories', $updateArray, array('id' => $this->input->post('id')));
                    redirect("/backend/categories", 'refresh');
                }else{
                    $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                    $this->data['message'] = $messge;
                    $this->data['categories'] = $this->build_nestable_menu(0);
                    $this->data['home_masterview'] = 'categories';
                    $this->load->view(HOME_GENERALVIEW, $this->data);
                }
            } else {
                $validate = array(
                    array(
                        'field' => 'name',
                        'label' => 'Category Name',
                        'rules' => 'trim|required|max_length[255]',
                    ),
                    array(
                        'field' => 'slug',
                        'label' => 'Category Slug',
                        'rules' => 'trim|max_length[255]|callback_check_slug',
                    ),
                    array(
                        'field' => 'description',
                        'label' => 'Category Description',
                        'rules' => 'trim|max_length[255]',
                    ),
                    array(
                        'field' => 'status',
                        'label' => 'Category Status',
                        'rules' => 'trim|required|max_length[1]',
                    ),
                    array(
                        'field' => 'ci_b64',
                        'label' => 'Category Image',
                        'rules' => 'required',
                    )
                );
                if(($this->input->post('featured') == "on")){
                  $validate[] = array(
                      'field' => 'featured',
                      'label' => 'Featured',
                      'rules' => 'callback_check_featured',
                  );
                }

                $this->form_validation->set_message('required', 'Please enter %s');
                $this->form_validation->set_rules($validate);
                $this->form_validation->set_error_delimiters('', '');
                $arr['type'] = 'add';

                if ($this->form_validation->run() == TRUE) {

                  $updateArray = array(
                      'name'=>$this->input->post('name'),
                      'slug'=>$this->input->post('slug'),
                      'description' => $this->input->post('description'),
                      'sorting_id'=>0,
                      'active'=>$this->input->post('status'),
                      'featured' => ($this->input->post('featured') == "on") ? 1:0,
                  );

                    if($this->input->post('ci_b64')){
                        $image64 = $this->input->post('ci_b64');
                        if(!empty($image64) || $image64 != ''){
                          list($type, $image64) = explode(';', $image64);
                          list(,$extension) = explode('/',$type);
                          list(, $image64)      = explode(',', $image64);
                        }
                        $image64 = base64_decode($image64);
                        $time = time();
                        $path = './uploads/categories';
                        $path1 = './uploads/categories/thumb/';
                        $filename = md5(rand(11111111,99999999)).'.'.$extension;
                        file_put_contents($path.$filename, $image64);
                        file_put_contents($path1.$filename, $image64);
                        $updateArray['image'] = $filename;
                    }

                    $this->db->insert('categories', $updateArray);

                    $insert_id = $this->db->insert_id();
                    redirect("/backend/categories", 'refresh');
                }else{
                    $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                    $this->data['message'] = $messge;
                    $this->data['categories'] = $this->build_nestable_menu(0);
                    $this->data['home_masterview'] = 'categories';
                    $this->load->view(HOME_GENERALVIEW, $this->data);
                }

            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }


//        print json_encode($arr);
    }
    function category_delete(){
        $required_rights = "categories_delete";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $this->recursiveDelete($this->input->post('id'));
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }


    }
    function recursiveDelete($id) {
        $query = $this->db->query("select * from categories where parent_id = '".$id."' ")->result_array();
        if ($query) {
            foreach($query as $current){
                $this->recursiveDelete($current['id']);
            }
        }
        $this->db->query("delete from categories where id = '".$id."' ");
    }

    //PRODUCTS
    public function add_product()
    {

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        /*else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }*/
        else
        {
            /*// set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //list the users
            $this->data['users'] = $this->ion_auth->users()->result();
            foreach ($this->data['users'] as $k => $user)
            {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }*/

            //$this->load->view('auth/index', $this->data);
            $this->data['categories'] = $this->build_nestable_menu(0, true);
            $this->data['home_masterview'] = 'add_product';
            //echo "<pre>";
            //print_r( $this->data);die;
            $this->load->view(HOME_GENERALVIEW, $this->data);
        }
    }


/*    function send_email(){
        //Load email library
        $this->load->library('email');

//SMTP & mail configuration
        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.office365.com',
            'smtp_port' => 587,
            'smtp_user' => 'noreply@marriagemindedonly.con',
            'smtp_pass' => 'Getmarried1!',
            'mailtype'  => 'html',
            'smtp_crypto'  => 'tls',
            'charset'   => 'utf-8'
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

//Email content
        $htmlContent = '<h1>Sending email via SMTP server</h1>';
        $htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';

        $this->email->to('fahad.noor@salsoft.net');
        $this->email->from('noreply@marriagemindedonly.con','noreply@marriagemindedonly.con');
        $this->email->subject('How to send email via SMTP ');
        $this->email->message($htmlContent);

//Send email
       echo $this->email->send();
       print_r($this->email->print_debugger());
    }*/

}
