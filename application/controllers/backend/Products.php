<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Users.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Products extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');
    }

    public function create_slug($name, $id=null)
    {
        $count = 0;
        $name = url_title($name);
        $slug_name = $name;             // Create temp name
        while(true)
        {
            $this->db->select('id');
            // $this->db->where('id !=', $id);
            $this->db->where('slug', $slug_name);   // Test temp name
            $query = $this->db->get('products');
            if ($query->num_rows() == 0) break;
            $slug_name = $name . '-' . (++$count);  // Recreate new temp name
        }
        return strtolower($slug_name);      // Return temp name
    }

    public function place_slug(){
      return $this->output
              ->set_output($this->create_slug($this->input->post('name')));
    }

    public function setFeatured($id){
        $chk = $this->Home_Model->selectWhere('products', ['id'=>$id]);
        if($chk){
          //if prod is going to remove from featured do it
          if($chk['featured']){
            $this->Home_Model->updateDB('products', ['id'=>$id], ['featured'=>!$chk['featured']]);
            return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status' => 200, 'msg'=>'Prod is removed from featured']));
          }
          else{
            $count = $this->Home_Model->countResult('products',['featured'=>1]);
            if($count >= 6){
              return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status' => 500, 'msg'=>'You can only set 6 Products to be featured']));
            }
            else{
              $this->Home_Model->updateDB('products', ['id'=>$id], ['featured'=>!$chk['featured']]);
              return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status' => 200, 'msg'=>'Product is set to featured']));
            }
          }
        }
        return $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode(['status' => 500, 'msg' => 'Could not perform operation']));
    }


    public function deleteImage($id){
        if(!$id){
          return $this->output
                  ->set_content_type('application/json')
                  ->set_output(json_encode(['status' => 500, 'Image can not be deleted']));
        }
        else{
         $this->Home_Model->deleteWhere('images',['id' => $id]);
          return $this->output
                  ->set_content_type('application/json')
                  ->set_output(json_encode(['status' => 200, 'msg' => 'Image is be deleted']));

        }

    }

    function check_slug($str)
    {
        $id = $this->input->post('product_id');
        $condition = array('id !='=>$id,'slug'=>$str);
        $ret = $this->Home_Model->selectWhere('products', $condition);
        if($ret==true){
            $this->form_validation->set_message('check_slug', '%s already exist!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function check_sku($str)
    {
        $id = $this->input->post('product_id');
        $condition = array('id !='=>$id,'sku'=>$str);
        $ret = $this->Home_Model->selectWhere('products', $condition);
        if($ret==true){
            $this->form_validation->set_message('check_sku', '%s already exist!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function SampleDateCheck($sampleDate){
        if (preg_match("/\d{4}\-\d{2}-\d{2}/", $sampleDate)) {
            $sampleDate=date_format(date_create($sampleDate),"Y-m-d");
            if($sampleDate<date("Y-m-d")){
                $this->form_validation->set_message('SampleDateCheck', '%s Invalid Date!');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->form_validation->set_message('SampleDateCheck', '%s Invalid Date!');
            return FALSE;
        }

    }

    function is_money($str) {
        if($str !=''){
            if(is_int($str) || is_float($str)){
                return true;
            }else{
                $this->form_validation->set_message('is_money', '%s is not valid!');
                return false;
            }
        }
    }

    public function index()
    {
        $required_rights = "products_view";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
            $this->data['products'] = $this->db->query('select * from products order by id desc')->result();
            foreach ($this->data['products'] as $k => $product) {
                $this->data['products'][$k]->cats = $this->db->query('SELECT a.name FROM categories a INNER JOIN products_cat_link pc ON a.id=pc.cat_id WHERE pc.product_id=' . $product->id)->result();
            }
            $this->data['home_masterview'] = 'products';
            $this->load->view(HOME_GENERALVIEW, $this->data);
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }
    public function deleteReview($id){
        $this->Home_Model->deleteWhere('feedback',['id' => $id]);
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function review($id){
        $required_rights = "rating_view";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
            $this->data['name'] = $this->db->query('select name from products WHERE id = '.$id)->result();
            $this->data['reviews'] = $this->db->query('select * from feedback WHERE product_id = '.$id)->result();
            $this->data['home_masterview'] = 'product_review';
            $this->load->view(HOME_GENERALVIEW, $this->data);
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function add_product()
    {

       // die;
        $required_rights = "products_add";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
            if (!$this->ion_auth->logged_in()) {
                // redirect them to the login page
                redirect('auth/login', 'refresh');
            } elseif (isset($_POST) && !empty($_POST)) {
                $config = array(
                    array(
                        'field' => 'product_name',
                        'label' => 'Product Name',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'product_slug',
                        'label' => 'Product Slug',
                        'rules' => 'trim|callback_check_slug|required|alpha_dash',
                    ),
                    array(
                        'field' => 'product_sku',
                        'label' => 'Product S.K.U',
                        'rules' => 'trim|required|callback_check_sku',
                    ),
                    array(
                        'field' => 'product_price',
                        'label' => 'Sales Price',
                        'rules' => 'trim|required|numeric|less_than_equal_to['.$this->input->post('reg_price').']',
                    ),
                    array(
                        'field' => 'reg_price',
                        'label' => 'Regular Price',
                        'rules' => 'trim|required|numeric',
                    ),
                    array(
                        'field' => 'product_description',
                        'label' => 'Product Description',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'product_specifications',
                        'label' => 'Product Specifications',
                        'rules' => 'trim|required',
                    )
                );

                $config[] = array(
                    'field' => 'categories[]',
                    'label' => 'Categories',
                    'rules' => 'trim|required',
                );

                $this->form_validation->set_message('valid_email', 'Invalid %s');
                $this->form_validation->set_message('required', 'Please enter %s');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('', '');
                if ($this->form_validation->run() == FALSE) {
                    $error_message = '';
                    if (form_error('product_name')) {
                        $error_message = form_error('product_name');
                    } elseif (form_error('product_slug')) {
                        $error_message = form_error('product_slug');
                    } elseif (form_error('product_sku')) {
                        $error_message = form_error('product_sku');
                    } elseif (form_error('product_price')) {
                        $error_message = form_error('product_price');
                    } elseif (form_error('reg_price')) {
                        $error_message = form_error('reg_price');
                    } elseif (form_error('sale_from')) {
                        $error_message = form_error('sale_from');
                    } elseif (form_error('sale_to')) {
                        $error_message = form_error('sale_to');
                    } elseif (form_error('stock_qty')) {
                        $error_message = form_error('stock_qty');
                    } elseif (form_error('stock_status')) {
                        $error_message = form_error('stock_status');
                    } elseif (form_error('stock_qty')) {
                        $error_message = form_error('stock_qty');
                    } elseif (form_error('product_description')) {
                        $error_message = form_error('product_description');
                    } elseif (form_error('product_specifications')) {
                        $error_message = form_error('product_specifications');
                    } elseif (form_error('categories[]')) {
                        $error_message = form_error('categories[]');
                    }
                    $this->data['attrs'] = $this->Home_Model->simpleSelect('attributes');
                    $this->data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                    $this->data['categories'] = $this->build_nestable_menu(0, true, true);
                    $this->data['home_masterview'] = 'add_product';
                    $this->load->view(HOME_GENERALVIEW, $this->data);

                } else {

                    $data = array(
                        'name' => $this->input->post('product_name'),
                        'slug' => (($this->input->post('product_slug') != '') ? $this->input->post('product_slug') : $this->create_slug($this->input->post('product_name'), $this->input->post('product_id'))),
                        'sku' => $this->input->post('product_sku'),
                        'price' => $this->input->post('product_price'),
                        'reg_price' => $this->input->post('reg_price'),
                        'sale_from' => $this->input->post('sale_from'),
                        'sale_to' => $this->input->post('sale_to'),
                        'stock_qty' => $this->input->post('stock_qty'),
                        'stock_status' => $this->input->post('stock_status'),
                        'stock_management' => (($this->input->post('stock_management') != '') ? $this->input->post('stock_management') : 0),
                        'description' => $this->input->post('product_description'),
                        'product_specifications' => $this->input->post('product_specifications'),
                        'createdate' => CURRENT_DATETIME,
                        'createby' => $this->ion_auth->get_user_id()
                    );

                    if ($this->input->post('stock_management') == 1 && $this->input->post('stock_qty') == 0) {
                        $data['stock_status'] = 'out of stock';
                    }
                    $this->Home_Model->insertDB('products', $data);
                    $product_id = $this->db->insert_id();
                    foreach ($_POST['categories'] as $cat_id) {
                        $this->Home_Model->insertDB('products_cat_link',
                            [
                                'product_id' => $product_id,
                                'cat_id' => $cat_id,
                            ]
                        );
                    }

                    $image64 = $this->input->post('pi_b64');
                    if(!empty($image64) || $image64 != ''){
                      list($type, $image64) = explode(';', $image64);
                      list(,$extension) = explode('/',$type);
                      list(, $image64)      = explode(',', $image64);
                    }
                    $image64 = base64_decode($image64);
                    $time = time();
                    $path = './uploads/';
                    $filename = md5(rand(11111111,99999999)).'.'.$extension;
                    file_put_contents($path.$filename, $image64);

                    $this->Home_Model->insertDB('images',
                        [
                            'image_path' => $filename,
                            'createdate' => CURRENT_DATETIME,
                            'main_img' => 1,
                            'product_id' => $product_id,
                        ]
                    );


                    $message = array('message' => 'Product Successfully created!', 'class' => 'alert-success');
                    $this->session->set_flashdata('message', $message);
                    redirect("backend/products", 'refresh');
                }

            } else {
                $this->data['attrs'] = $this->Home_Model->simpleSelect('attributes');
                $this->data['categories'] = $this->build_nestable_menu(0, true, true);
                $this->data['home_masterview'] = 'add_product';

                $this->load->view(HOME_GENERALVIEW, $this->data);
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }

    public function deleteProduct($id){
        $required_rights = "products_edit";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $this->db->delete('products_cat_link', array('product_id' => $id));
            $this->db->delete('products', array('id' => $id));
            redirect("/backend/products", 'refresh');
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }

    public function if_product_image_not_exists($field, $id){
        $chk = $this->Home_Model->countResult('images',['product_id' => $id]);
        if(!$chk){
          if(!$this->input->post('pi_b64')){
            $this->form_validation->set_message('if_product_image_not_exists', 'There should be atleast one image of Product');
            return FALSE;
          }
        }
        return TRUE;
   }

    public function edit_product($id)
    {

        $required_rights = "products_edit";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {


            if(isset($_POST) && !empty($_POST) && !empty($id)){
                $config = array(
                    array(
                        'field' => 'product_name',
                        'label' => 'Product Name',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'product_slug',
                        'label' => 'Product Slug',
                        'rules' => 'trim|callback_check_slug|required|alpha_dash',
                    ),
                    array(
                        'field' => 'product_sku',
                        'label' => 'Product S.K.U',
                        'rules' => 'trim|required|callback_check_sku',
                    ),
                    array(
                        'field' => 'stock_qty',
                        'label' => 'Stock Qty',
                        'rules' => 'trim|required|numeric',
                    ),
                    array(
                        'field' => 'product_price',
                        'label' => 'Sales Price',
                        'rules' => 'trim|numeric|less_than_equal_to['.$this->input->post('reg_price').']',
                    ),
                    array(
                        'field' => 'reg_price',
                        'label' => 'reg_price',
                        'rules' => 'trim|required|numeric',
                    ),
                    array(
                        'field' => 'product_description',
                        'label' => 'Product Description',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'product_specifications',
                        'label' => 'Product Specifications',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'product_images',
                        'label' => 'Product Images',
                        'rules' => 'callback_if_product_image_not_exists['.$id.']',
                    )
                );

                $config[] = array(
                    'field' => 'categories[]',
                    'label' => 'Categories',
                    'rules' => 'trim|required',
                );


                $this->form_validation->set_message('valid_email', 'Invalid %s');
                $this->form_validation->set_message('required', 'Please enter %s');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('', '');

                if ($this->form_validation->run() == FALSE) {
                    $error_message='';
                    if (form_error('product_name')) {
                        $error_message = form_error('product_name');
                    }elseif (form_error('product_slug')) {
                        $error_message = form_error('product_slug');
                    }elseif (form_error('product_sku')) {
                        $error_message = form_error('product_sku');
                    }elseif (form_error('product_price')) {
                        $error_message = form_error('product_price');
                    }elseif (form_error('reg_price')) {
                        $error_message = form_error('reg_price');
                    }elseif (form_error('sale_from')) {
                        $error_message = form_error('sale_from');
                    }elseif (form_error('sale_to')) {
                        $error_message = form_error('sale_to');
                    }elseif (form_error('stock_qty')) {
                        $error_message = form_error('stock_qty');
                    }elseif (form_error('stock_qty')) {
                        $error_message = form_error('stock_qty');
                    }elseif (form_error('product_description')) {
                        $error_message = form_error('product_description');
                    }elseif (form_error('product_specifications')) {
                        $error_message = form_error('product_specifications');
                    }elseif (form_error('categories[]')) {
                        $error_message = form_error('categories[]');
                    }elseif (form_error('product_images')) {
                        $error_message = form_error('product_images');
                    }

                    $this->data['product'] = $this->Home_Model->selectWhere('products', ['id'=>$id]);
                    $this->data['images'] = $this->Home_Model->selectWhereResult('images', ['product_id'=>$id]);
                    $this->data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                    $this->data['categories'] = $this->build_nestable_menu(0, true, true);
                    $attributes = $this->Home_Model->queryDB('SELECT a.name, a.id  FROM  `attributes` a JOIN `link_attributes_products` b ON a.id=b.`attribute_id` WHERE b.`product_id`='.$id.' GROUP BY a.id');
                    $attr_array = array();
                    $i=0;
                    foreach($attributes as $attr){
                        $attr_array[$i]['id'] = $attr['id'];
                        $attr_array[$i]['name'] = $attr['name'];
                        $attr_array[$i]['values'] = $this->Home_Model->queryDB('SELECT a.id,a.name,b.price,b.image_path  FROM  `attributes_values` a JOIN `link_attributes_products` b ON a.id=b.`attribute_value_id` WHERE b.`product_id`="'.$id.'"');
                        $i++;
                    }

                    $this->data['home_masterview'] = 'edit_product';
                    $this->load->view(HOME_GENERALVIEW, $this->data);

                }else{

                    $data = array(
                        'name'=>$this->input->post('product_name'),
                        'slug'=>(($this->input->post('product_slug') != '')?$this->input->post('product_slug'):$this->create_slug($this->input->post('product_name'),$this->input->post('product_id'))),
                        'sku'=>$this->input->post('product_sku'),
                        'price'=>$this->input->post('product_price'),
                        'reg_price'=>$this->input->post('reg_price'),
                        'sale_from'=>$this->input->post('sale_from'),
                        'sale_to'=>$this->input->post('sale_to'),
                        'stock_qty'=>$this->input->post('stock_qty'),
                        'stock_status'=>$this->input->post('stock_status'),
                        'stock_management'=>(($this->input->post('stock_management') != '') ? $this->input->post('stock_management') : 0),
                        'description'=>$this->input->post('product_description'),
                        'product_specifications'=>$this->input->post('product_specifications'),
                        'updatedate'=>CURRENT_DATETIME,
                        'updateby'=>$this->ion_auth->get_user_id()
                    );

                    if($this->input->post('stock_management') == 1 && $this->input->post('stock_qty')==0){
                        $data['stock_status'] = 'out of stock';
                    }
                    $this->Home_Model->updateDB('products',['id'=>$id],$data);
                    $this->Home_Model->deleteWhere('products_cat_link', ['product_id'=>$id]);
                    //$product_id=$this->db->insert_id();
                    foreach($_POST['categories'] as $cat_id){
                        $this->Home_Model->insertDB('products_cat_link',
                            [
                                'product_id'=>$id,
                                'cat_id'=>$cat_id,
                            ]
                        );
                    }

                    if($this->input->post('pi_b64')){
                        $image64 = $this->input->post('pi_b64');
                        if(!empty($image64) || $image64 != ''){
                          list($type, $image64) = explode(';', $image64);
                          list(,$extension) = explode('/',$type);
                          list(, $image64)      = explode(',', $image64);
                        }
                        $image64 = base64_decode($image64);
                        $time = time();
                        $path = './uploads/';
                        $filename = md5(rand(11111111,99999999)).'.'.$extension;
                        file_put_contents($path.$filename, $image64);

                        $this->Home_Model->insertDB('images',
                        [
                          'image_path' => $filename,
                          'createdate' => CURRENT_DATETIME,
                          'main_img' => 1,
                          'product_id' => $id,
                        ]
                      );
                    }

                    $message = array('message' => 'Product updated !', 'class' => 'alert-success');
                    $this->session->set_flashdata('message', $message);
                    redirect("backend/products", 'refresh');
                }
            }else{

                $this->data['product'] = $this->Home_Model->selectWhere('products', ['id'=>$id]);
                $this->data['attrs'] = $this->Home_Model->simpleSelect('attributes');
                $this->data['images'] = $this->Home_Model->selectWhereResult('images', ['product_id'=>$id]);
                $select_categories = $this->Home_Model->selectWhereResult('products_cat_link', ['product_id'=>$id]);
                $push_sc = array();
                foreach($select_categories as $sc){
                    $push_sc[] = $sc['cat_id'];
                }
                $attributes = $this->Home_Model->queryDB('SELECT a.name, a.id  FROM  `attributes` a JOIN `link_attributes_products` b ON a.id=b.`attribute_id` WHERE b.`product_id`='.$id.' GROUP BY a.id');
                $attr_array = array();
                $i=0;
                foreach($attributes as $attr){
                    $attr_array[$i]['id'] = $attr['id'];
                    $attr_array[$i]['name'] = $attr['name'];
                    $attr_array[$i]['values'] = $this->Home_Model->queryDB('SELECT a.id,a.name,b.price,b.image_path  FROM  `attributes_values` a JOIN `link_attributes_products` b ON a.id=b.`attribute_value_id` WHERE b.`product_id`="'.$id.'"');
                    $i++;
                }
                $this->data['selected_attrs'] = $attr_array;

                //echo "<pre>";print_r($attr_array);die;
                $this->data['categories'] = $this->build_nestable_menu(0, true,true, $push_sc);
                $this->data['home_masterview'] = 'edit_product';

                $this->load->view(HOME_GENERALVIEW, $this->data);
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }

    public function delete_product($product_id=false)
    {
        $required_rights = "products_delete";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){

            if( ! $product_id )
            {
                $this->session->set_flashdata('message', "No product ID passed");
                redirect("backend/products", 'refresh');
            }



            if($this->Home_Model->selectWhere('products', ['id'=>$product_id])){
                $this->Home_Model->deleteWhere('products', ['id'=>$product_id]);
                //print_r($this->ion_auth->messages());die;
                $message = array('message' => 'Product Successfully Deleted', 'class' => 'alert-success');
                $this->session->set_flashdata('message', $message);
                redirect("backend/products", 'refresh');
            }else{
                $message = array('message' =>'Product Not Found!', 'class' => 'alert-danger');
                $this->session->set_flashdata('message', $message);
                redirect("backend/products", 'refresh');
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function attributes($id=false)
    {
        $required_rights = "attributes_view";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
            if($id){
                $attribute = $this->Home_Model->queryDB("select name, id from attributes where id='".$id."'", true);
                $query = $this->Home_Model->queryDB("select * from attributes_values where attribute_id='".$id."'");
                if(!$attribute){
                    redirect('products/attributes', 'redirect');
                }
                $data['attribute'] = $attribute['name'];
                $data['attribute_id'] = $attribute['id'];
                $data['attributes_values'] = $query;
                $data['home_masterview'] = 'attributes_values';
                $this->load->view(HOME_GENERALVIEW, $data);
            }else{
                $data['attributes'] = $this->Home_Model->queryDB('SELECT attr.id,attr.name, COUNT(attrv.name) AS count_values,attr.createdate FROM attributes attr LEFT JOIN attributes_values attrv ON attr.id=attrv.attribute_id GROUP BY attr.name');
                $data['home_masterview'] = 'attributes';
                $this->load->view(HOME_GENERALVIEW, $data);
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function delete_attribute($attr_id=false)
    {
        $required_rights = "attributes_delete";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){

            if( ! $attr_id )
            {
                $this->session->set_flashdata('message', "No Attr ID passed");
                redirect("backend/products/attributes", 'refresh');
            }



            if($this->Home_Model->selectWhere('attributes', ['id'=>$attr_id])){
                $this->Home_Model->deleteWhere('attributes', ['id'=>$attr_id]);
                $this->Home_Model->deleteWhere('attributes_values', ['attribute_id'=>$attr_id]);
                //print_r($this->ion_auth->messages());die;
                $message = array('message' => 'Attributes Successfully Deleted', 'class' => 'alert-success');
                $this->session->set_flashdata('message', $message);
                redirect("backend/products/attributes", 'refresh');
            }else{
                $message = array('message' =>'Attributes Not Found!', 'class' => 'alert-danger');
                $this->session->set_flashdata('message', $message);
                redirect("backend/products/attributes", 'refresh');
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function delete_attribute_value($attr_id=false, $value_id=false)
    {
        $required_rights = "attributes_delete";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){

            if( ! $attr_id )
            {
                $this->session->set_flashdata('message', "No Attr ID && Value ID passed");
                redirect("backend/products/attributes", 'refresh');
            }



            if($this->Home_Model->selectWhere('attributes_values', ['id'=>$value_id, 'attribute_id'=>$attr_id])){
                $this->Home_Model->deleteWhere('attributes_values', ['id'=>$value_id, 'attribute_id'=>$attr_id]);
                //print_r($this->ion_auth->messages());die;
                $message = array('message' => 'Attributes Value Successfully Deleted', 'class' => 'alert-success');
                $this->session->set_flashdata('message', $message);
                redirect("backend/products/attributes/".$attr_id, 'refresh');
            }else{
                $message = array('message' =>'Attributes Not Found!', 'class' => 'alert-danger');
                $this->session->set_flashdata('message', $message);
                redirect("backend/products/attributes/".$attr_id, 'refresh');
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function load_attr_values(){
        $id = $this->input->post('id');
        if($id){
            $query = $this->Home_Model->selectWhereResult('attributes_values', ['attribute_id'=>$id]);
            echo json_encode($query);
            /*$this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($query));*/
        }else{

        }
    }

    public function add_attribute($id=false)
    {
        $required_rights = "attributes_add";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
            if(isset($_POST) && !empty($_POST)){
                if($this->input->post('attribute_type') == 'attribute_value'){
                    $config = array(
                        array(
                            'field' => 'attribute_value',
                            'label' => 'Attribute Value',
                            'rules' => 'trim|required',
                        )
                    );
                    $this->form_validation->set_message('valid_email', 'Invalid %s');
                    $this->form_validation->set_message('required', 'Please enter %s');
                    $this->form_validation->set_rules($config);
                    $this->form_validation->set_error_delimiters('', '');
                    if ($this->form_validation->run() == FALSE) {
                        $error_message = '';
                        if (form_error('attribute_value')) {
                            $error_message = form_error('attribute_value');
                        }
                        $attribute = $this->Home_Model->queryDB("select name, id from attributes where id='".$id."'", true);
                        $data['attribute'] = $attribute['name'];
                        $data['attribute_id'] = $attribute['id'];
                        $data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                        $data['home_masterview'] = 'add_attribute';
                        $this->load->view(HOME_GENERALVIEW, $data);
                    }else{
                        $data = array(
                            'name' => $this->input->post('attribute_value'),
                            'attribute_id' => $this->input->post('attribute_id'),
                            'createdate' => CURRENT_DATETIME,
                            'createby' => $this->session->userdata('user_id')
                        );

                        $this->Home_Model->insertDB('attributes_values', $data);
                        $status = array('message' => $this->input->post('attribute') . ' Value successfully added!', 'class' => 'alert-success');
                        $this->session->set_flashdata('message', $status);
                        redirect(base_url().'products/attributes/'.$this->input->post('attribute_id'));
                    }
                }else if($this->input->post('attribute_type') == 'attribute'){
                    $config = array(
                        array(
                            'field' => 'attribute_value',
                            'label' => 'Attribute',
                            'rules' => 'trim|required',
                        )
                    );
                    $this->form_validation->set_message('valid_email', 'Invalid %s');
                    $this->form_validation->set_message('required', 'Please enter %s');
                    $this->form_validation->set_rules($config);
                    $this->form_validation->set_error_delimiters('', '');
                    if ($this->form_validation->run() == FALSE) {
                        $error_message = '';
                        if (form_error('attribute_value')) {
                            $error_message = form_error('attribute_value');
                        }
                        $data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                        $data['home_masterview'] = 'add_attribute';
                        $this->load->view(HOME_GENERALVIEW, $data);
                    }else{
                        $data = array(
                            'name' => $this->input->post('attribute_value'),
                            'createdate' => CURRENT_DATETIME,
                            'createby' => $this->session->userdata('user_id')
                        );

                        $this->Home_Model->insertDB('attributes', $data);
                        $status = array('message' => $this->input->post('attribute_value') . ' successfully added!', 'class' => 'alert-success');
                        $this->session->set_flashdata('message', $status);
                        redirect(base_url().'products/attributes');
                    }
                }else{

                }
            }else{
                $attribute = $this->Home_Model->queryDB("select name, id from attributes where id='".$id."'", true);
                $data['attribute'] = $attribute['name'];
                $data['attribute_id'] = $attribute['id'];
                $data['home_masterview'] = 'add_attribute';
                $this->load->view(HOME_GENERALVIEW, $data);
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }

    public function edit_attribute($id=false,$type=false)
    {
        $required_rights = "attributes_edit";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
            if(isset($_POST) && !empty($_POST) && !empty($id)){
                //echo $this->input->post('attribute_type');die;
                if($this->input->post('attribute_type') == 'attribute_value'){

                    $config = array(
                        array(
                            'field' => 'attribute_value',
                            'label' => 'Attribute Value',
                            'rules' => 'trim|required',
                        )
                    );
                    $this->form_validation->set_message('valid_email', 'Invalid %s');
                    $this->form_validation->set_message('required', 'Please enter %s');
                    $this->form_validation->set_rules($config);
                    $this->form_validation->set_error_delimiters('', '');
                    if ($this->form_validation->run() == FALSE) {
                        $error_message = '';
                        if (form_error('attribute_value')) {
                            $error_message = form_error('attribute_value');
                        }
                        $attribute = $this->Home_Model->queryDB("SELECT a.name, b.id, b.name AS attribute_value FROM attributes_values b JOIN attributes a ON b.`attribute_id`=a.id WHERE b.id='".$id."'", true);
                        $data['attribute'] = $attribute['name'];
                        $data['attribute_id'] = $attribute['id'];
                        $data['attribute_value'] = $attribute['attribute_value'];
                        $data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                        $data['home_masterview'] = 'edit_attribute';
                        $this->load->view(HOME_GENERALVIEW, $data);
                    }else{
                        $data = array(
                            'name' => $this->input->post('attribute_value'),
                            /*'attribute_id' => $this->input->post('attribute_id'),*/
                            'updatedate' => CURRENT_DATETIME,
                            'updateby' => $this->session->userdata('user_id')
                        );

                        $attribute = $this->Home_Model->queryDB("SELECT attribute_id FROM attributes_values WHERE id='".$id."'", true);
                        echo $this->Home_Model->updateDB('attributes_values', ['id'=>$id], $data);
                        $status = array('message' => $this->input->post('attribute') . ' value successfully updated!', 'class' => 'alert-success');
                        $this->session->set_flashdata('message', $status);
                        redirect(base_url().'products/attributes/'.$attribute['attribute_id']);
                    }
                }else if($this->input->post('attribute_type') == 'attribute'){
                    $config = array(
                        array(
                            'field' => 'attribute',
                            'label' => 'Attribute',
                            'rules' => 'trim|required',
                        )
                    );
                    $this->form_validation->set_message('valid_email', 'Invalid %s');
                    $this->form_validation->set_message('required', 'Please enter %s');
                    $this->form_validation->set_rules($config);
                    $this->form_validation->set_error_delimiters('', '');
                    if ($this->form_validation->run() == FALSE) {
                        $error_message = '';
                        if (form_error('attribute_value')) {
                            $error_message = form_error('attribute_value');
                        }
                        $data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                        $data['home_masterview'] = 'edit_attribute';
                        $this->load->view(HOME_GENERALVIEW, $data);
                    }else{
                        $data = array(
                            'name' => $this->input->post('attribute'),
                            'createdate' => CURRENT_DATETIME,
                            'createby' => $this->session->userdata('user_id')
                        );

                        $this->Home_Model->updateDB('attributes', ['id'=>$id], $data);
                        $status = array('message' => $this->input->post('attribute') . ' successfully updated!', 'class' => 'alert-success');
                        $this->session->set_flashdata('message', $status);
                        redirect(base_url().'products/attributes');
                    }
                }else{
                    echo "error";
                }
            }else{
                if($type == 'value'){
                    $attribute = $this->Home_Model->queryDB("SELECT a.name, b.id, b.name AS attribute_value FROM attributes_values b JOIN attributes a ON b.`attribute_id`=a.id WHERE b.id='".$id."'", true);
                    $data['attribute'] = $attribute['name'];
                    $data['attribute_id'] = $attribute['id'];
                    $data['attribute_value'] = $attribute['attribute_value'];
                    $data['type'] = 'values';
                }else{
                    $attribute = $this->Home_Model->queryDB("select name, id from attributes where id='".$id."'", true);
                    $data['attribute'] = $attribute['name'];
                    $data['attribute_id'] = $attribute['id'];
                    $data['type'] = 'attribute';
                }
                $data['home_masterview'] = 'edit_attribute';
                $this->load->view(HOME_GENERALVIEW, $data);
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }

}
