<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Dashboard.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Orders extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');
    }

    public function index()
    {
        $cuser = $this->ion_auth->user()->row();
        if($this->ion_auth->is_admin()  || $cuser->user_id == 1){
            $data['orders'] = $this->Home_Model->getOrders();
        }else{
            $data['orders'] = $this->Home_Model->getOrders(false, $cuser->user_id);
        }


        // print_r($data['orders']);exit;
        $data['home_masterview'] = 'orders';
        $this->load->view(HOME_GENERALVIEW, $data);
    }

    public function add_order(){

        $cuser = $this->ion_auth->user()->row();
        if($this->ion_auth->is_admin()  || $cuser->user_id == 1){
            $data['products'] = $this->Home_Model->simpleSelect('products');
        }else{
            $data['products'] = $this->Home_Model->selectWhereResult('products',['vendor_id'=> $cuser->user_id ]);
        }

        $data['users'] = $this->ion_auth->users([2])->result();
        foreach ( $data['users'] as $k => $user)
        {
            $cuser = $this->ion_auth->user()->row();

            if($this->ion_auth->is_admin()  || $cuser->user_id != 1){
                if($user->vendor_id != $cuser->user_id ){
                    unset($data['users'][$k]);
                }
            }
            if($user->id == $cuser->user_id || $user->id == "25"){
                unset( $data['users'][$k]);
            }
        }



        $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
        $data['message'] = $messge;

        $data['home_masterview'] = 'add_order';
        $this->load->view(HOME_GENERALVIEW, $data);
    }

    public function getTotalAmount($total){
        $total = $total;
        $tax=0;
        $cartS = $this->Home_Model->simpleSelect('cart_settings');
        foreach ($cartS as $key => $cs){
           $tax += $total*$cs['rate']/100;
        }

        $total = $total+$tax;
        if($this->session->userdata('promocode')){
          $disc =  $total*$this->session->userdata('promocode')['disc']/100;
          $total = $total - $disc;
        }
        return $total;
    }

    public function req_add_order(){

        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else {
            $required_rights = "order_add";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
                $this->form_validation->set_rules('users', 'Users Required', 'required|trim');
                $this->form_validation->set_rules('ids[]', 'Products Required', 'required|trim');
                $this->form_validation->set_rules('sum', 'Products Required', 'required|trim');
                $this->form_validation->set_rules('billing_address_1', 'Billing Address Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('billing_address_2', 'Billing Address Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('billing_city', 'Billing City Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('billing_state', 'Billing State Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('billing_country', 'Billing Country Required', 'required|trim|max_length[20]');

                $this->form_validation->set_rules('shipping_address_1', 'Shipping Address Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('shipping_address_2', 'Shipping Address Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('shipping_city', 'Shipping City Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('shipping_state', 'Shipping State Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('shipping_country', 'Shipping Country Required', 'required|trim|max_length[20]');

                $this->form_validation->set_rules('firstname', 'Card Holder First Name Required', 'required|trim|max_length[10]');
                $this->form_validation->set_rules('lastname', 'Card Holder Last Name Required', 'required|trim|max_length[10]');
                $this->form_validation->set_rules('cc_number', 'Card Number Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('cc_cvv', 'Card CVV Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('cc_month', 'Card Expiry Month Required', 'required|trim|max_length[20]');
                $this->form_validation->set_rules('cc_year', 'Card Expiry Year Required', 'required|trim|max_length[20]');


                $this->form_validation->set_message('required', '%s ');

                $cuser = $this->ion_auth->user()->row();

                if( $this->form_validation->run() === FALSE )
                {

                    if($this->ion_auth->is_admin()  || $cuser->user_id == 1){
                        $data['products'] = $this->Home_Model->simpleSelect('products');
                    }else{
                        $data['products'] = $this->Home_Model->selectWhereResult('products',['vendor_id'=> $cuser->user_id ]);
                    }



                    $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                    $data['message'] = $messge;

                    $data['home_masterview'] = 'add_order';
                    $this->load->view(HOME_GENERALVIEW, $data);
                }
                else
                {
                    $order = array();
                    $order['billing_address_1'] = $this->input->post('billing_address_1');
                    $order['billing_address_2'] = $this->input->post('billing_address_2');
                    $order['billing_city'] = $this->input->post('billing_city');
                    $order['billing_state'] = $this->input->post('billing_state');
                    $order['billing_country'] = $this->input->post('billing_country');
                    $order['billing_postcode'] = $this->input->post('billing_postcode');
                    $order['shipping_address_1'] = $this->input->post('shipping_address_1');
                    $order['shipping_address_2'] = $this->input->post('shipping_address_2');
                    $order['shipping_city'] = $this->input->post('shipping_city');
                    $order['shipping_state'] = $this->input->post('shipping_state');
                    $order['shipping_country'] = $this->input->post('shipping_country');
                    $order['shipping_postcode'] = $this->input->post('shipping_postcode');
                    $order['OrderAmount'] = $this->input->post('sum');
                    $order['OrderTotal'] = $this->getTotalAmount($this->input->post('sum'));
                    $order['VendorID'] = $cuser->user_id;
                    $order['CustomerID'] = $this->input->post('users');
                    $order['cart_settings'] = serialize($this->Home_Model->simpleSelect('cart_settings'));
                    $order['coupon'] = ($this->session->userdata('promocode')) ? serialize($this->session->userdata('promocode')) : "";

                    $OrderId = $this->Home_Model->insertDB('orders',$order);


                    $ids = $this->input->post('ids');
                    $names = $this->input->post('names');
                    $qtys = $this->input->post('qtys');
                    $prices = $this->input->post('prices');
                    $totals = $this->input->post('totals');

                    foreach ($ids as $index => $id){
                        $orderDetails = array();
                        $orderDetails['ProductID'] = $id;
                        $orderDetails['ProductName'] = $names[$index];
                        $orderDetails['ProductPrice'] = $prices[$index];
                        $orderDetails['ProductQuantity'] = $qtys[$index];
                        $orderDetails['ProductTotal'] = $totals[$index];
                        $orderDetails['OrderID'] = $OrderId;

                        $this->Home_Model->insertDB('orderdetails',$orderDetails);
                    }
//                    die();

                    $firstname = $this->input->post('firstname');
                    $lastname = $this->input->post('lastname');
                    $cc_number = $this->input->post('cc_number');
                    $cc_cvv = $this->input->post('cc_cvv');
                    $cc_month = $this->input->post('cc_month');
                    $cc_year = $this->input->post('cc_year');


                    $paypalParams = array(
                        'paymentAction' => 'Sale',
                        'amount' => $order['OrderTotal'],
                        'currencyCode' => 'USD',
                        'creditCardType' => '',
                        'creditCardNumber' => trim(str_replace(" ","",$cc_number)),
                        'expMonth' => $cc_month,
                        'expYear' => $cc_year,
                        'cvv' => $cc_cvv,
                        'firstName' => $firstname,
                        'lastName' => $lastname,
                        'city' =>  $order['billing_city'],
                        'zip'  => $order['billing_postcode'],
                        'countryCode' => $order['billing_country'],
                    );

                    $this->load->model('paypal_model');
                    $response = $this->paypal_model->paypalCall($paypalParams);
//                     print_r($response);
                    $paymentStatus = strtoupper($response["ACK"]);
//                    $paymentStatus = "SUCCESS";
                    if ($paymentStatus == "SUCCESS"){
                        $transactionID = $response['TRANSACTIONID'];
                        $timestamp = $response['TIMESTAMP'];
                        $correlation_id = $response['CORRELATIONID'];
                        $ack = $response['ACK'];
                        $version = $response['VERSION'];
                        $build = $response['BUILD'];
                        $amount = $response['AMT'];
                        $currency_code = $response['CURRENCYCODE'];
                        $avs_code = $response['AVSCODE'];
                        $cvv2 = $response['CVV2MATCH'];
                        $this->Home_Model->insertDB('order_payments', array(
                                'fkUserId'=>$cuser->user_id,
                                'OrderId' => $OrderId,
                                'paypal_timestamp'=>$timestamp,
                                'correlation_id'=>$correlation_id,
                                'ack'=>$ack,
                                'version'=>$version,
                                'build'=>$build,
                                'amt'=>$amount,
                                'currencycode'=>$currency_code,
                                'avscode'=>$avs_code,
                                'cvv2match'=>$cvv2,
                                'transactionid'=>$transactionID,
                                'payment_type'=>'Paypal Pro',
                                'date_added'=>date('Y-m-d H:i:s')

                            )
                        );
                        $this->Home_Model->updateDB('orders',['OrderID'=>$OrderId],['TransactionStatus'=>"1"]);
                        $u =  $this->Home_Model->selectWhere('users',['id' =>$this->input->post('users')]);
                        $data = array(
                            'invoice_no' => 'INV-'.$OrderId,
                            'name' =>  $u['first_name'].' '.$u['last_name'],
                            'phone' => $u['phone'],
                            'email' => $u['email'],
                            'address' => $this->input->post('full_billing_address'),
                            'city' => $this->session->userdata('billing_city'),
                            'zip' => $this->session->userdata('billing_postcode'),
                            'order_no' => $OrderId,
                            'order_date' => date('Y-m-d'),
                            'transaction_id' => $transactionID,
                            'transaction_time' => date('Y-m-d H:i:s'),
                            'invoice_total' => $amount,
                        );
                        $this->Home_Model->sendMail('do-not-reply@gascontrolparts.com',
                                                  $u['email'],
                                                  'Gas Control Parts',
                                                  'Order Reciept Email',
                                                  'emails/order_recipt',
                                                  $data);
                        $us = $this->Home_Model->simpleSelect('users');
                        foreach ($us as $key => $u) {
                            if($this->ion_auth->is_admin($u['id'])){
                              $this->Home_Model->sendMail('do-not-reply@gascontrolparts.com',
                                                          $u['email'],
                                                          'Gas Control Parts',
                                                          'Order Reciept Email',
                                                          'emails/order_recipt',
                                                          $data);
                            }
                        }


                        redirect("backend/orders",'refresh');
                    }else {
                        $messge = array('message' => $response['L_LONGMESSAGE0'] ,'class' => 'alert-danger');
                        $this->session->set_flashdata('message', $messge);
                        redirect("backend/orders/view_order/".$OrderId,'refresh');
                    }


                }
            }else{
                return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
            }
        }
    }


    public function view_order($id=false){
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else {
            $required_rights = "order_edit";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
                $messge = array('message' => validation_errors(), 'class' => 'alert-danger');
                $data['message'] = $messge;
                if (!$id) {
                    show_404();
                }
                // echo $id;
                $data['id'] = $id;

                $data['users'] = $this->ion_auth->users([2])->result();

                foreach ($data['users'] as $k => $user) {
                    $cuser = $this->ion_auth->user()->row();

                    // if ($this->ion_auth->is_admin() || $cuser->user_id != 1) {
                    //     if ($user->vendor_id != $cuser->user_id) {
                    //         unset($data['users'][$k]);
                    //     }
                    // }
                    if ($user->id == $cuser->user_id || $user->id == "25") {
                        unset($data['users'][$k]);
                    }
                }

                $data['orders'] = $this->Home_Model->getOrders($id);
                if (empty($data['orders'])) {
                    show_404();
                }
                $data['ordersDetails'] = $this->Home_Model->getOrderDetails($id);
                // print_r($data);exit;
                $data['home_masterview'] = 'view_order';
                $this->load->view(HOME_GENERALVIEW, $data);
            }else{
                return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
            }
        }
    }

    public function payNow(){
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else {
            $required_rights = "order_edit";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
                if ($this->input->post()) {
                    $firstname = $this->input->post('firstname');
                    $lastname = $this->input->post('lastname');
                    $cc_number = $this->input->post('cc_number');
                    $cc_cvv = $this->input->post('cc_cvv');
                    $cc_month = $this->input->post('cc_month');
                    $cc_year = $this->input->post('cc_year');

                    $billing_city = $this->input->post('billing_city');
                    $billing_postcode = $this->input->post('billing_postcode');
                    $billing_country = $this->input->post('billing_country');
                    $amount = $this->input->post('amount');

                    $OrderId = $this->input->post('OrderId');


                    //            print_r($this->input->post());

                    $paypalParams = array(
                        'paymentAction' => 'Sale',
                        'amount' => $amount,
                        'currencyCode' => 'USD',
                        'creditCardType' => '',
                        'creditCardNumber' => trim(str_replace(" ", "", $cc_number)),
                        'expMonth' => $cc_month,
                        'expYear' => $cc_year,
                        'cvv' => $cc_cvv,
                        'firstName' => $firstname,
                        'lastName' => $lastname,
                        'city' => $billing_city,
                        'zip' => $billing_postcode,
                        'countryCode' => $billing_country,
                    );

                    $this->load->model('paypal_model');
                    $response = $this->paypal_model->paypalCall($paypalParams);
                    //            print_r($response);
                    $paymentStatus = strtoupper($response["ACK"]);
                    //                    $paymentStatus = "SUCCESS";
                    if ($paymentStatus == "SUCCESS") {
                        $transactionID = $response['TRANSACTIONID'];
                        $timestamp = $response['TIMESTAMP'];
                        $correlation_id = $response['CORRELATIONID'];
                        $ack = $response['ACK'];
                        $version = $response['VERSION'];
                        $build = $response['BUILD'];
                        $amount = $response['AMT'];
                        $currency_code = $response['CURRENCYCODE'];
                        $avs_code = $response['AVSCODE'];
                        $cvv2 = $response['CVV2MATCH'];
                        $cuser = $this->ion_auth->user()->row();
                        $this->Home_Model->insertDB('order_payments', array(
                                'fkUserId' => $cuser->user_id,
                                'OrderId' => $OrderId,
                                'paypal_timestamp' => $timestamp,
                                'correlation_id' => $correlation_id,
                                'ack' => $ack,
                                'version' => $version,
                                'build' => $build,
                                'amt' => $amount,
                                'currencycode' => $currency_code,
                                'avscode' => $avs_code,
                                'cvv2match' => $cvv2,
                                'transactionid' => $transactionID,
                                'payment_type' => 'Paypal Pro',
                                'date_added' => date('Y-m-d H:i:s')

                            )
                        );
                        $this->Home_Model->updateDB('orders', ['OrderID' => $OrderId], ['TransactionStatus' => "1"]);

                        redirect("backend/orders", 'refresh');
                    } else {
                        $messge = array('message' => $response['L_LONGMESSAGE0'], 'class' => 'alert-danger');
                        $data['message'] = $messge;
                        if (!$OrderId) {
                            show_404();
                        }
                        // echo $id;
                        $data['id'] = $OrderId;
                        $data['orders'] = $this->Home_Model->getOrders($OrderId);
                        if (empty($data['orders'])) {
                            show_404();
                        }
                        $data['ordersDetails'] = $this->Home_Model->getOrderDetails($OrderId);
                        // print_r($data);exit;
                        $data['home_masterview'] = 'edit_order';
                        $this->load->view(HOME_GENERALVIEW, $data);
                    }
                } else {

                    redirect("backend/orders", 'refresh');
                }
            }else{
                return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
            }
        }
    }

    public function edit_order($id=false){
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else {
            $required_rights = "order_edit";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
                $messge = array('message' => validation_errors(), 'class' => 'alert-danger');
                $data['message'] = $messge;
                if (!$id) {
                    show_404();
                }
                // echo $id;
                $data['id'] = $id;

                $data['users'] = $this->ion_auth->users([2])->result();
                foreach ($data['users'] as $k => $user) {
                    $cuser = $this->ion_auth->user()->row();

                    // if ($this->ion_auth->is_admin() || $cuser->user_id != 1) {
                    //     if ($user->vendor_id != $cuser->user_id) {
                    //         unset($data['users'][$k]);
                    //     }
                    // }
                    if ($user->id == $cuser->user_id || $user->id == "25") {
                        unset($data['users'][$k]);
                    }
                }

                $data['orders'] = $this->Home_Model->getOrders($id);
                if (empty($data['orders'])) {
                    show_404();
                }
                $data['ordersDetails'] = $this->Home_Model->getOrderDetails($id);
                $data['products'] = $this->Home_Model->simpleSelect('products');
                // print_r($data);exit;
                $data['home_masterview'] = 'edit_order';
                $this->load->view(HOME_GENERALVIEW, $data);
            }else{
                return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
            }
        }
    }

    public function req_edit_order(){
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else {
            $required_rights = "order_edit";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {

                $OrderId = $this->input->post('OrderId');
                if($this->input->post('OrderTrackingNo')){
                  $OrderTrackingNo = $this->input->post('OrderTrackingNo');
                  $this->Home_Model->updateDB('orders', ['OrderID' => $OrderId], ['OrderTrackingNo' =>$OrderTrackingNo,'Status'=>'1']);                  
                }
                $o = $this->Home_Model->selectWhere('orders',['OrderID' => $OrderId]);
                $this->Home_Model->updateDB('orders', ['OrderID' => $OrderId], [
                  'Status'=> $this->input->post('order_status'),
                  'billing_address_1' => $this->input->post('billing_address_1'),
                  'billing_address_2' => $this->input->post('billing_address_2'),
                  'billing_city' => $this->input->post('billing_city'),
                  'billing_state' => $this->input->post('billing_state'),
                  'billing_country' => $this->input->post('billing_country'),
                  'shipping_address_1' => $this->input->post('shipping_address_1'),
                  'shipping_address_2' => $this->input->post('shipping_address_2'),
                  'shipping_city' => $this->input->post('shipping_city'),
                  'shipping_state' => $this->input->post('shipping_state'),
                  'shipping_country' => $this->input->post('shipping_country'),
                  'billing_postcode' => $this->input->post('billing_postcode'),
                ]);
                $statusarr = array(
                   '0' => 'Pending',
                   '1' => 'Processing',
                   '2' => 'On hold',
                   '3' => 'Completed',
                   '4' => 'Cancelled',
                   '6' => 'Refunded',
                   '7' => 'Failed',
                );
                $o =  $this->Home_Model->selectWhere('orders', ['OrderID' => $OrderId]);
                if($o['CustomerID']){
                    $u =  $this->Home_Model->selectWhere('users',['id' =>$o['CustomerID']]);
                    $em = $u['email'];
                    $name = $u['first_name'].' '.$u['last_name'];
                }
                else{
                    $em = $o['billing_email'];
                    $name = $o['billing_first_name'].' '.$o['billing_last_name'];
                }
                $this->Home_Model->sendMail('do-not-reply@gascontrolparts.com',
                                             $em,
                                            'Gas Control Parts',
                                            'Order # '.$OrderId.' Status',
                                            'emails/order_status',
                                             array('name' => $name,
                                                  'orderid' => 'ORD-'.$OrderId,
                                                  'date' => date('Y-m-d'),
                                                  'status' => $statusarr[$this->input->post('order_status')],
                                            ));




                redirect("backend/orders", 'refresh');
            }
        }
    }



    public function delete_order($id=false){
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else {
            $required_rights = "order_delete";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
                if (!$id) {
                    show_404();
                }

                $this->Home_Model->deleteWhere('orders', ["OrderID" => $id]);
                $this->Home_Model->deleteWhere('orderdetails', ["OrderID" => $id]);
                redirect('/backend/orders');
            }else{
                return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
            }
        }
    }

    public function changestatus($id){
        $status = $this->input->post("Status");
        $this->Home_Model->updateDB("orders",["OrderID"=>$id],["Status"=>$status]);
        $this->session->set_flashdata('message','Status Changed Successfully');
        redirect('backend/orders/view_order/'.$id);
    }

}
