<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Users.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Settings extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        if( ! $this->ion_auth->logged_in() )
            redirect('/login');
    }

    public function testimonials(){
      $required_rights = "testimonials_view";
      if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
          $this->data['testimonials'] = $this->Home_Model->simpleSelect('testimonials');
          $this->data['home_masterview'] = 'testimonials';
          $this->load->view(HOME_GENERALVIEW, $this->data);
      }else{
          return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
      }
    }

    public function brandings(){
      $required_rights = "branding_view";
      if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
          $this->data['brandings'] = $this->Home_Model->simpleSelect('brandings');
          $this->data['home_masterview'] = 'brandings';
          $this->load->view(HOME_GENERALVIEW, $this->data);
      }else{
          return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
      }
    }

    public function updateBranding(){
        $required_rights = "branding_update";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
            if (!$this->ion_auth->logged_in()) {
                redirect('auth/login', 'refresh');
            } elseif (isset($_POST) && !empty($_POST)) {
                $config = array(
                    array(
                        'field' => 'footer_col_title',
                        'label' => 'Footer Column Title',
                        'rules' => 'trim|required',
                    ),
                    // array(
                    //     'field' => 'footer_col_body',
                    //     'label' => 'Footer Column Body',
                    //     'rules' => 'trim|required',
                    // ),
                    array(
                        'field' => 'fp_title',
                        'label' => 'Featured Product Heading',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'fp_subtitle',
                        'label' => 'Featured Product Subheading',
                        'rules' => 'trim|required',
                    ),
                    // array(
                    //     'field' => 'fc_title',
                    //     'label' => 'Featured Category Heading',
                    //     'rules' => 'trim|required',
                    // ),
                    // array(
                    //     'field' => 'fc_subtitle',
                    //     'label' => 'Featured Category Subheading',
                    //     'rules' => 'trim|required',
                    // ),
                    // array(
                    //     'field' => 'bl_title',
                    //     'label' => 'Blog Heading',
                    //     'rules' => 'trim|required',
                    // ),
                    // array(
                    //     'field' => 'bl_subtitle',
                    //     'label' => 'Blog Subheading',
                    //     'rules' => 'trim|required',
                    // ),
                    array(
                        'field' => 'fb_link',
                        'label' => 'Facebook Link',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'tw_link',
                        'label' => 'Twitter Link',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'lk_link',
                        'label' => 'Youtube Link',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'gp_link',
                        'label' => 'Google+ Link',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'inst_link',
                        'label' => 'Instagram Link',
                        'rules' => 'trim|required',
                    )
                );


                $this->form_validation->set_message('valid_email', 'Invalid %s');
                $this->form_validation->set_message('required', 'Please enter %s');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('', '');

                if ($this->form_validation->run() == FALSE) {
                    $error_message = '';
                    if (form_error('author')) {
                        $error_message = form_error('author');
                    } elseif (form_error('body')) {
                        $error_message = form_error('body');
                    }

                    $this->data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                    $this->data['home_masterview'] = 'brandings';
                    $this->load->view(HOME_GENERALVIEW, $this->data);

                } else {
                    $data = array(
                    	'footer_col_title'=> $this->input->post('footer_col_title'),
//                    	'footer_col_body'=> $this->input->post('footer_col_body'),
                    	'fp_title'=> $this->input->post('fp_title'),
                    	'fp_subtitle'=> $this->input->post('fp_subtitle'),
                    	// 'fc_title'=> $this->input->post('fc_title'),
                    	// 'fc_subtitle'=> $this->input->post('fc_subtitle'),
                    	// 'bl_title'=> $this->input->post('bl_title'),
                    	// 'bl_subtitle'=> $this->input->post('bl_subtitle'),
                    	'fb_link'=> $this->input->post('fb_link'),
                    	'tw_link'=> $this->input->post('tw_link'),
                    	'lk_link'=> $this->input->post('lk_link'),
                    	'gp_link'=> $this->input->post('gp_link'),
                    	'inst_link'=> $this->input->post('inst_link'),
                    );
                    $this->db->truncate('brandings');
                    $this->db->replace('brandings', $data);
                    $message = array('message' => 'brandings Successfully updated!', 'class' => 'alert-success');
                    $this->session->set_flashdata('message', $message);
                    redirect("backend/settings/brandings", 'refresh');
              }
            } else {
                $this->data['home_masterview'] = 'brandings';
                $this->load->view(HOME_GENERALVIEW, $this->data);
            }
        }
        else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }

    public function add_testimonials(){
        $required_rights = "testimonials_add";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
            if (!$this->ion_auth->logged_in()) {
                redirect('auth/login', 'refresh');
            } elseif (isset($_POST) && !empty($_POST)) {
                $config = array(
                    array(
                        'field' => 'author',
                        'label' => 'Testimonial Author',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'body',
                        'label' => 'Testimonial Body',
                        'rules' => 'trim|required',
                    ),
                );


                $this->form_validation->set_message('valid_email', 'Invalid %s');
                $this->form_validation->set_message('required', 'Please enter %s');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('', '');

                if ($this->form_validation->run() == FALSE) {
                    $error_message = '';
                    if (form_error('author')) {
                        $error_message = form_error('author');
                    } elseif (form_error('body')) {
                        $error_message = form_error('body');
                    }

                    $this->data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                    $this->data['home_masterview'] = 'add_testimonials';
                    $this->load->view(HOME_GENERALVIEW, $this->data);

                } else {
                    $data = array(
                        'author' => $this->input->post('author'),
                        'body' => $this->input->post('body'),
                    );

                    $this->Home_Model->insertDB('testimonials', $data);
                    $message = array('message' => 'Testimonial Successfully added!', 'class' => 'alert-success');
                    $this->session->set_flashdata('message', $message);
                    redirect("backend/settings/testimonials", 'refresh');
              }
            } else {
                $this->data['home_masterview'] = 'add_testimonials';
                $this->load->view(HOME_GENERALVIEW, $this->data);
            }
        }
        else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }

    public function edit_testimonials($id){
          $required_rights = "testimonial_edit";
          if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {


              if(isset($_POST) && !empty($_POST) && !empty($id)){
                  $config = array(
                    array(
                        'field' => 'author',
                        'label' => 'Testimonial Author',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'body',
                        'label' => 'Testimonial Body',
                        'rules' => 'required',
                    ),
                  );
                  $this->form_validation->set_message('valid_email', 'Invalid %s');
                  $this->form_validation->set_message('required', 'Please enter %s');
                  $this->form_validation->set_rules($config);
                  $this->form_validation->set_error_delimiters('', '');

                  if ($this->form_validation->run() == FALSE) {
                      $error_message='';
                      if (form_error('author')) {
                          $error_message = form_error('author');
                      }elseif (form_error('body')) {
                          $error_message = form_error('body');
                      }

                      $this->data['testimonials'] = $this->Home_Model->selectWhere('testimonials', ['id'=>$id]);
                      $this->data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                      $this->data['home_masterview'] = 'edit_testimonials';
                      $this->load->view(HOME_GENERALVIEW, $this->data);

                  }else{

                    $id = $this->input->post('test_id');
                    $data = array(
                        'author' => $this->input->post('author'),
                        'body' => $this->input->post('body'),
                   );
                   $this->Home_Model->updateDB('testimonials',['id'=>$id],$data);

                    $message = array('message' => 'Testimonial updated !', 'class' => 'alert-success');
                    $this->session->set_flashdata('message', $message);
                    redirect("backend/settings/testimonials", 'refresh');
                  }
              }else{

                  $this->data['testimonials'] = $this->Home_Model->selectWhere('testimonials', ['id'=>$id]);
                  $this->data['home_masterview'] = 'edit_testimonials';
                  $this->load->view(HOME_GENERALVIEW, $this->data);
              }
          }else{
              return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
          }
      }

      public function deleteTestimonials($id){
        $required_rights = "testimonial_delete";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $this->db->delete('testimonials', array('id' => $id));
            $message = array('message' => 'Testimonial deleted !', 'class' => 'alert-success');
            $this->session->set_flashdata('message', $message);
            redirect("/backend/settings/testimonials", 'refresh');
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }


    public function homePageSetting($id=null){
      if($id){
        $this->data['elements'] = $this->db->query('select * from homepage_elements WHERE type = '.$id)->result();
      }
      else{
        redirect('backend/dashboard');
      }
      $this->data['id'] = $id;
      $this->data['home_masterview'] = 'homepage_elements';
      $this->load->view(HOME_GENERALVIEW, $this->data);
    }

    public function hompage_element_edit($id){
        $data['element'] = $this->Home_Model->selectWhere('homepage_elements',['id'=>$id]);
        $this->data['id'] = $id;
        $data['home_masterview'] = 'edit_homepage_elements';
        $this->load->view(HOME_GENERALVIEW, $data);
    }

    public function update_hompage_element(){
      $required_rights = "setting_edit";
      if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
          if(isset($_POST) && !empty($_POST)){
            $id = $this->input->post('hp_e_id');
            if(!$id){
              $message = array(
                'class'=>'alert-danger',
                'message'=>'can not update settings',
              );
              $this->session->set_flashdata('message', $message);
              redirect("/backend/settings/hompage_element_edit/$id", 'refresh');
            }
            $data = array(
              'title' => $this->input->post('title'),
              'subtitle' => $this->input->post('subtitle'),
              'on_hover_text' => $this->input->post('description'),
              'btn_text' => $this->input->post('btn_text'),
              'btn_link' => $this->input->post('btn_link'),
              'percentoff' => $this->input->post('percentoff'),
            );

            $rec = $this->Home_Model->selectWhere('homepage_elements',['id'=>$id]);
            if($rec['type'] == 0){
                $path = './uploads/hp_elements/below_banner';
            }
            else if($rec['type'] == 1){
              $path = './uploads/hp_elements/above_featured';
            }
            else{
              $path = './uploads/hp_elements/below_featured';
            }

            if ($_FILES['image']['size'] > 0) {
                $config['upload_path'] = $path;
                $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
                $config['encrypt_name'] = true;
                $config['remove_spaces'] = true;
                $config['file_ext_tolower'] = true;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('product_image')) {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();
                    $data['image'] = $data['file_name'];
                }
            }

            $this->Home_Model->updateDB('homepage_elements',['id'=>$id],$data);
            $message = array(
              'class'=>'alert-success',
              'message'=>'Settings updated',
            );
            $this->session->set_flashdata('message', $message);
            redirect("/backend/settings/hompage_element_edit/$id", 'refresh');

          }
      }else{
          return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
      }

    }

    public function homepageBanner(){
      $this->data['banners'] = $this->db->query('select * from homepagebanner order by id desc')->result();
      $this->data['home_masterview'] = 'homepage_banner';
      $this->load->view(HOME_GENERALVIEW, $this->data);
    }

    public function cartSettings(){
      $this->data['cartSettings'] = $this->Home_Model->simpleSelect('cart_settings');
      $this->data['home_masterview'] = 'cart_settings';
      $this->load->view(HOME_GENERALVIEW, $this->data);
    }

    public function setBlogFeatured($id){
        $chk = $this->Home_Model->selectWhere('blogs', ['id'=>$id]);
        if($chk){
          //if blos is going to remove from featured do it
          if($chk['isfeatured']){
            $this->Home_Model->updateDB('blogs', ['id'=>$id], ['isfeatured'=>!$chk['isfeatured']]);
            return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status' => 200, 'msg'=>'Blog is removed from featured']));
          }
          else{
            $count = $this->Home_Model->countResult('blogs',['isfeatured'=>1]);
            if($count >= 4){
              return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode(['status' => 500, 'msg'=>'You can only set 4 blogs to be featured']));
            }
            else{
              $this->Home_Model->updateDB('blogs', ['id'=>$id], ['isfeatured'=>!$chk['isfeatured']]);
              return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status' => 200, 'msg'=>'Blog is set to featured']));
            }
          }
        }
        return $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode(['status' => 500, 'msg' => 'Could not perform operation']));
    }

    public function updateCartSettings(){
      $required_rights = "products_edit";
      if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
          if(isset($_POST) && !empty($_POST)){

              $config = array();
              $cartS = $this->Home_Model->simpleSelect('cart_settings');
              foreach ($cartS as $key => $value) {
                array_push($config,
                  array(
                      'field' => 'attr_rate'.$value['id'],
                      'label' => 'Rate',
                      'rules' => 'trim|required|greater_than[0]',
                    ));
                }

              $this->form_validation->set_message('valid_email', 'Invalid %s');
              $this->form_validation->set_message('required', 'Please enter %s');
              $this->form_validation->set_rules($config);
              $this->form_validation->set_error_delimiters('', '');
              if ($this->form_validation->run() == FALSE) {
                  $error_message='Please fill all the fields and rate can not be zero ';
                  $this->data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                  $this->data['cartSettings'] = $this->Home_Model->simpleSelect('cart_settings');
                  $this->data['home_masterview'] = 'cart_settings';
                  $this->load->view(HOME_GENERALVIEW, $this->data);

              }else{
                foreach ($cartS as $key => $v) {
                  $data = array(
                    'rate' => $this->input->post('attr_rate'.$v['id']),
                  );
                  $this->Home_Model->updateDB('cart_settings', ['id' => $v['id']], $data);
                }
                $message = array('message' => 'Settings Successfully updated!', 'class' => 'alert-success');
                $this->session->set_flashdata('message', $message);
                redirect("backend/settings/cartSettings", 'refresh');

              }
          }
        }
    }

    public function getHomePageBannerDetails(){
      $data['banners'] = $this->Home_Model->simpleSelect('homepagebanner');
      return $this->load->view('admin_area/getHomePageBannerDetails', $data);
    }

    public function getHpBannerInfo($id){
      $banners = $this->Home_Model->selectWhere('homepagebanner',['id' => $id]);
      return $this->output
              ->set_content_type('application/json')
              ->set_output(json_encode([$banners]));
    }

    public function deleteHpBanner($id){
      $this->db->delete('homepagebanner', array('id' => $id));
    }

    public function add_homepage_banner(){
      if (empty($_POST['hp_banner_text']))
      {
        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status'=>500,'msg'=>'Banner text is required'  ]));
      }
      $filename = $id = '';
       if($this->input->post('hp_banner_id') != 0){
         $id = $this->input->post('hp_banner_id');
       }
       else{
         if ($_FILES['hp_image']['size'] > 0) {
            //do nothing
         }
         else{
           return $this->output
                   ->set_content_type('application/json')
                   ->set_output(json_encode(['status'=>500,'msg'=>'Image is required']));

         }
       }


        if ($_FILES['hp_image']['size'] > 0) {
            // $config['upload_path'] = './uploads/hp_banners/';
            // $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config['upload_path'] = './uploads/hpbanners/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            // $config['max_width'] = '1440';
            // $config['max_height'] = '560';
            $config['encrypt_name'] = true;
            $config['remove_spaces'] = true;
            $config['file_ext_tolower'] = true;
            $this->upload->initialize($config);

            if (!empty($_FILES['hp_image'])) {
              if (!$this->upload->do_upload('hp_image')) {

                return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(['status'=>500,'msg'=>$this->upload->display_errors()]));
              } else {
                  $data = $this->upload->data();
                  $filename = $data['file_name'];
              }
            }
        }


        if($filename != ""){
          $data = array(
              'image'      => $filename,
              'text'       => $this->input->post('hp_banner_text')
          );
        }
        else{
          $data = array(
              'text'       => $this->input->post('hp_banner_text')
          );
        }
        if($id)
          $this->Home_Model->updateDB('homepagebanner', ['id' => $id], $data);
        else
          $this->db->insert('homepagebanner', $data);

        return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['status'=>200,'msg'=>'Banner is added successfully']));


    }

    public function blogs(){
      $required_rights = "products_view";
      if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
          $this->data['blogs'] = $this->db->query('select * from blogs order by id desc')->result();
          $this->data['home_masterview'] = 'blogs';
          $this->load->view(HOME_GENERALVIEW, $this->data);
      }else{
          return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
      }
    }

    public function edit_blogs($id){
          $required_rights = "products_edit";
          if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {


              if(isset($_POST) && !empty($_POST) && !empty($id)){
                  $config = array(
                    array(
                        'field' => 'blog_title',
                        'label' => 'Blog Title',
                        'rules' => 'trim|required',
                    ),
                    // array(
                    //     'field' => 'blog_slug',
                    //     'label' => 'Blog Slug',
                    //     'rules' => 'trim|required|alpha_dash',
                    // ),
                    array(
                        'field' => 'description',
                        'label' => 'Blog body',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'author',
                        'label' => 'Blog Author',
                        'rules' => 'trim|required',
                    ),
                  );
                  $this->form_validation->set_message('valid_email', 'Invalid %s');
                  $this->form_validation->set_message('required', 'Please enter %s');
                  $this->form_validation->set_rules($config);
                  $this->form_validation->set_error_delimiters('', '');

                  if ($this->form_validation->run() == FALSE) {
                      $error_message='';
                      if (form_error('blog_title')) {
                          $error_message = form_error('blog_title');
                      }elseif (form_error('blog_slug')) {
                          $error_message = form_error('blog_slug');
                      }elseif (form_error('description')) {
                          $error_message = form_error('description');
                      }elseif (form_error('author')) {
                          $error_message = form_error('author');
                      }

                      $this->data['blog'] = $this->Home_Model->selectWhere('blogs', ['id'=>$id]);
                      $this->data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                      $this->data['home_masterview'] = 'edit_blog';
                      $this->load->view(HOME_GENERALVIEW, $this->data);

                  }else{

                    $id = $this->input->post('blog_id');
                    $data = array(
                        'blog_title' => $this->input->post('blog_title'),
                        'body' => $this->input->post('description'),
                        'author' => $this->input->post('author'),
                        'keywords' => @$this->input->post('keywords'),
                   );
                   $this->Home_Model->updateDB('blogs',['id'=>$id],$data);

                    if($this->input->post('pi_b64')){
                        $image64 = $this->input->post('pi_b64');
                        if(!empty($image64) || $image64 != ''){
                          list($type, $image64) = explode(';', $image64);
                          list(,$extension) = explode('/',$type);
                          list(, $image64)      = explode(',', $image64);
                        }
                        $image64 = base64_decode($image64);
                        $time = time();
                        $path = './uploads/blogs/';
                        $filename = md5(rand(11111111,99999999)).'.'.$extension;
                        file_put_contents($path.$filename, $image64);
                        $this->Home_Model->updateDB('blogs',['id'=>$id],['featured_image' => $filename]);

                    }

                    $message = array('message' => 'Blog updated !', 'class' => 'alert-success');
                    $this->session->set_flashdata('message', $message);
                    redirect("backend/settings/blogs", 'refresh');
                  }
              }else{

                  $this->data['blog'] = $this->Home_Model->selectWhere('blogs', ['id'=>$id]);
                  $this->data['home_masterview'] = 'edit_blog';
                  $this->load->view(HOME_GENERALVIEW, $this->data);
              }
          }else{
              return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
          }
      }

      public function deleteBlogs($id){
        $required_rights = "products_edit";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $this->db->delete('blogs', array('id' => $id));
            redirect("/backend/settings/blogs", 'refresh');
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }


    public function create_slug($name, $id=null)
    {
        $count = 0;
        $name = url_title($name);
        $slug_name = $name;             // Create temp name
        while(true)
        {
            $this->db->select('id');
            // if($id)
            //   $this->db->where('id !=', $id);
            $this->db->where('blog_slug', $slug_name);   // Test temp name
            $query = $this->db->get('blogs');
            if ($query->num_rows() == 0) break;
            $slug_name = $name . '-' . (++$count);  // Recreate new temp name
        }
        return strtolower($slug_name);      // Return temp name
    }

    public function place_slug(){
      return $this->output
              ->set_output($this->create_slug($this->input->post('name'),@$this->input->post('id')));
    }


    public function add_blog(){
          $required_rights = "products_add";
          if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()) {
              if (!$this->ion_auth->logged_in()) {
                  redirect('auth/login', 'refresh');
              } elseif (isset($_POST) && !empty($_POST)) {
                  $config = array(
                      array(
                          'field' => 'blog_title',
                          'label' => 'Blog Title',
                          'rules' => 'trim|required',
                      ),
                      array(
                          'field' => 'blog_slug',
                          'label' => 'Blog Slug',
                          'rules' => 'trim|required|alpha_dash',
                      ),
                      array(
                          'field' => 'description',
                          'label' => 'Blog body',
                          'rules' => 'required',
                      ),
                      array(
                          'field' => 'author',
                          'label' => 'Blog Author',
                          'rules' => 'trim|required',
                      ),
                      array(
                          'field' => 'pi_b64',
                          'label' => 'Blog Featured Image',
                          'rules' => 'required',
                      ),
                  );


                  $this->form_validation->set_message('valid_email', 'Invalid %s');
                  $this->form_validation->set_message('required', 'Please enter %s');
                  $this->form_validation->set_rules($config);
                  $this->form_validation->set_error_delimiters('', '');

                  if ($this->form_validation->run() == FALSE) {
                      $error_message = '';
                      if (form_error('blog_title')) {
                          $error_message = form_error('blog_title');
                      } elseif (form_error('blog_slug')) {
                          $error_message = form_error('blog_slug');
                      } elseif (form_error('description')) {
                          $error_message = form_error('description');
                      } elseif (form_error('author')) {
                          $error_message = form_error('author');
                      } elseif (form_error('pi_b64')) {
                          $error_message = form_error('pi_b64');
                      }

                      $this->data['message'] = array('message' => $error_message, 'class' => 'alert-danger');
                      $this->data['home_masterview'] = 'add_blog';
                      $this->load->view(HOME_GENERALVIEW, $this->data);

                  } else {


                      $image64 = $this->input->post('pi_b64');
                      if(!empty($image64) || $image64 != ''){
                        list($type, $image64) = explode(';', $image64);
                        list(,$extension) = explode('/',$type);
                        list(, $image64)      = explode(',', $image64);
                      }
                      $image64 = base64_decode($image64);
                      $time = time();
                      $path = './uploads/blogs/';
                      $filename = md5(rand(11111111,99999999)).'.'.$extension;
                      file_put_contents($path.$filename, $image64);

                      $data = array(
                          'blog_title' => $this->input->post('blog_title'),
                          'blog_slug' => (($this->input->post('blog_slug') != '') ? $this->input->post('blog_slug') : $this->create_slug($this->input->post('blog_title'), $this->input->post('blog_id'))),
                          'body' => $this->input->post('description'),
                          'author' => $this->input->post('author'),
                          'created_at' => CURRENT_DATETIME,
                          'featured_image' => $filename,
                          'keywords' => @$this->input->post('keywords'),
                      );

                      $this->Home_Model->insertDB('blogs', $data);

                      $message = array('message' => 'Blog Successfully added!', 'class' => 'alert-success');
                      $this->session->set_flashdata('message', $message);
                      redirect("backend/settings/blogs", 'refresh');
                }
              } else {
                  $this->data['home_masterview'] = 'add_blog';
                  $this->load->view(HOME_GENERALVIEW, $this->data);
              }
          }
          else{
              return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
          }
      }
}
