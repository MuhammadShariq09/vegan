<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Dashboard.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Coupons extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');
    }

    public function index()
    {
        $data['coupons'] = $this->Home_Model->simpleSelect('coupons','id','desc');
        // print_r($data['orders']);exit;
        $data['home_masterview'] = 'coupons';
        $this->load->view(HOME_GENERALVIEW, $data);
    }

    public function getCategories(){
        $categories = $this->Home_Model->simpleSelect('categories');
        echo json_encode($categories);
    }
    public function getProducts(){
        $categories = $this->Home_Model->simpleSelect('products');
        echo json_encode($categories);
    }

    public function add(){
        $this->form_validation->set_rules('CouponName', 'Coupon Name', 'trim|required');
        $this->form_validation->set_rules('CouponCode', 'Coupon Code', 'trim|required|is_unique[coupons.CouponCode]');
        $this->form_validation->set_rules('CouponPercentage', 'Coupon Percentage', 'trim|required|less_than[100]');
        //$this->form_validation->set_rules('CouponLimit', 'Coupon Limit', 'trim|required');
        $this->form_validation->set_rules('CouponFrom', 'Coupon From ', 'trim|required');
        $this->form_validation->set_rules('CouponTo', 'Coupon To ', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() === TRUE)
        {

//            $userid = explode(" - ",$this->input->post('daterangepicker'));
            $formdata['CouponFrom'] = $this->input->post('CouponFrom');
            $formdata['CouponTo'] = $this->input->post('CouponTo');
            $formdata['CouponName'] = $this->input->post('CouponName');
            $formdata['CouponCode'] = $this->input->post('CouponCode');
            $formdata['CouponPercentage'] = $this->input->post('CouponPercentage');
            $formdata['status'] = $this->input->post('status');

            $this->Home_Model->insertDB('coupons',$formdata);
            $message = array('message' => 'Coupon is created !', 'class' => 'alert-success');
            $this->session->set_flashdata('message', $message);
            redirect('backend/coupons',"refresh");

        }else{

            if(validation_errors()){
                $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }elseif($this->ion_auth->errors()){
                $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }else{
                $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                $this->data['message'] = $messge;
            }

            $this->data['home_masterview'] = 'add_coupon';
            $this->load->view(HOME_GENERALVIEW, $this->data);
        }
    }

    public function edit($id){
        $this->form_validation->set_rules('CouponName', 'Coupon Name', 'trim|required');
        $this->form_validation->set_rules('CouponCode', 'Coupon Code', 'trim|required');
        $this->form_validation->set_rules('CouponPercentage', 'Coupon Percentage', 'trim|required|less_than[100]');
        $this->form_validation->set_rules('CouponFrom', 'Coupon From ', 'trim|required');
        $this->form_validation->set_rules('CouponTo', 'Coupon To ', 'trim|required');
//        $this->form_validation->set_rules('daterangepicker', 'From - To ', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() === TRUE)
        {


            // $userid = explode(" - ",$this->input->post('daterangepicker'));
            // $formdata['CouponFrom'] = $userid[0];
            // $formdata['CouponTo'] = $userid[1];
            $formdata['CouponFrom'] = $this->input->post('CouponFrom');
            $formdata['CouponTo'] = $this->input->post('CouponTo');
            $formdata['CouponName'] = $this->input->post('CouponName');
            $formdata['CouponCode'] = $this->input->post('CouponCode');
            $formdata['CouponPercentage'] = $this->input->post('CouponPercentage');
            $formdata['status'] = $this->input->post('status');
            //print_r($formdata);
            //die;
            $this->Home_Model->updateDB('coupons',["id"=>$id],$formdata);
            $message = array('message' => 'Coupon is updated !', 'class' => 'alert-success');
            $this->session->set_flashdata('message', $message);
            redirect('backend/coupons',"refresh");
        }else{
            $this->data['id'] = $id;
            $this->data['coupon'] = $this->Home_Model->selectWhere('coupons',["id"=>$id]);

            if(validation_errors()){
                $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }elseif($this->ion_auth->errors()){
                $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }else{
                $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                $this->data['message'] = $messge;
            }

            $this->data['home_masterview'] = 'edit_coupon';
            $this->load->view(HOME_GENERALVIEW, $this->data);
        }
    }


    public function delete($id=false){
        if(!$id){
            show_404();
        }
        $this->Home_Model->deleteWhere('coupons',["id"=>$id]);
        $message = array('message' => 'Coupon is deleted !', 'class' => 'alert-success');
        $this->session->set_flashdata('message', $message);
        redirect('backend/coupons');

    }

}
