<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Dashboard.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');
    }

    public function index()
    {
        $data['users'] = $this->ion_auth->users([2])->result();
        foreach ($data['users'] as $k => $user)
        {
            $cuser = $this->ion_auth->user()->row();
            if($user->id == $cuser->user_id || $user->id == "25" || $user->id == "1"){
                unset($data['users'][$k]);
            }
        }


        $this->db->select('*');
        $this->db->from('users as u');// I use aliasing make joins easier
        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id',2);
        $this->db->order_by('u.id','DESC');
        $data['no_of_users'] = count($this->db->get()->result_array());

        // recent buyers
        $this->db->select('o.*, u.first_name, u.last_name, u.profileImage');
        $this->db->from('orders as o');// I use aliasing make joins easier
        $this->db->join('users as u', 'u.id = o.CustomerID');
        $this->db->where('o.CustomerID !=','0');
        $this->db->order_by('o.orderId','DESC');
        $this->db->group_by('o.CustomerID');
        $this->db->limit(5);
        $data['rec_buy'] = $this->db->get()->result_array();


        $this->db->select('*');
        $this->db->from('orders as o');// I use aliasing make joins easier
        $this->db->order_by('o.orderId','DESC');
        $this->db->limit(5);
        $data['orders'] = $this->db->get()->result_array();
        $data['home_masterview'] = 'dashboard';

        $this->load->view(HOME_GENERALVIEW, $data);
    }

}
