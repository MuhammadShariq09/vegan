<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Users.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Users extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');

        /*if( ! $this->ion_auth_acl->has_permission('access_admin') )
            redirect('/dashboard');*/
    }

    public function index()
    {

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        /*else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }*/
        else
        {
            $required_rights = "users_view";
            if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
                // set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                //list the users
                $this->data['users'] = $this->ion_auth->users([2])->result();
                foreach ($this->data['users'] as $k => $user)
                {
                    $cuser = $this->ion_auth->user()->row();

                    if($user->id == $cuser->user_id || $user->id == "25" || $user->id == "1"){
                        unset($this->data['users'][$k]);
                    }

                    // $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
                }

                //$this->load->view('auth/index', $this->data);
                $this->data['home_masterview'] = 'users';
                //echo "<pre>";
                //print_r( $this->data);die;
                $this->load->view(HOME_GENERALVIEW, $this->data);
            }else{
                return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
            }
        }
    }

    /**
     * Create a new user
     */
    public function create_user()
    {

        $required_rights = "users_add";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $this->data['title'] = $this->lang->line('create_user_heading');

            if (!$this->ion_auth->logged_in())
            {
                redirect('auth', 'refresh');
            }

            $tables = $this->config->item('tables', 'ion_auth');
            $identity_column = $this->config->item('identity', 'ion_auth');
            $this->data['identity_column'] = $identity_column;

            // validate form input
            $this->form_validation->set_rules('first_name', 'Username', 'trim|required');

            if ($identity_column !== 'email')
            {
                $this->form_validation->set_rules('identity', 'Identity', 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            }
            else
            {
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
            }
            // $this->form_validation->set_rules('phone', 'Phone', 'trim|numeric');
            // $this->form_validation->set_rules('company', 'Company', 'trim');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm','Confirm Password', 'required');

            $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
//            $this->form_validation->set_rules('dateofbirth', 'Date Of Birth', 'trim|required');


            $more_data = array();


            if(isset($_FILES['profileImage'])&&$_FILES['profileImage']['tmp_name']!=""){
                $more_data['profileImage'] = $this->image_upload('profileImage','jpg|png|jpeg|gif');
            }else {
               // $this->form_validation->set_rules('profileImage', 'Profile Image', 'required');
            }

            $this->form_validation->set_error_delimiters('<div>', '</div>');
            if ($this->form_validation->run() === TRUE)
            {
                $email = strtolower($this->input->post('email'));
                $identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
                $password = $this->input->post('password');
                $cuser = $this->ion_auth->user()->row();
                $additional_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                    'fulladdress' => $this->input->post('fulladdress'),
                    'address_1' => $this->input->post('address_1'),
                    'address_2' => $this->input->post('address_2'),
                    'address_3' => $this->input->post('address_3'),
                    'city' => $this->input->post('city'),
                    'postcode' => $this->input->post('postcode'),
                    'country' => $this->input->post('country'),
                    'state' => $this->input->post('state'),
                    'gender' => $this->input->post('gender'),
                    'dateofbirth' => $this->input->post('dateofbirth'),
                    'vendor_id'=> $cuser->user_id
                );
                $additional_data = array_merge($additional_data,$more_data);
            }
            if ($this->form_validation->run() === TRUE && $this->ion_auth->register($identity, $password, $email, $additional_data))
            {
                // check to see if we are creating the user
                // redirect them back to the admin page

                $this->Home_Model->sendMail('do-not-reply@gascontrolparts.com',
                                            $identity,
                                            'Gas Control Parts',
                                            'Welcome Email',
                                            'emails/welcome',
                                            array('name' => $this->input->post('first_name').' '.$this->input->post('last_name')));
    
                $users = $this->ion_auth->users([2])->result();
                foreach ($users as $k => $user)
                {
                    $cuser = $this->ion_auth->user()->row();
                    if($user->id != "25"){
                        unset($users[$k]);
                    }
        
                }
                foreach($users as $u){
                    $this->Home_Model->sendMail('do-not-reply@gascontrolparts.com',
                                            $u->email,
                                            'Gas Control Parts',
                                            'Welcome Email',
                                            'emails/welcome_admin',
                                            array('name' => $this->input->post('first_name').' '.$this->input->post('last_name')));
                }


                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("backend/users/create_user", 'refresh');
            }
            else
            {
                // display the create user form
                // set the flash data error message if there is one
                //$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
                if(validation_errors()){
                    $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                    $this->data['message'] = $messge;
                }elseif($this->ion_auth->errors()){
                    $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                    $this->data['message'] = $messge;
                }else{
                    $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                    $this->data['message'] = $messge;
                }
                $this->data['first_name'] = array(
                    'placeholder'  => 'First Name',
                    'class'  => 'form-control border-primary',
                    'name' => 'first_name',
                    'id' => 'first_name',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('first_name'),
                );
                $this->data['last_name'] = array(
                    'placeholder'  => 'Last Name',
                    'class'  => 'form-control border-primary',
                    'name' => 'last_name',
                    'id' => 'last_name',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('last_name'),
                );
                $this->data['identity'] = array(
                    'placeholder'  => 'Username',
                    'class'  => 'form-control border-primary',
                    'name' => 'identity',
                    'id' => 'identity',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('identity'),
                );
                $this->data['email'] = array(
                    'placeholder'  => 'Email',
                    'class'  => 'form-control border-primary',
                    'name' => 'email',
                    'id' => 'email',
                    'type' => 'email',
                    'value' => $this->form_validation->set_value('email'),
                );
                $this->data['company'] = array(
                    'placeholder'  => 'Company',
                    'class'  => 'form-control border-primary',
                    'name' => 'company',
                    'id' => 'company',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('company'),
                );
                $this->data['phone'] = array(
                    'placeholder'  => 'Phone',
                    'class'  => 'form-control border-primary',
                    'name' => 'phone',
                    'id' => 'phone',
                    'type' => 'number',
                    'value' => $this->form_validation->set_value('phone'),
                );
                $this->data['password'] = array(
                    'placeholder'  => 'Passowrd',
                    'class'  => 'form-control border-primary',
                    'name' => 'password',
                    'id' => 'password',
                    'type' => 'password',
                    'value' => $this->form_validation->set_value('password'),
                );
                $this->data['password_confirm'] = array(
                    'placeholder'  => 'Confirm Passowrd',
                    'class'  => 'form-control border-primary',
                    'name' => 'password_confirm',
                    'id' => 'password_confirm',
                    'type' => 'password',
                    'value' => $this->form_validation->set_value('password_confirm'),
                );
                $this->data['state'] = array(
                    'placeholder'  => 'State / Province',
                    'class'  => 'form-control border-primary',
                    'name' => 'state',
                    'id' => 'state',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('state'),
                );
                $this->data['profileImage'] = array(
                    'placeholder'  => 'Profile Image',
                    'class'  => 'form-control border-primary',
                    'name' => 'profileImage',
                    'id' => 'profileImage',
                    'type' => 'file',
                    'accept'=>"image/*",
                    'value' => $this->form_validation->set_value('profileImage'),
                );

                $time = strtotime("-18 year", time());
                $date = date("Y-m-d", $time);


                $this->data['dateofbirth'] = array(
                    'placeholder'  => 'Date Of Birth',
                    'class'  => 'form-control border-primary',
                    'name' => 'dateofbirth',
                    'id' => 'dateofbirth',
                    'max'=> $date,
                    'type' => 'date',
                    'value' => $this->form_validation->set_value('dateofbirth'),
                );
                $this->data['gender_options'] =  array("Male" => "Male","Female" => "Female","Trans" => "Trans");
                $this->data['gender'] = array(
                    'placeholder'  => 'Gender',
                    'class'  => 'form-control border-primary',
                    'name' => 'gender',
                    'id' => 'gender',
                    'type' => 'text',
                );



                $this->data['home_masterview'] = 'add_user';
                $this->load->view(HOME_GENERALVIEW, $this->data);
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }

    /**
     * Edit a user
     *
     * @param int|string $id
     */
    public function edit_user($id)
    {
        $required_rights = "users_edit";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $this->data['title'] = $this->lang->line('edit_user_heading');

            if (!$this->ion_auth->logged_in())
            {
                redirect('auth', 'refresh');
            }
            $user = $this->ion_auth->user($id)->row();
            $groups = $this->ion_auth->groups()->result_array();
            $currentGroups = $this->ion_auth->get_users_groups($id)->result();

            // validate form input
            $this->form_validation->set_rules('first_name', 'Username', 'trim|required');

//            $this->form_validation->set_rules('phone', "Phone", 'trim|required|numeric');

            $this->form_validation->set_rules('address_1', 'Address line 1', 'trim');
            $this->form_validation->set_rules('address_2', 'Address line 2', 'trim');
            $this->form_validation->set_rules('address_3', 'Address line 3', 'trim');
            $this->form_validation->set_rules('city', 'City', 'trim');
            $this->form_validation->set_rules('state', 'State / Province', 'trim');
            $this->form_validation->set_rules('country', 'Country', 'trim');
            $this->form_validation->set_rules('postcode', 'Postcode / ZIP', 'trim');

            $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
//            $this->form_validation->set_rules('dateofbirth', 'Date Of Birth', 'trim|required');


            $more_data = array();

            //All File Uploads Validation
            if(isset($_FILES['profileImage'])&&$_FILES['profileImage']['tmp_name']!=""){
                $more_data['profileImage'] = $this->image_upload('profileImage','jpg|png|jpeg|gif');
            }


            $this->form_validation->set_error_delimiters('<div>', '</div>');

            if (isset($_POST) && !empty($_POST))
            {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
                {
                    show_error($this->lang->line('error_csrf'));
                }

                // update the password if it was posted
                if ($this->input->post('password'))
                {
                    $this->form_validation->set_rules('password', "Password", 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                    $this->form_validation->set_rules('password_confirm', "Confirm Password", 'required');
                }

                if ($this->form_validation->run() === TRUE)
                {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'company' => $this->input->post('company'),
                        'phone' => $this->input->post('phone'),
                        'address_1' => $this->input->post('address_1'),
                        'address_2' => $this->input->post('address_2'),
                        'address_3' => $this->input->post('address_3'),
                        'city' => $this->input->post('city'),
                        'postcode' => $this->input->post('postcode'),
                        'country' => $this->input->post('country'),
                        'state' => $this->input->post('state'),
                        'user_type' => $this->input->post('user_type'),
                        'gender' => $this->input->post('gender'),
                        'dateofbirth' => $this->input->post('dateofbirth'),
                    );

                    $data = array_merge($data,$more_data);
                    // print_r($data);exit;
                    // update the password if it was posted
                    if ($this->input->post('password'))
                    {
                        $data['password'] = $this->input->post('password');
                    }

                    // Only allow updating groups if user is admin
                    if ($this->ion_auth->is_admin())
                    {
                        // Update the groups user belongs to
                        $groupData = $this->input->post('groups');

                        if (isset($groupData) && !empty($groupData))
                        {

                            $this->ion_auth->remove_from_group('', $id);

                            foreach ($groupData as $grp)
                            {
                                $this->ion_auth->add_to_group($grp, $id);
                            }

                        }
                    }

                    // check to see if we are updating the user
                    if ($this->ion_auth->update($user->id, $data))
                    {
                        /*// redirect them back to the admin page if admin, or to the base url if non admin
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        if ($this->ion_auth->is_admin())
                        {
                            redirect('auth', 'refresh');
                        }
                        else
                        {
                            redirect('/', 'refresh');
                        }*/
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect(uri_string(), 'refresh');

                    }
                    else
                    {
                        // redirect them back to the admin page if admin, or to the base url if non admin
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        /*if ($this->ion_auth->is_admin())
                        {
                            redirect(uri_string(), 'refresh');
                        }
                        else
                        {
                            redirect('/', 'refresh');
                        }*/
                        redirect(uri_string(), 'refresh');
                    }

                }
            }

            // display the edit user form
            $this->data['csrf'] = $this->_get_csrf_nonce();
            if(validation_errors()){
                $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }elseif($this->ion_auth->errors()){
                $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }else{
                $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                $this->data['message'] = $messge;
            }
            // set the flash data error message if there is one
            //$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            // pass the user to the view
            $this->data['user'] = $user;
            $this->data['groups'] = $groups;
            $this->data['currentGroups'] = $currentGroups;

            $this->data['first_name'] = array(
                'placeholder'  => 'First Name',
                'class'  => 'form-control border-primary',
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name', $user->first_name),
            );
            $this->data['last_name'] = array(
                'placeholder'  => 'Last Name',
                'class'  => 'form-control border-primary',
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name', $user->last_name),
            );
            $this->data['email'] = array(
                'placeholder'  => 'Email',
                'class'  => 'form-control border-primary',
                'readonly'  => 'readonly',
                'name' => 'email',
                'id' => 'email',
                'type' => 'email',
                'value' => $this->form_validation->set_value('email', $user->email),
            );
            $this->data['company'] = array(
                'placeholder'  => 'Company',
                'class'  => 'form-control border-primary',
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company', $user->company),
            );
            $this->data['phone'] = array(
                'placeholder'  => 'Phone',
                'class'  => 'form-control border-primary',
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'number',
                'value' => $this->form_validation->set_value('phone', $user->phone),
            );
            $this->data['password'] = array(
                'placeholder'  => 'Leave blank if your dont want to change password!',
                'class'  => 'form-control border-primary',
                'name' => 'password',
                'id'   => 'password',
                'type' => 'password'
            );
            $this->data['password_confirm'] = array(
                'placeholder'  => 'Leave blank if your dont want to change password!',
                'class'  => 'form-control border-primary',
                'name' => 'password_confirm',
                'id'   => 'password_confirm',
                'type' => 'password'
            );


            $this->data['profileImage'] = array(
                'placeholder'  => 'Profile Image',
                'class'  => 'form-control border-primary',
                'name' => 'profileImage',
                'id' => 'profileImage',
                'type' => 'file',
                'accept'=>"image/*",
                'value' => $this->form_validation->set_value('profileImage'),
            );
            $time = strtotime("-18 year", time());
            $date = date("Y-m-d", $time);


            $this->data['dateofbirth'] = array(
                'placeholder'  => 'Date Of Birth',
                'class'  => 'form-control border-primary',
                'name' => 'dateofbirth',
                'id' => 'dateofbirth',
                'max'=> $date,
                'type' => 'date',
                'value' => $this->form_validation->set_value('dateofbirth',$user->dateofbirth),
            );
            $this->data['gender_options'] =  array("Male" => "Male","Female" => "Female","Trans" => "Trans");
            $this->data['gender'] = array(
                'class'  => 'form-control border-primary',
                'name' => 'gender',
                'id' => 'gender',
            );
            $this->data['value_gender'] = $user->gender;

            //$this->_render_page('auth/edit_user', $this->data);
            $this->data['home_masterview'] = 'edit_user';
            $this->load->view(HOME_GENERALVIEW, $this->data);
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }



    public function manage()
    {
        $this->load->view('admin/manage');
    }
    public function permissions()
    {
        $required_rights = "permissions_view";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $data['permissions']    =   $this->ion_auth_acl->permissions('full');

            $data['home_masterview'] = 'permissions';
            //echo "<pre>";
            //print_r( $this->data);die;
            $this->load->view(HOME_GENERALVIEW, $data);
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }


    public function add_permission()
    {
        $required_rights = "permissions_add";
        if ($this->ion_auth_acl->has_permission($required_rights)  || $this->ion_auth->is_admin()){
            if( $this->input->post() && $this->input->post('cancel') )
                redirect('/admin/permissions', 'refresh');

            $this->form_validation->set_rules('perm_key', 'key', 'required|trim');
            $this->form_validation->set_rules('perm_name', 'name', 'required|trim');

            $this->form_validation->set_message('required', 'Please enter a %s');
            $this->form_validation->set_error_delimiters('', '');
            if( $this->form_validation->run() === FALSE )
            {
                //$data['message']    = ($this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : $this->session->flashdata('message'));
                if(validation_errors()){

                    if (form_error('perm_name')) {
                        $messge = array('message' => form_error('perm_name') ,'class' => 'alert-danger');
                    }elseif(form_error('perm_key')){
                        $messge = array('message' => form_error('perm_key') ,'class' => 'alert-danger');
                    }
                    $data['message'] = $messge;
                }elseif($this->ion_auth->errors()){
                    $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                    $data['message'] = $messge;
                }else{
                    $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                    $data['message'] = $messge;
                }

                $data['permission_key'] = array(
                    'placeholder'  => 'Permission Key',
                    'class'  => 'form-control border-primary',
                    'name'    => 'perm_key',
                    'id'      => 'perm_key',
                    'type'    => 'text',
                    'value'   => $this->form_validation->set_value('perm_key')
                );
                $data['permission_name'] = array(
                    'placeholder'  => 'Permission Name',
                    'class'  => 'form-control border-primary',
                    'name'  => 'perm_name',
                    'id'    => 'perm_name',
                    'type'  => 'text',
                    'value' => $this->form_validation->set_value('perm_name'),
                );

                //$this->load->view('admin/add_permission', $data);
                $data['home_masterview'] = 'add_permission';
                $this->load->view(HOME_GENERALVIEW, $data);
            }
            else
            {
                $new_permission_id = $this->ion_auth_acl->create_permission($this->input->post('perm_key'), $this->input->post('perm_name'));
                if($new_permission_id)
                {
                    // check to see if we are creating the permission
                    // redirect them back to the admin page
                    $status = array('message' => $this->lang->line('permission_creation_successful') , 'class' => 'alert-success');
                    $this->session->set_flashdata('message', $status);
                    redirect("backend/users/permissions", 'refresh');
                }
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function update_permission()
    {
        $required_rights = "permissions_edit";
        if ($this->ion_auth_acl->has_permission($required_rights)  || $this->ion_auth->is_admin()){
            if( $this->input->post() && $this->input->post('cancel') )
                redirect('admin/permissions', 'refresh');

            $permission_id  =   $this->uri->segment(3);

            if( ! $permission_id )
            {
                $this->session->set_flashdata('message', "No permission ID passed");
                redirect("admin/permissions", 'refresh');
            }

            $permission =   $this->ion_auth_acl->permission($permission_id);

            $this->form_validation->set_rules('perm_key', 'key', 'required|trim');
            $this->form_validation->set_rules('perm_name', 'name', 'required|trim');


            $this->form_validation->set_message('required', 'Please enter a %s');

            if( $this->form_validation->run() === FALSE )
            {
                //$data['message']    = ($this->ion_auth_acl->errors() ? $this->ion_auth_acl->errors() : $this->session->flashdata('message'));
                if(validation_errors()){
                    $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                    $data['message'] = $messge;
                }elseif($this->ion_auth->errors()){
                    $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                    $data['message'] = $messge;
                }else{
                    $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                    $data['message'] = $messge;
                }
                $data['permission'] = $permission;

                $data['permission_key'] = array(
                    'placeholder'  => 'Permission Key',
                    'class'  => 'form-control border-primary',
                    'name'    => 'perm_key',
                    'id'      => 'perm_key',
                    'type'    => 'text',
                    'value'   => $this->form_validation->set_value('perm_key', $permission->perm_key)
                );
                $data['permission_name'] = array(
                    'placeholder'  => 'Permission Name',
                    'class'  => 'form-control border-primary',
                    'name'  => 'perm_name',
                    'id'    => 'perm_name',
                    'type'  => 'text',
                    'value' => $this->form_validation->set_value('perm_name', $permission->perm_name),
                );

                //$this->load->view('admin/edit_permission', $data);
                $data['home_masterview'] = 'edit_permission';
                $this->load->view(HOME_GENERALVIEW, $data);
            }
            else
            {
                $additional_data    =   array(
                    'perm_name' =>  $this->input->post('perm_name')
                );

                $update_permission = $this->ion_auth_acl->update_permission($permission_id, $this->input->post('perm_key'), $additional_data);
                if($update_permission)
                {
                    // check to see if we are creating the permission
                    // redirect them back to the admin page
                    //$this->session->set_flashdata('message', $this->ion_auth->messages());
                    $status = array('message' => $this->ion_auth->messages() , 'class' => 'alert-success');
                    $this->session->set_flashdata('message', $status);
                    $data['message'] = $this->ion_auth->messages();
                    redirect("/users/permissions", 'refresh');
                }
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function delete_permission($permission_id)
    {
        $required_rights = "permissions_delete";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){

            if( ! $permission_id )
            {
                $this->session->set_flashdata('message', "No permission ID passed");
                redirect("backend/users/permissions", 'refresh');
            }

            if( $this->ion_auth_acl->remove_permission($permission_id) ){
                $message = array('message' => $this->lang->line('group_permission_delete_successful'), 'class' => 'alert-success');
                $this->session->set_flashdata('message', $message);
                redirect("backend/users/permissions", 'refresh');
            } else {
                $message = array('message' =>$this->ion_auth_acl->errors(), 'class' => 'alert-danger');
                $this->session->set_flashdata('message', $message);
                redirect("backend/users/permissions", 'refresh');
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function groups()
    {
        $required_rights = "groups_view";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            $data['groups'] = $this->ion_auth->groups()->result();

            $data['home_masterview'] = 'groups';
            $this->load->view(HOME_GENERALVIEW, $data);
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function group_permissions()
    {
        $required_rights = "groups_permissions_edit";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            if( $this->input->post() && $this->input->post('cancel') )
                redirect('users/groups', 'refresh');

            $group_id  =   $this->uri->segment(3);

            if( ! $group_id )
            {
                $this->session->set_flashdata('message', "No group ID passed");
                redirect("backend/users/groups", 'refresh');
            }



            if( $this->input->post() && $this->input->post('save') )
            {
                foreach($this->input->post() as $k => $v)
                {
                    if( substr($k, 0, 5) == 'perm_' )
                    {
                        $permission_id  =   str_replace("perm_","",$k);

                        if( $v == "X" )
                            $this->ion_auth_acl->remove_permission_from_group($group_id, $permission_id);
                        else
                            $this->ion_auth_acl->add_permission_to_group($group_id, $permission_id, $v);
                    }
                }
                $this->session->set_flashdata('message', 'Group Permissions Updated!');
                $s_mgs = $this->session->flashdata('message');
                if($s_mgs){
                    $messge = array('message' =>$s_mgs ,'class' => 'alert-success');
                    $data['message'] = $messge;
                }
                redirect(current_url(), 'refresh');
            }

            $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
            $data['message'] = $messge;
            $data['group']            =   $this->ion_auth_acl_model->group($group_id);
            if(!$data['group']){
                redirect("backend/users/groups", 'refresh');
            }
            $data['permissions']            =   $this->ion_auth_acl->permissions('full', 'perm_key');
            $data['group_permissions']      =   $this->ion_auth_acl->get_group_permissions($group_id);

            //$this->load->view('/admin/group_permissions', $data);
            $data['home_masterview'] = 'group_permissions';
            $this->load->view(HOME_GENERALVIEW, $data);
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }

    }

    public function users()
    {
        $data['users']  =   $this->ion_auth->users()->result();

        $this->load->view('/admin/users', $data);
    }

    public function manage_user()
    {
        $user_id  =   $this->uri->segment(3);

        if( ! $user_id )
        {
            $this->session->set_flashdata('message', "No user ID passed");
            redirect("admin/users", 'refresh');
        }

        $data['user']               =   $this->ion_auth->user($user_id)->row();
        $data['user_groups']        =   $this->ion_auth->get_users_groups($user_id)->result();
        $data['user_acl']           =   $this->ion_auth_acl->build_acl($user_id);

        $this->load->view('/admin/manage_user', $data);
    }

    public function user_permissions()
    {
        $user_id  =   $this->uri->segment(3);


        if( ! $user_id )
        {
            $status = array('message' => 'No user ID passed' ,'class' => 'alert-danger');
            $this->session->set_flashdata('message', $status);
            redirect("backend/users", 'refresh');
        }

        if( $this->input->post() && $this->input->post('cancel') )
            redirect(current_url(), 'refresh');


        if( $this->input->post() && $this->input->post('save') )
        {
            foreach($this->input->post() as $k => $v)
            {
                if( substr($k, 0, 5) == 'perm_' )
                {
                    $permission_id  =   str_replace("perm_","",$k);

                    if( $v == "X" )
                        $this->ion_auth_acl->remove_permission_from_user($user_id, $permission_id);
                    else
                        $this->ion_auth_acl->add_permission_to_user($user_id, $permission_id, $v);
                }
            }
            $status = array('message' => 'Users Permissions Updated!' ,'class' => 'alert-success');
            $this->session->set_flashdata('message', $status);

            redirect(current_url(), 'refresh');
        }

        $user_groups    =   $this->ion_auth_acl->get_user_groups($user_id);
        $data['user']               =   $this->ion_auth->user($user_id)->row();


        $data['user_id']                =   $user_id;
        $data['permissions']            =   $this->ion_auth_acl->permissions('full', 'perm_key');
        $data['group_permissions']      =   $this->ion_auth_acl->get_group_permissions($user_groups);
        $data['users_permissions']      =   $this->ion_auth_acl->build_acl($user_id);

        //$this->load->view('/admin/user_permissions', $data);
        $data['home_masterview'] = 'user_permissions';
        $this->load->view(HOME_GENERALVIEW, $data);
    }

    public function image_upload($name, $types)
    {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = $types;

        $this->load->library('upload', $config);



        if ($this->upload->do_upload($name))
        {
            $imgData = $this->upload->data();
//            $path3Thumb = $config['upload_path'].'/73x81';
//            $this->resize_image($config['upload_path'].'/' . $imgData['file_name'], $path3Thumb . '/'.$imgData['file_name'],$path3Thumb,'73','81', $imgData);
            return $imgData['file_name'];
        }
        else
        {
            $this->session->set_flashdata("error", "Couldn't upload your photo.");
        }
    }



    public function delete_user($id){
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth', 'refresh');
        }


        $this->ion_auth->delete_user($id);

        redirect("backend/users", 'refresh');
    }
    /**
     * Create a new group
     */

    public function create_group()
    {

        $this->data['title'] = $this->lang->line('create_group_title');

        $required_rights = "groups_add";
        if ($this->ion_auth_acl->has_permission($required_rights) || $this->ion_auth->is_admin()){
            // validate form input
            $this->form_validation->set_rules('group_name', 'Name', 'trim|required|alpha_dash');
            $this->form_validation->set_error_delimiters('', '');

            if ($this->form_validation->run() === TRUE)
            {
                $new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
                if ($new_group_id)
                {
                    // check to see if we are creating the group
                    // redirect them back to the admin page
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    redirect("backend/users/create_group", 'refresh');
                }
            }
            else
            {
                // display the create group form
                // set the flash data error message if there is one
                if(validation_errors()){
                    $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                    $this->data['message'] = $messge;
                }elseif($this->ion_auth->errors()){
                    $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                    $this->data['message'] = $messge;
                }else{
                    $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                    $this->data['message'] = $messge;
                }

                $this->data['group_name'] = array(
                    'placeholder'  => 'Group Name',
                    'class'  => 'form-control border-primary',
                    'name'    => 'group_name',
                    'id'      => 'group_name',
                    'type'    => 'text',
                    'value'   => $this->form_validation->set_value('group_name'),
                );
                $this->data['group_description'] = array(
                    'placeholder'  => 'Group Description',
                    'class'  => 'form-control border-primary',
                    'name'  => 'description',
                    'id'    => 'description',
                    'type'  => 'text',
                    'value' => $this->form_validation->set_value('description'),
                );

                $this->data['home_masterview'] = 'add_group';
                $this->load->view(HOME_GENERALVIEW, $this->data);
            }
        }else{
            return show_error('You must be have {'.strtoupper($required_rights).'} permission to view this page.');
        }
    }
    /**
     * Edit a group
     *
     * @param int|string $id
     */
    public function edit_group($id)
    {
        // bail if no group id given
        if (!$id || empty($id))
        {
            redirect('users/groups', 'refresh');
        }

        $this->data['title'] = $this->lang->line('edit_group_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('auth', 'refresh');
        }

        $group = $this->ion_auth->group($id)->row();

        // validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

        if (isset($_POST) && !empty($_POST))
        {
            if ($this->form_validation->run() === TRUE)
            {
                $group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

                if ($group_update)
                {
                    $this->session->set_flashdata('message', 'Group Successfully Updated!');
                }
                else
                {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                }
                redirect(uri_string(), 'refresh');
            }
        }

        // set the flash data error message if there is one
        if(validation_errors()){
            $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
            $this->data['message'] = $messge;
        }elseif($this->ion_auth->errors()){
            $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
            $this->data['message'] = $messge;
        }else{
            $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
            $this->data['message'] = $messge;
        }

        // pass the user to the view
        $this->data['group'] = $group;

        $readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

        $this->data['group_name'] = array(
            'placeholder'  => 'Group Name',
            'class'  => 'form-control border-primary',
            'name'    => 'group_name',
            'id'      => 'group_name',
            'type'    => 'text',
            'value'   => $this->form_validation->set_value('group_name', $group->name),
            $readonly => $readonly,
        );
        $this->data['group_description'] = array(
            'placeholder'  => 'Group Description',
            'class'  => 'form-control border-primary',
            'name'  => 'group_description',
            'id'    => 'group_description',
            'type'  => 'text',
            'value' => $this->form_validation->set_value('group_description', $group->description),
        );

        $this->data['home_masterview'] = 'edit_group';
        $this->load->view(HOME_GENERALVIEW, $this->data);
    }




    public function importExport(){


    }

}
