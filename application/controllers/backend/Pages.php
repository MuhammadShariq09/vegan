<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Dashboard.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Pages extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');
    }

    public function index()
    {
        $data['pages'] = $this->Home_Model->simpleSelect('pages',true);
        $data['home_masterview'] = 'pages';
        $this->load->view(HOME_GENERALVIEW, $data);
    }

    public function create_slug($name, $id=null)
    {
        $count = 0;
        $name = url_title($name);
        $slug_name = $name;             // Create temp name
        while(true)
        {
            $this->db->select('id');
            if($id)
                $this->db->where('id !=', $id);
            $this->db->where('slug', $slug_name);   // Test temp name
            $query = $this->db->get('pages');
            if ($query->num_rows() == 0) break;
            $slug_name = $name . '-' . (++$count);  // Recreate new temp name
        }
        return strtolower($slug_name);      // Return temp name
    }

    public function place_slug(){
      return $this->output
              ->set_output($this->create_slug($this->input->post('title'), $this->input->post('id')));
    }



    function check_slug($str)
    {
        $id = $this->input->post('page_id');
        $condition = array('id !='=>$id,'slug'=>$str);
        $ret = $this->Home_Model->selectWhere('pages', $condition);
        if($ret==true){
            $this->form_validation->set_message('check_slug', '%s already exist!');
            return FALSE;
        } else {
            return TRUE;
        }
    }


    public function add(){
        $this->form_validation->set_rules('title', 'Page Name', 'trim|required');
        $this->form_validation->set_rules('body', 'Page Name', 'trim|required');
        $this->form_validation->set_rules('slug', 'Page Slug', 'trim|callback_check_slug|required|alpha_dash');
        $this->form_validation->set_rules('status', 'status', 'trim|required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() === TRUE)
        {

            $formdata['title'] = $this->input->post('title');
            $formdata['body'] = $this->input->post('body');
            $formdata['slug'] = $this->input->post('slug');
            $formdata['status'] = $this->input->post('status');

            $this->Home_Model->insertDB('pages',$formdata);
            $message = array('message' => 'Page is added!', 'class' => 'alert-success');
            $this->session->set_flashdata('message', $message);
            redirect('backend/pages',"refresh");

        }else{

            if(validation_errors()){
                $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }elseif($this->ion_auth->errors()){
                $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }else{
                $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                $this->data['message'] = $messge;
            }

            $this->data['home_masterview'] = 'add_page';
            $this->load->view(HOME_GENERALVIEW, $this->data);
        }
    }

    public function edit($id){
        $this->form_validation->set_rules('title', 'Page Name', 'trim|required');
        $this->form_validation->set_rules('body', 'Page Name', 'trim|required');
        $this->form_validation->set_rules('slug', 'Page Slug', 'trim|callback_check_slug|required|alpha_dash');
        $this->form_validation->set_rules('status', 'status', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() === TRUE)
        {


            $formdata['title'] = $this->input->post('title');
            $formdata['body'] = $this->input->post('body');
            $formdata['slug'] = $this->input->post('slug');
            $formdata['status'] = $this->input->post('status');
            $id = $this->input->post('page_id');
            $this->Home_Model->updateDB('pages',["id"=>$id],$formdata);
            $message = array('message' => 'Page is updated !', 'class' => 'alert-success');
            $this->session->set_flashdata('message', $message);
            redirect('backend/pages',"refresh");
        }else{
            $this->data['id'] = $id;
            $this->data['page'] = $this->Home_Model->selectWhere('pages',["id"=>$id]);

            if(validation_errors()){
                $messge = array('message' => validation_errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }elseif($this->ion_auth->errors()){
                $messge = array('message' =>$this->ion_auth->errors() ,'class' => 'alert-danger');
                $this->data['message'] = $messge;
            }else{
                $messge = array('message' =>$this->session->flashdata('message') ,'class' => 'alert-success');
                $this->data['message'] = $messge;
            }

            $this->data['home_masterview'] = 'edit_page';
            $this->load->view(HOME_GENERALVIEW, $this->data);
        }
    }


    public function delete($id=false){
        if(!$id){
            show_404();
        }
        $this->Home_Model->deleteWhere('pages',["id"=>$id]);
        $message = array('message' => 'Page is deleted !', 'class' => 'alert-success');
        $this->session->set_flashdata('message', $message);
        redirect('backend/pages');

    }

}
