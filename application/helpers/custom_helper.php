<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function encodeData($id) {
    return urlencode(base64_encode($id));
}
function decodeData($id) {
    return base64_decode(urldecode($id));
}
function stripHTMLtags($str){
    $t = preg_replace('/<[^<|>]+?>/', '', htmlspecialchars_decode($str));
    $t = htmlentities($t, ENT_QUOTES, "UTF-8");
    $t = str_replace(PHP_EOL, '', $t);
    return $t;
}

function getUserIP(){
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }
    return $ip;
}
function convertDateTimeToLocalDateTime($convertDate,$convertToZone='',$converFromZone='', $convertDateFormat="Y-m-d H:i:s", $returnDateFormat="M d,Y h:i a"){
    $res = '';
    if($convertToZone==''){
        echo $ip = getUserIP();
        $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
        $ipInfo = json_decode($ipInfo);
        print_r($ipInfo);
        exit;
        $convertToZone = $ipInfo->timezone;
    }
    if($converFromZone==''){
        $converFromZone = date_default_timezone_get();
    }
    if(!in_array($converFromZone, timezone_identifiers_list())) { // check source timezone
        trigger_error(__FUNCTION__ . ': Invalid source timezone ' . $converFromZone, E_USER_ERROR);
    } elseif(!in_array($convertToZone, timezone_identifiers_list())) { // check destination timezone
        trigger_error(__FUNCTION__ . ': Invalid destination timezone ' . $convertToZone, E_USER_ERROR);
    } else {
        // create DateTime object
        $d = DateTime::createFromFormat($convertDateFormat, $convertDate, new DateTimeZone($converFromZone));
        // check source datetime
        if($d && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0) {
            // convert timezone
            $d->setTimeZone(new DateTimeZone($convertToZone));
            // convert dateformat
            $res = $d->format($returnDateFormat);
        } else {
            trigger_error(__FUNCTION__ . ': Invalid source datetime ' . $convertDate . ', ' . $convertDateFormat, E_USER_ERROR);
        }
    }
    return $res;
}