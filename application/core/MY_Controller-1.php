<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Admin.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class MY_Controller extends CI_Controller
{
    /**
     * @return array A CSRF key-value pair
     */
    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    /**
     * @return bool Whether the posted CSRF token matches
     */
    public function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue'))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * @param string     $view
     * @param array|null $data
     * @param bool       $returnhtml
     *
     * @return mixed
     */
    public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        // This will return html on 3rd argument being true
        if ($returnhtml)
        {
            return $view_html;
        }
    }



    public function build_nestable_menu($parent_id, $product_menu=false,$cats=array()){
        // print_r($cats);exit;
        if($product_menu){
            $sqlquery = " SELECT * FROM categories where parent_id='" .$parent_id . "' and active ='1' order by if(sorting_id=0,1,0),sorting_id";
        }else{
            $sqlquery = " SELECT * FROM categories where parent_id='" .$parent_id . "' order by if(sorting_id=0,1,0),sorting_id";
        }

        $query=$this->db->query($sqlquery)->result_array();
        $menu='';
        if($query){
            if($product_menu) {
                $menu = '<ul>';
            }else{
                $menu = '<ol class="dd-list">';
            }
        }
        foreach($query as $row)
        {
            if($product_menu){
                $checked = "";
                if(!empty($cats)){
                    if (in_array($row["id"], $cats)) {
                        $checked = "checked";
                        $row['id'];
                    }
                }
                $menu .= '<li class="dd-item dd3-item" data-id="' . $row["id"] . '">
                    
                        <input type="checkbox" value="'.$row["id"].'" name="categories[]" '.$checked.'><span id="name_show' . $row["id"] . '">' . $row["name"] . '</span> 
                        
                        (<span><b>/<span id="slug_show' . $row["id"] . '">' . $row["slug"] . '</span></b></span>)
                       
                    ' . $this->build_nestable_menu($row["id"], $product_menu,$cats) . '
                </li>';
            } else {
                $menu .= '<li class="dd-item dd3-item" data-id="' . $row["id"] . '">
                    <div class="dd-handle dd3-handle">Drag</div>
                    <div class="dd3-content">
                        <span id="name_show' . $row["id"] . '">' . $row["name"] . '</span>
                        <div class="pull-right"><span class="tip-hide"></span>
                            ' . (($row["active"] == 1) ? '<a class="cat_status" id="cat_status_'.$row["id"].'" data-id="'.$row["id"].'" >Click to Deactive</a>' : '<a class="cat_status" id="cat_status_'.$row["id"].'" data-id="'.$row["id"].'">Click to Active</a>') . ' |
                            <a class="edit-button" id="edit_button' . $row["id"] . '" data-id="' . $row["id"] . '" data-slug="' . $row["slug"] . '" data-name="' . $row["name"] . '" data-status="' . $row["active"] . '" data-image="' . $row["image"] . '"><i class="fa fa-pencil"></i></a> |
                            <a class="del-button" data-id="' . $row["id"] . '"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                    ' . $this->build_nestable_menu($row["id"], $product_menu) . '
                </li>';
            }
        }
        if($query){
            if($product_menu) {
                $menu .= '</ul>';
            }else{
                $menu .= '</ol>';
            }
        }
        return $menu;
    }

}