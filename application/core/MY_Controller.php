<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Admin.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class MY_Controller extends CI_Controller
{
    /**
     * @return array A CSRF key-value pair
     */
    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    /**
     * @return bool Whether the posted CSRF token matches
     */
    public function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue'))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * @param string     $view
     * @param array|null $data
     * @param bool       $returnhtml
     *
     * @return mixed
     */
    public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        // This will return html on 3rd argument being true
        if ($returnhtml)
        {
            return $view_html;
        }
    }



    public function build_nestable_menu($parent_id, $product_menu=false,$first_ul=false, $selected_array=array()){
        $sqlquery = " SELECT * FROM categories where parent_id='" .$parent_id . "' order by if(sorting_id=0,1,0),sorting_id";
        $query=$this->db->query($sqlquery)->result_array();
        $menu='';
        $i=0;
        if($query){
            if($product_menu) {
                $menu = '<ul '.(($first_ul)?'class="first-ul"':'').'>';
            }else{
                $menu = '<ol class="dd-list">';
            }
        }
        foreach($query as $row)
        {
            if($product_menu){
                if($row["active"] == 1){
                    $menu .= '<li class="dd-item dd3-item" data-id="' . $row["id"] . '">
                    
                            <input type="checkbox" id="cat_id' . $row["id"] . '" value="'.$row["id"].'" name="categories[]" '.((in_array($row["id"], $selected_array))?'checked': '').' '.set_checkbox('categories[]', $row["id"]).' >
                            <label style="margin-left: 10px;" for="cat_id' . $row["id"] . '" id="name_show' . $row["id"] . '">' . $row["name"] . ' (<span><b>/<span id="slug_show' . $row["id"] . '">' . $row["slug"] . '</span></b></span>)</label>
                            
                            
                           
                        ' . $this->build_nestable_menu($row["id"], $product_menu, false, $selected_array) . '
                    </li>';
                }

            } else {
                $menu .= '<li class="dd-item dd3-item" data-id="' . $row["id"] . '">
                    <div class="dd-handle dd3-handle">Drag</div>
                    <div class="dd3-content">
                        <span id="name_show' . $row["id"] . '">' . $row["name"] . '</span>
                        <div class="pull-right"><span class="tip-hide"></span>
                            <span><b>/<span id="slug_show' . $row["id"] . '">' . $row["slug"] . '</span></b></span> 
                            ' . (($this->ion_auth_acl->has_permission('categories_edit') || $this->ion_auth->is_admin())? ' | '.(($row["active"] == 1) ? '<a class="cat_status" id="cat_status_'.$row["id"].'" data-id="'.$row["id"].'" >Deactive</a>' : '<a class="cat_status" id="cat_status_'.$row["id"].'" data-id="'.$row["id"].'">Active</a>') . '': '') .
                            (($this->ion_auth_acl->has_permission('categories_edit') || $this->ion_auth->is_admin())?' | <a class="edit-button" id="edit_button' . $row["id"] . '" data-id="' . $row["id"] . '" data-slug="' . $row["slug"] . '" data-name="' . $row["name"] . '" data-image="' . base_url('uploads/') . $row["image"] . '"><i class="fa fa-pencil"></i></a>':'').'
                            '. (($this->ion_auth_acl->has_permission('categories_delete') || $this->ion_auth->is_admin())?' | <a class="del-button" data-id="' . $row["id"] . '"><i class="fa fa-trash"></i></a>':'').'
                        </div>
                    </div>
                    ' . $this->build_nestable_menu($row["id"]) . '
                </li>';
            }
            $i++;
        }
        if($query){
            if($product_menu) {
                $menu .= '</ul>';
            }else{
                $menu .= '</ol>';
            }
        }
        return $menu;
    }
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function base64ToImage($imageData){
        $data = 'data:image/png;base64,AAAFBfj42Pj4';
        list($type, $imageData) = explode(';', $imageData);
        list(,$extension) = explode('/',$type);
        list(,$imageData)      = explode(',', $imageData);
        $fileName = $this->generateRandomString(20 ).'.'.$extension;
        $imageData = base64_decode($imageData);
        file_put_contents('./uploads/'.$fileName, $imageData);
        return $fileName;
    }
}