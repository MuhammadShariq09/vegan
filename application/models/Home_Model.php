<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Home_Model extends CI_Model {

	public function queryDB($sql, $status = false) {
        $query = $this->db->query($sql);
		if($status == false){
			$Return = $query->result_array();
		}else{
			$Return = $query->row_array();
		}
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function insertDB($table, $dataArray) {
        $this->db->insert($table, $dataArray);
        return $this->db->insert_id();
        //return true;
    }


    public function updateDB($table, $whereArray, $dataArray) {
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $this->db->update($table, $dataArray);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function simpleSelect($table,$field=false, $orderby=false) {

        if($field){
            $this->db->order_by($field, $orderby);
        }
        $query = $this->db->get($table);
        $Return = $query->result_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }


    public function simpleSelectResult($table,$field=false, $orderby=false, $limit = "") {
        if($field){
            $this->db->order_by($field, $orderby);
        }
        if($limit){
            $this->db->limit($limit);
        }
        $query = $this->db->get($table);
        $Return = $query->result();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }




    public function selectWhere($table, $whereArray) {
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($table);
        $Return = $query->row_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function selectRowWhere($table, $whereArray) {
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($table);
        return $Return = $query->row();
    }

    public function deleteWhere($table, $whereArray) {
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        return $this->db->delete($table);
    }

    public function countResult($table, $whereArray = '') {
        if ($whereArray) {
            foreach ($whereArray as $field => $value) {
                $this->db->where($field, $value);
            }
        }
        $Return = $this->db->count_all_results($table);
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }
    
    public function countResult_new($table, $whereArray = '') {
        if ($whereArray) {
            foreach ($whereArray as $field => $value) {
                $this->db->where($field, $value);
            }
        }
        $Return = $this->db->count_all_results($table);
        return $Return;
    }    

    public function selectWhereResult($table, $whereArray,$field=false, $orderby=false, $limit = "",$obj = false) {
        if($field){
            $this->db->order_by($field, $orderby);
        }
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        if($limit){
            $this->db->limit($limit);
        }
        $query = $this->db->get($table);
        if($obj){
            $Return = $query->result();
        }else {
            $Return = $query->result_array();
        }
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function selectWhereResultWithFields($table, $whereArray,$fields=false,$field=false, $orderby=false) {
	    if($fields){
	        $this->db->select($fields);
        }
        if($field){
            $this->db->order_by($field, $orderby);
        }
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($table);
        $Return = $query->result_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function selectWhereWithFields($table, $whereArray,$fields=false) {
        if($fields){
            $this->db->select($fields);
        }
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($table);
        $Return = $query->row_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function selectRowWhereWithFields($table, $whereArray,$fields=false) {
        if($fields){
            $this->db->select($fields);
        }
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($table);
        return $Return = $query->row_array();
    }

    public function selectLimit($table, $limit) {
        $this->db->limit($limit);
        $query = $this->db->get($table);
        $Return = $query->result_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

		public function selectLimitOrderBy($table,$limit,$field,$orderby){
			$this->db->select('*');
			$this->db->order_by($field, $orderby);
			$this->db->from($table);
			$this->db->limit($limit);
			$query = $this->db->get();
			return $query->result_array();
		}


    public function selectLimitWhere($table, $limit, $where, $field, $orderby) {
        $this->db->order_by($field, $orderby);
        $this->db->where($where);
        if (!empty($limit)) {
            $this->db->limit($limit);
        }
        $query = $this->db->get($table);
        $Return = $query->result_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function countOrders($status){
        $this->db->where("orders.status",$status);
        $Return = $this->db->count_all_results('orders');
    }

    public function getOrders($orderId = false ,$customerId = false){
        $this->db->select('O.*,U.first_name as UserFirstName, U.last_name as UserLastName');
        $this->db->from('orders O');
        $this->db->join('users U', 'U.id = O.CustomerID', 'left');
        if($customerId){
            $this->db->where("U.id",$customerId);
        }
        if($orderId){
            $this->db->where("O.OrderID",$orderId);
        }

        $this->db->order_by('O.OrderID', 'desc');
        $query = $this->db->get();
        if($orderId){
            $Return = $query->row_array();
        }else{
            $Return = $query->result();
        }
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }
    public function getOrderDetails($orderId){
        $this->db->select('*');
        $this->db->from('orderdetails');
        $this->db->where("OrderID",$orderId);
        $query = $this->db->get();
        $Return = $query->result();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function applyCoupon($CouponCode){
        date_default_timezone_set("Asia/Karachi");
        $this->db->select('*');
        $this->db->from('coupons');
        $this->db->where("CouponCode",$CouponCode);
        $this->db->where("status","1");
        $this->db->where("CouponFrom <= ",date("Y-m-d H:i:s"));
        $this->db->where("CouponTo >=",date("Y-m-d H:i:s"));

//        $sql = $this->db->get_compiled_select();
//        echo $sql;
        $query = $this->db->get();
        $Return = $query->row_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

		public function sendMail($from_email, $to_email, $name, $subject, $view, $data){
			$config = Array(
			  'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
			  'smtp_user' => 'charlestsmith888@gmail.com',
			  'smtp_pass' => 'Charles@888',
			  'smtp_timeout' => '4',
			  'mailtype' => 'html',
			  'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			// $config = Array(
			// 	'protocol' => 'smtp',
			// 	'smtp_host' => 'ssl://smtp.googlemail.com',
			// 	'smtp_port' => 465,
			// 	'smtp_user' => 'charlestsmith888@gmail.com',
			// 	'smtp_pass' => 'Charles@888',
			// 	'mailtype'  => 'html',
			// 	'charset'   => 'iso-8859-1'
			// );

			/*$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';*/
			// $config['protocol'] = 'mail';
			// $config['mailpath'] = '/usr/sbin/mail';
	    // $config['charset'] = 'iso-8859-1';
	    // $config['mailtype'] = 'html';
	    // $config['wordwrap'] = TRUE;
	    $this->load->library('email', $config);
	  	$this->email->set_newline("\r\n");
			$this->email->set_mailtype("html");
	    $this->email->from($from_email, $name);
	    $this->email->to($to_email); // replace it with receiver mail id
	    $this->email->subject($subject); // replace it with relevant subject
	    $body = $this->load->view($view,$data,TRUE);
	    $this->email->message($body);
	    $this->email->send();
		}


}
