<?php
class Home_Model extends CI_Model {
	
	public function queryDB($sql, $status = false) {
        $query = $this->db->query($sql);
		if($status == false){
			$Return = $query->result_array();
		}else{
			$Return = $query->row_array();
		}
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }
	
    public function insertDB($table, $dataArray) {
        $this->db->insert($table, $dataArray);
        return $this->db->insert_id();
        //return true;
    }

    public function updateDB($table, $whereArray, $dataArray) {
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $this->db->update($table, $dataArray);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function simpleSelect($table,$result=false) {
        $query = $this->db->get($table);
        if($result){
            $Return = $query->result();
        }else{
            $Return = $query->result_array();
        }
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }


    public function selectWhere($table, $whereArray) {
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($table);
        $Return = $query->row_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function deleteWhere($table, $whereArray) {
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        return $this->db->delete($table);
    }

    public function countResult($table, $whereArray = '') {
        if ($whereArray) {
            foreach ($whereArray as $field => $value) {
                $this->db->where($field, $value);
            }
        }
        $Return = $this->db->count_all_results($table);
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function selectWhereResult($table, $whereArray, $field=false, $orderby=false) {

        if($field){
            $this->db->order_by($field, $orderby);
        }
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($table);
        $Return = $query->result_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function selectLimit($table, $limit) {
        $this->db->limit($limit);
        $query = $this->db->get($table);
        $Return = $query->result_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }
    public function selectLimitWhere($table, $limit, $where, $field, $orderby) {
        $this->db->order_by($field, $orderby);
        $this->db->where($where);
        if (!empty($limit)) {
            $this->db->limit($limit);
        }
        $query = $this->db->get($table);
        $Return = $query->result_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }

    public function getOrders($orderId = false ,$customerId = false){
//	    echo $customerId;
        $this->db->select('O.*,U.first_name as UserFirstName, U.last_name as UserLastName');
        $this->db->from('orders O');
        $this->db->join('users U', 'U.id = O.VendorID');
        if($customerId){
            $this->db->where("U.id",$customerId);
        }
        if($orderId){
            $this->db->where("O.OrderID",$orderId);
        }
        $query = $this->db->get();
        if($orderId){
            $Return = $query->row_array();
        }else{
            $Return = $query->result();
        }
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }
    public function getOrderDetails($orderId){
        $this->db->select('*');
        $this->db->from('orderdetails');
        $this->db->where("OrderID",$orderId);
        $query = $this->db->get();
        $Return = $query->result();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }


    public function selectRowWhere($table, $whereArray) {
        foreach ($whereArray as $field => $value) {
            $this->db->where($field, $value);
        }
        $query = $this->db->get($table);
        return $Return = $query->row();
    }


    public function applyCoupon($CouponCode){
        date_default_timezone_set("Asia/Karachi");
        $this->db->select('*');
        $this->db->from('coupons');
        $this->db->where("CouponCode",$CouponCode);
        $this->db->where("CouponFrom <=",date("Y-m-d H:i:s"));
        $this->db->where("CouponTo >=",date("Y-m-d H:i:s"));

//        $sql = $this->db->get_compiled_select();
//        echo $sql;
        $query = $this->db->get();
        $Return = $query->row_array();
        if ($Return > 0) {
            return $Return;
        } else {
            return false;
        }
    }
}
